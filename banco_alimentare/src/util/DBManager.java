package util;

import java.util.*;
import java.sql.*;

import dto.DocumentoCampiDTO;
import dto.DocumentoDTO;

import beans.*;

public class DBManager {
	String DbDriver;
	String DbURL;
	String username;
	String password;
	Connection conn;

	/**
	 * Metodo che restituisce true se la connessione e' aperta.
	 */
	public boolean isOpen() {
		if (this.conn == null)
			return false;
		else
			return true;
	}

	/**
	 * Costruttore della classe. Riceve l'url del driver jdbc, la username e la
	 * password necessari per la connessione al database.
	 */
	public DBManager() {
		
		//config per web

//		this.DbDriver = "com.mysql.jdbc.Driver";
//		this.DbURL = "jdbc:mysql://204.93.216.11/daticala_bancodev";
//		this.username = "daticala_root";
//		this.password = "dati_root1";
		
		



//		locale win7 e raspberry
		
		this.DbDriver = "com.mysql.jdbc.Driver";
		this.DbURL = "jdbc:mysql://localhost/daticala_bancodev";
		this.username = "root";
		this.password = "root";


	}

	public DBManager(String url, String usn, String psw) {
		this.DbDriver = "com.mysql.jdbc.Driver";
		this.DbURL = url;
		this.username = usn;
		this.password = psw;
	}

	private boolean startConnection() {
		if (isOpen())
			return true;
		try {
			Class.forName(DbDriver);// Carica il Driver del DBMS
			conn = DriverManager.getConnection(DbURL, username, password);// Apertura
																			// connessione
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean closeConnection() {
		if (!isOpen())
			return true;
		try {
			conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Metodo che esegue una query di update o delete. Riceve la stringa sql e
	 * restituisce true se la query viene eseguita correttamente, false
	 * altrimenti.
	 */
	public boolean executeUpdate(String sql) {
		startConnection();
		Statement st;
		try {
			st = conn.createStatement();
			st.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		closeConnection();
		return true;
	}
	
	public boolean executeUpdateByStatement(PreparedStatement statement) {
		boolean esito=false;
		//startConnection();

		try {
       
			int row = statement.executeUpdate();
			if (row > 0) {
	           esito=true;
			 }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		closeConnection();
		return esito;
	}

	/**
	 * restituice i punti vendita dell'area che riceve come parametro
	
	 */
	
	public Vector<Object> executeSelectAreaPV(String sql, String area) {
		
		startConnection();
		Vector<Object> v = new Vector<Object>();
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			
			st.setString(1, area);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				
				
					PuntoVendita p;
					p = new PuntoVendita();
					p.setIdPuntoVendita(rs.getInt("idPuntoVendita"));
					p.setGruppo(rs.getString("Gruppo"));
					p.setInsegna(rs.getString("Insegna"));
					p.setProvincia(rs.getString("Provincia"));
					p.setNumCasse(rs.getString("NumCasse"));
					p.setIndirizzo(rs.getString("Indirizzo"));
					p.setCap(rs.getString("Cap"));
					p.setComune(rs.getString("Comune"));
					p.setCitta(rs.getString("Citta"));
					p.setTelefono(rs.getString("Telefono"));
					p.setCellulare(rs.getString("Cellulare"));
					p.setRegione(rs.getString("Regione"));
					p.setEmail(rs.getString("Email"));
					p.setArea(rs.getString("Area"));
					p.setMQvendita(rs.getString("MQvendita"));
					p.setVisibile(rs.getInt("Visibile"));
					p.setOrario(rs.getString("Orario"));
					v.add(p);

			
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		closeConnection();
		System.out.println("dimensione vettore risultato executeSelectAreaPV=" + v.size());
		return v;
	
	}

	/**
	 * restituisce i modelli b sommati dell'area specificata
	 */
	
	public Vector<Object> executeSelectModBSommatiByArea(String sql, String area) {
		
		startConnection();
		Vector<Object> v = new Vector<Object>();
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			
			st.setString(1, area);
			
			System.out.println(sql);
			
			ResultSet rs = st.executeQuery();
			
						while (rs.next()) {
				
				
				ModB p;
				p = new ModB();
				p.setIdScheda(rs.getInt("idScheda"));
				p.setOra(rs.getInt("ora"));
				p.setNumeroScheda(rs.getString("numScheda"));
				p.setIdCapoEquipe(rs.getInt("idCapoEquipe"));
				p.setIdPuntoVendita(rs.getInt("idPuntoVendita"));

				p.setPeso_tot_olio(rs.getDouble("peso_tot_olio"));
				p.setScatoli_tot_olio(rs.getInt("scatoli_tot_olio"));

				p.setPeso_tot_omogeinizzati(rs
						.getDouble("peso_tot_omogeneizzati"));
				p.setScatoli_tot_omogeinizzati(rs
						.getInt("scatoli_tot_omogeneizzati"));

				p.setPeso_tot_infanzia(rs.getDouble("peso_tot_infanzia"));
				p.setScatoli_tot_infanzia(rs.getInt("scatoli_tot_infanzia"));

				p.setPeso_tot_tonno(rs.getDouble("peso_tot_tonno"));
				p.setScatoli_tot_tonno(rs.getInt("scatoli_tot_tonno"));

				p.setPeso_tot_carne(rs.getDouble("peso_tot_carne"));
				p.setScatoli_tot_carne(rs.getInt("scatoli_tot_carne"));

				p.setPeso_tot_pelati(rs.getDouble("peso_tot_pelati"));
				p.setScatoli_tot_pelati(rs.getInt("scatoli_tot_pelati"));

				p.setPeso_tot_legumi(rs.getDouble("peso_tot_legumi"));
				p.setScatoli_tot_legumi(rs.getInt("scatoli_tot_legumi"));

				p.setPeso_tot_pasta(rs.getDouble("peso_tot_pasta"));
				p.setScatoli_tot_pasta(rs.getInt("scatoli_tot_pasta"));

				p.setPeso_tot_riso(rs.getDouble("peso_tot_riso"));
				p.setScatoli_tot_riso(rs.getInt("scatoli_tot_riso"));

				p.setPeso_tot_zucchero(rs.getDouble("peso_tot_zucchero"));
				p.setScatoli_tot_zucchero(rs.getInt("scatoli_tot_zucchero"));

				p.setPeso_tot_latte(rs.getDouble("peso_tot_latte"));
				p.setScatoli_tot_latte(rs.getInt("scatoli_tot_latte"));

				p.setPeso_tot_biscotti(rs.getDouble("peso_tot_biscotti"));
				p.setScatoli_tot_biscotti(rs.getInt("scatoli_tot_biscotti"));
				
				p.setPeso_tot_varie(rs.getDouble("peso_tot_varie"));
				p.setScatoli_tot_varie(rs.getInt("scatoli_tot_varie"));

				v.add(p);


			
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		closeConnection();
		System.out.println("dimensione vettore risultato executeSelectModBSommatiByArea=" + v.size());
		return v;
	
	}

	public String getUserArea(String sql){
		
		startConnection();
		String area="NESSUNA";
		Statement st;
		try {
			st =conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				
				area=rs.getString("area");

				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		closeConnection();
		System.out.println("area user corrente= " +area);
		
		return area;
	
	}
public String getUserNome(String sql){
		
		startConnection();
		String nome="Nome";
		Statement st;
		try {
			st =conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				
				nome=rs.getString("nome");

				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		closeConnection();
		System.out.println("nome user corrente= " +nome);
		
		return nome;
	
	}
public String getUserCognome(String sql){
	
	startConnection();
	String cognome="cognome";
	Statement st;
	try {
		st =conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while (rs.next()) {
			
			cognome=rs.getString("cognome");

			
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	closeConnection();
	System.out.println("cognome user corrente= " +cognome);
	
	return cognome;

}
	
	
	

	
	
	/**
	 * Metodo che esegue una query di select. Riceve la stringa sql e la stringa
	 * che indica la tipologia di classe su cui si vuol restituire il vettore
	 * dei risultati. Restituisce il vettore dei risultati.
	 */
	
public List<Object> executeSelectList(String sql, String type) {
	startConnection();
	List<Object> lista = new ArrayList<Object>();
	Statement st;
	try {
		st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while (rs.next()) {
			if (type.equals("Documento")) {
				DocumentoDTO s = new DocumentoDTO();
				s.setId(rs.getInt("id"));
				s.setIdConvenzione(rs.getInt("idconvenzione"));
				s.setTipoDocumento(rs.getString("tipoDocumento"));
				s.setCampo1(rs.getString("campo1"));
				s.setCampo2(rs.getString("campo2"));
				s.setCampo3(rs.getString("campo3"));
				s.setCampo4(rs.getString("campo4"));
				s.setCampo5(rs.getString("campo5"));
				s.setCampo6(rs.getString("campo6"));
				s.setCampo7(rs.getString("campo7"));
				s.setCampo8(rs.getString("campo8"));
				s.setCampo9(rs.getString("campo9"));
				s.setCampo10(rs.getString("campo10"));
				s.setCampo11(rs.getString("campo11"));
				s.setCampo12(rs.getString("campo12"));
				s.setCampo13(rs.getString("campo13"));
				s.setCampo14(rs.getString("campo14"));
				s.setCampo15(rs.getString("campo15"));
				lista.add(s);
				

			}
			
			if (type.equals("DocumentoCampi")) {
				DocumentoCampiDTO s = new DocumentoCampiDTO();
				s.setId(rs.getInt("id"));
				s.setTipoDocumento(rs.getString("tipoDocumento"));
				s.setAttributo(rs.getString("attributo"));
				s.setNomeCampo(rs.getString("nomeCampo"));
				lista.add(s);

			}
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	closeConnection();
	System.out.println("dimensione lista risultato "+type+"=" + lista.size());
	return lista;
}
	
	
	
	public Vector<Object> executeSelect(String sql, String type) {
		startConnection();
		Vector<Object> v = new Vector<Object>();
		Statement st;
		try {
			st = conn.createStatement();

			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				if (type.equals("Stat1")) {
					Stat1 s = new Stat1();
					s = new Stat1();
					s.setOra(rs.getString("ORA"));
					s.setPesoTot(rs.getDouble("Totale"));
					s.setAREA(rs.getString("AREA"));
					v.add(s);

				}
				if (type.equals("File")) {
					FileBean s = new FileBean();
					s = new FileBean();
					s.setIdFile(rs.getInt("idFile"));
					s.setIdEnte(rs.getInt("idEnte"));
					s.setIdUser(rs.getInt("idUser"));
					s.setDescrizione(rs.getString("descrizione"));
					s.setNomeFile(rs.getString("nomeFile"));
					v.add(s);

				}
				
				if (type.equals("Documento")) {
					DocumentoDTO s = new DocumentoDTO();
					s.setId(rs.getInt("id"));
					s.setIdConvenzione(rs.getInt("idconvenzione"));
					s.setTipoDocumento(rs.getString("tipoDocumento"));
					s.setCampo1(rs.getString("campo1"));
					s.setCampo2(rs.getString("campo2"));
					s.setCampo3(rs.getString("campo3"));
					s.setCampo4(rs.getString("campo4"));
					s.setCampo5(rs.getString("campo5"));
					s.setCampo6(rs.getString("campo6"));
					s.setCampo7(rs.getString("campo7"));
					s.setCampo8(rs.getString("campo8"));
					s.setCampo9(rs.getString("campo9"));
					s.setCampo10(rs.getString("campo10"));
					s.setCampo11(rs.getString("campo11"));
					s.setCampo12(rs.getString("campo12"));
					s.setCampo13(rs.getString("campo13"));
					s.setCampo14(rs.getString("campo14"));
					s.setCampo15(rs.getString("campo15"));
					v.add(s);

				}
				
				if (type.equals("DocumentoCampi")) {
					DocumentoCampiDTO s = new DocumentoCampiDTO();
					s.setId(rs.getInt("id"));
					s.setTipoDocumento(rs.getString("tipoDocumento"));
					s.setAttributo(rs.getString("attributo"));
					s.setNomeCampo(rs.getString("nomecampo"));
					v.add(s);

				}
				
				if (type.equals("Sede")) {
					Sede s = new Sede();
					s = new Sede();
					s.setIdEnte(rs.getInt("idEnte"));
					s.setIdSede(rs.getInt("idSede"));
					s.setTipologia(rs.getString("Tipologia"));
					s.setIndirizzo(rs.getString("Indirizzo"));
					s.setComune(rs.getString("Comune"));
					s.setCitta(rs.getString("Citta"));
					s.setCap(rs.getString("Cap"));
					s.setProvincia(rs.getString("Provincia"));
					s.setTelefono(rs.getString("Telefono"));
					s.setFax(rs.getString("Fax"));
					s.setEmail(rs.getString("Email"));
					v.add(s);
				}
				if (type.equals("Ente")) {
					Ente e;
					e = new Ente();
					e.setIdEnte(rs.getInt("idEnte"));
					e.setNome(rs.getString("Nome"));
					e.setDescrizione(rs.getString("Descrizione"));
					e.setPartitaIva(rs.getString("PartitaIva"));
					e.setCodiceStruttura(rs.getInt("codiceStruttura"));
					e.setCodiceSap(rs.getInt("codiceSap"));
					v.add(e);
				}
				if (type.equals("Aree")) {
					Aree a;
					a = new Aree();
					a.setRegione(rs.getString("Regione"));
					a.setArea(rs.getString("Area"));
					v.add(a);
				}
				if (type.equals("Persona")) {
					Persona p;
					p = new Persona();
					p.setIdPersona(rs.getInt("idPersona"));
					p.setIdEnte(rs.getInt("idEnte"));
					p.setTitoloReferenza(rs.getString("TitoloReferenza"));
					p.setNome(rs.getString("Nome"));
					p.setCognome(rs.getString("Cognome"));
					p.setIndirizzo(rs.getString("Indirizzo"));
					p.setCap(rs.getString("Cap"));
					p.setComune(rs.getString("Comune"));
					p.setCitta(rs.getString("Citta"));
					p.setTelefono(rs.getString("Telefono"));
					p.setCellulare(rs.getString("Cellulare"));
					p.setEmail(rs.getString("Email"));
					p.setCF(rs.getString("CF"));
					p.setDataNascita(rs.getString("DataNascita"));
					p.setLuogoNascita(rs.getString("LuogoNascita"));
					v.add(p);
				}
				if (type.equals("Volontario")) {
					Volontario p;
					p = new Volontario();
					p.setIdVolontario(rs.getInt("idVolontario"));
					p.setNome(rs.getString("Nome"));
					p.setCognome(rs.getString("Cognome"));
					p.setIndirizzo(rs.getString("Indirizzo"));
					p.setCap(rs.getString("Cap"));
					p.setCitta(rs.getString("Citta"));
					p.setTelefono(rs.getString("Telefono"));
					p.setEmail(rs.getString("Email"));
					p.setDataNascita(rs.getString("DataNascita"));
					p.setOccupazione(rs.getString("Occupazione"));
					p.setEmail(rs.getString("email"));
					v.add(p);
				}
				if (type.equals("PuntoVendita")) {
					PuntoVendita p;
					p = new PuntoVendita();
					p.setIdPuntoVendita(rs.getInt("idPuntoVendita"));
					p.setGruppo(rs.getString("Gruppo"));
					p.setInsegna(rs.getString("Insegna"));
					p.setProvincia(rs.getString("Provincia"));
					p.setNumCasse(rs.getString("NumCasse"));
					p.setIndirizzo(rs.getString("Indirizzo"));
					p.setCap(rs.getString("Cap"));
					p.setComune(rs.getString("Comune"));
					p.setCitta(rs.getString("Citta"));
					p.setTelefono(rs.getString("Telefono"));
					p.setCellulare(rs.getString("Cellulare"));
					p.setRegione(rs.getString("Regione"));
					p.setEmail(rs.getString("Email"));
					p.setArea(rs.getString("Area"));
					p.setMQvendita(rs.getString("MQvendita"));
					p.setVisibile(rs.getInt("Visibile"));
					p.setOrario(rs.getString("Orario"));
					v.add(p);
				}
			

				if (type.equals("Users")) {
					UserBean p;
					p = new UserBean();
					p.setIdUser(rs.getInt("idUsers"));
					p.setUserName(rs.getString("username"));
					p.setPassword(rs.getString("password"));
					p.setnome(rs.getString("nome"));
					p.setcognome(rs.getString("cognome"));
					p.setarea(rs.getString("area"));
					p.setRuolo(rs.getString("ruolo"));
					p.setAnnoConvenzioni(rs.getInt("annoConvenzioni"));
					p.setProvinciaConvenzioni(rs.getString("provinciaConvenzioni"));
					

					v.add(p);
				}

				 if ( type.equals("Evento") )
				 {
				 Evento p;
				 p=new Evento();
				 p.setIdEvento(rs.getInt("idEvento"));
				 p.setNome(rs.getString("nome"));
				 p.setDescrizione(rs.getString("descrizione"));
				 p.setData(rs.getString("data"));
				 v.add(p);
				 }
				 
				if (type.equals("ModB")) {
					ModB p;
					p = new ModB();
					p.setIdScheda(rs.getInt("idScheda"));
					p.setIdEvento(rs.getInt("idEvento"));
					p.setOra(rs.getInt("ora"));
					p.setNumeroScheda(rs.getString("numScheda"));
					p.setIdCapoEquipe(rs.getInt("idCapoEquipe"));
					p.setIdPuntoVendita(rs.getInt("idPuntoVendita"));

					p.setPeso_tot_olio(rs.getDouble("peso_tot_olio"));
					p.setScatoli_tot_olio(rs.getInt("scatoli_tot_olio"));

					p.setPeso_tot_omogeinizzati(rs
							.getDouble("peso_tot_omogeneizzati"));
					p.setScatoli_tot_omogeinizzati(rs
							.getInt("scatoli_tot_omogeneizzati"));

					p.setPeso_tot_infanzia(rs.getDouble("peso_tot_infanzia"));
					p.setScatoli_tot_infanzia(rs.getInt("scatoli_tot_infanzia"));

					p.setPeso_tot_tonno(rs.getDouble("peso_tot_tonno"));
					p.setScatoli_tot_tonno(rs.getInt("scatoli_tot_tonno"));

					p.setPeso_tot_carne(rs.getDouble("peso_tot_carne"));
					p.setScatoli_tot_carne(rs.getInt("scatoli_tot_carne"));

					p.setPeso_tot_pelati(rs.getDouble("peso_tot_pelati"));
					p.setScatoli_tot_pelati(rs.getInt("scatoli_tot_pelati"));

					p.setPeso_tot_legumi(rs.getDouble("peso_tot_legumi"));
					p.setScatoli_tot_legumi(rs.getInt("scatoli_tot_legumi"));

					p.setPeso_tot_pasta(rs.getDouble("peso_tot_pasta"));
					p.setScatoli_tot_pasta(rs.getInt("scatoli_tot_pasta"));

					p.setPeso_tot_riso(rs.getDouble("peso_tot_riso"));
					p.setScatoli_tot_riso(rs.getInt("scatoli_tot_riso"));

					p.setPeso_tot_zucchero(rs.getDouble("peso_tot_zucchero"));
					p.setScatoli_tot_zucchero(rs.getInt("scatoli_tot_zucchero"));

					p.setPeso_tot_latte(rs.getDouble("peso_tot_latte"));
					p.setScatoli_tot_latte(rs.getInt("scatoli_tot_latte"));
					
					p.setPeso_tot_biscotti(rs.getDouble("peso_tot_biscotti"));
					p.setScatoli_tot_biscotti(rs.getInt("scatoli_tot_biscotti"));

					p.setPeso_tot_varie(rs.getDouble("peso_tot_varie"));
					p.setScatoli_tot_varie(rs.getInt("scatoli_tot_varie"));

					v.add(p);
				}
				if (type.equals("Convenzione")) {
					Convenzione conv;
					conv = new Convenzione();
					conv.setIdConvenzione(rs.getInt("IDCONVENZIONE"));
					conv.setIdEnte(rs.getInt("IDENTE"));
					conv.setAnno(rs.getInt("ANNO"));
					conv.setCellaFrigo(rs.getBoolean("CELLAFRIGO"));
					conv.setDataFirma(rs.getString("DATAFIRMA"));
					conv.setM_0_5(rs.getInt("M_0_5"));
					conv.setM_19_65(rs.getInt("M_19_65"));
					conv.setM_cena(rs.getBoolean("M_cena"));
					conv.setM_centroAccoglienzaDiurno(rs
							.getBoolean("M_centroAccoglienzaDiurno"));
					conv.setM_colazione(rs.getBoolean("M_colazione"));
					conv.setM_giorniApertura(rs.getInt("M_giorniApertura"));
					conv.setM_mediaPresenze(rs.getInt("M_mediaPresenze"));
					conv.setM_over65(rs.getInt("M_over65"));
					conv.setM_pastiFreddi(rs.getBoolean("M_pastiFreddi"));
					conv.setM_pranzo(rs.getBoolean("M_pranzo"));
					conv.setM_totIndigenti(rs.getInt("M_totIndigenti"));
					conv.setP_0_5(rs.getInt("P_0_5"));
					conv.setP_19_65(rs.getInt("P_19_65"));
					conv.setP_Altro(rs.getString("P_Altro"));
					conv.setP_consegnaDomicilio(rs
							.getBoolean("P_consegnaDomicilio"));
					conv.setP_distribuzionePacchi(rs
							.getBoolean("P_distribuzionePacchi"));
					conv.setP_giorniApertura(rs.getInt("P_giorniApertura"));
					conv.setP_mediaPresenze(rs.getInt("P_mediaPresenze"));
					conv.setP_over65(rs.getInt("P_over65"));
					conv.setP_totIndigenti(rs.getInt("P_totIndigenti"));
					conv.setR_0_5(rs.getInt("R_0_5"));
					conv.setR_19_65(rs.getInt("R_19_65"));
					conv.setR_comunitaAnziani(rs
							.getBoolean("R_comunitaAnziani"));
					conv.setR_casaFamiglia(rs.getBoolean("R_casaFamiglia"));
					conv.setR_comunitaDisabili(rs
							.getBoolean("R_comunitaDisabili"));
					conv.setR_comunitaMinori(rs.getBoolean("R_comunitaMinori"));
					conv.setR_comunitaTossicodipendenti(rs
							.getBoolean("R_comunitaTossicodipendenti"));
					conv.setR_giorniApertura(rs.getInt("R_giorniApertura"));
					conv.setR_mediaPresenze(rs.getInt("R_mediaPresenze"));
					conv.setR_over65(rs.getInt("R_over65"));
					conv.setR_totIndigenti(rs.getInt("R_totIndigenti"));
					conv.setM_6_18(rs.getInt("M_6_18"));
					conv.setP_6_18(rs.getInt("P_6_18"));
					conv.setR_6_18(rs.getInt("R_6_18"));
					conv.setIdRappLegaleEnte(rs.getInt("IdRappLegaleEnte"));
					conv.setIdSedeLegaleEnte(rs.getInt("IdSedeLegaleEnte"));
					conv.setIdSedeOperativaEnte(rs.getInt("IdSedeOperativaEnte"));
					conv.setIdSedeMagazzinoEnte(rs.getInt("IdSedeMagazzinoEnte"));
					conv.setM_6_65(rs.getInt("M_6_65"));
					conv.setP_6_65(rs.getInt("P_6_65"));
					conv.setE_6_65(rs.getInt("E_6_65"));
					conv.setE_0_5(rs.getInt("E_0_5"));
					conv.setE_over_65(rs.getInt("E_over65"));
					conv.setM_saltuari(rs.getInt("M_saltuari"));
					conv.setP_saltuari(rs.getInt("P_saltuari"));
					conv.setE_saltuari(rs.getInt("E_saltuari"));
					conv.setU_saltuari(rs.getInt("U_saltuari"));
					conv.setU_mediaPresenze(rs.getInt("U_mediaPresenze"));
					conv.setU_giorniApertura(rs.getInt("U_giorniApertura"));
					conv.setE_mediaPresenze(rs.getInt("E_mediaPresenze"));
					conv.setE_giorniApertura(rs.getInt("E_giorniApertura"));


					v.add(conv);
				}

				if (type.equals("ConvenzioneFrom2017")) {
					ConvenzioneFrom2017 conv;
					conv = new ConvenzioneFrom2017();
					conv.setIdConvenzione(rs.getInt("IDCONVENZIONE"));
					conv.setIdEnte(rs.getInt("IDENTE"));
					conv.setIdRappLegaleEnte(rs.getInt("IdRappLegaleEnte"));
					conv.setIdSedeLegaleEnte(rs.getInt("IdSedeLegaleEnte"));
					conv.setIdSedeOperativaEnte(rs.getInt("IdSedeOperativaEnte"));
					conv.setIdSedeMagazzinoEnte(rs.getInt("IdSedeMagazzinoEnte"));
					
					
					conv.setAnno(rs.getInt("ANNO"));
					conv.setCellaFrigo(rs.getBoolean("CELLAFRIGO"));
					conv.setDataFirma(rs.getString("DATAFIRMA"));
					conv.setTipoPartner(rs.getString("tipopartner"));

					conv.setM_0_15(rs.getInt("M_0_15"));
					conv.setM_16_64(rs.getInt("M_16_64"));
					conv.setM_over_65(rs.getInt("M_over_65"));
					conv.setM_saltuari(rs.getInt("M_saltuari"));


					conv.setP_0_15(rs.getInt("P_0_15"));
					conv.setP_16_64(rs.getInt("P_16_64"));
					conv.setP_over_65(rs.getInt("P_over_65"));
					conv.setP_saltuari(rs.getInt("P_saltuari"));


					conv.setE_0_15(rs.getInt("E_0_15"));
					conv.setE_16_64(rs.getInt("E_16_64"));
					conv.setE_over_65(rs.getInt("E_over_65"));

					conv.setD_0_15(rs.getInt("D_0_15"));
					conv.setD_16_64(rs.getInt("D_16_64"));
					conv.setD_over_65(rs.getInt("D_over_65"));

					conv.setM_totIndigenti(rs.getInt("M_totIndigenti"));
					conv.setP_totIndigenti(rs.getInt("P_totIndigenti"));
					conv.setE_totIndigenti(rs.getInt("E_totIndigenti"));
					conv.setD_totIndigenti(rs.getInt("D_totIndigenti"));
					conv.setU_totIndigenti(rs.getInt("U_totIndigenti"));

					conv.setM_donne(rs.getInt("M_Donne"));
					conv.setP_donne(rs.getInt("P_Donne"));
					conv.setE_donne(rs.getInt("E_Donne"));
					conv.setD_donne(rs.getInt("D_Donne"));
					conv.setU_donne(rs.getInt("U_Donne"));

					conv.setM_disabili(rs.getInt("M_disabili"));
					conv.setP_disabili(rs.getInt("P_disabili"));
					conv.setE_disabili(rs.getInt("E_disabili"));
					conv.setD_disabili(rs.getInt("D_disabili"));
					conv.setU_disabili(rs.getInt("U_disabili"));

					conv.setM_migranti(rs.getInt("M_migranti_Minoranze"));
					conv.setP_migranti(rs.getInt("P_migranti_Minoranze"));
					conv.setE_migranti(rs.getInt("E_migranti_Minoranze"));
					conv.setD_migranti(rs.getInt("D_migranti_Minoranze"));
					conv.setU_migranti(rs.getInt("U_migranti_Minoranze"));

					conv.setM_senzaFissaDimora(rs.getInt("M_senza_Fissa_Dimora"));
					conv.setP_senzaFissaDimora(rs.getInt("P_senza_Fissa_Dimora"));
					conv.setE_senzaFissaDimora(rs.getInt("E_senza_Fissa_Dimora"));
					conv.setD_senzaFissaDimora(rs.getInt("D_senza_Fissa_Dimora"));
					conv.setU_senzaFissaDimora(rs.getInt("U_senza_Fissa_Dimora"));

					conv.setAccoglienza(rs.getBoolean("accoglienza"));
					conv.setInformazione(rs.getBoolean("informazione"));
					conv.setAccompagnamento(rs.getBoolean("accompagnamento"));
					conv.setSpsicologico(rs.getBoolean("spsicologico"));
					conv.setEducativa(rs.getBoolean("educativa"));
					conv.setConsulenza(rs.getBoolean("consulenza"));
					conv.setSscolastico(rs.getBoolean("sscolastico"));
					conv.setSorientamento(rs.getBoolean("sorientamento"));
					conv.setPrimaassistenza(rs.getBoolean("primaassistenza"));
					conv.setTutela(rs.getBoolean("TUTELA"));
					conv.setAltroTtxt(rs.getString("ALTRO"));
					conv.setTipologia(rs.getString("tipologia"));


					v.add(conv);
				}

				if (type.equals("Count")) {
					int cont = rs.getInt("COUNT(*)");
					v.add(cont);
				}
				

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		closeConnection();
		System.out.println("dimensione vettore risultato "+type+"=" + v.size());
		return v;
	}



	

	public Connection getConnectionForPreparedStatement() {
		startConnection();
		return this.conn;
	}
}
