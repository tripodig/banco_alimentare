package util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import beans.CatalogoConvenzioni;
import beans.CatalogoEnti;
import beans.CatalogoPersona;
import beans.CatalogoPuntoVendita;
import beans.CatalogoSede;
import beans.Convenzione;
import beans.ConvenzioneFrom2017;
import beans.DAO_Documento;
import beans.DAO_Evento;
import beans.DocumentoBean;
import beans.Ente;
import beans.Evento;
import beans.Persona;
import beans.PuntoVendita;
import beans.Sede;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;


public class FirstPdf {
	//String path=
	//private static String FILE = "fileSupporto/Bollettinoor.pdf";
	//	private static String FILE2 = "c:/temp/Bollettinomodificato.pdf";

	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
			Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
			Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.BOLD);



	// iText allows to add metadata to the PDF which can be viewed in your Adobe
	// Reader
	// under File -> Properties
	public static void modA_2013 (String path0,String path1, int idpdv, int idEvento)throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");
			DAO_Evento catEvento=new DAO_Evento();
			Evento evento=catEvento.getEvento(idEvento);
			CatalogoPuntoVendita cpdv=new CatalogoPuntoVendita();
			PuntoVendita pdv=new PuntoVendita();
			pdv=cpdv.getPuntoVendita(idpdv);
			System.out.println(path0);
			System.out.println(path1);

			pdv.getIdPuntoVendita();
			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			PdfContentByte overContent = pdfStamper.getOverContent(1); //individua la pagina 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);

			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);

			overContent.showTextAligned(Element.ALIGN_LEFT, evento.getData(), 600,558, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, ""+pdv.getIdPuntoVendita(), 445,452, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getInsegna().toUpperCase(), 500,452, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getIndirizzo(), 372,432, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getComune()+" ("+pdv.getProvincia()+")", 600,432, 0);

			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	public static void modB_2013 (String path0,String path1, int idpdv, int idEvento)throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");
			DAO_Evento catEvento=new DAO_Evento();
			Evento evento=catEvento.getEvento(idEvento);
			CatalogoPuntoVendita cpdv=new CatalogoPuntoVendita();
			PuntoVendita pdv=new PuntoVendita();
			pdv=cpdv.getPuntoVendita(idpdv);


			pdv.getIdPuntoVendita();
			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			PdfContentByte overContent = pdfStamper.getOverContent(1); //individua la pagina 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);

			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);

			overContent.showTextAligned(Element.ALIGN_LEFT, evento.getData(), 600,558, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, ""+pdv.getIdPuntoVendita(), 445,452, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getInsegna().toUpperCase(), 500,452, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getIndirizzo(), 372,432, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, pdv.getComune()+" ("+pdv.getProvincia()+")", 600,432, 0);

			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static void StampaModuli_Convenzione_Banco_2015(String path0,String path1, int idConvenzione) throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");

			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();
			CatalogoConvenzioni catc=new CatalogoConvenzioni();
			Convenzione c=catc.getConvenzione(idConvenzione);

			int idEnte=c.getIdEnte();


			int idSedeOperativaEnte=0;
			int idSedeMagazzinoEnte=0;
			Sede soperativa=null;
			Sede smagazzino=null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());

			if(c.getIdSedeOperativaEnte()!=0){
				idSedeOperativaEnte=c.getIdSedeOperativaEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			if(c.getIdSedeMagazzinoEnte()!=0){
				idSedeMagazzinoEnte=c.getIdSedeMagazzinoEnte();
				smagazzino=cats.getSede(idSedeMagazzinoEnte);
			}


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));
			String annoCorr=String.valueOf(c.getAnno()-1)+"-"+String.valueOf(c.getAnno());

			PdfContentByte overContent = pdfStamper.getOverContent(2); //individua la pagina 2

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			String idalternativoEnte=String.valueOf(idEnte);

			overContent.setColorFill(BaseColor.BLACK);
			overContent.showTextAligned(Element.ALIGN_RIGHT, e.getNome(), 530,727, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 530,695, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 330,592, 0);

			overContent = pdfStamper.getOverContent(4);  //individua la pagina 4
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 227,418, 0);


			overContent = pdfStamper.getOverContent(1);  //individua la pagina 1 riassunto dati
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, CodiceStruttura, 80,570, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			System.out.println(soperativa);
			System.out.println(slegale);
			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 250,599, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getFax(), 250,579, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getEmail(), 250,559, 0);
			}
			else{
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), 250,599, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 250,579, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 250,559, 0);

			}

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCognome()+" "+rapp.getNome(), 250,520, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCellulare(), 250,500, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getTelefono(), 250,480, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getEmail(), 250,460, 0);
			if (altroRef!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCognome()+" "+altroRef.getNome(), 250,420, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCellulare(), 250,400, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getTelefono(), 250,380, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getEmail(), 250,360, 0);


			}

			overContent = pdfStamper.getOverContent(5);  //individua la pagina 5 carico e scarico
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			if(c.getIdSedeOperativaEnte()==0){
				idSedeOperativaEnte=c.getIdSedeLegaleEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			overContent = pdfStamper.getOverContent(6);  //individua la pagina 6 primo foglio informativa

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			// overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,257, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,257, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,240, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,240, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,222, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,199, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,199, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 110,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 510,181, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			overContent = pdfStamper.getOverContent(7);  //individua la pagina sec foglio informativa 

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			overContent = pdfStamper.getOverContent(8);  //individua la pagina 8 modulo agea
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 450,815, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, String.valueOf(c.getAnno()), 530,792, 0);
			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,752, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,752, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,738, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,738, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,722, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 140,700, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 570,700, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 210,640, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 110,685, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 360,685, 0);
			//overContent.showTextAligned(Element.ALIGN_LEFT, sleg.getFax(), 50,670, 0);
			// overContent.showTextAligned(Element.ALIGN_LEFT, sleg.getEmail(), 195,670, 0);

			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 280,620, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), 205,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), 275,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 545,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 50,605, 0);
			}

			if(smagazzino!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), 315,580, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), 205,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), 275,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getProvincia(), 545,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), 50,565, 0);
			}



			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 570,685, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 85,669, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 212,669, 0);

			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 228,539, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 274,539, 0);


			String M_0_5=String.valueOf(c.getM_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_5, 149,356, 0);       
			String R_0_5=String.valueOf(c.getR_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_0_5, 149,306, 0);
			String P_0_5=String.valueOf(c.getP_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_0_5, 149,245, 0);

			String M_19_65=String.valueOf(c.getM_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_19_65, 350,356, 0);       
			String R_19_65=String.valueOf(c.getR_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_19_65, 350,306, 0);
			String P_19_65=String.valueOf(c.getP_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_19_65, 350,245, 0);


			String M_6_18=String.valueOf(c.getM_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_6_18, 250,356, 0);       
			String R_6_18=String.valueOf(c.getR_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_6_18, 250,306, 0);
			String P_6_18=String.valueOf(c.getP_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_6_18, 250,245, 0);

			String M_over65=String.valueOf(c.getM_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, 149,345, 0);       
			String R_over65=String.valueOf(c.getR_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_over65, 149,293, 0);
			String P_over65=String.valueOf(c.getP_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_over65, 149,230, 0);

			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, 450,356, 0);       
			String R_totIndigenti=String.valueOf(c.getR_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, R_totIndigenti, 450,316, 0); 
			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, 450,246, 0); 

			String M_mediaPresenze=String.valueOf(c.getM_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_mediaPresenze, 500,356, 0);       
			String R_mediaPresenze=String.valueOf(c.getR_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, R_mediaPresenze, 500,316, 0);       
			String P_mediaPresenze=String.valueOf(c.getP_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_mediaPresenze, 500,246, 0);       

			String M_giorniApertura=String.valueOf(c.getM_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_giorniApertura, 550,356, 0);       
			String P_giorniApertura=String.valueOf(c.getP_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_giorniApertura, 550,246, 0);       

			if(c.isM_pranzo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,373, 0);
			if(c.isM_cena())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 86,373, 0);
			if(c.isM_colazione())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 127,373, 0);       
			if(c.isM_pastiFreddi())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 191,373, 0);       
			if(c.isM_centroAccoglienzaDiurno())	
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 267,373, 0);       

			if(c.isR_casaFamiglia())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,321, 0);
			if(c.isR_comunitaDisabili())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 104,321, 0);
			if(c.isR_comunitaTossicodipendenti())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 181,321, 0);
			if(c.isR_comunitaAnziani())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 271,321, 0);
			if(c.isR_comunitaMinori())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 360,321, 0);

			if(c.isP_distribuzionePacchi())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,260, 0);

			if(c.isP_consegnaDomicilio())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 135,260, 0);

			if(!(c.getP_Altro().equals("no"))){
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 262,260, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, c.getP_Altro(), 291,260, 0);
			}

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,40, 0);

			overContent = pdfStamper.getOverContent(9);  //individua la pagina 9 dichiarazione legale rappresentante
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			//  overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 155,375, 0);


			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


	public static void StampaModuli_Convenzione_old(String path0,String path1, int idConvenzione) throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");

			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();
			CatalogoConvenzioni catc=new CatalogoConvenzioni();
			Convenzione c=catc.getConvenzione(idConvenzione);

			int idEnte=c.getIdEnte();

			int idSedeOperativaEnte=0;
			int idSedeMagazzinoEnte=0;
			Sede soperativa=null;
			Sede smagazzino=null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());

			if(c.getIdSedeOperativaEnte()!=0){
				idSedeOperativaEnte=c.getIdSedeOperativaEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			if(c.getIdSedeMagazzinoEnte()!=0){
				idSedeMagazzinoEnte=c.getIdSedeMagazzinoEnte();
				smagazzino=cats.getSede(idSedeMagazzinoEnte);
			}


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));
			String annoCorr=String.valueOf(c.getAnno()-1)+"-"+String.valueOf(c.getAnno());
			String annoCorrlight=String.valueOf(c.getAnno()-2001)+"/"+String.valueOf(c.getAnno()-2000);
			PdfContentByte overContent = pdfStamper.getOverContent(2); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			String idalternativoEnte=String.valueOf(idEnte);

			overContent.setColorFill(BaseColor.BLACK);
			overContent.showTextAligned(Element.ALIGN_RIGHT, e.getNome(), 530,727, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 530,695, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 330,592, 0);

			overContent = pdfStamper.getOverContent(4);  //individua la pagina 4 accordo collaborazione pag 3
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 226,418, 0);

			overContent = pdfStamper.getOverContent(1);  //individua la pagina 1 riassunto dati
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, CodiceStruttura+"/"+idalternativoEnte, 60,570, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			System.out.println(soperativa);
			System.out.println(slegale);
			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getEmail(), 250,558, 0);
			}
			else{
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 250,558, 0);

			}

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCognome()+" "+rapp.getNome(), 250,520, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCellulare(), 250,499, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getTelefono(), 250,479, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getEmail(), 250,459, 0);
			if (altroRef!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCognome()+" "+altroRef.getNome(), 250,420, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCellulare(), 250,400, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getTelefono(), 250,381, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getEmail(), 250,361, 0);


			}


			overContent = pdfStamper.getOverContent(5);  //individua la pagina 5 modulo agea
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);


			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 527,793, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,742, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,742, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,725, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,725, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,707, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,684, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,684, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,664, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 210,640, 0);


			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 100,600, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 450,600, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), 120,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), 210,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 515,585, 0);

			}

			if(smagazzino!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), 100,550, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), 450,550, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), 120,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), 210,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getProvincia(), 515,530, 0);

			}



			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 275,505, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 370,505, 0);

			//DA QUI MODIFICARE!

			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorrlight, 235,489, 0);
			//MENSA

			String M_0_5=String.valueOf(c.getM_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_5, 130,306, 0);       

			String M_6_65=String.valueOf(c.getM_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_6_65, 200,306, 0);       

			String M_over65=String.valueOf(c.getM_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, 260,306, 0);    

			String M_totIndCont=String.valueOf(c.getM_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndCont, 320,306, 0);       

			String M_IndSalt=String.valueOf(c.getM_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_IndSalt, 380,306, 0);    


			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, 430,306, 0);       

			String M_mediaPresenze=String.valueOf(c.getM_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_mediaPresenze, 480,306, 0);       

			String M_giorniApertura=String.valueOf(c.getM_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_giorniApertura, 530,306, 0);       


			//PACCHI

			String P_0_5=String.valueOf(c.getP_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_0_5, 130,280, 0);       

			String P_6_65=String.valueOf(c.getP_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_6_65, 200,280, 0);       

			String P_over65=String.valueOf(c.getP_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_over65, 260,280, 0);    

			String P_totIndCont=String.valueOf(c.getP_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndCont, 320,280, 0);       

			String P_IndSalt=String.valueOf(c.getP_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_IndSalt, 380,280, 0);    


			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, 430,280, 0);       

			String P_mediaPresenze=String.valueOf(c.getP_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_mediaPresenze, 480,280, 0);       

			String P_giorniApertura=String.valueOf(c.getP_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_giorniApertura, 530,280, 0);     

			//EMPORIO

			String E_0_5=String.valueOf(c.getE_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_0_5, 130,254, 0);       

			String E_6_65=String.valueOf(c.getE_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_6_65, 200,254, 0);       

			String E_over65=String.valueOf(c.getE_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_over65, 260,254, 0);    

			String E_totIndCont=String.valueOf(c.getE_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndCont, 320,254, 0);       

			String E_IndSalt=String.valueOf(c.getE_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_IndSalt, 380,254, 0);    


			String E_totIndigenti=String.valueOf(c.getE_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndigenti, 430,254, 0);       

			String E_mediaPresenze=String.valueOf(c.getE_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_mediaPresenze, 480,254, 0);       

			String E_giorniApertura=String.valueOf(c.getE_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_giorniApertura, 530,254, 0);     



			//STRADA

			String U_IndSalt=String.valueOf(c.getU_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_IndSalt, 380,228, 0);    

			String U_totIndigenti=String.valueOf(c.getU_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_totIndigenti, 430,228, 0);       

			String U_mediaPresenze=String.valueOf(c.getU_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_mediaPresenze, 480,228, 0);       

			String U_giorniApertura=String.valueOf(c.getU_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_giorniApertura, 530,228, 0); 


			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,40, 0);



			overContent = pdfStamper.getOverContent(6);  //individua la pagina 6 copertina registro
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);


			String codENom=CodiceStruttura+"/"+idalternativoEnte+" "+e.getNome();
			overContent.showTextAligned(Element.ALIGN_CENTER, codENom, 400,220, 0);



			if(c.getIdSedeOperativaEnte()==0){
				idSedeOperativaEnte=c.getIdSedeLegaleEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			overContent = pdfStamper.getOverContent(10);  //individua la pagina 10
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,694, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,694, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,670, 0);        

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,670, 0);


			overContent = pdfStamper.getOverContent(11);  //individua la pagina 11
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			overContent = pdfStamper.getOverContent(12);  //individua la pagina 12 primo foglio informativa

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			// overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,257, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,257, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,240, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,240, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,222, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,199, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,199, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 110,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 510,181, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			overContent = pdfStamper.getOverContent(13);  //individua la pagina 13 

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);

			overContent = pdfStamper.getOverContent(14);  //individua la pagina 14

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			// overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,576, 0);

			overContent = pdfStamper.getOverContent(16);  //individua la pagina 16

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,100, 0);

			overContent = pdfStamper.getOverContent(17);  //individua la pagina 17 dichiarazione del legale rappresentante

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			//  overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 150,360, 0);


			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void StampaModuli_Convenzione_2015(String path0,String path1, int idConvenzione) throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");

			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();
			CatalogoConvenzioni catc=new CatalogoConvenzioni();
			Convenzione c=catc.getConvenzione(idConvenzione);

			int idEnte=c.getIdEnte();

			int idSedeOperativaEnte=0;
			int idSedeMagazzinoEnte=0;
			Sede soperativa=null;
			Sede smagazzino=null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());

			if(c.getIdSedeOperativaEnte()!=0){
				idSedeOperativaEnte=c.getIdSedeOperativaEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			if(c.getIdSedeMagazzinoEnte()!=0){
				idSedeMagazzinoEnte=c.getIdSedeMagazzinoEnte();
				smagazzino=cats.getSede(idSedeMagazzinoEnte);
			}


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));
			String annoCorr=String.valueOf(c.getAnno()-1)+"-"+String.valueOf(c.getAnno());
			String annoCorrlight=String.valueOf(c.getAnno()-2001)+"/"+String.valueOf(c.getAnno()-2000);
			PdfContentByte overContent = pdfStamper.getOverContent(2); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			String idalternativoEnte=String.valueOf(idEnte);

			overContent.setColorFill(BaseColor.BLACK);
			overContent.showTextAligned(Element.ALIGN_RIGHT, e.getNome(), 530,727, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 530,695, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 330,592, 0);

			overContent = pdfStamper.getOverContent(4);  //individua la pagina 4 accordo collaborazione pag 3
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 226,418, 0);

			overContent = pdfStamper.getOverContent(1);  //individua la pagina 1 riassunto dati
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, CodiceStruttura+"/"+idalternativoEnte, 60,570, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			System.out.println(soperativa);
			System.out.println(slegale);
			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getEmail(), 250,558, 0);
			}
			else{
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 250,558, 0);

			}

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCognome()+" "+rapp.getNome(), 250,520, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCellulare(), 250,499, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getTelefono(), 250,479, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getEmail(), 250,459, 0);
			if (altroRef!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCognome()+" "+altroRef.getNome(), 250,420, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCellulare(), 250,400, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getTelefono(), 250,381, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getEmail(), 250,361, 0);


			}


			overContent = pdfStamper.getOverContent(5);  //individua la pagina 5 modulo agea
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);


			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 527,793, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,742, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,742, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,725, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,725, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,707, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,684, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,684, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,664, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 210,640, 0);


			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 100,600, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 450,600, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), 120,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), 210,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 515,585, 0);

			}

			if(smagazzino!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), 100,550, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), 450,550, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), 120,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), 210,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getProvincia(), 515,530, 0);

			}



			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 275,505, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 370,505, 0);

			//DA QUI MODIFICARE!

			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorrlight, 235,489, 0);
			//MENSA

			String M_0_5=String.valueOf(c.getM_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_5, 130,306, 0);       

			String M_6_65=String.valueOf(c.getM_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_6_65, 200,306, 0);       

			String M_over65=String.valueOf(c.getM_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, 260,306, 0);    

			String M_totIndCont=String.valueOf(c.getM_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndCont, 320,306, 0);       

			String M_IndSalt=String.valueOf(c.getM_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_IndSalt, 380,306, 0);    


			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, 430,306, 0);       

			String M_mediaPresenze=String.valueOf(c.getM_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_mediaPresenze, 480,306, 0);       

			String M_giorniApertura=String.valueOf(c.getM_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_giorniApertura, 530,306, 0);       


			//PACCHI

			String P_0_5=String.valueOf(c.getP_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_0_5, 130,280, 0);       

			String P_6_65=String.valueOf(c.getP_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_6_65, 200,280, 0);       

			String P_over65=String.valueOf(c.getP_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_over65, 260,280, 0);    

			String P_totIndCont=String.valueOf(c.getP_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndCont, 320,280, 0);       

			String P_IndSalt=String.valueOf(c.getP_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_IndSalt, 380,280, 0);    


			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, 430,280, 0);       

			String P_mediaPresenze=String.valueOf(c.getP_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_mediaPresenze, 480,280, 0);       

			String P_giorniApertura=String.valueOf(c.getP_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_giorniApertura, 530,280, 0);     

			//EMPORIO

			String E_0_5=String.valueOf(c.getE_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_0_5, 130,254, 0);       

			String E_6_65=String.valueOf(c.getE_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_6_65, 200,254, 0);       

			String E_over65=String.valueOf(c.getE_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_over65, 260,254, 0);    

			String E_totIndCont=String.valueOf(c.getE_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndCont, 320,254, 0);       

			String E_IndSalt=String.valueOf(c.getE_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_IndSalt, 380,254, 0);    


			String E_totIndigenti=String.valueOf(c.getE_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndigenti, 430,254, 0);       

			String E_mediaPresenze=String.valueOf(c.getE_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_mediaPresenze, 480,254, 0);       

			String E_giorniApertura=String.valueOf(c.getE_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_giorniApertura, 530,254, 0);     



			//STRADA

			String U_IndSalt=String.valueOf(c.getU_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_IndSalt, 380,228, 0);    

			String U_totIndigenti=String.valueOf(c.getU_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_totIndigenti, 430,228, 0);       

			String U_mediaPresenze=String.valueOf(c.getU_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_mediaPresenze, 480,228, 0);       

			String U_giorniApertura=String.valueOf(c.getU_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_giorniApertura, 530,228, 0); 


			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,40, 0);



			overContent = pdfStamper.getOverContent(6);  //individua la pagina 6 copertina registro
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);


			String codENom=CodiceStruttura+"/"+idalternativoEnte+" "+e.getNome();
			overContent.showTextAligned(Element.ALIGN_CENTER, codENom, 400,220, 0);



			if(c.getIdSedeOperativaEnte()==0){
				idSedeOperativaEnte=c.getIdSedeLegaleEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			overContent = pdfStamper.getOverContent(10);  //individua la pagina 10
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,694, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,694, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,670, 0);        

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,670, 0);


			overContent = pdfStamper.getOverContent(11);  //individua la pagina 11
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			overContent = pdfStamper.getOverContent(12);  //individua la pagina 12 primo foglio informativa

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			// overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,257, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,257, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,240, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,240, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,222, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,199, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,199, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 110,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 510,181, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			overContent = pdfStamper.getOverContent(13);  //individua la pagina 13 

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);

			overContent = pdfStamper.getOverContent(14);  //individua la pagina 14

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);

			// overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);



			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,576, 0);

			//       overContent = pdfStamper.getOverContent(16);  //individua la pagina 16
			//        
			//        bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			//        overContent.setFontAndSize(bf, 12);
			//        overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,100, 0);
			//        
			//        overContent = pdfStamper.getOverContent(17);  //individua la pagina 17 dichiarazione del legale rappresentante
			//        
			//        bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			//        overContent.setFontAndSize(bf, 12);
			//        overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+idalternativoEnte, 100,815, 0);
			//        
			//      //  overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);
			//        
			//        
			//       
			//        overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			//        overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);
			//        
			//        overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			//        overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);
			//        
			//        overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			//        
			//        overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			//        overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);
			//        
			//        overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,577, 0);
			//        overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,577, 0);
			//        overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,577, 0);
			//        overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 150,360, 0);
			//        

			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


	public static void StampaModuli_Convenzione(String path0,String path1, int idConvenzione) throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");

			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();
			CatalogoConvenzioni catc=new CatalogoConvenzioni();
			DAO_Documento catdoc = new DAO_Documento();
			Convenzione c=catc.getConvenzione(idConvenzione);

			int idEnte=c.getIdEnte();

			int idSedeOperativaEnte=0;
			int idSedeMagazzinoEnte=0;
			Sede soperativa=null;
			Sede smagazzino=null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());

			if(c.getIdSedeOperativaEnte()!=0){
				idSedeOperativaEnte=c.getIdSedeOperativaEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			if(c.getIdSedeMagazzinoEnte()!=0){
				idSedeMagazzinoEnte=c.getIdSedeMagazzinoEnte();
				smagazzino=cats.getSede(idSedeMagazzinoEnte);
			}


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			String annoCorr=String.valueOf(c.getAnno());



			//--------------------------------------------------------------------------------------------
			//--------------------- accordo collaborazione pag 1------------------------------------------
			//-------------------------------------------------------------------------------------------- 

			PdfContentByte overContent = pdfStamper.getOverContent(2); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			String codiceSAP=String.valueOf(e.getCodiceSap());

			overContent.setColorFill(BaseColor.BLACK);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP+" "+e.getNome(), 530,707, 0);



			//--------------------------------------------------------------------------------------------
			//--------------------- accordo collaborazione pag 2------------------------------------------
			//-------------------------------------------------------------------------------------------- 

			overContent = pdfStamper.getOverContent(3);  //individua la pagina 3 accordo collaborazione pag 2
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 310,393, 0);



			//--------------------------------------------------------------------------------------------
			//--------------------- riassunto dati--------------------------------------------------------
			//--------------------------------------------------------------------------------------------

			overContent = pdfStamper.getOverContent(1);  //individua la pagina 1 riassunto dati
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, CodiceStruttura, 60,590, 0);        
			overContent.showTextAligned(Element.ALIGN_LEFT, "SAP:"+codiceSAP, 60,570, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			System.out.println(soperativa);
			System.out.println(slegale);
			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getEmail(), 250,558, 0);
			}
			else{
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 250,558, 0);

			}

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCognome()+" "+rapp.getNome(), 250,520, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCellulare(), 250,499, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getTelefono(), 250,479, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getEmail(), 250,459, 0);
			if (altroRef!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCognome()+" "+altroRef.getNome(), 250,430, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCellulare(), 250,410, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getTelefono(), 250,391, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getEmail(), 250,371, 0);	

			}


			//--------------------------------------------------------------------------------------------
			//----------------- domanda iscrizione--------------------------------------------------------
			//--------------------------------------------------------------------------------------------   

			overContent = pdfStamper.getOverContent(4);  //individua la pagina 5 modulo agea
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);


			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 491,793, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,742, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,742, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,725, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,725, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,707, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,684, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,684, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,664, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,664, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 210,640, 0);


			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 100,600, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 450,600, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), 120,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), 210,585, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 515,585, 0);

			}

			if(smagazzino!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), 100,550, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), 450,550, 0);

				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), 120,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), 210,530, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getProvincia(), 515,530, 0);

			}



			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 275,505, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 370,505, 0);


			String M_0_5=String.valueOf(c.getM_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_5, 130,306, 0);       

			String M_6_65=String.valueOf(c.getM_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_6_65, 200,306, 0);       

			String M_over65=String.valueOf(c.getM_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, 260,306, 0);    

			String M_totIndCont=String.valueOf(c.getM_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndCont, 320,306, 0);       

			String M_IndSalt=String.valueOf(c.getM_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_IndSalt, 380,306, 0);    


			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, 430,306, 0);       

			String M_mediaPresenze=String.valueOf(c.getM_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_mediaPresenze, 480,306, 0);       

			String M_giorniApertura=String.valueOf(c.getM_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_giorniApertura, 530,306, 0);       


			//PACCHI

			String P_0_5=String.valueOf(c.getP_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_0_5, 130,280, 0);       

			String P_6_65=String.valueOf(c.getP_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_6_65, 200,280, 0);       

			String P_over65=String.valueOf(c.getP_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_over65, 260,280, 0);    

			String P_totIndCont=String.valueOf(c.getP_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndCont, 320,280, 0);       

			String P_IndSalt=String.valueOf(c.getP_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_IndSalt, 380,280, 0);    


			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, 430,280, 0);       

			String P_mediaPresenze=String.valueOf(c.getP_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_mediaPresenze, 480,280, 0);       

			String P_giorniApertura=String.valueOf(c.getP_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_giorniApertura, 530,280, 0);     

			//EMPORIO

			String E_0_5=String.valueOf(c.getE_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_0_5, 130,254, 0);       

			String E_6_65=String.valueOf(c.getE_6_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_6_65, 200,254, 0);       

			String E_over65=String.valueOf(c.getE_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_over65, 260,254, 0);    

			String E_totIndCont=String.valueOf(c.getE_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndCont, 320,254, 0);       

			String E_IndSalt=String.valueOf(c.getE_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_IndSalt, 380,254, 0);    


			String E_totIndigenti=String.valueOf(c.getE_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndigenti, 430,254, 0);       

			String E_mediaPresenze=String.valueOf(c.getE_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_mediaPresenze, 480,254, 0);       

			String E_giorniApertura=String.valueOf(c.getE_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_giorniApertura, 530,254, 0);     



			//STRADA

			String U_IndSalt=String.valueOf(c.getU_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_IndSalt, 380,228, 0);    

			String U_totIndigenti=String.valueOf(c.getU_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_totIndigenti, 430,228, 0);       

			String U_mediaPresenze=String.valueOf(c.getU_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_mediaPresenze, 480,228, 0);       

			String U_giorniApertura=String.valueOf(c.getU_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_giorniApertura, 530,228, 0); 

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,40, 0);


			//--------------------------------------------------------------------------------------------
			//----------------- copertina registro carico/scarico-----------------------------------------
			//-------------------------------------------------------------------------------------------- 

			overContent = pdfStamper.getOverContent(5);  
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			String codENom=CodiceStruttura+"/"+codiceSAP+" "+e.getNome();
			overContent.showTextAligned(Element.ALIGN_CENTER, codENom, 400,220, 0);

			if(c.getIdSedeOperativaEnte()==0){
				idSedeOperativaEnte=c.getIdSedeLegaleEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}


			//--------------------------------------------------------------------------------------------
			//----------------- verbale verifica fascicoli -----------------------------------------------
			//--------------------------------------------------------------------------------------------  
			DocumentoBean verbaleFascicoli= null;
			verbaleFascicoli=catdoc.getDocumento(idConvenzione, "verbale_fascicoli");

			overContent = pdfStamper.getOverContent(9);  //individua la pagina 10
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,694, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,694, 0);      
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,670, 0);               
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,670, 0);

			if(verbaleFascicoli.getTipoDocumento()!=null){

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("data"), 95,644, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("incaricato"), 235,644, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("presenzaElenco"), 510,548, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numPersone"), 510,521, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numNuclei"), 510,505, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("presenzaFascicoli"), 510,462, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoli"), 510,433, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("fascicoliCarenti"), 510,407, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoliEsaminati"), 510,382, 0);//verificare altezza
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoliCarenti"), 510,379, 0);


				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("annotazioni"), 95,322, 0);


			}

			//--------------------------------------------------------------------------------------------
			//----------------- verbale registro carico e scarico ----------------------------------------
			//--------------------------------------------------------------------------------------------  
			DocumentoBean verbaleRegistro= null;
			verbaleRegistro=catdoc.getDocumento(idConvenzione, "verbale_registro");

			overContent = pdfStamper.getOverContent(10);  //individua la pagina 11
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			if(verbaleRegistro.getTipoDocumento()!=null){

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("data"), 95,644, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("incaricato"), 235,644, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaRegistro"), 510,578, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroInformatizzato"), 510,552, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("fogliInamovibili"), 510,536, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroErrato"), 510,522, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroFirmato"), 510,495, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaDichiarazione"), 510,467, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaFirma"), 510,427, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaNumData"), 510,410, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaAttestati"), 510,382, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("annotazioni"), 95,320, 0);
			}

			//--------------------------------------------------------------------------------------------
			//----------------- prima pagina allegato 6 informativa   ------------------------------------
			//-------------------------------------------------------------------------------------------- 

			overContent = pdfStamper.getOverContent(11);  //individua la pagina 12 primo foglio informativa

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,257, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,257, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,240, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,240, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,222, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,199, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,199, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 110,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 510,181, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);

			//--------------------------------------------------------------------------------------------
			//----------------- seconda pagina allegato 6  informativa -----------------------------------
			//-------------------------------------------------------------------------------------------- 

			overContent = pdfStamper.getOverContent(12);  //individua la pagina 13 

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);



			//--------------------------------------------------------------------------------------------
			//----------------- dichiarazione consegna agli indigenti-------------------------------------
			//--------------------------------------------------------------------------------------------  

			overContent = pdfStamper.getOverContent(13);  //individua la pagina 14

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,576, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,576, 0);



			//--------------------------------------------------------------------------------------------
			//----------------- seconda pagina informativa trattamento dati ------------------------------
			//--------------------------------------------------------------------------------------------         

			overContent = pdfStamper.getOverContent(15);

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,100, 0);



			//--------------------------------------------------------------------------------------------
			//----------------- dichiarazione legale rappresentante --------------------------------------
			//--------------------------------------------------------------------------------------------              

			overContent = pdfStamper.getOverContent(16);  //individua la pagina 17 dichiarazione del legale rappresentante

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 150,300, 0);


			pdfStamper.close();
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}
	}

	public static void StampaModuli_Convenzione_Banco(String path0,String path1, int idConvenzione) throws IOException {
		try {

			File f=new File(path1);
			if(f.exists()){
				System.out.println("il file esiste");
				f.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");


			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();
			CatalogoConvenzioni catc=new CatalogoConvenzioni();
			DAO_Documento catdoc = new DAO_Documento();
			Convenzione c=catc.getConvenzione(idConvenzione);

			String annoCorr=String.valueOf(c.getAnno());
			int idEnte=c.getIdEnte();


			int idSedeOperativaEnte=0;
			int idSedeMagazzinoEnte=0;
			Sede soperativa=null;
			Sede smagazzino=null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());

			if(c.getIdSedeOperativaEnte()!=0){
				idSedeOperativaEnte=c.getIdSedeOperativaEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			if(c.getIdSedeMagazzinoEnte()!=0){
				idSedeMagazzinoEnte=c.getIdSedeMagazzinoEnte();
				smagazzino=cats.getSede(idSedeMagazzinoEnte);
			}


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			String codiceSAP=String.valueOf(e.getCodiceSap());

			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());
			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));



			//--------------------------------------------------------------------------------------------
			//----------------- Pagina scheda anagrafica (riassunto dati) --------------------------------
			//--------------------------------------------------------------------------------------------

			PdfContentByte overContent = pdfStamper.getOverContent(1);  //individua la pagina 1 riassunto dati
			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, CodiceStruttura, 60,590, 0);        
			overContent.showTextAligned(Element.ALIGN_LEFT, "SAP:"+codiceSAP, 60,570, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);
			System.out.println(soperativa);
			System.out.println(slegale);
			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getEmail(), 250,558, 0);
			}
			else{
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), 250,598, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 250,578, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 250,558, 0);

			}

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCognome()+" "+rapp.getNome(), 250,520, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCellulare(), 250,499, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getTelefono(), 250,479, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getEmail(), 250,459, 0);
			if (altroRef!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCognome()+" "+altroRef.getNome(), 250,430, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getCellulare(), 250,410, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getTelefono(), 250,391, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, altroRef.getEmail(), 250,371, 0);

			}


			//--------------------------------------------------------------------------------------------
			//----------------- Prima pagina accordo di collaborazione  ----------------------------------
			//--------------------------------------------------------------------------------------------

			overContent = pdfStamper.getOverContent(2); //individua la pagina 2 (accordo collaborazione pag 1)

			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			overContent.setColorFill(BaseColor.BLACK);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP+" "+e.getNome(), 530,707, 0);


			//--------------------------------------------------------------------------------------------
			//----------------- Seconda pagina accordo di collaborazione  --------------------------------
			//--------------------------------------------------------------------------------------------        

			overContent = pdfStamper.getOverContent(3);  //individua la pagina 3 (accordo collaborazione pag 2)
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 310,423, 0);



			//--------------------------------------------------------------------------------------------
			//----------------------- pagina domanda iscrizione ------------------------------------------
			//--------------------------------------------------------------------------------------------

			overContent = pdfStamper.getOverContent(4);  //individua la pagina 4 modulo iscrizione

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 529,794, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,752, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,752, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,738, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,738, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,722, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 140,700, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 570,700, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 160,644, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 110,685, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 360,685, 0);

			if(soperativa!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 280,620, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), 205,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), 275,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 545,605, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), 50,605, 0);
			}

			if(smagazzino!=null){
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), 315,580, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), 205,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), 275,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getProvincia(), 545,565, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), 50,565, 0);
			}

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 570,685, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getFax(), 85,669, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getEmail(), 212,669, 0);

			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 228,539, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 274,539, 0);

			String M_0_5=String.valueOf(c.getM_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_5, 149,356, 0);       
			String R_0_5=String.valueOf(c.getR_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_0_5, 149,306, 0);
			String P_0_5=String.valueOf(c.getP_0_5());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_0_5, 149,245, 0);

			String M_19_65=String.valueOf(c.getM_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_19_65, 350,356, 0);       
			String R_19_65=String.valueOf(c.getR_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_19_65, 350,306, 0);
			String P_19_65=String.valueOf(c.getP_19_65());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_19_65, 350,245, 0);

			String M_6_18=String.valueOf(c.getM_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_6_18, 250,356, 0);       
			String R_6_18=String.valueOf(c.getR_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_6_18, 250,306, 0);
			String P_6_18=String.valueOf(c.getP_6_18());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_6_18, 250,245, 0);

			String M_over65=String.valueOf(c.getM_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, 149,345, 0);       
			String R_over65=String.valueOf(c.getR_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT, R_over65, 149,293, 0);
			String P_over65=String.valueOf(c.getP_over65());
			overContent.showTextAligned(Element.ALIGN_LEFT,P_over65, 149,230, 0);

			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, 450,356, 0);       
			String R_totIndigenti=String.valueOf(c.getR_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, R_totIndigenti, 450,316, 0); 
			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, 450,246, 0); 

			String M_mediaPresenze=String.valueOf(c.getM_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_mediaPresenze, 500,356, 0);       
			String R_mediaPresenze=String.valueOf(c.getR_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, R_mediaPresenze, 500,316, 0);       
			String P_mediaPresenze=String.valueOf(c.getP_mediaPresenze());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_mediaPresenze, 500,246, 0);       

			String M_giorniApertura=String.valueOf(c.getM_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_giorniApertura, 550,356, 0);       
			String P_giorniApertura=String.valueOf(c.getP_giorniApertura());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_giorniApertura, 550,246, 0);       

			if(c.isM_pranzo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,373, 0);
			if(c.isM_cena())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 86,373, 0);
			if(c.isM_colazione())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 127,373, 0);       
			if(c.isM_pastiFreddi())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 191,373, 0);       
			if(c.isM_centroAccoglienzaDiurno())	
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 267,373, 0);       

			if(c.isR_casaFamiglia())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,321, 0);
			if(c.isR_comunitaDisabili())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 104,321, 0);
			if(c.isR_comunitaTossicodipendenti())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 181,321, 0);
			if(c.isR_comunitaAnziani())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 271,321, 0);
			if(c.isR_comunitaMinori())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 360,321, 0);

			if(c.isP_distribuzionePacchi())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 35,260, 0);

			if(c.isP_consegnaDomicilio())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 135,260, 0);

			if(!(c.getP_Altro().equals("no"))){
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 262,260, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, c.getP_Altro(), 291,260, 0);
			}

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 70,32, 0);



			//--------------------------------------------------------------------------------------------
			//----------------- carico e scarico ------------------------------------------
			//--------------------------------------------------------------------------------------------        

			DocumentoBean verbaleRegistro= null;
			verbaleRegistro=catdoc.getDocumento(idConvenzione, "verbale_registro");
			overContent = pdfStamper.getOverContent(5);  //individua la pagina 5 carico e scarico
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			if(c.getIdSedeOperativaEnte()==0){
				idSedeOperativaEnte=c.getIdSedeLegaleEnte();
				soperativa=cats.getSede(idSedeOperativaEnte);
			}

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			if(verbaleRegistro.getTipoDocumento()!=null){

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("data"), 95,644, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("incaricato"), 235,644, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaRegistro"), 510,578, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroInformatizzato"), 510,552, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("fogliInamovibili"), 510,536, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroErrato"), 510,522, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroFirmato"), 510,495, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaDichiarazione"), 510,470, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaFirma"), 510,428, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaNumData"), 510,412, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaAttestati"), 510,385, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("annotazioni"), 95,320, 0);
			}


			//--------------------------------------------------------------------------------------------
			//----------------- primo foglio informativa -------------------------------------------------
			//-------------------------------------------------------------------------------------------- 

			overContent = pdfStamper.getOverContent(6);  //individua la pagina 6 primo foglio informativa

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,257, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,257, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,240, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,240, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,222, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,199, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,199, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 110,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,181, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 510,181, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			//--------------------------------------------------------------------------------------------
			//----------------- secondo foglio informativa ----------------------------------------------
			//--------------------------------------------------------------------------------------------        

			overContent = pdfStamper.getOverContent(7);  //individua la pagina sec foglio informativa 

			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 60,50, 0);


			//--------------------------------------------------------------------------------------------
			//----------------- dichiarazione legale rappresentante --------------------------------------
			//--------------------------------------------------------------------------------------------              

			overContent = pdfStamper.getOverContent(8);  
			overContent.beginText();
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_CENTER, rapp.getNome()+" "+rapp.getCognome(), 205,654, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, rapp.getCF(), 530,654, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(), 100,637, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), 400,637, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 250,619, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), 160,596, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getTelefono(), 530,596, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), 120,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), 210,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getProvincia(), 515,577, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, c.getDataFirma(), 155,375, 0);


			pdfStamper.close();
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void StampaModuli_Convenzione2017(String path0,String path1, ConvenzioneFrom2017 c) throws IOException {
		try {

			File f1=new File(path1);
			if(f1.exists()){
				System.out.println("il file target esiste gi�");
				f1.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");

			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();

			//DAO_Documento catdoc = new DAO_Documento();


			int idEnte=c.getIdEnte();

			Sede soperativa=(c.getIdSedeOperativaEnte()!=0) ? cats.getSede(c.getIdSedeOperativaEnte()) : null;
			Sede smagazzino=(c.getIdSedeMagazzinoEnte()!=0) ? cats.getSede(c.getIdSedeMagazzinoEnte()): null;
			Sede slegale=cats.getSede(c.getIdSedeLegaleEnte());


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			Persona altroRef=catp.getPersonaAltroRef(idEnte);


			String cellafrigoSI="";
			String cellafrigoNO="";
			if(c.isCellaFrigo())
				cellafrigoSI="X";
			else
				cellafrigoNO="X";


			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			String annoCorr=String.valueOf(c.getAnno());
			String codiceSAP=String.valueOf(e.getCodiceSap());
			PdfContentByte overContent = pdfStamper.getOverContent(1); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);



			//--------------------------------------------------------------------------------------------
			//----------------- domanda iscrizione--------------------------------------------------------
			//--------------------------------------------------------------------------------------------   


			int h=20;
			int xInizio=120;
			int yInizio=520;

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura+"/"+codiceSAP, 100,815, 0);


			//overContent.showTextAligned(Element.ALIGN_RIGHT, annoCorr, 491,793, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getNome()+" "+rapp.getCognome(), xInizio,yInizio, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getCF(), xInizio+255,yInizio, 0);
			yInizio-=h;
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getLuogoNascita(),xInizio ,yInizio, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, rapp.getDataNascita(), xInizio+320,yInizio, 0);
			yInizio-=h;
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), xInizio+100,yInizio, 0);
			yInizio-=h;
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getIndirizzo(), xInizio+50,yInizio, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getTelefono(), xInizio+350,yInizio, 0);
			yInizio-=h;
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getCap(), xInizio,yInizio, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, slegale.getComune(), xInizio+50,yInizio, 0);
			overContent.showTextAligned(Element.ALIGN_RIGHT, slegale.getProvincia(), xInizio+410,yInizio, 0);
			yInizio-=2*h;
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getPartitaIva(), 230,yInizio+10, 0);


			if(soperativa!=null){
				int ysoperativa=yInizio-2*h-3;
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), xInizio,ysoperativa, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getTelefono(), xInizio+350,ysoperativa, 0);
				ysoperativa-=h;	
				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getCap(), xInizio,ysoperativa, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getComune(), xInizio+50,ysoperativa, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, soperativa.getProvincia(), xInizio+410,ysoperativa, 0);

			}

			if(smagazzino!=null){
				int ysmagazzino=yInizio-5*h-6;
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getIndirizzo(), xInizio,ysmagazzino, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getTelefono(), xInizio+350,ysmagazzino, 0);
				ysmagazzino-=h;
				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getCap(), xInizio,ysmagazzino, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, smagazzino.getComune(), xInizio+50,ysmagazzino, 0);
				overContent.showTextAligned(Element.ALIGN_RIGHT, smagazzino.getProvincia(), xInizio+410,ysmagazzino, 0);

			}



			if(c.isCellaFrigo())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", xInizio+150,250, 0);
			else
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", xInizio+245,250, 0);

			if(c.getTipoPartner()!=null &&c.getTipoPartner().equals("pubblico"))
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 66,104, 0);
			if(c.getTipoPartner()!=null &&c.getTipoPartner().equals("no_lucro"))
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", 66,71, 0);


			//--------------------------------------------------------------------------------------------
			//----------------- tabella           --------------------------------------------------------
			//--------------------------------------------------------------------------------------------  

			overContent = pdfStamper.getOverContent(2);  //individua la pagina  modulo agea
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);
			overContent.beginText();

			int altezzariga=25;
			int larghezzaCella=30;
			int largezzaCellaBig=45;
			int x=160;
			int y=635;

			String M_0_15=String.valueOf(c.getM_0_15());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_0_15, x,y, 0);       
			x+=larghezzaCella;

			String M_16_64=String.valueOf(c.getM_16_64());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_16_64, x,y, 0);       
			x+=larghezzaCella;
			String M_over65=String.valueOf(c.getM_over_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, M_over65, x,y, 0);    
			x+=largezzaCellaBig;
			String M_totIndCont=String.valueOf(c.getM_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndCont, x,y, 0);       
			x+=largezzaCellaBig;
			String M_IndSalt=String.valueOf(c.getM_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_IndSalt, x,y, 0);    
			x+=largezzaCellaBig;
			String M_totIndigenti=String.valueOf(c.getM_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_totIndigenti, x,y, 0);       
			x+=largezzaCellaBig;
			String M_donne=String.valueOf(c.getM_donne());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_donne, x,y, 0);       
			x+=largezzaCellaBig;
			String M_migranti=String.valueOf(c.getM_migranti());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_migranti, x,y, 0);  
			x+=largezzaCellaBig;
			String M_disabili=String.valueOf(c.getM_disabili());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_disabili, x,y, 0);  
			x+=largezzaCellaBig;
			String M_senzaFissaDimora=String.valueOf(c.getM_senzaFissaDimora());
			overContent.showTextAligned(Element.ALIGN_CENTER, M_senzaFissaDimora, x,y, 0);  


			//PACCHI
			y-=altezzariga;
			x=160;
			String P_0_15=String.valueOf(c.getP_0_15());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_0_15, x,y, 0);       
			x+=larghezzaCella;
			String P_16_64=String.valueOf(c.getP_16_64());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_16_64, x,y, 0);       
			x+=larghezzaCella;
			String P_over65=String.valueOf(c.getP_over_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, P_over65, x,y, 0);    
			x+=largezzaCellaBig;
			String P_totIndCont=String.valueOf(c.getP_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndCont, x,y, 0);       
			x+=largezzaCellaBig;
			String P_IndSalt=String.valueOf(c.getP_saltuari());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_IndSalt, x,y, 0);    
			x+=largezzaCellaBig;
			String P_totIndigenti=String.valueOf(c.getP_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_totIndigenti, x,y, 0);       
			x+=largezzaCellaBig;
			String P_donne=String.valueOf(c.getP_donne());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_donne, x,y, 0);       
			x+=largezzaCellaBig;
			String P_migranti=String.valueOf(c.getP_migranti());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_migranti, x,y, 0);  
			x+=largezzaCellaBig;
			String P_disabili=String.valueOf(c.getP_disabili());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_disabili, x,y, 0);  
			x+=largezzaCellaBig;
			String P_senzaFissaDimora=String.valueOf(c.getP_senzaFissaDimora());
			overContent.showTextAligned(Element.ALIGN_CENTER, P_senzaFissaDimora, x,y, 0);  

			//EMPORIO
			y-=altezzariga;
			x=160;
			String E_0_15=String.valueOf(c.getE_0_15());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_0_15, x,y, 0);       
			x+=larghezzaCella;
			String E_16_64=String.valueOf(c.getE_16_64());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_16_64, x,y, 0);       
			x+=larghezzaCella;
			String E_over65=String.valueOf(c.getE_over_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, E_over65, x,y, 0);    
			x+=2*largezzaCellaBig;
			String E_totIndCont=String.valueOf(c.getE_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndCont, x,y, 0);   
			x+=largezzaCellaBig;
			String E_totIndigenti=String.valueOf(c.getE_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_totIndigenti, x,y, 0); 
			x+=largezzaCellaBig;
			String E_donne=String.valueOf(c.getE_donne());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_donne, x,y, 0);       
			x+=largezzaCellaBig;
			String E_migranti=String.valueOf(c.getE_migranti());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_migranti, x,y, 0);  
			x+=largezzaCellaBig;
			String E_disabili=String.valueOf(c.getE_disabili());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_disabili, x,y, 0);  
			x+=largezzaCellaBig;
			String E_senzaFissaDimora=String.valueOf(c.getE_senzaFissaDimora());
			overContent.showTextAligned(Element.ALIGN_CENTER, E_senzaFissaDimora, x,y, 0);   

			//DISTRIBUZIONE
			y-=altezzariga;
			x=160;
			String D_0_15=String.valueOf(c.getD_0_15());
			overContent.showTextAligned(Element.ALIGN_LEFT, D_0_15, x,y, 0);       
			x+=larghezzaCella;
			String D_16_64=String.valueOf(c.getD_16_64());
			overContent.showTextAligned(Element.ALIGN_LEFT, D_16_64, x,y, 0);       
			x+=larghezzaCella;
			String D_over65=String.valueOf(c.getD_over_65());
			overContent.showTextAligned(Element.ALIGN_LEFT, D_over65, x,y, 0);    
			x+=2*largezzaCellaBig;
			String D_totIndCont=String.valueOf(c.getD_TotaleAssistitiContinuativi());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_totIndCont, x,y, 0);   
			x+=largezzaCellaBig;
			String D_totIndigenti=String.valueOf(c.getD_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_totIndigenti, x,y, 0); 
			x+=largezzaCellaBig;
			String D_donne=String.valueOf(c.getD_donne());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_donne, x,y, 0);       
			x+=largezzaCellaBig;
			String D_migranti=String.valueOf(c.getD_migranti());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_migranti, x,y, 0);  
			x+=largezzaCellaBig;
			String D_disabili=String.valueOf(c.getD_disabili());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_disabili, x,y, 0);  
			x+=largezzaCellaBig;
			String D_senzaFissaDimora=String.valueOf(c.getD_senzaFissaDimora());
			overContent.showTextAligned(Element.ALIGN_CENTER, D_senzaFissaDimora, x,y, 0);

			//STRADA
			y-=altezzariga;
			x=160;
			x=x+2*larghezzaCella+3*largezzaCellaBig;
			String U_totIndigenti=String.valueOf(c.getU_totIndigenti());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_totIndigenti, x,y, 0); 
			x+=largezzaCellaBig;
			String U_donne=String.valueOf(c.getU_donne());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_donne, x,y, 0);       
			x+=largezzaCellaBig;
			String U_migranti=String.valueOf(c.getU_migranti());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_migranti, x,y, 0);  
			x+=largezzaCellaBig;
			String U_disabili=String.valueOf(c.getU_disabili());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_disabili, x,y, 0);  
			x+=largezzaCellaBig;
			String U_senzaFissaDimora=String.valueOf(c.getU_senzaFissaDimora());
			overContent.showTextAligned(Element.ALIGN_CENTER, U_senzaFissaDimora, x,y, 0);

			x=110;
			y=400;
			int row=33;		
			if(c.isAccoglienza())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isInformazione())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isAccompagnamento())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isSpsicologico())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isEducativa())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isConsulenza())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isSscolastico())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isSorientamento())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isPrimaassistenza())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.isTutela())
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
			y-=row;
			if(c.getAltroTtxt()!=null && !c.getAltroTtxt().equals("no")){
				overContent.showTextAligned(Element.ALIGN_CENTER, "XX", x,y, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, c.getAltroTtxt(), x+100,y+2, 0);
			}
			//--------------------------------------------------------------------------------------------
			//----------------- pagina 3           --------------------------------------------------------
			//--------------------------------------------------------------------------------------------  

			overContent = pdfStamper.getOverContent(3);  //individua la pagina  modulo agea
			bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);
			overContent.beginText(); 
			overContent.showTextAligned(Element.ALIGN_CENTER, c.getDataFirma(), 100,410, 0);
			pdfStamper.close();
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}
	}

	public static void StampaModuli_verbale_fascicoli_2017(String path0,String path1, ConvenzioneFrom2017 c) throws IOException 
	{
		try {

			File f1=new File(path1);
			if(f1.exists()){
				System.out.println("il file target esiste gi�");
				f1.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");
			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();

			DAO_Documento catdoc=new DAO_Documento();


			int idEnte=c.getIdEnte();

			Sede soperativa=(c.getIdSedeOperativaEnte()!=0) ? cats.getSede(c.getIdSedeOperativaEnte()) : cats.getSede(c.getIdSedeLegaleEnte());
			


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			String annoCorr=String.valueOf(c.getAnno());
			String codiceSAP=String.valueOf(e.getCodiceSap());
			PdfContentByte overContent = pdfStamper.getOverContent(1); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);
			
			DocumentoBean verbaleFascicoli= null;
			
			verbaleFascicoli=catdoc.getDocumento(c.getIdConvenzione(), "verbale_fascicoli");

			

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,694, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,694, 0);      
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,670, 0);               
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,670, 0);

			if(verbaleFascicoli.getTipoDocumento()!=null){
int offset=-300;
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("data"), 95,644, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("incaricato"), 235,644, 0);

				
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("presenzaElenco"), 510,548+offset, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numPersone"), 510,521+offset, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numNuclei"), 510,505+offset, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("presenzaFascicoli"), 510,447+offset, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoli"), 510,418+offset, 0);
				
				 overContent = pdfStamper.getOverContent(2); //individua la pagina 2 accordo collaborazione pag 1

				overContent.beginText();
				 bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
				overContent.setFontAndSize(bf, 12);
				overContent.setColorFill(BaseColor.BLACK);
				 offset=+320;
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("fascicoliCarenti"), 510,420+offset, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoliEsaminati"), 510,382+offset, 0);//verificare altezza
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("numFascicoliCarenti"), 510,365+offset, 0);


				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleFascicoli.getCampi().get("annotazioni"), 85,272+offset, 0);
				pdfStamper.close();

			}
		}catch (DocumentException e1) {

			e1.printStackTrace();
		}

	}
	
	public static void StampaModuli_verbale_registro_2017(String path0,String path1, ConvenzioneFrom2017 c) throws IOException 
	{
		try {

			File f1=new File(path1);
			if(f1.exists()){
				System.out.println("il file target esiste gi�");
				f1.delete();
				System.out.println("eliminato il file");

			}
			else
				System.out.println("il file non esiste e verr� creato");
			CatalogoEnti cate = new CatalogoEnti();
			CatalogoSede cats= new CatalogoSede();
			CatalogoPersona catp=new CatalogoPersona();

			DAO_Documento catdoc = new DAO_Documento();


			int idEnte=c.getIdEnte();

			Sede soperativa=(c.getIdSedeOperativaEnte()!=0) ? cats.getSede(c.getIdSedeOperativaEnte()) : cats.getSede(c.getIdSedeLegaleEnte());
			


			Ente e=cate.getEnte(idEnte);	
			String CodiceStruttura=String.valueOf(e.getCodiceStruttura());
			Persona rapp=catp.getPersona(c.getIdRappLegaleEnte());

			PdfReader reader=new PdfReader(path0);
			PdfStamper pdfStamper;
			pdfStamper = new PdfStamper(reader, new FileOutputStream(path1));

			String annoCorr=String.valueOf(c.getAnno());
			String codiceSAP=String.valueOf(e.getCodiceSap());
			PdfContentByte overContent = pdfStamper.getOverContent(1); //individua la pagina 2 accordo collaborazione pag 1

			overContent.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, BaseFont.EMBEDDED);
			overContent.setFontAndSize(bf, 12);
			overContent.setColorFill(BaseColor.BLACK);
			
			DocumentoBean verbaleRegistro= null;
			
			verbaleRegistro=catdoc.getDocumento(c.getIdConvenzione(), "verbale_registro");
			
			
			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 100,815, 0);

			overContent.showTextAligned(Element.ALIGN_RIGHT, CodiceStruttura, 130,697, 0);

			overContent.showTextAligned(Element.ALIGN_LEFT, e.getNome(), 235,697, 0);

			

			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getIndirizzo(), 95,673, 0);
			overContent.showTextAligned(Element.ALIGN_LEFT, soperativa.getProvincia(), 510,673, 0);

			if(verbaleRegistro.getTipoDocumento()!=null){

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("data"), 95,644, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("incaricato"), 235,644, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaRegistro"), 510,578, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroInformatizzato"), 510,552, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("fogliInamovibili"), 510,536, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroErrato"), 510,522, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroFirmato"), 510,495, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaDichiarazione"), 510,470, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaFirma"), 510,428, 0);
				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("registroSenzaNumData"), 510,412, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("presenzaAttestati"), 510,385, 0);

				overContent.showTextAligned(Element.ALIGN_LEFT, verbaleRegistro.getCampi().get("annotazioni"), 95,320, 0);
				pdfStamper.close();
			}
		}catch (DocumentException e1) {

			e1.printStackTrace();
		}

	}


}