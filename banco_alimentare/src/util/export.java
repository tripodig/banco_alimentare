package util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import beans.*;

public class export  { 
	String directorySalvataggio;
	
    public void setdirectorySalvataggio(String path){
    	directorySalvataggio=path;
    } 
	
    public void sommaModBByArea(String area, int idEvento, String evento) throws Exception {
    	
    	if(area.equalsIgnoreCase("ALL")){
    		
    		sommaModB(idEvento);
    		return;
    		
    	}
    	
    	StringTokenizer st= new StringTokenizer(area,",");
    	int numAree=st.countTokens();
    	String[] nomiAree=new String[numAree];
    	
    	for(int j=0;j<numAree;j++){
    		nomiAree[j]=st.nextToken();
    		
    		
    	}
		 
		 GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
		
		
		File mioFile = new File(directorySalvataggio+"/Totali_Colletta_"+evento+".csv");
		
		FileOutputStream fileCreato = new FileOutputStream(mioFile);
		PrintStream fileScritto = new PrintStream(fileCreato);
		CatalogoPuntoVendita pdvs=new CatalogoPuntoVendita();
		DAO_ModB DAO =new DAO_ModB ();
		Vector<ModB> ModBs=new Vector<ModB>();
		ModBs=DAO.getModbSommatiByAree(area, idEvento);
		
		List<ModB> sommeModelliB= new LinkedList<ModB>();
		sommeModelliB=DAO.getSOMMAModBSommatiByAree(area, idEvento);
		
		
		fileScritto.println("INSEGNA;INDIRIZZO;LOCALIT�;AREA;Olio;;Omogeneizzati;;Alimenti infanzia;;Tonno;;Carne in Scatola;;Pelati;;Legumi;;Pasta;;Riso;;Zucchero;;Latte;;Biscotti;;Varie;;Tot. Supermercato");	
		fileScritto.println(";;;;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole");	

		
		for (int i=0;i<ModBs.size();i++)
			{
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getInsegna()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getIndirizzo()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getComune()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getArea()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_olio()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_olio()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_omogeinizzati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_omogeinizzati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_infanzia()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_infanzia()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_tonno()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_tonno()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_carne()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_carne()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pelati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pelati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_legumi()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_legumi()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pasta()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pasta()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_riso()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_riso()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_zucchero()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_zucchero()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_latte()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_latte()+";");

			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_biscotti()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_biscotti()+";");
			
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_varie()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_varie()+";");
			
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(0).toString().replace('.', ',')+";");
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(1).intValue());
			fileScritto.println();
			}
//Stampa delle righe finali dei totali
		
		Iterator<ModB> it= sommeModelliB.iterator();
		
		int i=0;
		while(it.hasNext()){
			ModB somme=new ModB();
			somme=(ModB)it.next();
		
		
		fileScritto.println();
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_olio()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_olio()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_omogeinizzati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_omogeinizzati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_infanzia()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_infanzia()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_tonno()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_tonno()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_carne()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_carne()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pelati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pelati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_legumi()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_legumi()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pasta()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pasta()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_riso()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_riso()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_zucchero()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_zucchero()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_latte()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_latte()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_biscotti()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_biscotti()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_varie()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_varie()+";");
		
		fileScritto.print(DAO.getTotaliModB(somme).get(0).toString().replace('.', ',')+";");
		fileScritto.print(DAO.getTotaliModB(somme).get(1).intValue());
		fileScritto.println();
		
		i++;
		}
		fileScritto.close();
	 }
 	
    public void sommaModBByArea(String area) throws Exception {
    	
    	if(area.equalsIgnoreCase("ALL")){
    		
    		sommaModB();
    		return;
    		
    	}
    	
    	StringTokenizer st= new StringTokenizer(area,",");
    	int numAree=st.countTokens();
    	String[] nomiAree=new String[numAree];
    	
    	for(int j=0;j<numAree;j++){
    		nomiAree[j]=st.nextToken();
    		
    		
    	}
		 
		 GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
		File mioFile = new File(directorySalvataggio+"/Totali_Colletta_"+anno+".csv");
		
		FileOutputStream fileCreato = new FileOutputStream(mioFile);
		PrintStream fileScritto = new PrintStream(fileCreato);
		CatalogoPuntoVendita pdvs=new CatalogoPuntoVendita();
		DAO_ModB DAO =new DAO_ModB ();
		Vector<ModB> ModBs=new Vector<ModB>();
		ModBs=DAO.getModbSommatiByAree(area);
		
		List<ModB> sommeModelliB= new LinkedList<ModB>();
		sommeModelliB=DAO.getSOMMAModBSommatiByAree(area);
		
		
		fileScritto.println("INSEGNA;INDIRIZZO;LOCALIT�;AREA;Olio;;Omogeneizzati;;Alimenti infanzia;;Tonno;;Carne in Scatola;;Pelati;;Legumi;;Pasta;;Riso;;Zucchero;;Latte;;Biscotti;;Varie;;Tot. Supermercato");	
		fileScritto.println(";;;;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole");	

		
		for (int i=0;i<ModBs.size();i++)
			{
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getInsegna()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getIndirizzo()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getComune()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getArea()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_olio()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_olio()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_omogeinizzati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_omogeinizzati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_infanzia()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_infanzia()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_tonno()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_tonno()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_carne()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_carne()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pelati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pelati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_legumi()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_legumi()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pasta()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pasta()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_riso()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_riso()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_zucchero()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_zucchero()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_latte()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_latte()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_biscotti()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_biscotti()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_varie()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_varie()+";");
			
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(0).toString().replace('.', ',')+";");
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(1).intValue());
			fileScritto.println();
			}
//Stampa delle righe finali dei totali
		
		Iterator<ModB> it= sommeModelliB.iterator();
		
		int i=0;
		while(it.hasNext()){
			ModB somme=new ModB();
			somme=(ModB)it.next();
		
		
		fileScritto.println();
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print("Totali area "+nomiAree[i]+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_olio()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_olio()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_omogeinizzati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_omogeinizzati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_infanzia()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_infanzia()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_tonno()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_tonno()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_carne()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_carne()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pelati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pelati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_legumi()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_legumi()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pasta()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pasta()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_riso()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_riso()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_zucchero()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_zucchero()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_latte()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_latte()+";");

		fileScritto.print(Double.toString(somme.getPeso_tot_biscotti()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_biscotti()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_varie()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_varie()+";");
		
		fileScritto.print(DAO.getTotaliModB(somme).get(0).toString().replace('.', ',')+";");
		fileScritto.print(DAO.getTotaliModB(somme).get(1).intValue());
		fileScritto.println();
		
		i++;
		}
		fileScritto.close();
	 }
    
	private void sommaModB() throws Exception {
		 
		 GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
		File mioFile = new File(directorySalvataggio+"/Totali_Colletta_"+anno+".csv");
		
		FileOutputStream fileCreato = new FileOutputStream(mioFile);
		PrintStream fileScritto = new PrintStream(fileCreato);
		CatalogoPuntoVendita pdvs=new CatalogoPuntoVendita();
		DAO_ModB DAO =new DAO_ModB ();
		Vector<ModB> ModBs=new Vector<ModB>();
		ModBs=DAO.getModbSommati();
		ModB somme=new ModB();
		somme=DAO.getSOMMAModbSommati();
		
		fileScritto.println("INSEGNA;INDIRIZZO;LOCALIT�;Olio;;Omogeneizzati;;Alimenti infanzia;;Tonno;;Carne in Scatola;;Pelati;;Legumi;;Pasta;;Riso;;Zucchero;;Latte;;Biscotti;;Varie;;Tot. Supermercato");	
		fileScritto.println(";;;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole");	

		
		for (int i=0;i<ModBs.size();i++)
			{
			
			
			
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getInsegna()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getIndirizzo()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getComune()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_olio()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_olio()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_omogeinizzati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_omogeinizzati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_infanzia()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_infanzia()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_tonno()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_tonno()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_carne()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_carne()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pelati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pelati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_legumi()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_legumi()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pasta()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pasta()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_riso()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_riso()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_zucchero()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_zucchero()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_latte()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_latte()+";");

			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_biscotti()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_biscotti()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_varie()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_varie()+";");
			
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(0).toString().replace('.', ',')+";");
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(1).intValue());
			fileScritto.println();
			}
//Stampa della riga finale dei totali
		fileScritto.println();
		fileScritto.print("Totali;");
		fileScritto.print("Totali;");
		fileScritto.print("Totali;");
		fileScritto.print(Double.toString(somme.getPeso_tot_olio()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_olio()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_omogeinizzati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_omogeinizzati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_infanzia()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_infanzia()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_tonno()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_tonno()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_carne()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_carne()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pelati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pelati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_legumi()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_legumi()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pasta()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pasta()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_riso()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_riso()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_zucchero()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_zucchero()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_latte()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_latte()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_biscotti()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_biscotti()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_varie()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_varie()+";");
		
		fileScritto.print(DAO.getTotaliModB(somme).get(0).toString().replace('.', ',')+";");
		fileScritto.print(DAO.getTotaliModB(somme).get(1).intValue());
		fileScritto.println();
		
		
		
		fileScritto.close();

	 }	
	 
	private void sommaModB(int idEvento) throws Exception {
		 
		 GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
		File mioFile = new File(directorySalvataggio+"/Totali_Colletta_"+anno+".csv");
		
		FileOutputStream fileCreato = new FileOutputStream(mioFile);
		PrintStream fileScritto = new PrintStream(fileCreato);
		CatalogoPuntoVendita pdvs=new CatalogoPuntoVendita();
		DAO_ModB DAO =new DAO_ModB ();
		Vector<ModB> ModBs=new Vector<ModB>();
		ModBs=DAO.getModbSommati(idEvento);
		ModB somme=new ModB();
		somme=DAO.getSOMMAModbSommati(idEvento);
		
		fileScritto.println("INSEGNA;INDIRIZZO;LOCALIT�;Olio;;Omogeneizzati;;Alimenti infanzia;;Tonno;;Carne in Scatola;;Pelati;;Legumi;;Pasta;;Riso;;Zucchero;;Latte;;Biscotti;;Varie;;Tot. Supermercato");	
		fileScritto.println(";;;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole;Kg;Scatole");	

		
		for (int i=0;i<ModBs.size();i++)
			{
			
			
			
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getInsegna()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getIndirizzo()+";");
			fileScritto.print(pdvs.getPuntoVendita(ModBs.get(i).getIdPuntoVendita()).getComune()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_olio()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_olio()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_omogeinizzati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_omogeinizzati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_infanzia()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_infanzia()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_tonno()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_tonno()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_carne()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_carne()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pelati()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pelati()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_legumi()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_legumi()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_pasta()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_pasta()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_riso()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_riso()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_zucchero()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_zucchero()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_latte()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_latte()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_biscotti()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_biscotti()+";");
			fileScritto.print(Double.toString(ModBs.get(i).getPeso_tot_varie()).replace('.', ',')+";");
			fileScritto.print(ModBs.get(i).getScatoli_tot_varie()+";");
			
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(0).toString().replace('.', ',')+";");
			fileScritto.print(DAO.getTotaliModB(ModBs.get(i)).get(1).intValue());
			fileScritto.println();
			}
//Stampa della riga finale dei totali
		fileScritto.println();
		fileScritto.print("Totali;");
		fileScritto.print("Totali;");
		fileScritto.print("Totali;");
		fileScritto.print(Double.toString(somme.getPeso_tot_olio()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_olio()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_omogeinizzati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_omogeinizzati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_infanzia()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_infanzia()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_tonno()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_tonno()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_carne()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_carne()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pelati()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pelati()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_legumi()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_legumi()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_pasta()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_pasta()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_riso()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_riso()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_zucchero()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_zucchero()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_latte()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_latte()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_biscotti()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_biscotti()+";");
		fileScritto.print(Double.toString(somme.getPeso_tot_varie()).replace('.', ',')+";");
		fileScritto.print(somme.getScatoli_tot_varie()+";");
		
		fileScritto.print(DAO.getTotaliModB(somme).get(0).toString().replace('.', ',')+";");
		fileScritto.print(DAO.getTotaliModB(somme).get(1).intValue());
		fileScritto.println();
		
		
		
		fileScritto.close();

	 }	
	 
	public void volontari() throws Exception {
			 
			GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
			File mioFile = new File(directorySalvataggio+"/Elenco_Volontari.csv");
			
			FileOutputStream fileCreato = new FileOutputStream(mioFile);
			PrintStream fileScritto = new PrintStream(fileCreato);
			CatalogoVolontario catu= new CatalogoVolontario();
			Vector<Volontario> u=new Vector<Volontario>();
			u=catu.getVolontari();
			fileScritto.println("IDUTENTE;NOME;COGNOME;INDIRIZZO;CAP;CITT�;TELEFONO;DATA DI NASCITA;OCCUPAZIONE;EMAIL");
			for (int i=0;i<u.size();i++)
			{
				fileScritto.print(u.get(i).getIdVolontario()+";");
				fileScritto.print(u.get(i).getNome()+";");
				fileScritto.print(u.get(i).getCognome()+";");
				fileScritto.print(u.get(i).getIndirizzo()+";");
				fileScritto.print(u.get(i).getCap()+";");
				fileScritto.print(u.get(i).getCitta()+";");	
				fileScritto.print(u.get(i).getTelefono()+";");
				fileScritto.print(u.get(i).getDataNascita().toString()+";");
				fileScritto.print(u.get(i).getOccupazione()+";");
				fileScritto.print(u.get(i).getEmail());
				fileScritto.println();
			}
			fileScritto.close();
		}
	
	public void puntiVendita() throws Exception {
			 
			GregorianCalendar d=new GregorianCalendar();
			int anno=d.get(Calendar.YEAR);
			File mioFile = new File(directorySalvataggio+"/Elenco_Punti_Vendita.csv");
			
			FileOutputStream fileCreato = new FileOutputStream(mioFile);
			PrintStream fileScritto = new PrintStream(fileCreato);
			CatalogoPuntoVendita catu= new CatalogoPuntoVendita();
			Vector<PuntoVendita> u=new Vector<PuntoVendita>();
			u=catu.getPuntoVendita();
			fileScritto.println("IDPUNTOVENDITA;GRUPPO;INSEGNA;PROVINCIA;INDIRIZZO;CAP;COMUNE;CITT�;TELEFONO;CELLULARE;ORARIO;NUM_CASSE;EMAIL;REGIONE;AREA;MQ_VENDITA");
			for (int i=0;i<u.size();i++)
			{
				fileScritto.print(u.get(i).getIdPuntoVendita()+";");
				fileScritto.print(u.get(i).getGruppo()+";");
				fileScritto.print(u.get(i).getInsegna()+";");
				fileScritto.print(u.get(i).getProvincia()+";");
				fileScritto.print(u.get(i).getIndirizzo()+";");
				fileScritto.print(u.get(i).getCap()+";");
				fileScritto.print(u.get(i).getComune()+";");
				fileScritto.print(u.get(i).getCitta()+";");	
				fileScritto.print(u.get(i).getTelefono()+";");
				fileScritto.print(u.get(i).getCellulare()+";");
				fileScritto.print(u.get(i).getOrario()+";");
				fileScritto.print(u.get(i).getNumCasse()+";");
				fileScritto.print(u.get(i).getEmail()+";");
				fileScritto.print(u.get(i).getRegione()+";");
				fileScritto.print(u.get(i).getArea()+";");
				fileScritto.print(u.get(i).getMQvendita());
				fileScritto.println();
			}
			fileScritto.close();
		}
		
public void convenzioni(int anno) throws Exception {
		 
		
		//Creo il file excel chiamato output.xls
		WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Convenzioni_"+anno+".xls"));

		//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)
		WritableSheet sheetA = workbook.createSheet("Convenzioni Enti"+anno, 0);
		WritableSheet sheetB = workbook.createSheet("Sedi Legali Enti"+anno, 1);
		WritableSheet sheetC = workbook.createSheet("Rappresentanti Legali Enti"+anno, 2);

		//Creo gli oggetti e richiamo dal database i dati che mi servono
		CatalogoConvenzioni catconv= new CatalogoConvenzioni();
		CatalogoSede catsedi=new CatalogoSede();
		CatalogoEnti catenti=new CatalogoEnti();
		CatalogoPersona catpersone=new CatalogoPersona();
		
		Vector<Convenzione> VC=new Vector<Convenzione>();
		Vector<Ente> VE=new Vector<Ente>();
		Vector<Sede> VS=new Vector<Sede>();
		VC=catconv.getConvenzioniAnno(anno);
		VE=catenti.getEnti();
		VS=catsedi.getSedi();
		
		
		Sede s=new Sede();
		Ente e=new Ente();
		Persona p= new Persona();
		
		
		
		//ORA CREO LE CELLE per il foglio A
		int ax=0;

		Vector<Label> intestazioni=new Vector<Label>();
		
		intestazioni.add(new Label(ax++,0, "ID CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ID ENTE"));
		intestazioni.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO SEDE LEGALE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TIPOLOGIA CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ANNO CONVENZIONE"));
		
		intestazioni.add(new Label(ax++,0, "TOTALE INDIGENTI"));
		intestazioni.add(new Label(ax++,0, "MENSA 0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 19-65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA over 65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA Totale Indigenti"));
		intestazioni.add(new Label(ax++,0, "MENSA Giorni Apertura"));
		intestazioni.add(new Label(ax++,0, "MENSA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 6-18 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "PACCHI 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Media Presenze g"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 0-5"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 6-18"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 19-65"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia over 65"));
		intestazioni.add(new Label(ax++,0, "Cella Frigo"));
		intestazioni.add(new Label(ax++,0, "Colazione"));
		intestazioni.add(new Label(ax++,0, "Pranzo"));
		intestazioni.add(new Label(ax++,0, "Cena"));
		intestazioni.add(new Label(ax++,0, "Centro Accoglienza"));
		intestazioni.add(new Label(ax++,0, "Pasti Freddi"));
		intestazioni.add(new Label(ax++,0, "Consegna Domicilio"));
		intestazioni.add(new Label(ax++,0, "Distribuzione Pacchi"));
		intestazioni.add(new Label(ax++,0, "Casa Famiglia"));
		intestazioni.add(new Label(ax++,0, "Comunit� Anziani"));
		intestazioni.add(new Label(ax++,0, "Comunit� Disabili"));
		intestazioni.add(new Label(ax++,0, "Comunit� Minori"));
		intestazioni.add(new Label(ax++,0, "Comunit� Tossicodipendenti"));
		intestazioni.add(new Label(ax++,0, "Altro"));
		intestazioni.add(new Label(ax++,0, "Data Firma"));
		
		intestazioni.add(new Label(ax++,0, "MENSA 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "PACCHI 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO 0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO over65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA saltuari"));
		intestazioni.add(new Label(ax++,0, "PACCHI saltuari"));
		intestazioni.add(new Label(ax++,0, "EMPORIO saltuari"));
		intestazioni.add(new Label(ax++,0, "STRADA saltuari"));
		intestazioni.add(new Label(ax++,0, "STRADA media Presenze"));
		intestazioni.add(new Label(ax++,0, "STRADA giorni Apertura"));
		intestazioni.add(new Label(ax++,0, "EMPORIO media Presenze"));
		intestazioni.add(new Label(ax++,0, "EMPORIO giorni Apertura"));
		
		intestazioni.add(new Label(ax++,0, "MENSA Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "PACCHI Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "EMPORIO Totale indigenti"));

		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioni.size();i++)
			sheetA.addCell(intestazioni.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		System.out.println(e.getIdEnte());
		
		if(VC.get(i).getIdSedeOperativaEnte()!=0)
			s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
		else
			s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenuto=new Vector<Label>();
		Vector<Number> contenutoint=new Vector<Number>();
		
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
		contenuto.add(new Label(x++, y, e.getNome()));
		contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
		 


		contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()+VC.get(i).getR_totIndigenti()+VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()+VC.get(i).getM_0_5()+VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()+VC.get(i).getM_6_18()+VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()+VC.get(i).getM_19_65()+VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()+VC.get(i).getM_over65()+VC.get(i).getR_over65()));
		System.out.print("x= "+x);
		System.out.print("contenuto= "+contenuto.size());
		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_colazione())
			contenuto.add(new Label(x++, y, "SI"));
		else
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_pranzo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_cena())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_centroAccoglienzaDiurno())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_pastiFreddi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_consegnaDomicilio())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_distribuzionePacchi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isR_casaFamiglia())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaAnziani())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaDisabili())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaMinori())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaTossicodipendenti())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getP_Altro())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_saltuari()));			
		contenutoint.add(new Number(x++, y, VC.get(i).getU_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_giorniApertura()));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_totIndigenti()));
		
		
		
		System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k)); 
		
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
		}
		
		//ORA CREO LE CELLE per il foglio B
		 ax=0;

		Vector<Label> intestazioniB=new Vector<Label>();
		
		intestazioniB.add(new Label(ax++,0, "ID CONVENZIONE"));
		intestazioniB.add(new Label(ax++,0, "ID ENTE"));
		intestazioniB.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioniB.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioniB.add(new Label(ax++,0, "ID SEDE"));
		intestazioniB.add(new Label(ax++,0, "TIPOLOGIA SEDE"));
		intestazioniB.add(new Label(ax++,0, "INDIRIZZO SEDE"));
		intestazioniB.add(new Label(ax++,0, "CAP"));
		intestazioniB.add(new Label(ax++,0, "COMUNE"));
		intestazioniB.add(new Label(ax++,0, "PROVINCIA"));	
		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioniB.size();i++)
			sheetB.addCell(intestazioniB.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		s=catsedi.getSedeLegale(VC.get(i).getIdEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenutoB=new Vector<Label>();
		Vector<Number> contenutointB=new Vector<Number>();
		
		contenutoB.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenutoB.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenutoB.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenutoB.add(new Label(x++, y, e.getNome()));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getIdSede())));
		contenutoB.add(new Label(x++, y, s.getTipologia()));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getCap())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getComune())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getProvincia())));
		
		for (int k=0;k<contenutoB.size();k++)
			sheetB.addCell(contenutoB.get(k)); 
		
		for (int n=0;n<contenutointB.size();n++)
			sheetB.addCell(contenutointB.get(n));
		}
		
		
		
		//ORA CREO LE CELLE per il foglio C
		 ax=0;

		Vector<Label> intestazioniC=new Vector<Label>();
		
		intestazioniC.add(new Label(ax++,0, "ID CONVENZIONE"));	
		intestazioniC.add(new Label(ax++,0, "ID ENTE"));
		intestazioniC.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioniC.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioniC.add(new Label(ax++,0, "ID RAPPRESENTANTE LEGALE"));
		intestazioniC.add(new Label(ax++,0, "NOME"));
		intestazioniC.add(new Label(ax++,0, "COGNOME"));
		intestazioniC.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioniC.add(new Label(ax++,0, "LUOGO DI NASCITA"));
		intestazioniC.add(new Label(ax++,0, "DATA DI NASCITA"));
		intestazioniC.add(new Label(ax++,0, "INDIRIZZO"));
		intestazioniC.add(new Label(ax++,0, "CAP"));
		intestazioniC.add(new Label(ax++,0, "COMUNE"));
		intestazioniC.add(new Label(ax++,0, "PROVINCIA"));
		intestazioniC.add(new Label(ax++,0, "TELEFONO"));
		intestazioniC.add(new Label(ax++,0, "CELLULARE"));
		intestazioniC.add(new Label(ax++,0, "EMAIL"));
		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioniC.size();i++)
			sheetC.addCell(intestazioniC.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenutoC=new Vector<Label>();

		
		contenutoC.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenutoC.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenutoC.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenutoC.add(new Label(x++, y, e.getNome()));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
		for (int k=0;k<contenutoC.size();k++)
			sheetC.addCell(contenutoC.get(k)); 
		

		}
		
		
		 
		//Scrivo effettivamente tutte le celle ed i dati aggiunti
		workbook.write();
		 
		//Chiudo il foglio excel
		workbook.close();
		
		}	
	
	
	public void convenzioni_old(int anno) throws Exception {
		 
		
		//Creo il file excel chiamato output.xls
		WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Convenzioni_"+anno+".xls"));

		//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)
		WritableSheet sheetA = workbook.createSheet("Convenzioni Enti"+anno, 0);
		WritableSheet sheetB = workbook.createSheet("Sedi Legali Enti"+anno, 1);
		WritableSheet sheetC = workbook.createSheet("Rappresentanti Legali Enti"+anno, 2);

		//Creo gli oggetti e richiamo dal database i dati che mi servono
		CatalogoConvenzioni catconv= new CatalogoConvenzioni();
		CatalogoSede catsedi=new CatalogoSede();
		CatalogoEnti catenti=new CatalogoEnti();
		CatalogoPersona catpersone=new CatalogoPersona();
		
		Vector<Convenzione> VC=new Vector<Convenzione>();
		Vector<Ente> VE=new Vector<Ente>();
		Vector<Sede> VS=new Vector<Sede>();
		VC=catconv.getConvenzioniAnno(anno);
		VE=catenti.getEnti();
		VS=catsedi.getSedi();
		
		
		Sede s=new Sede();
		Ente e=new Ente();
		Persona p= new Persona();
		
		
		
		//ORA CREO LE CELLE per il foglio A
		int ax=0;

		Vector<Label> intestazioni=new Vector<Label>();
		
		intestazioni.add(new Label(ax++,0, "ID CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ID ENTE"));
		intestazioni.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO SEDE LEGALE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TIPOLOGIA CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ANNO CONVENZIONE"));
		
		intestazioni.add(new Label(ax++,0, "TOTALE INDIGENTI"));
		intestazioni.add(new Label(ax++,0, "MENSA 0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 19-65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA over 65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA Totale Indigenti"));
		intestazioni.add(new Label(ax++,0, "MENSA Giorni Apertura"));
		intestazioni.add(new Label(ax++,0, "MENSA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 6-18 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "PACCHI 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Media Presenze g"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 0-5"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 6-18"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 19-65"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia over 65"));
		intestazioni.add(new Label(ax++,0, "Cella Frigo"));
		intestazioni.add(new Label(ax++,0, "Colazione"));
		intestazioni.add(new Label(ax++,0, "Pranzo"));
		intestazioni.add(new Label(ax++,0, "Cena"));
		intestazioni.add(new Label(ax++,0, "Centro Accoglienza"));
		intestazioni.add(new Label(ax++,0, "Pasti Freddi"));
		intestazioni.add(new Label(ax++,0, "Consegna Domicilio"));
		intestazioni.add(new Label(ax++,0, "Distribuzione Pacchi"));
		intestazioni.add(new Label(ax++,0, "Casa Famiglia"));
		intestazioni.add(new Label(ax++,0, "Comunit� Anziani"));
		intestazioni.add(new Label(ax++,0, "Comunit� Disabili"));
		intestazioni.add(new Label(ax++,0, "Comunit� Minori"));
		intestazioni.add(new Label(ax++,0, "Comunit� Tossicodipendenti"));
		intestazioni.add(new Label(ax++,0, "Altro"));
		intestazioni.add(new Label(ax++,0, "Data Firma"));
		
		intestazioni.add(new Label(ax++,0, "MENSA 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "PACCHI 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO 6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO 0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "EMPORIO over65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA saltuari"));
		intestazioni.add(new Label(ax++,0, "PACCHI saltuari"));
		intestazioni.add(new Label(ax++,0, "EMPORIO saltuari"));
		intestazioni.add(new Label(ax++,0, "STRADA saltuari"));
		intestazioni.add(new Label(ax++,0, "STRADA media Presenze"));
		intestazioni.add(new Label(ax++,0, "STRADA giorni Apertura"));
		intestazioni.add(new Label(ax++,0, "EMPORIO media Presenze"));
		intestazioni.add(new Label(ax++,0, "EMPORIO giorni Apertura"));
		
		intestazioni.add(new Label(ax++,0, "MENSA Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "PACCHI Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "EMPORIO Totale indigenti"));

		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioni.size();i++)
			sheetA.addCell(intestazioni.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		System.out.println(e.getIdEnte());
		
		if(VC.get(i).getIdSedeOperativaEnte()!=0)
			s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
		else
			s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenuto=new Vector<Label>();
		Vector<Number> contenutoint=new Vector<Number>();
		
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
		contenuto.add(new Label(x++, y, e.getNome()));
		contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
		 


		contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()+VC.get(i).getR_totIndigenti()+VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()+VC.get(i).getM_0_5()+VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()+VC.get(i).getM_6_18()+VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()+VC.get(i).getM_19_65()+VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()+VC.get(i).getM_over65()+VC.get(i).getR_over65()));
		System.out.print("x= "+x);
		System.out.print("contenuto= "+contenuto.size());
		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_colazione())
			contenuto.add(new Label(x++, y, "SI"));
		else
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_pranzo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_cena())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_centroAccoglienzaDiurno())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_pastiFreddi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_consegnaDomicilio())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_distribuzionePacchi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isR_casaFamiglia())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaAnziani())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaDisabili())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaMinori())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaTossicodipendenti())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getP_Altro())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_saltuari()));			
		contenutoint.add(new Number(x++, y, VC.get(i).getU_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_giorniApertura()));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_totIndigenti()));
		
		
		
		System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k)); 
		
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
		}
		
		//ORA CREO LE CELLE per il foglio B
		 ax=0;

		Vector<Label> intestazioniB=new Vector<Label>();
		
		intestazioniB.add(new Label(ax++,0, "ID CONVENZIONE"));
		intestazioniB.add(new Label(ax++,0, "ID ENTE"));
		intestazioniB.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioniB.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioniB.add(new Label(ax++,0, "ID SEDE"));
		intestazioniB.add(new Label(ax++,0, "TIPOLOGIA SEDE"));
		intestazioniB.add(new Label(ax++,0, "INDIRIZZO SEDE"));
		intestazioniB.add(new Label(ax++,0, "CAP"));
		intestazioniB.add(new Label(ax++,0, "COMUNE"));
		intestazioniB.add(new Label(ax++,0, "PROVINCIA"));	
		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioniB.size();i++)
			sheetB.addCell(intestazioniB.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		s=catsedi.getSedeLegale(VC.get(i).getIdEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenutoB=new Vector<Label>();
		Vector<Number> contenutointB=new Vector<Number>();
		
		contenutoB.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenutoB.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenutoB.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenutoB.add(new Label(x++, y, e.getNome()));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getIdSede())));
		contenutoB.add(new Label(x++, y, s.getTipologia()));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getCap())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getComune())));
		contenutoB.add(new Label(x++, y, String.valueOf(s.getProvincia())));
		
		for (int k=0;k<contenutoB.size();k++)
			sheetB.addCell(contenutoB.get(k)); 
		
		for (int n=0;n<contenutointB.size();n++)
			sheetB.addCell(contenutointB.get(n));
		}
		
		
		
		//ORA CREO LE CELLE per il foglio C
		 ax=0;

		Vector<Label> intestazioniC=new Vector<Label>();
		
		intestazioniC.add(new Label(ax++,0, "ID CONVENZIONE"));	
		intestazioniC.add(new Label(ax++,0, "ID ENTE"));
		intestazioniC.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioniC.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioniC.add(new Label(ax++,0, "ID RAPPRESENTANTE LEGALE"));
		intestazioniC.add(new Label(ax++,0, "NOME"));
		intestazioniC.add(new Label(ax++,0, "COGNOME"));
		intestazioniC.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioniC.add(new Label(ax++,0, "LUOGO DI NASCITA"));
		intestazioniC.add(new Label(ax++,0, "DATA DI NASCITA"));
		intestazioniC.add(new Label(ax++,0, "INDIRIZZO"));
		intestazioniC.add(new Label(ax++,0, "CAP"));
		intestazioniC.add(new Label(ax++,0, "COMUNE"));
		intestazioniC.add(new Label(ax++,0, "PROVINCIA"));
		intestazioniC.add(new Label(ax++,0, "TELEFONO"));
		intestazioniC.add(new Label(ax++,0, "CELLULARE"));
		intestazioniC.add(new Label(ax++,0, "EMAIL"));
		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioniC.size();i++)
			sheetC.addCell(intestazioniC.get(i));
		
		//popolo le celle del foglio
		for(int i=0;i<VC.size();i++)
		{
		e=catenti.getEnte(VC.get(i).getIdEnte());
		p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());

		int x=0;	//posizione orizzontale
		int y=1+i; 	//posizione verticale
		
		Vector<Label> contenutoC=new Vector<Label>();

		
		contenutoC.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
		contenutoC.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenutoC.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenutoC.add(new Label(x++, y, e.getNome()));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenutoC.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
		for (int k=0;k<contenutoC.size();k++)
			sheetC.addCell(contenutoC.get(k)); 
		

		}
		
		
		 
		//Scrivo effettivamente tutte le celle ed i dati aggiunti
		workbook.write();
		 
		//Chiudo il foglio excel
		workbook.close();
		
		}	
	
	
	public void enti() throws Exception {
			 
			
			//Creo il file excel chiamato output.xls
			WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Enti.xls"));

			//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)

			WritableSheet sheetA = workbook.createSheet("Enti Sedi Legali", 0);


			//Creo gli oggetti e richiamo dal database i dati che mi servono
			
			CatalogoSede catsedi=new CatalogoSede();
			CatalogoEnti catenti=new CatalogoEnti();
			CatalogoPersona catpersone=new CatalogoPersona();
			

			Vector<Ente> VE=new Vector<Ente>();
			Vector<Sede> VS=new Vector<Sede>();

			VE=catenti.getEnti();
			VS=catsedi.getSedi();
			
			
			Sede s=new Sede();
			Ente e=new Ente();
			Persona p= new Persona();
			
			
			

			//ORA CREO LE CELLE per il foglio A
			int ax=0;

			Vector<Label> intestazioniA=new Vector<Label>();
			

			intestazioniA.add(new Label(ax++,0, "ID ENTE "));
			intestazioniA.add(new Label(ax++,0, "CODICE STRUTTURA"));
			intestazioniA.add(new Label(ax++,0, "CODICE SAP"));
			intestazioniA.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
			intestazioniA.add(new Label(ax++,0, "CODICE FISCALE"));
			intestazioniA.add(new Label(ax++,0, "ID SEDE"));
			intestazioniA.add(new Label(ax++,0, "TIPOLOGIA SEDE"));
			intestazioniA.add(new Label(ax++,0, "INDIRIZZO SEDE"));
			intestazioniA.add(new Label(ax++,0, "CAP"));
			intestazioniA.add(new Label(ax++,0, "COMUNE"));
			intestazioniA.add(new Label(ax++,0, "PROVINCIA"));	
			
			//scrivo le intestazioni delle celle sul foglio
			for (int i=0;i<intestazioniA.size();i++)
				sheetA.addCell(intestazioniA.get(i));
			
			//popolo le celle del foglio
			for(int i=0;i<VE.size();i++)
			{
			e=catenti.getEnte(VE.get(i).getIdEnte());
			s=catsedi.getSedeLegale(VE.get(i).getIdEnte());

			int x=0;	//posizione orizzontale
			int y=1+i; 	//posizione verticale
			
			Vector<Label> contenutoA=new Vector<Label>();
			Vector<Number> contenutointA=new Vector<Number>();
			

			contenutoA.add(new Label(x++, y, String.valueOf(VE.get(i).getIdEnte())));
			contenutoA.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenutoA.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
			contenutoA.add(new Label(x++, y, e.getNome()));
			contenutoA.add(new Label(x++, y, e.getPartitaIva()));
			if(s!=null){
			contenutoA.add(new Label(x++, y, String.valueOf(s.getIdSede())));
			contenutoA.add(new Label(x++, y, s.getTipologia()));
			contenutoA.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenutoA.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenutoA.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenutoA.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			}
			for (int k=0;k<contenutoA.size();k++)
				sheetA.addCell(contenutoA.get(k)); 
			
			for (int n=0;n<contenutointA.size();n++)
				sheetA.addCell(contenutointA.get(n));
			}
			
			

			
			 
			//Scrivo effettivamente tutte le celle ed i dati aggiunti
			workbook.write();
			 
			//Chiudo il foglio excel
			workbook.close();
			
			}
		
	
	public void convenzioniAGEA(int anno) throws Exception {
			 
			
			//Creo il file excel chiamato output.xls
			WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Convenzioni_AGEA_"+anno+".xls"));

			//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)
			WritableSheet sheetA = workbook.createSheet("Convenzioni Enti"+anno, 0);


			//Creo gli oggetti e richiamo dal database i dati che mi servono
			CatalogoConvenzioni catconv= new CatalogoConvenzioni();
			CatalogoSede catsedi=new CatalogoSede();
			CatalogoEnti catenti=new CatalogoEnti();
			CatalogoPersona catpersone=new CatalogoPersona();
			
			Vector<Convenzione> VC=new Vector<Convenzione>();
			Vector<Ente> VE=new Vector<Ente>();
			Vector<Sede> VS=new Vector<Sede>();
			VC=catconv.getConvenzioniAnno(anno);
			VE=catenti.getEnti();
			VS=catsedi.getSedi();
			
			
			Sede s=new Sede();
			Ente e=new Ente();
			Persona p= new Persona();
			
			
			
			//ORA CREO LE CELLE per il foglio A
			int ax=0;

			Vector<Label> intestazioni=new Vector<Label>();
			
			intestazioni.add(new Label(ax++,0, "ID CONVENZIONE"));
			intestazioni.add(new Label(ax++,0, "ID ENTE"));
			intestazioni.add(new Label(ax++,0, "CODICE STRUTTURA"));
			intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
			intestazioni.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
			intestazioni.add(new Label(ax++,0, "INDIRIZZO SEDE LEGALE STRUTTURA"));
			intestazioni.add(new Label(ax++,0, "CAP"));
			intestazioni.add(new Label(ax++,0, "COMUNE"));
			intestazioni.add(new Label(ax++,0, "PROVINCIA"));
			intestazioni.add(new Label(ax++,0, "TIPOLOGIA CONVENZIONE"));
			intestazioni.add(new Label(ax++,0, "ANNO CONVENZIONE"));
			
			intestazioni.add(new Label(ax++,0, "MENSA Totale Indigenti"));
			intestazioni.add(new Label(ax++,0, "MENSA 0-5 Anni"));
			intestazioni.add(new Label(ax++,0, "MENSA 6-65 Anni"));
			intestazioni.add(new Label(ax++,0, "MENSA over 65 Anni"));
			intestazioni.add(new Label(ax++,0, "MENSA saltuari"));
			intestazioni.add(new Label(ax++,0, "MENSA Media Presenze g"));
			intestazioni.add(new Label(ax++,0, "MENSA Giorni Apertura"));
	
			
			intestazioni.add(new Label(ax++,0, "PACCHI Totale Indigenti"));	
			intestazioni.add(new Label(ax++,0, "PACCHI 0-5 Anni"));			
			intestazioni.add(new Label(ax++,0, "PACCHI 6-65 Anni"));			
			intestazioni.add(new Label(ax++,0, "PACCHI over 65 Anni"));
			intestazioni.add(new Label(ax++,0, "PACCHI saltuari"));
			intestazioni.add(new Label(ax++,0, "PACCHI Media Presenze g"));		
			intestazioni.add(new Label(ax++,0, "PACCHI Giorni Apertura"));			

			intestazioni.add(new Label(ax++,0, "EMPORIO Totale indigenti"));
			intestazioni.add(new Label(ax++,0, "EMPORIO 0-5 Anni"));
			intestazioni.add(new Label(ax++,0, "EMPORIO 6-65 Anni"));
			intestazioni.add(new Label(ax++,0, "EMPORIO over65 Anni"));
			intestazioni.add(new Label(ax++,0, "EMPORIO saltuari"));
			intestazioni.add(new Label(ax++,0, "EMPORIO Media Presenze"));
			intestazioni.add(new Label(ax++,0, "EMPORIO Giorni Apertura"));
			
			intestazioni.add(new Label(ax++,0, "STRADA Totale indigenti"));
			intestazioni.add(new Label(ax++,0, "STRADA saltuari"));
			intestazioni.add(new Label(ax++,0, "STRADA media Presenze"));
			intestazioni.add(new Label(ax++,0, "STRADA giorni Apertura"));

			intestazioni.add(new Label(ax++,0, "Cella Frigo"));
			intestazioni.add(new Label(ax++,0, "Data Firma"));
			intestazioni.add(new Label(ax++,0, "ID RAPPRESENTANTE LEGALE"));
			intestazioni.add(new Label(ax++,0, "NOME"));
			intestazioni.add(new Label(ax++,0, "COGNOME"));
			intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
			intestazioni.add(new Label(ax++,0, "LUOGO DI NASCITA"));
			intestazioni.add(new Label(ax++,0, "DATA DI NASCITA"));
			intestazioni.add(new Label(ax++,0, "INDIRIZZO"));
			intestazioni.add(new Label(ax++,0, "CAP"));
			intestazioni.add(new Label(ax++,0, "COMUNE"));
			intestazioni.add(new Label(ax++,0, "PROVINCIA"));
			intestazioni.add(new Label(ax++,0, "TELEFONO"));
			intestazioni.add(new Label(ax++,0, "CELLULARE"));
			intestazioni.add(new Label(ax++,0, "EMAIL"));
			

		

			
			//scrivo le intestazioni delle celle sul foglio
			for (int i=0;i<intestazioni.size();i++)
				sheetA.addCell(intestazioni.get(i));
			
			//popolo le celle del foglio
			int correzione_posizione=0;
			for(int i=0;i<VC.size();i++)
			{
		
				if(VC.get(i).tipologiaConvenzione().equals("agea")){
					
			e=catenti.getEnte(VC.get(i).getIdEnte());

			p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
			System.out.println(e.getIdEnte()+" M "+VC.get(i).getM_TotaleAssistitiContinuativi()+" P "+VC.get(i).getP_TotaleAssistitiContinuativi()+" E "+VC.get(i).getE_TotaleAssistitiContinuativi()+" U "+VC.get(i).getU_totIndigenti()+" ");
			
			if(VC.get(i).getIdSedeOperativaEnte()!=0)
				s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
			else
				s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

			int x=0;	//posizione orizzontale
			int y=1+i-correzione_posizione; 	//posizione verticale
			
			Vector<Label> contenuto=new Vector<Label>();
			Vector<Number> contenutoint=new Vector<Number>();
			
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
			contenuto.add(new Label(x++, y, e.getNome()));
			contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
			 
			contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
			
			
			contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));			
			contenutoint.add(new Number(x++, y, VC.get(i).getM_0_5()));
			contenutoint.add(new Number(x++, y, VC.get(i).getM_6_65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getM_over65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getM_saltuari()));
			contenutoint.add(new Number(x++, y, VC.get(i).getM_mediaPresenze()));
			contenutoint.add(new Number(x++, y, VC.get(i).getM_giorniApertura()));

			contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_6_65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_saltuari()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_mediaPresenze()));
			contenutoint.add(new Number(x++, y, VC.get(i).getP_giorniApertura()));

			contenutoint.add(new Number(x++, y, VC.get(i).getE_totIndigenti()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_0_5()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_6_65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_over65()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_saltuari()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_mediaPresenze()));
			contenutoint.add(new Number(x++, y, VC.get(i).getE_giorniApertura()));
			
			contenutoint.add(new Number(x++, y, VC.get(i).getU_totIndigenti()));
			contenutoint.add(new Number(x++, y, VC.get(i).getU_saltuari()));
			contenutoint.add(new Number(x++, y, VC.get(i).getU_mediaPresenze()));
			contenutoint.add(new Number(x++, y, VC.get(i).getU_giorniApertura()));
			
			System.out.print("x= "+x);
			System.out.print("contenuto= "+contenuto.size());
			System.out.print("contenutoint= "+contenutoint.size());
			
			if (VC.get(i).isCellaFrigo())
				contenuto.add(new Label(x++, y, "SI"));
			else 
				contenuto.add(new Label(x++, y, "NO"));


			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));	
			
			

			contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
			contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
			
			
			System.out.print("x= "+x);
		    		
			

			
			for (int k=0;k<contenuto.size();k++)
				sheetA.addCell(contenuto.get(k)); 
			
			for (int n=0;n<contenutoint.size();n++)
				sheetA.addCell(contenutoint.get(n));
			}
				else
					correzione_posizione++;
			}
			 
			//Scrivo effettivamente tutte le celle ed i dati aggiunti
			workbook.write();
			 
			//Chiudo il foglio excel
			workbook.close();
			
			}
	
	
	public void convenzioniAGEAxSIAN(int anno) throws Exception {
		 
		
		//Creo il file excel chiamato output.xls
		WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Convenzioni_AGEA_SIAN_"+anno+".xls"));

		//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)
		WritableSheet sheetA = workbook.createSheet("Convenzioni Enti"+anno, 0);

		//Creo gli oggetti e richiamo dal database i dati che mi servono
		CatalogoConvenzioni catconv= new CatalogoConvenzioni();
		CatalogoSede catsedi=new CatalogoSede();
		CatalogoEnti catenti=new CatalogoEnti();
		CatalogoPersona catpersone=new CatalogoPersona();
		
		Vector<Convenzione> VC=new Vector<Convenzione>();
		Vector<Ente> VE=new Vector<Ente>();
		Vector<Sede> VS=new Vector<Sede>();
		VC=catconv.getConvenzioniAGEAAnno(anno);
		VE=catenti.getEnti();
		VS=catsedi.getSedi();
		
		
		Sede s=new Sede();
		Ente e=new Ente();
		Persona p= new Persona();
		
		
		
		//ORA CREO LE CELLE per il foglio A
		int ax=0;

		Vector<Label> intestazioni=new Vector<Label>();
		
		intestazioni.add(new Label(ax++,0, "ID CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ID ENTE"));
		intestazioni.add(new Label(ax++,0, "CODICE SAP"));
		intestazioni.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO SEDE LEGALE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TIPOLOGIA CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ANNO CONVENZIONE"));
		
		intestazioni.add(new Label(ax++,0, "ATTIVITA"));
		
		intestazioni.add(new Label(ax++,0, "Totale Indigenti"));
		intestazioni.add(new Label(ax++,0, "0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "6-65 Anni"));
		intestazioni.add(new Label(ax++,0, "over 65 Anni"));
		intestazioni.add(new Label(ax++,0, "saltuari"));
		intestazioni.add(new Label(ax++,0, "Media Presenze g"));
		intestazioni.add(new Label(ax++,0, "Giorni Apertura"));

		intestazioni.add(new Label(ax++,0, "Cella Frigo"));
		intestazioni.add(new Label(ax++,0, "Data Firma"));
		intestazioni.add(new Label(ax++,0, "ID RAPPRESENTANTE LEGALE"));
		intestazioni.add(new Label(ax++,0, "NOME"));
		intestazioni.add(new Label(ax++,0, "COGNOME"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "LUOGO DI NASCITA"));
		intestazioni.add(new Label(ax++,0, "DATA DI NASCITA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TELEFONO"));
		intestazioni.add(new Label(ax++,0, "CELLULARE"));
		intestazioni.add(new Label(ax++,0, "EMAIL"));
		

	

		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioni.size();i++)
			sheetA.addCell(intestazioni.get(i));
		
		//popolo le celle del foglio
		int correzione_posizione=1;
		int y=0;
		
		Vector<Label> contenuto=new Vector<Label>();
		Vector<Number> contenutoint=new Vector<Number>();
		
		for(int i=0;i<VC.size();i++)
		{
			
			System.out.println("correzione_posizione= "+correzione_posizione);
			System.out.println("Convenzione "+i+1+"/"+(VC.size())+"convenzione n. "+VC.get(i).getIdConvenzione());
			System.out.println(e.getIdEnte()+" M "+VC.get(i).getM_TotaleAssistitiContinuativi()+" P "+VC.get(i).getP_TotaleAssistitiContinuativi()+" E "+VC.get(i).getE_TotaleAssistitiContinuativi()+" U "+VC.get(i).getU_totIndigenti()+" ");
			
			if(VC.get(i).tipologiaConvenzione().equals("agea")){
				System.out.print("AGEA");
				
		
		
		//inserisco la riga relatica alla convenzione mensa
		if(VC.get(i).getM_totIndigenti()>0){
			contenutoint.clear();
			contenuto.clear();
			e=catenti.getEnte(VC.get(i).getIdEnte());
			System.out.print(" M "+VC.get(i).getIdEnte()+" "+e.getNome());
			p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
			System.out.println(e.getIdEnte());
			
			if(VC.get(i).getIdSedeOperativaEnte()!=0)
				s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
			else
				s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

			int x=0;	//posizione orizzontale
			y=correzione_posizione; 	//posizione verticale
			
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
			contenuto.add(new Label(x++, y, e.getNome()));
			contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
			 
			contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));	
			System.out.println(contenuto.size());
			contenuto.add(new Label(x++, y,"M"));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));			
		contenutoint.add(new Number(x++, y, VC.get(i).getM_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_giorniApertura()));

		
//		System.out.print("x= "+x);
//		System.out.print("contenuto= "+contenuto.size());
//		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));


		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));	
		
		

		contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
	//	System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k));
			
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
			


		correzione_posizione++; 

		}
		//inserisco la riga relatica alla convenzione pacchi
		if(VC.get(i).getP_totIndigenti()>0){
			contenuto.clear();
			contenutoint.clear();
			
			e=catenti.getEnte(VC.get(i).getIdEnte());
			System.out.print(" P "+VC.get(i).getIdEnte()+" "+e.getNome());
			p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
			//System.out.println(e.getIdEnte());
			
			if(VC.get(i).getIdSedeOperativaEnte()!=0)
				s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
			else
				s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

			int x=0;	//posizione orizzontale
			y=correzione_posizione; 	//posizione verticale
			
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
			contenuto.add(new Label(x++, y, e.getNome()));
			contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
			 
			contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
			
			contenuto.add(new Label(x++, y,"P"));
		


		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_giorniApertura()));

		
//		System.out.print("x= "+x);
//		System.out.print("contenuto= "+contenuto.size());
//		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));


		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));	
		
		

		contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
//		System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k));
			
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
			

		correzione_posizione++;
		
		
		}
		
		//inserisco la riga relatica alla convenzione EMPORIO
		if(VC.get(i).getE_totIndigenti()>0){
			contenutoint.clear();
			contenuto.clear();
			e=catenti.getEnte(VC.get(i).getIdEnte());
			System.out.print(" E "+VC.get(i).getIdEnte()+" "+e.getNome());
			p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
			System.out.println(e.getIdEnte());
			
			if(VC.get(i).getIdSedeOperativaEnte()!=0)
				s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
			else
				s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

			int x=0;	//posizione orizzontale
			y=correzione_posizione; 	//posizione verticale
			
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));

			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
			contenuto.add(new Label(x++, y, e.getNome()));
			contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
			 
			contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));	
			contenuto.add(new Label(x++, y,"E"));


		contenutoint.add(new Number(x++, y, VC.get(i).getE_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_6_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getE_giorniApertura()));

		
//		System.out.print("x= "+x);
//		System.out.print("contenuto= "+contenuto.size());
//		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));


		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));	
		
		

		contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
//		System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k));
			
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
			

		 
		
		correzione_posizione++;

		}
		
		//inserisco la riga relatica alla convenzione strada
		if(VC.get(i).getU_totIndigenti()>0){
			contenutoint.clear();
			contenuto.clear();
			e=catenti.getEnte(VC.get(i).getIdEnte());
			System.out.print(" U "+VC.get(i).getIdEnte()+" "+e.getNome());
			p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
		//	System.out.println(e.getIdEnte());
			
			if(VC.get(i).getIdSedeOperativaEnte()!=0)
				s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
			else
				s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

			int x=0;	//posizione orizzontale
			y=correzione_posizione; 	//posizione verticale
			
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
			contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
			contenuto.add(new Label(x++, y, e.getNome()));
			contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
			contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
			contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
			 
			contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
			
			contenuto.add(new Label(x++, y,"U"));

		
		contenutoint.add(new Number(x++, y, VC.get(i).getU_totIndigenti()));
		contenuto.add(new Label(x++, y, ""));
		contenuto.add(new Label(x++, y, ""));
		contenuto.add(new Label(x++, y, ""));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_saltuari()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getU_giorniApertura()));
		
//		System.out.print("x= "+x);
//		System.out.print("contenuto= "+contenuto.size());
//		System.out.print("contenutoint= "+contenutoint.size());
//		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));


		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));	
		
		

		contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		
	//	System.out.print("x= "+x);
	    		
		

		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k));
			
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
			
		
		correzione_posizione++;
		
		}
		
			}
			
		}

		//Scrivo effettivamente tutte le celle ed i dati aggiunti
		workbook.write();
		//Chiudo il foglio excel
		workbook.close();
		
		}
	
	
public void convenzioniBANCO(int anno) throws Exception {
		 
		
		//Creo il file excel chiamato output.xls
		WritableWorkbook workbook = Workbook.createWorkbook(new File(directorySalvataggio+"/Elenco_Convenzioni_BANCO_"+anno+".xls"));

		//Creo il foglio, che chiamo �primo foglio� nel quale scrivere le celle ed il loro contenuto (il primo foglio ha indice 0)
		WritableSheet sheetA = workbook.createSheet("Convenzioni Enti"+anno, 0);

		//Creo gli oggetti e richiamo dal database i dati che mi servono
		CatalogoConvenzioni catconv= new CatalogoConvenzioni();
		CatalogoSede catsedi=new CatalogoSede();
		CatalogoEnti catenti=new CatalogoEnti();
		CatalogoPersona catpersone=new CatalogoPersona();
		
		Vector<Convenzione> VC=new Vector<Convenzione>();
		Vector<Ente> VE=new Vector<Ente>();
		Vector<Sede> VS=new Vector<Sede>();
		VC=catconv.getConvenzioniBANCOAnno(anno);
		VE=catenti.getEnti();
		VS=catsedi.getSedi();
		
		
		Sede s=new Sede();
		Ente e=new Ente();
		Persona p= new Persona();
		
		
		
		//ORA CREO LE CELLE per il foglio A
		int ax=0;

		Vector<Label> intestazioni=new Vector<Label>();
		
		
		
//		intestazioni.add(new Label(ax++,0, "ID CONVENZIONE"));
//		intestazioni.add(new Label(ax++,0, "ID ENTE"));
		intestazioni.add(new Label(ax++,0, "CODICE SAP"));
		intestazioni.add(new Label(ax++,0, "CODICE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "DENOMINAZIONE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO SEDE LEGALE STRUTTURA"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TIPOLOGIA CONVENZIONE"));
		intestazioni.add(new Label(ax++,0, "ANNO CONVENZIONE"));
		
		intestazioni.add(new Label(ax++,0, "TOTALE INDIGENTI"));
		intestazioni.add(new Label(ax++,0, "MENSA 0-5 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA 19-65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA over 65 Anni"));
		intestazioni.add(new Label(ax++,0, "MENSA Totale Indigenti"));
		intestazioni.add(new Label(ax++,0, "MENSA Giorni Apertura"));
		intestazioni.add(new Label(ax++,0, "MENSA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 6-18 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "RESIDENZA Media Presenze g"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 0-5 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI 6-18 Anni"));
		intestazioni.add(new Label(ax++,0, "PACCHI 19-65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI over 65 Anni"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Totale Indigenti"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Giorni Apertura"));			
		intestazioni.add(new Label(ax++,0, "PACCHI Media Presenze g"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 0-5"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 6-18"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia 19-65"));
		intestazioni.add(new Label(ax++,0, "Totale indigenti fascia over 65"));
		intestazioni.add(new Label(ax++,0, "Cella Frigo"));
		intestazioni.add(new Label(ax++,0, "Colazione"));
		intestazioni.add(new Label(ax++,0, "Pranzo"));
		intestazioni.add(new Label(ax++,0, "Cena"));
		intestazioni.add(new Label(ax++,0, "Centro Accoglienza"));
		intestazioni.add(new Label(ax++,0, "Pasti Freddi"));
		intestazioni.add(new Label(ax++,0, "Consegna Domicilio"));
		intestazioni.add(new Label(ax++,0, "Distribuzione Pacchi"));
		intestazioni.add(new Label(ax++,0, "Casa Famiglia"));
		intestazioni.add(new Label(ax++,0, "Comunit� Anziani"));
		intestazioni.add(new Label(ax++,0, "Comunit� Disabili"));
		intestazioni.add(new Label(ax++,0, "Comunit� Minori"));
		intestazioni.add(new Label(ax++,0, "Comunit� Tossicodipendenti"));
		intestazioni.add(new Label(ax++,0, "Altro"));
		intestazioni.add(new Label(ax++,0, "Data Firma"));
		
//		intestazioni.add(new Label(ax++,0, "MENSA 6-65 Anni"));
//		intestazioni.add(new Label(ax++,0, "PACCHI 6-65 Anni"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO 6-65 Anni"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO 0-5 Anni"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO over65 Anni"));
//		intestazioni.add(new Label(ax++,0, "MENSA saltuari"));
//		intestazioni.add(new Label(ax++,0, "PACCHI saltuari"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO saltuari"));
//		intestazioni.add(new Label(ax++,0, "STRADA saltuari"));
//		intestazioni.add(new Label(ax++,0, "STRADA media Presenze"));
//		intestazioni.add(new Label(ax++,0, "STRADA giorni Apertura"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO media Presenze"));
//		intestazioni.add(new Label(ax++,0, "EMPORIO giorni Apertura"));
		
		intestazioni.add(new Label(ax++,0, "MENSA Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "RESIDENZA Totale indigenti"));
		intestazioni.add(new Label(ax++,0, "PACCHI  Totale indigenti"));
				
		
//		intestazioni.add(new Label(ax++,0, "ID RAPPRESENTANTE LEGALE"));
		intestazioni.add(new Label(ax++,0, "NOME"));
		intestazioni.add(new Label(ax++,0, "COGNOME"));
		intestazioni.add(new Label(ax++,0, "CODICE FISCALE"));
		intestazioni.add(new Label(ax++,0, "LUOGO DI NASCITA"));
		intestazioni.add(new Label(ax++,0, "DATA DI NASCITA"));
		intestazioni.add(new Label(ax++,0, "INDIRIZZO"));
		intestazioni.add(new Label(ax++,0, "CAP"));
		intestazioni.add(new Label(ax++,0, "COMUNE"));
		intestazioni.add(new Label(ax++,0, "PROVINCIA"));
		intestazioni.add(new Label(ax++,0, "TELEFONO"));
		intestazioni.add(new Label(ax++,0, "CELLULARE"));
		intestazioni.add(new Label(ax++,0, "EMAIL"));
		

	

		
		//scrivo le intestazioni delle celle sul foglio
		for (int i=0;i<intestazioni.size();i++)
			sheetA.addCell(intestazioni.get(i));
		
		//popolo le celle del foglio
		
		int y=0;
		
		Vector<Label> contenuto=new Vector<Label>();
		Vector<Number> contenutoint=new Vector<Number>();
		
		for(int i=0;i<VC.size();i++)
		{
			

			System.out.println("Convenzione "+i+1+"/"+(VC.size())+"convenzione n. "+VC.get(i).getIdConvenzione());
			System.out.println(e.getIdEnte()+" M "+VC.get(i).getM_TotaleAssistitiContinuativi()+" P "+VC.get(i).getP_TotaleAssistitiContinuativi()+" E "+VC.get(i).getE_TotaleAssistitiContinuativi()+" U "+VC.get(i).getU_totIndigenti()+" ");
			
	
		e=catenti.getEnte(VC.get(i).getIdEnte());
		p=catpersone.getPersona(VC.get(i).getIdRappLegaleEnte());
		
		if(VC.get(i).getIdSedeOperativaEnte()!=0)
			s=catsedi.getSede(VC.get(i).getIdSedeOperativaEnte());
		else
			s=catsedi.getSede(VC.get(i).getIdSedeLegaleEnte());

		int x=0;	//posizione orizzontale
			y=1+i; 	//posizione verticale
		

		
//		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdConvenzione())));
//		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getIdEnte())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceSap())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getCodiceStruttura())));
		contenuto.add(new Label(x++, y, String.valueOf(e.getPartitaIva())));
		contenuto.add(new Label(x++, y, e.getNome()));
		contenuto.add(new Label(x++, y, String.valueOf(s.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(s.getProvincia())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).tipologiaConvenzione())));
		 
		contenutoint.add(new Number(x++, y, VC.get(i).getAnno()));
		
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()+VC.get(i).getR_totIndigenti()+VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getM_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_giorniApertura()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_mediaPresenze()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_0_5()+VC.get(i).getM_0_5()+VC.get(i).getR_0_5()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_18()+VC.get(i).getM_6_18()+VC.get(i).getR_6_18()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_19_65()+VC.get(i).getM_19_65()+VC.get(i).getR_19_65()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_over65()+VC.get(i).getM_over65()+VC.get(i).getR_over65()));
		System.out.print("x= "+x);
		System.out.print("contenuto= "+contenuto.size());
		System.out.print("contenutoint= "+contenutoint.size());
		
		if (VC.get(i).isCellaFrigo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_colazione())
			contenuto.add(new Label(x++, y, "SI"));
		else
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_pranzo())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isM_cena())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_centroAccoglienzaDiurno())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isM_pastiFreddi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_consegnaDomicilio())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isP_distribuzionePacchi())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));
		
		if (VC.get(i).isR_casaFamiglia())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaAnziani())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaDisabili())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaMinori())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		if (VC.get(i).isR_comunitaTossicodipendenti())
			contenuto.add(new Label(x++, y, "SI"));
		else 
			contenuto.add(new Label(x++, y, "NO"));

		
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getP_Altro())));
		contenuto.add(new Label(x++, y, String.valueOf(VC.get(i).getDataFirma())));
		
//		contenutoint.add(new Number(x++, y, VC.get(i).getM_6_65()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getP_6_65()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_6_65()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_0_5()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_over65()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getM_saltuari()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getP_saltuari()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_saltuari()));			
//		contenutoint.add(new Number(x++, y, VC.get(i).getU_saltuari()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getU_mediaPresenze()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getU_giorniApertura()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_mediaPresenze()));
//		contenutoint.add(new Number(x++, y, VC.get(i).getE_giorniApertura()));
		
		contenutoint.add(new Number(x++, y, VC.get(i).getM_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getR_totIndigenti()));
		contenutoint.add(new Number(x++, y, VC.get(i).getP_totIndigenti()));
		
		
		
//		contenuto.add(new Label(x++, y, String.valueOf(p.getIdPersona())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getNome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCognome())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCF())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getLuogoNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getDataNascita())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getIndirizzo())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCap())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getComune())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCitta())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getTelefono())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getCellulare())));
		contenuto.add(new Label(x++, y, String.valueOf(p.getEmail())));
		
		System.out.print("x= "+x);
	    		
		

		

		}
		
		for (int k=0;k<contenuto.size();k++)
			sheetA.addCell(contenuto.get(k)); 
		
		for (int n=0;n<contenutoint.size();n++)
			sheetA.addCell(contenutoint.get(n));
			

			
		

		//Scrivo effettivamente tutte le celle ed i dati aggiunti
		workbook.write();
		//Chiudo il foglio excel
		workbook.close();
		
		}
	
}


	 