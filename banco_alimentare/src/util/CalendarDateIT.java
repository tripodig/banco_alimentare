//Peppet
package util;
import java.sql.Date;


import java.util.GregorianCalendar;


public class CalendarDateIT extends GregorianCalendar{


	public CalendarDateIT() {
		super();
	}
	public void setData(Date d){
	this.setTime(d);
	}

	public void setData(int year, int month, int day) {
		this.set(year, month-1, day);
	}
	 public void setAnno(int value){
		 this.set(YEAR, value);
		 return;
	 }	 
	 public void setMese(int value){
		 this.set(MONTH, value-1);
		 return;
	 }
	 public void setGiorno(int value){
		 this.set(DAY_OF_MONTH, value);
			 return;
	 }
	public int getAnno(){
		return this.get(CalendarDateIT.YEAR);
	}
	public int getMese(){
		return this.get(CalendarDateIT.MONTH);//attenzione i mesi in questo caso vanno da 0-11 come nel GregorianCalendar
	}
	public int getGiorno(){
		return this.get(CalendarDateIT.DAY_OF_MONTH);// attenzione il giorno � intero  1-31
	}
	public int getGiornoSettimana(){ //attenzione il giorno � 0 per domenica, 1 per luned�....6 per sabato come nel GregorianCaledar
		return this.get(CalendarDateIT.DAY_OF_WEEK);
	}
	public String getGiornoSettimanaStrg(){ //attenzione il giorno � 0 per domenica, 1 per luned�....6 per sabato come nel GregorianCaledar
		int giorno=(this.get(CalendarDateIT.DAY_OF_WEEK))-1;
		String giornosett=null;
	if(giorno==0)giornosett="Domenica";
	if(giorno==1)giornosett="Luned�";
	if(giorno==2)giornosett="Marted�";
	if(giorno==3)giornosett="Mercoled�";
	if(giorno==4)giornosett="Gioved�";
	if(giorno==5)giornosett="Venerd�";
	if(giorno==6)giornosett="Sabato";
	return giornosett;
	}

public String DatatoStringIT(){
	String toString = null;
	int mese=this.getMese();
	String meseIT= null;
	if (mese==0)meseIT="GEN";
	if (mese==1)meseIT="FEB";
	if (mese==2)meseIT="MAR";
	if (mese==3)meseIT="APR";
	if (mese==4)meseIT="MAG";
	if (mese==5)meseIT="GIU";
	if (mese==6)meseIT="LUG";
	if (mese==7)meseIT="AGO";
	if (mese==8)meseIT="SET";
	if (mese==9)meseIT="OTT";
	if (mese==10)meseIT="NOV";
	if (mese==11)meseIT="DIC";
	toString = this.getGiorno()+"-"+meseIT+"-"+this.getAnno(); //compone la stringa di uscita nel formato gg-MMM-aa

	return toString;
}
public String DatatoStringIT1(){
	String toString = null;
	int giorno=this.getGiornoSettimana();
	String giornsettIT= null;
	if (giorno==0)giornsettIT="DOMENICA";
	if (giorno==1)giornsettIT="LUNEDI";
	if (giorno==2)giornsettIT="MARTEDI";
	if (giorno==3)giornsettIT="MERCOLEDI";
	if (giorno==4)giornsettIT="GIOVEDI";
	if (giorno==5)giornsettIT="VENERDI";
	if (giorno==6)giornsettIT="SABATO";
	toString = this.getGiornoSettimana()+"-"+giornsettIT+"-"; //compone la stringa di uscita nel formato gg-MMM-aa

	return toString;
}

//da testare
public int[] toDataint(String value){
	int[] data = null;
	int mese =-1;
	data[1]=Integer.parseInt(value.substring(0, 2)) ; //estrae (il giorno)i primi due caratteri dalla stringa della data gg-MMM-aa
	String meseStrng=null;
	meseStrng=(value.substring(3, 6));//estrae (il mese)i  caratteri del mese dalla stringa della data gg-MM-aa
	if (meseStrng=="GEN")mese=0;//converte la stringa del mese in un numero per rispettare il formato del CalendarDateIT e quindi del GregorianCalendar
	if (meseStrng=="FEB")mese=1;
	if (meseStrng=="MAR")mese=2;
	if (meseStrng=="APR")mese=3;
	if (meseStrng=="MAG")mese=4;
	if (meseStrng=="GIU")mese=5;
	if (meseStrng=="LUG")mese=6;
	if (meseStrng=="AGO")mese=7;
	if (meseStrng=="SET")mese=8;
	if (meseStrng=="OTT")mese=9;
	if (meseStrng=="NOV")mese=10;
	if (meseStrng=="DIC")mese=11;
	data[3] =Integer.parseInt(value.substring(7)) ; //estrae l'anno
	data[2]= mese;
	return data;
	
	
}
public long getDifferenzaGiorni (CalendarDateIT data1, CalendarDateIT data2){
	long millisecondi1=data1.getTimeInMillis();
	long millisecondi2=data2.getTimeInMillis();
	long diff=(millisecondi1-millisecondi2);
	long diffdays=diff/(24*60*60*1000);
	return diffdays;
}

//da testare
public CalendarDateIT DbtoDate(String value){
	CalendarDateIT data= null;
	int giorno = -1;
	int mese = -1;
	int anno = -1;
	String meseStrng=null;
	data= new CalendarDateIT();//oggetto di uscita non inizializzato
	giorno=Integer.parseInt(value.substring(0, 2)) ; //estrae i primi due caratteri dalla stringa della data gg-MMM-aa
	meseStrng=(value.substring(3, 6));//estrae i  caratteri del mese dalla stringa della data gg-MM-aa
	if (meseStrng=="GEN")mese=0;//converte la stringa del mese in un numero per rispettare il formato del CalendarDateIT e quindi del GregorianCalendar
	if (meseStrng=="FEB")mese=1;
	if (meseStrng=="MAR")mese=2;
	if (meseStrng=="APR")mese=3;
	if (meseStrng=="MAG")mese=4;
	if (meseStrng=="GIU")mese=5;
	if (meseStrng=="LUG")mese=6;
	if (meseStrng=="AGO")mese=7;
	if (meseStrng=="SET")mese=8;
	if (meseStrng=="OTT")mese=9;
	if (meseStrng=="NOV")mese=10;
	if (meseStrng=="DIC")mese=11;
	anno =Integer.parseInt(value.substring(7)) ; //estrae gli ultimi due caratteri dalla stringa della data gg-MMM-aa
	data.set(anno, mese-1, giorno); //inizializza l'oggetto di uscita il -1 c'� perch� i mesi vanno da 0-11
 
	return data;
}
}