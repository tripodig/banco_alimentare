package util;
import java.util.Collections;


import javax.servlet.http.Part;
import java.util.Vector;



public class utility {
	
	public static String pathWindows="C:";
	public static String UPLOAD_DIRECTORY = "upload";

	

	public static Vector<String> distinct (Vector<String> unordered) /* Vettore NON ordinato */
	{
		int i, j;
		boolean duplicato;

		Vector<String> res=new Vector<String>();
		for ( i = 0; i < unordered.size(); i++) /* per tutti gli elementi a partire dal secondo */
		{
			duplicato= false;

			for (j=0; j<i; j++)
			{
				if (unordered.get(i).equals(unordered.get(j)) )
					duplicato = true;
			}

			if (!duplicato)//se l'elemento non ha duplicati lo aggiungi al vettor risultato
			{
				res.add(unordered.get(i));

			}
			Collections.sort(res);
		}
		return res;
	} 



	public static String getFilename(Part part) {
		for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().startsWith("filename")) {
				String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
			}
		}

		return null;
	}

}
