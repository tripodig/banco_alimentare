package beans;

public class UserBean {
	
    private String username;
    private String password;
    private String nome;
    private String cognome;
    private String area;
    private String ruolo;
    private int annoConvenzioni;
    private int id;
    private String provinciaConvenzioni;
    public boolean valid;
	
	
    public String getnome() {
       return nome;
	}

    public void setnome(String newnome) {
       nome = newnome;
	}

	
    public String getcognome() {
       return cognome;
			}

    public void setcognome(String newcognome) {
       cognome = newcognome;
			}
			

    public String getPassword() {
       return password;
	}

    public void setPassword(String newPassword) {
       password = newPassword;
	}
	
			
    public String getUsername() {
       return username;
			}

    public void setUserName(String newUsername) {
       username = newUsername;
			}

				
    public boolean isValid() {
       return valid;
	}

    public void setValid(boolean newValid) {
       valid = newValid;
	}

	public void setarea(String newarea) {
		area=newarea;
		
	}
	public String getArea() {
		return area;
		
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public int getAnnoConvenzioni() {
		return annoConvenzioni;
	}

	public void setIdUser(int id) {
		this.id = id;
	}
	public int getIdUser() {
		return id;
	}

	public void setAnnoConvenzioni(int annoConvenzioni) {
		this.annoConvenzioni = annoConvenzioni;
	}

	public String getProvinciaConvenzioni() {
		return provinciaConvenzioni;
	}

	public void setProvinciaConvenzioni(String provinciaConvenzioni) {
		this.provinciaConvenzioni = provinciaConvenzioni;
	}
}
