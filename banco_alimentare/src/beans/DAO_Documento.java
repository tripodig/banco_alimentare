package beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import dto.DocumentoCampiDTO;
import dto.DocumentoDTO;

import util.DBManager;



public class DAO_Documento {
	DBManager db;


	public DAO_Documento()
	{
		db=new DBManager();
	}

	public DAO_Documento(DBManager db)
	{
		this.db=db;
	}

	public DocumentoBean getDocumento(int idDocumento)
	{
		DocumentoDTO documentoDTO = new DocumentoDTO();
		Vector<DocumentoCampiDTO> documentoCampiDTO = new Vector<DocumentoCampiDTO>();
		DocumentoBean documentoBean = new DocumentoBean();



		String query = "SELECT * FROM documento WHERE id="+idDocumento;
		Vector<Object> v =db.executeSelect(query, "Documento");
		documentoDTO = (DocumentoDTO)v.get(0);

		String query2 = "SELECT * FROM documento_campo WHERE tipodocumento='"+documentoDTO.getTipoDocumento()+"'";
		Vector<Object> v2 =db.executeSelect(query2, "DocumentoCampi");

		if(v2.size()!=0){ 
			documentoCampiDTO = new Vector<DocumentoCampiDTO>();
			for(int i=0; i<v2.size();i++)
				documentoCampiDTO.add((DocumentoCampiDTO)v2.get(i));
		}

		documentoBean.setIdDocumento(documentoDTO.getId());
		documentoBean.setIdConvenzione(documentoDTO.getIdConvenzione());
		documentoBean.setTipoDocumento(documentoDTO.getTipoDocumento());
		Map<String,String> listaCampi =new HashMap<String,String>();

		listaCampi.put(documentoCampiDTO.get(0).getAttributo(),documentoDTO.getCampo1());
		listaCampi.put(documentoCampiDTO.get(1).getAttributo(),documentoDTO.getCampo2());
		listaCampi.put(documentoCampiDTO.get(2).getAttributo(),documentoDTO.getCampo3());
		listaCampi.put(documentoCampiDTO.get(3).getAttributo(),documentoDTO.getCampo4());
		listaCampi.put(documentoCampiDTO.get(4).getAttributo(),documentoDTO.getCampo5());
		listaCampi.put(documentoCampiDTO.get(5).getAttributo(),documentoDTO.getCampo6());
		listaCampi.put(documentoCampiDTO.get(6).getAttributo(),documentoDTO.getCampo7());
		listaCampi.put(documentoCampiDTO.get(7).getAttributo(),documentoDTO.getCampo8());
		listaCampi.put(documentoCampiDTO.get(8).getAttributo(),documentoDTO.getCampo9());
		listaCampi.put(documentoCampiDTO.get(9).getAttributo(),documentoDTO.getCampo10());
		listaCampi.put(documentoCampiDTO.get(10).getAttributo(),documentoDTO.getCampo11());
		listaCampi.put(documentoCampiDTO.get(11).getAttributo(),documentoDTO.getCampo12());
		listaCampi.put(documentoCampiDTO.get(12).getAttributo(),documentoDTO.getCampo13());
		listaCampi.put(documentoCampiDTO.get(13).getAttributo(),documentoDTO.getCampo14());
		listaCampi.put(documentoCampiDTO.get(14).getAttributo(),documentoDTO.getCampo15());

		documentoBean.setCampi(listaCampi);


		return documentoBean;
	}
	
	public DocumentoBean getDocumento(int idconvenzione, String tipoDocumento)
	{
		DocumentoDTO documentoDTO = new DocumentoDTO();
		Vector<DocumentoCampiDTO> documentoCampiDTO = new Vector<DocumentoCampiDTO>();
		DocumentoBean documentoBean = new DocumentoBean();



		String query = "SELECT * FROM documento WHERE idconvenzione="+idconvenzione+" and tipoDocumento='"+tipoDocumento+"'";
		Vector<Object> v =db.executeSelect(query, "Documento");
		if(v.size()>0){
			//se non c'� un risultato termino il metodo
		documentoDTO = (DocumentoDTO)v.get(0);
		
		String query2 = "SELECT * FROM documento_campo WHERE tipodocumento='"+tipoDocumento+"'";
		Vector<Object> v2 =db.executeSelect(query2, "DocumentoCampi");

		if(v2.size()!=0){ 
			documentoCampiDTO = new Vector<DocumentoCampiDTO>();
			for(int i=0; i<v2.size();i++)
				documentoCampiDTO.add((DocumentoCampiDTO)v2.get(i));
		}

		documentoBean.setIdDocumento(documentoDTO.getId());
		documentoBean.setIdConvenzione(documentoDTO.getIdConvenzione());
		documentoBean.setTipoDocumento(documentoDTO.getTipoDocumento());
		Map<String,String> listaCampi =new HashMap<String,String>();

		listaCampi.put(documentoCampiDTO.get(0).getAttributo(),documentoDTO.getCampo1());
		listaCampi.put(documentoCampiDTO.get(1).getAttributo(),documentoDTO.getCampo2());
		listaCampi.put(documentoCampiDTO.get(2).getAttributo(),documentoDTO.getCampo3());
		listaCampi.put(documentoCampiDTO.get(3).getAttributo(),documentoDTO.getCampo4());
		listaCampi.put(documentoCampiDTO.get(4).getAttributo(),documentoDTO.getCampo5());
		listaCampi.put(documentoCampiDTO.get(5).getAttributo(),documentoDTO.getCampo6());
		listaCampi.put(documentoCampiDTO.get(6).getAttributo(),documentoDTO.getCampo7());
		listaCampi.put(documentoCampiDTO.get(7).getAttributo(),documentoDTO.getCampo8());
		listaCampi.put(documentoCampiDTO.get(8).getAttributo(),documentoDTO.getCampo9());
		listaCampi.put(documentoCampiDTO.get(9).getAttributo(),documentoDTO.getCampo10());
		listaCampi.put(documentoCampiDTO.get(10).getAttributo(),documentoDTO.getCampo11());
		listaCampi.put(documentoCampiDTO.get(11).getAttributo(),documentoDTO.getCampo12());
		listaCampi.put(documentoCampiDTO.get(12).getAttributo(),documentoDTO.getCampo13());
		listaCampi.put(documentoCampiDTO.get(13).getAttributo(),documentoDTO.getCampo14());
		listaCampi.put(documentoCampiDTO.get(14).getAttributo(),documentoDTO.getCampo15());

		documentoBean.setCampi(listaCampi);

		}
		return documentoBean;
	}

	public List<DocumentoBean> getDocumentibyIdConvenzione(int idConvenzione)
	{
		List<DocumentoDTO> listaDocumentoDTO = new ArrayList<DocumentoDTO>();
		List<DocumentoBean> listDocumentoBean = new ArrayList<DocumentoBean>();

		String query = "SELECT * FROM documento WHERE idConvenzione="+idConvenzione;
		List<Object> v =db.executeSelectList(query, "Documento");
		if(v.size()>0){ 
			for(int s=0; s<v.size();s++){
				listaDocumentoDTO.add((DocumentoDTO)v.get(s));

				DocumentoBean documentoBean= new DocumentoBean();

				for(int a=0; a<listaDocumentoDTO.size();a++){
					String query2 = "SELECT * FROM documento_campo WHERE tipodocumento='"+listaDocumentoDTO.get(a).getTipoDocumento()+"'";
					List<Object> v2 =db.executeSelectList(query2, "DocumentoCampi");
					Vector<DocumentoCampiDTO> vettore=new Vector<DocumentoCampiDTO>();
					
					if(v2.size()>0){ 
						
						for(int i=0; i<v2.size();i++){
							vettore.add((DocumentoCampiDTO)v2.get(i));
						}
					}

					documentoBean.setIdDocumento(listaDocumentoDTO.get(a).getId());
					documentoBean.setIdConvenzione(listaDocumentoDTO.get(a).getIdConvenzione());
					documentoBean.setTipoDocumento(listaDocumentoDTO.get(a).getTipoDocumento());
					Map<String, String> listaCampi =new HashMap<String,String>();

					listaCampi.put(vettore.get(0).getAttributo(),listaDocumentoDTO.get(a).getCampo1());
					listaCampi.put(vettore.get(1).getAttributo(),listaDocumentoDTO.get(a).getCampo2());
					listaCampi.put(vettore.get(2).getAttributo(),listaDocumentoDTO.get(a).getCampo3());
					listaCampi.put(vettore.get(3).getAttributo(),listaDocumentoDTO.get(a).getCampo4());
					listaCampi.put(vettore.get(4).getAttributo(),listaDocumentoDTO.get(a).getCampo5());
					listaCampi.put(vettore.get(5).getAttributo(),listaDocumentoDTO.get(a).getCampo6());
					listaCampi.put(vettore.get(6).getAttributo(),listaDocumentoDTO.get(a).getCampo7());
					listaCampi.put(vettore.get(7).getAttributo(),listaDocumentoDTO.get(a).getCampo8());
					listaCampi.put(vettore.get(8).getAttributo(),listaDocumentoDTO.get(a).getCampo9());
					listaCampi.put(vettore.get(9).getAttributo(),listaDocumentoDTO.get(a).getCampo10());
					listaCampi.put(vettore.get(10).getAttributo(),listaDocumentoDTO.get(a).getCampo11());
					listaCampi.put(vettore.get(11).getAttributo(),listaDocumentoDTO.get(a).getCampo12());
					listaCampi.put(vettore.get(12).getAttributo(),listaDocumentoDTO.get(a).getCampo13());
					listaCampi.put(vettore.get(13).getAttributo(),listaDocumentoDTO.get(a).getCampo14());
					listaCampi.put(vettore.get(14).getAttributo(),listaDocumentoDTO.get(a).getCampo15());

					documentoBean.setCampi(listaCampi);


				}
				listDocumentoBean.add(documentoBean);
			}
		}

		return listDocumentoBean;
	}

	public boolean salva(DocumentoDTO u)
	{
		boolean esito=false;

		String query = "INSERT INTO documento (id,idConvenzione, tipodocumento , campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9,campo10,campo11,campo12,campo13,campo14,campo15) VALUES ( 0 ,'"+u.getIdConvenzione()+"','"+u.getTipoDocumento()+"','"+u.getCampo1()+"' ,'"+u.getCampo2()+"','"+u.getCampo3()+"','"+u.getCampo4()+"','"+u.getCampo5()+"','"+u.getCampo6()+"','"+u.getCampo7()+"','"+u.getCampo8()+"','"+u.getCampo9()+"','"+u.getCampo10()+"','"+u.getCampo11()+"','"+u.getCampo12()+"','"+u.getCampo13()+"','"+u.getCampo14()+"','"+u.getCampo15()+"')";
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	

	
	public boolean eliminaDocumentoById(int idDocumento)
	{
		boolean esito;
		String query = "DELETE FROM documento WHERE id = " +idDocumento;
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean hasVerbaleRegistro(int idConvenzione)
	{
		
		String query = "SELECT count(*)"
				+ " FROM documento d"
				+ " WHERE d.idConvenzione='"+idConvenzione+"'"
						+ " and d.tipodocumento='verbale_registro' ";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont>0)
			return true;
		else
			return false;
	}
	
	public boolean hasVerbaleFascicoli(int idConvenzione)
	{
		
		String query = "SELECT count(*)"
				+ " FROM documento d"
				+ " WHERE d.idConvenzione='"+idConvenzione+"'"
						+ " and d.tipodocumento='verbale_fascicoli' ";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont>0)
			return true;
		else
			return false;
	}

}
