//by Peppet
package beans;


public class Sede {
	
	
	private int idEnte;
	private int idSede;
    private String Tipologia;
    private String Indirizzo;
    private String Comune;
    private String Citta;
    private String Cap;
    private String Provincia;
    private String Telefono;
    private String Fax;
    private String Email;
    
    public Sede(int idEnte, String tipologia, String indirizzo,
			String comune, String citta, String cap, String provincia,
			String telefono, String fax, String email) {
		super();
		this.idEnte = idEnte;

		Tipologia = tipologia;
		Indirizzo = indirizzo;
		Comune = comune;
		Citta = citta;
		Cap = cap;
		Provincia = provincia;
		Telefono = telefono;
		Fax = fax;
		Email=email;
	}
    public Sede(int idEnte, int idSede, String tipologia, String indirizzo,
			String comune, String citta, String cap, String provincia,
			String telefono, String fax, String email) {
		super();
		this.idEnte = idEnte;
		this.idSede = idSede;
		Tipologia = tipologia;
		Indirizzo = indirizzo;
		Comune = comune;
		Citta = citta;
		Cap = cap;
		Provincia = provincia;
		Telefono = telefono;
		Fax = fax;
		Email=email;
	}
    
    public Sede() {
 		
 		idEnte = 0;
 		idSede = 0;
 		Tipologia = "";
 		Indirizzo = "";
 		Comune = "";
 		Citta = "";
 		Cap = "";
 		Provincia = "";
 		Telefono = "";
 		Fax = "";
 		Email="";
 	}
    
	public int getIdEnte() {
		return idEnte;
	}
	public void setIdEnte(int idEnte) {
		this.idEnte = idEnte;
	}
	public int getIdSede() {
		return idSede;
	}
	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}
	public String getTipologia() {
		return Tipologia;
	}
	public void setTipologia(String tipologia) {
		Tipologia = tipologia;
	}
	public String getIndirizzo() {
		return Indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		indirizzo=indirizzo.replace("'", "`");
		Indirizzo = indirizzo;
	}
	public String getComune() {
		
		return Comune;
	}
	public void setComune(String comune) {
		comune=comune.replace("'", "`");
		Comune = comune;
	}
	public String getCitta() {
		return Citta;
	}
	public void setCitta(String citta) {
		citta=citta.replace("'", "`");
		Citta = citta;
	}
	public String getCap() {
		return Cap;
	}
	public void setCap(String cap) {
		Cap = cap;
	}
	public String getProvincia() {
		
		return Provincia;
	}
	public void setProvincia(String provincia) {
		Provincia = provincia;
	}
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String telefono) {
		Telefono = telefono;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	
	
	public String toStringTable() {
		return "<b>"+Tipologia + "</b><br> "+Indirizzo + "<br> "+Comune
				+ " "+ Cap +" "+ Provincia + "<br> Telefono: " + Telefono + " Fax: " + Fax +"<br> Email: " + Email ;
	}
	@Override
	public String toString() {
		return "" + Indirizzo + " " + Comune
				+ " " + Citta + " " + Cap + " "
				+ Provincia + "<br> Telefono:" + Telefono + " Fax:" + Fax
				+ " Email:" + Email + "";
	}
	
	public String toString2line() {
		return "<b>"+Tipologia + "</b><font size=\"2\"> " + Indirizzo + " " + Comune
				+ " " + Citta + " " + Cap + " "
				+ Provincia + "<br> Telefono:" + Telefono + " Fax:" + Fax
				+ " Email:" + Email + "</font>";
	}
    
	public String toStringIndirizzo() {
		return "" + Indirizzo + " " + Comune
				+ " " + Citta + " " + Cap + " "
				+ Provincia + "";
	}
}
   