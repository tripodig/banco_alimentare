package beans;


public class Persona {
	public Persona(int idPersona, int idEnte, String titoloReferenza,
			String nome, String cognome, String indirizzo, String cap,
			String comune, String citta, String telefono, String cellulare,
			String email, String cf, String datanascita, String luogonascita) {
		super();
		this.idPersona = idPersona;
		this.idEnte = idEnte;
		TitoloReferenza = titoloReferenza;
		Nome = nome;
		Cognome = cognome;
		Indirizzo = indirizzo;
		Cap = cap;
		Comune = comune;
		Citta = citta;
		Telefono = telefono;
		Cellulare = cellulare;
		Email = email;
		CF=cf;
		setDataNascita(datanascita);
		setLuogoNascita(luogonascita);
	}
		public Persona() {
			this.idPersona = 0;
			this.idEnte = 0;
			TitoloReferenza = "";
			Nome = "";
			Cognome = "";
			Indirizzo = "";
			Cap = "";
			Comune = "";
			Citta = "";
			Telefono = "---";
			Cellulare = "---";
			Email = "---";
			CF="";
			setDataNascita("");
			setLuogoNascita("");
		}


	private int idPersona;
	private int idEnte;
	private String TitoloReferenza;
    private String Nome;
    private String Cognome;
    private String Indirizzo;
    private String Cap;
    private String Comune;
    private String Citta;
    private String Telefono;
    private String Cellulare;
    private String Email;
    private String CF;
    private String DataNascita;
    private String LuogoNascita;
    
	public boolean equals(Object anObject)
	{
		if ( anObject == null ) return false;
	    if (! (anObject instanceof Persona) ) return false;

	    Persona ut = (Persona) anObject;
	    if (idPersona != ut.idPersona) return false;
	    
	    return true;
	}


	public int getIdPersona() {
		return idPersona;
	}


	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}


	public int getIdEnte() {
		return idEnte;
	}


	public void setIdEnte(int idEnte) {
		this.idEnte = idEnte;
	}


	public String getTitoloReferenza() {
		return TitoloReferenza;
	}


	public void setTitoloReferenza(String titoloReferenza) {
		TitoloReferenza = titoloReferenza;
	}


	public String getNome() {
		return Nome;
	}


	public void setNome(String nome) {
		nome=nome.replace("'", "`");
		Nome = nome;
	}


	public String getCognome() {
		return Cognome;
	}


	public void setCognome(String cognome) {
		cognome=cognome.replace("'", "`");
		Cognome = cognome;
	}


	public String getIndirizzo() {
		
		return Indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		indirizzo=indirizzo.replace("'", "`");
		Indirizzo = indirizzo;
	}


	public String getCap() {
		return Cap;
	}


	public void setCap(String cap) {
		Cap = cap;
	}


	public String getComune() {
		return Comune;
	}


	public void setComune(String comune) {
		comune=comune.replace("'", "`");
		Comune = comune;
	}


	public String getCitta() {
		return Citta;
	}


	public void setCitta(String citta) {
		citta=citta.replace("'", "`");
		Citta = citta;
	}


	public String getTelefono() {
		return Telefono;
	}


	public void setTelefono(String telefono) {
		Telefono = telefono;
	}


	public String getCellulare() {
		return Cellulare;
	}


	public void setCellulare(String cellulare) {
		Cellulare = cellulare;
	}


	public String getEmail() {
		return Email;
	}


	public void setEmail(String email) {
		Email = email;
	}


	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", idEnte=" + idEnte
				+ ", TitoloReferenza=" + TitoloReferenza + ", Nome=" + Nome
				+ ", Cognome=" + Cognome + ", Indirizzo=" + Indirizzo
				+ ", Cap=" + Cap + ", Comune=" + Comune + ", Citta=" + Citta
				+ ", Telefono=" + Telefono + ", Cellulare=" + Cellulare
				+ ", Email=" + Email + ", CF=" + CF + "]";
	}
	public String toStringTable() {
		return "<b>"+TitoloReferenza + "</b> <br>" + Nome
				+ " " + Cognome + "<br> " + CF+ "<br>"+Indirizzo
				+ "<br> " + Cap + " " + Comune + " " + Citta
				+ " <br> Telefono:" + Telefono + " Cellulare:" + Cellulare
				+ "<br>  Email:" + Email;
	}
	public String toStringInfo() {
		return Nome
				+ " " + Cognome + "<br> " + CF+"<br>"+ Indirizzo
				+ "<br> " + Cap + " " + Comune + " " + Citta
				+ " <br> Telefono:" + Telefono + " Cellulare:" + Cellulare
				+ "<br>  Email:" + Email;
	}
	
	
	
	public String toStringline() {
		return Nome
				+ " " + Cognome + " " + CF+" "+ Indirizzo
				+ " " + Cap + " " + Comune + " " + Citta
				+ " <br> Telefono:" + Telefono + " Cellulare: " + Cellulare
				+ " Email: " + Email;
	}
	public String toString3line() {
		return "<b>"+TitoloReferenza+"</b> "+Nome
				+ " " + Cognome + "<font size=\"2\"> " + CF+"<br> "+ Indirizzo
				+ " " + Cap + " " + Comune + " " + Citta
				+ " <br> Telefono:" + Telefono + " Cellulare: " + Cellulare
				+ " Email: " + Email+"</font>";
	}
	public String getCF() {
		return CF;
	}
	public void setCF(String cF) {
		CF = cF;
	}
	public String getDataNascita() {
		return DataNascita;
	}
	public void setDataNascita(String dataNascita) {
		DataNascita = dataNascita;
	}
	public String getLuogoNascita() {
		return LuogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		LuogoNascita = luogoNascita;
	}

 }
