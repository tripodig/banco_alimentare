//by Peppet
package beans;



import java.util.Vector;

import util.DBManager;

public class CatalogoSede {

	DBManager db;
	
	public CatalogoSede()
	{
		db=new DBManager();
	}
	public CatalogoSede(DBManager db)
	{
		this.db=db;
	}
	
	public Sede getSede(int idSede)
	{
		Sede res = new Sede();
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE idSede = "+idSede+" AND visibile=1";
		Vector<Object> v =db.executeSelect(query, "Sede");
		res = (Sede)v.get(0);
		return res;
	}
	public Sede getSedeLegale(int idEnte)
	{
		Sede res =null;
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE idEnte = "+idEnte+" AND visibile=1 AND Tipologia like 'Sede legale%'";
		Vector<Object> v =db.executeSelect(query, "Sede");
		if(v.size()!=0){
			res= new Sede();
			res = (Sede)v.get(0);
			}
		return res;
	}
	
	public Sede getSedeOperativa(int idEnte)
	{
		Sede res =null;
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE idEnte = "+idEnte+" AND visibile=1 AND Tipologia like 'Sede operativa'";
		Vector<Object> v =db.executeSelect(query, "Sede");
		if(v.size()!=0){
			res= new Sede();
			res = (Sede)v.get(0);
			}
		return res;
	}
	
	public Sede getSedeMagazzino(int idEnte)
	{
		Sede res = null;
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE idEnte = "+idEnte+" AND visibile=1 AND Tipologia like 'Sede magazzino'";
		Vector<Object> v =db.executeSelect(query, "Sede");
		if(v.size()!=0){
			res= new Sede();
			res = (Sede)v.get(0);
			}
		
		return res;
	}
	
	public Vector<Sede> getSedi()
	{
		Vector<Sede> res = new Vector<Sede>();
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE visibile=1";
		Vector<Object> supp = db.executeSelect(query, "Sede");
		for(int i=0;i<supp.size();i++)
			res.add((Sede)supp.get(i));
		
		return res;
	}
	public Vector<Sede> getSediEnte(int idEnte)
	{
		Vector<Sede> res = new Vector<Sede>();
		String query = "SELECT idEnte, idSede, tipologia, indirizzo, comune, citta, cap, provincia, telefono, fax, email, visibile FROM sede WHERE idEnte = "+idEnte+" AND visibile=1";
		Vector<Object> supp = db.executeSelect(query, "Sede");
		for(int i=0;i<supp.size();i++)
			res.add((Sede)supp.get(i));
		
		return res;
	}
	
	public String getIndirizzolegale(int idEnte)
	{
		Sede res = null;
		String indirizzo=null;
		String query = "SELECT indirizzo FROM sede WHERE idEnte = "+idEnte+" AND visibile=1 AND Tipologia like 'Sede legale'";
		Vector<Object> v =db.executeSelect(query, "Sede");
		if(v.size()!=0){
			res= new Sede();
			res = (Sede)v.get(0);
			indirizzo= res.getIndirizzo();
			return indirizzo;
			}
		
		return indirizzo;
	}
	public boolean salvaSede(Sede e)
	{
		boolean esito;
				
		
		String query = "INSERT INTO sede (idEnte, Tipologia, Indirizzo, Comune, Citta, Cap, Provincia, Telefono, Fax ,Email) VALUES ( '"+e.getIdEnte()+"' ,'"+e.getTipologia()+"','"+e.getIndirizzo()+"','"+e.getComune()+"','"+e.getCitta()+"','"+e.getCap()+"','"+e.getProvincia()+"','"+e.getTelefono()+"','"+e.getFax()+"','"+e.getEmail()+"')";
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	public boolean aggiornaSede(Sede e)
	{
		boolean esito;
				
		
		String query = "Update sede set idEnte='"+e.getIdEnte()+"', Tipologia='"+e.getTipologia()+"', Indirizzo='"+e.getIndirizzo()+"', Comune='"+e.getComune()+"', Citta='"+e.getCitta()+"', Cap='"+e.getCap()+"', Provincia='"+e.getProvincia()+"', Telefono='"+e.getTelefono()+"', Fax='"+e.getFax()+"', Email='"+e.getEmail()+"' where idSede="+e.getIdSede();
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean disableSede(Sede e)
	{
		boolean esito;
				
		
		String query = "UPDATE sede SET visibile=0 WHERE idEnte='"+e.getIdEnte();
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	//da verificare come sostituire la *
	public boolean isCancellable(int idSede)
	{
		
		String query = "SELECT count(*)"
				+ " FROM sede s, convenzione c"
				+ " WHERE s.idsede='"+idSede+"'"
						+ " and (c.idSedeLegaleEnte=s.idSede "
						+ " or c.idSedeMagazzinoEnte=s.idSede "
						+ " or c.idSedeOperativaEnte=s.idSede);";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont==0)
			return true;
		else
			return false;
	}

	
	public boolean eliminaSede(Sede e)
	{
		boolean esito=false;
		String query = "DELETE FROM sede WHERE idSede = " +e.getIdSede();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	

}
