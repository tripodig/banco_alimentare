package beans;
import util.CalendarDateIT;

import java.util.StringTokenizer;
import java.util.Vector;

import util.DBManager;

import java.lang.Object;

public class CatalogoConvenzioni {
	 	DBManager db;
		
		public CatalogoConvenzioni()
		{
			db=new DBManager();
		}
		public CatalogoConvenzioni(DBManager db)
		{
			this.db=db;
		}
		
		public Convenzione getConvenzione(int idConvenzione)
		{
			Convenzione res = new Convenzione();
			String query = "SELECT * FROM convenzione WHERE idconvenzione = "+idConvenzione;
			Vector<Object> v =db.executeSelect(query, "Convenzione");
			res =(Convenzione)v.get(0);
			return res;
		}
		
		public Vector<Convenzione> getConvenzioni()
		{
			Vector<Convenzione> res = new Vector<Convenzione>();
			String query = "SELECT * FROM convenzione ORDER BY ANNO desc";
			Vector<Object> supp = db.executeSelect(query, "Convenzione");
			System.out.println( "convenzioni prelevate ="+supp.size());
			for(int i=0;i<supp.size();i++){
				res.add((Convenzione)supp.get(i));
			}
			return res;
		}
		 
		public Vector<Convenzione> getConvenzioniAnno(int anno)
		{
			Vector<Convenzione> res = new Vector<Convenzione>();
			String query = "SELECT * FROM convenzione WHERE anno="+anno;
			Vector<Object> supp = db.executeSelect(query, "Convenzione");
			for(int i=0;i<supp.size();i++)
				res.add((Convenzione)supp.get(i));
			System.out.println( "convenzioni x anno "+anno+" prelevate ="+supp.size());	
			
			return res;
		}
		
		public Vector<Convenzione> getConvenzioniAGEAAnno(int anno)
		{
			Vector<Convenzione> res = new Vector<Convenzione>();
			String query = "SELECT * "
							+ "FROM convenzione c "
							+ "WHERE anno="+anno+" "
							+ "and ((E_0_5+E_6_65+E_over65+E_saltuari+R_6_18+R_19_65)>0 "
							+ "or  (M_0_5+M_6_65+M_over65+M_saltuari+M_6_18+M_19_65)>0 "
							+ "or (P_0_5+P_6_65+P_over65+P_saltuari+P_6_18+P_19_65)>0 "
							+ "or (U_saltuari)>0) "
							+ "and (M_totindigenti+P_totindigenti+R_totindigenti)=0";

			Vector<Object> supp = db.executeSelect(query, "Convenzione");
			for(int i=0;i<supp.size();i++)
				res.add((Convenzione)supp.get(i));
			System.out.println( "convenzioni AGEA x anno "+anno+" prelevate ="+supp.size());	
			
			return res;
		}
		
		public Vector<Convenzione> getConvenzioniBANCOAnno(int anno)
		{
			Vector<Convenzione> res = new Vector<Convenzione>();
			String query = "SELECT * "
							+ "FROM convenzione c "
							+ "WHERE anno="+anno+" "
							+ "and (M_totindigenti+P_totindigenti+R_totindigenti)>0";

			Vector<Object> supp = db.executeSelect(query, "Convenzione");
			for(int i=0;i<supp.size();i++)
				res.add((Convenzione)supp.get(i));
			System.out.println( "convenzioni BANCO x anno "+anno+" prelevate ="+supp.size());	
			
			return res;
		}
		
		public Vector<String> getAnnoConvenzioni()
		{
			Vector<String> res = new Vector<String>();
			String query = "SELECT distinct anno FROM convenzione";
			Vector<Object> supp = db.executeSelect(query, "Anno");
			for(int i=0;i<supp.size();i++){
				res.add((String)supp.get(i));
			System.out.println(res.get(i));
			}
				return res;
		}

		//restituisce il numero di convenzioni che ha l'ente
		public int getControlloConvenzioni (int idEnte){
	        Integer res;
			String query = "SELECT COUNT(*) FROM convenzione where idente="+idEnte;
			Vector<Object> supp = db.executeSelect(query, "Count");			 
				res=(Integer)supp.get(0);
			int cont=res.intValue();
			return cont;
		}
		
		public boolean isConvenzionabile (int idEnte){
	        Integer res;
	        boolean esito=false;
			String query = "SELECT count(*) FROM persona p, sede s where p.idEnte="+idEnte+" "+
							"and p.TitoloReferenza like '%Rappresentante legale%' "+
							"and p.idEnte=s.idEnte and s.Tipologia like '%Sede legale%'";
			System.out.println(query);
			Vector<Object> supp = db.executeSelect(query, "Count");
			System.out.println("isConvenzionabile valore di supp.get(0)="+supp.get(0));
			if(supp.get(0)!=null){
				res=(Integer)supp.get(0);
			int cont=res.intValue();
			if (cont> 0) {
				esito=true;
				System.out.println(idEnte+" isConvenzionabile="+esito);
				return esito;
				}}
			System.out.println(idEnte+" isConvenzionabile="+esito);
			return esito;
		}
		
		public boolean ConvenzioneScaduta (int idEnte){
	        boolean esito=false;
	        int contatore=0;
	        CalendarDateIT oggi=new CalendarDateIT();
	        int annoSuccessivo=oggi.getAnno()+1;
			String query = "SELECT COUNT(*) FROM convenzione where idente="+idEnte+"AND anno="+annoSuccessivo;
			Vector<Object> v=db.executeSelect(query, "Count");		 
			contatore=(Integer) v.get(0);
			
			System.out.println(contatore);
				if (contatore> 0) {
					esito=true;
					System.out.println(esito);
					return esito;
					}
			
			return esito;
			
		}

		
		public boolean salvaConvenzione(Convenzione conv)
		{
			boolean esito;
			/*
			 * la gestione degli attributi da inserire per il nuovo utente pu� essere
			 * gestita dinamicamente controllando quali sono i campi del bean il cui valore � diverso
			 * da quello di default (impostato nel costruttore)
			 */
			
			String query = "INSERT INTO convenzione (IDENTE,idRappLegaleEnte,idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,ANNO,CELLAFRIGO ,dataFirma,M_0_5 ,M_19_65 ,M_cena ,M_centroAccoglienzaDiurno ,M_colazione ,M_giorniApertura ,M_mediaPresenze ,M_over65 ,M_pastiFreddi ,M_pranzo ,M_totIndigenti ,P_0_5 ,P_19_65 ,P_Altro ,P_consegnaDomicilio ,P_distribuzionePacchi ,P_giorniApertura ,P_mediaPresenze , P_over65 ,P_totIndigenti ,R_0_5 ,R_19_65 ,R_comunitaAnziani ,R_comunitaDisabili ,R_comunitaMinori ,R_comunitaTossicodipendenti ,R_giorniApertura ,R_mediaPresenze ,R_over65 ,R_totIndigenti, R_casaFamiglia, M_6_18, P_6_18, R_6_18) "
					+ " VALUES("+conv.getIdEnte()+","+conv.getIdRappLegaleEnte()+","+conv.getIdSedeLegaleEnte()+", "+conv.getIdSedeOperativaEnte()+", "+conv.getIdSedeMagazzinoEnte()+","+conv.getAnno()+", "+conv.isCellaFrigo()+", '"+conv.getDataFirma()+"', "+conv.getM_0_5()+", "+conv.getM_19_65()+", "+conv.isM_cena()+", "+conv.isM_centroAccoglienzaDiurno()+", "+conv.isM_colazione()+", "+conv.getM_giorniApertura()+", "+conv.getM_mediaPresenze()+", "+conv.getM_over65()+", "+conv.isM_pastiFreddi()+", "+conv.isM_pranzo()+", "+conv.getM_totIndigenti()+", "+conv.getP_0_5()+", "+conv.getP_19_65()+", '"+conv.getP_Altro()+"', "+conv.isP_consegnaDomicilio()+", "+conv.isP_distribuzionePacchi()+", "+conv.getP_giorniApertura()+", "+conv.getP_mediaPresenze()+", "+conv.getP_over65()+", "+conv.getP_totIndigenti()+", "+conv.getR_0_5()+", "+conv.getR_19_65()+", "+conv.isR_comunitaAnziani()+", "+conv.isR_comunitaDisabili()+", "+conv.isR_comunitaMinori()+", "+conv.isR_comunitaTossicodipendenti()+", "+conv.getR_giorniApertura()+", "+conv.getR_mediaPresenze()+", "+conv.getR_over65()+", "+conv.getR_totIndigenti()+","+conv.isR_casaFamiglia()+","+conv.getM_6_18()+","+conv.getP_6_18()+","+conv.getR_6_18()+")";
			System.out.println(query);
			esito = db.executeUpdate(query);
			return esito;
		}
		
		public boolean salvaConvenzionenew(Convenzione conv)
		{
			boolean esito;
			/*
			 * la gestione degli attributi da inserire per il nuovo utente pu� essere
			 * gestita dinamicamente controllando quali sono i campi del bean il cui valore � diverso
			 * da quello di default (impostato nel costruttore)
			 */
			
			String query = "INSERT INTO convenzione (IDENTE,idRappLegaleEnte,idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,ANNO,CELLAFRIGO ,dataFirma,M_0_5,M_6_65,M_over65,P_0_5 ,P_6_65, P_over65 ,E_0_5 ,E_6_65, E_over65,M_saltuari,P_saltuari,E_saltuari, U_saltuari,M_giorniApertura, M_mediaPresenze,P_giorniApertura, P_mediaPresenze,E_giorniApertura, E_mediaPresenze,U_giorniApertura, U_mediaPresenze) "
						+ "VALUES("+conv.getIdEnte()+","+conv.getIdRappLegaleEnte()+","+conv.getIdSedeLegaleEnte()+", "+conv.getIdSedeOperativaEnte()+", "+conv.getIdSedeMagazzinoEnte()+","+conv.getAnno()+", "+conv.isCellaFrigo()+","
										+ "'"+conv.getDataFirma()+"',"
										+ " "+conv.getM_0_5()+", "+conv.getM_6_65()+", "+conv.getM_over65()+","
										+ " "+conv.getP_0_5()+", "+conv.getP_6_65()+", "+conv.getP_over65()+","
										+ " "+conv.getE_0_5()+", "+conv.getE_6_65()+", "+conv.getE_over_65()+","
										+ " "+conv.getM_saltuari()+", "+conv.getP_saltuari()+","+conv.getE_saltuari()+","+conv.getU_saltuari()+","
										+ " "+conv.getM_giorniApertura()+", "+conv.getM_mediaPresenze()+", "+conv.getP_giorniApertura()+", "+conv.getP_mediaPresenze()+", "+conv.getE_giorniApertura()+", "+conv.getE_mediaPresenze()+", "+conv.getU_giorniApertura()+", "+conv.getU_mediaPresenze()+")";
			System.out.println(query);
			esito = db.executeUpdate(query);
			return esito;
		}
		
		public boolean updateConvenzionenew(Convenzione conv)
		{
			boolean esito;
			/*
			 * la gestione degli attributi da inserire per il nuovo utente pu� essere
			 * gestita dinamicamente controllando quali sono i campi del bean il cui valore � diverso
			 * da quello di default (impostato nel costruttore)
			 */
			
			String query = "UPDATE convenzione set "
								+ "IDENTE	="+	conv.getIdEnte()+","
								+ "idRappLegaleEnte	="+conv.getIdRappLegaleEnte()+","
								+ "idSedeLegaleEnte	="+conv.getIdSedeLegaleEnte()+","
								+ "idSedeOperativaEnte="+conv.getIdSedeOperativaEnte()+","
								+ "idSedeMagazzinoEnte ="+conv.getIdSedeMagazzinoEnte()+","
								+ "ANNO							="+conv.getAnno()+","
								+ "CELLAFRIGO 				="+conv.isCellaFrigo()+","
								+ "dataFirma					='"+conv.getDataFirma()+"',"
								+ "M_0_5							="+conv.getM_0_5()+","
								+ "M_6_65						="+conv.getM_6_65()+","
								+ "M_over65					="+conv.getM_over65()+","
								+ "P_0_5 						="+conv.getP_0_5()+","
								+ "P_6_65						="+conv.getP_6_65()+","
								+ "P_over65 					="+conv.getP_over65()+","
								+ "E_0_5 						="+conv.getE_0_5()+","
								+ "E_6_65						="+conv.getE_6_65()+","
								+ "E_over65					="+conv.getE_over_65()+","
								+ "M_saltuari				="+conv.getM_saltuari()+","
								+ "P_saltuari				="+conv.getP_saltuari()+","
								+ "E_saltuari				="+conv.getE_saltuari()+","
								+ "U_saltuari				="+conv.getU_saltuari()+","
								+ "M_giorniApertura	="+conv.getM_giorniApertura()+","
								+ "M_mediaPresenze	="+conv.getM_mediaPresenze()+","
								+ "P_giorniApertura	="+conv.getP_giorniApertura()+","
								+ "P_mediaPresenze	="+conv.getP_mediaPresenze()+","
								+ "E_giorniApertura	="+conv.getE_giorniApertura()+","
								+ "E_mediaPresenze	="+conv.getE_mediaPresenze()+","
								+ "U_giorniApertura	="+conv.getU_giorniApertura()+","
								+ "U_mediaPresenze	="+conv.getU_mediaPresenze()+" "
						+ "WHERE idConvenzione="+conv.getIdConvenzione();
			System.out.println(query);
			esito = db.executeUpdate(query);
			return esito;
		}
		
		public boolean eliminaConvenzione(Convenzione conv)
		{
			boolean esito;
			String query = "DELETE FROM convenzione WHERE idconvenzione = " +conv.getIdConvenzione();
			esito = db.executeUpdate(query);
			return esito;
		}
		
		public boolean eliminaConvenzioneNonValida(int idconv)
		{
			boolean esito;
			String query = "DELETE FROM convenzione WHERE idconvenzione = " +idconv;
			esito = db.executeUpdate(query);
			return esito;
		}
		
		/*
		 * eventualmente inserire altri metodi
		 */
		
		public Convenzione getConvenzioneEnte(int idEnte){
			
			Convenzione res = null;
			String query = "SELECT * FROM convenzione WHERE idente = "+idEnte+"ORDER BY ANNO desc";
			Vector<Object> v =db.executeSelect(query, "Convenzione");
			if(v.size()!=0){
				res = new Convenzione();
				res =(Convenzione)v.get(0);
				} 
				                        
				return res;
			
		}
		
		
		
		public Vector<Convenzione> getConvenzioniEnte(int idEnte){
			
			Vector<Convenzione> res = null;
			String query = "SELECT * FROM convenzione WHERE idente = "+idEnte+" order by anno desc, datafirma desc";
			Vector<Object> v =db.executeSelect(query, "Convenzione");
			if(v.size()!=0){
				res = new Vector<Convenzione>();
				for(int i=0;i<v.size();i++)
					res.add((Convenzione)v.get(i));
				} 
				                        
				return res;
			
		}
		
		public boolean daConvenzionare(int idEnte, int anno){
						
			Integer res;
	        boolean esito=true;
	        boolean ricerca=true; //vuol dire che ha una convenzione nell'anno
	        boolean requisiti=false;
	        requisiti=isConvenzionabile(idEnte); //vuol dire che ha una sede legale ed un rapp legale � true in caso positivo
	        if (!requisiti)
	        	esito=false;
	        
			String query = "SELECT count(*) FROM convenzione WHERE idEnte="+idEnte+" "+
							"and anno="+anno;
			System.out.println(query);
			Vector<Object> supp = db.executeSelect(query, "Count");	
			if(supp.get(0)!=null){
				res=(Integer)supp.get(0);
			int cont=res.intValue();
			if (cont> 0) {
				ricerca=true;
				System.out.println(idEnte+"  non � da convenzionare per l'anno="+anno);
				
				return esito=(requisiti&&!ricerca); //restituisce vero solo se requisiti=true e ricerca=false
				}}
			return esito;
		
			
		}
		
public int getidconvenzione(int idEnte, int anno){
			
			int res = 0;
			Convenzione c=null;
			String query = "SELECT * FROM convenzione WHERE idente = "+idEnte+" and anno="+anno;
			Vector<Object> supp =db.executeSelect(query, "Convenzione");
			if(supp.size()!=0){
				c=(Convenzione)supp.get(0);
				res=c.getIdConvenzione();
				} 
				                        
				return res;
			
		}
	
		
		public Convenzione getUltimaConvenzionebyEnte(int idEnte)
		{
			Convenzione res = new Convenzione();
			String query = "SELECT * FROM convenzione WHERE idEnte = "+idEnte+" ORDER BY ANNO DESC";
			System.out.println(query);
			Vector<Object> v =db.executeSelect(query, "Convenzione");
			if(v.get(0)!=null){
			res =(Convenzione)v.get(0);
			return res;
			}
			else{
				String query2 = "SELECT * FROM convenzione WHERE idEnte = "+0+" ORDER BY ANNO DESC";
			System.out.println(query2);
			Vector<Object> v2 =db.executeSelect(query2, "Convenzione");
			res =(Convenzione)v2.get(0);
			return res;
			}
		}

	 
		public Vector<Object> getNumeroAssistiti(){
			CalendarDateIT annoattuale= new CalendarDateIT();
			int annoi=annoattuale.getAnno();
			String query = "SELECT totindigentimensa+totindigentipacchi+totindigentiresidenza AS numeroassistiti, idente FROM convenzione WHERE anno="+annoi+ "ORDER BY totindigentimensa+totindigentipacchi+totindigentiresidenza desc"; 
			Vector<Object> supp = db.executeSelect(query, "StatisticheConvenzioni");
			return supp;
		}
		
		public int getidconvenzione(int idEnte, int idSedeLegaleEnte, int idSedeOperativaEnte, int idSedeMagazzinoEnte, int anno) {
			
			int res = 0;
			Convenzione c=null;
			String query = "SELECT * FROM convenzione WHERE idente = "+idEnte+" and  idSedeLegaleEnte="+idSedeLegaleEnte+" and idSedeOperativaEnte="+idSedeOperativaEnte+" and idSedeMagazzinoEnte="+idSedeMagazzinoEnte+" and anno="+anno+" order by idconvenzione desc";
			Vector<Object> supp =db.executeSelect(query, "Convenzione");
			if(supp.size()!=0){
				c=(Convenzione)supp.get(0);
				res=c.getIdConvenzione();
				} 
				                        
				return res;
			
		}
		public Vector<Convenzione> getConvenzioni(int anno, String provincia) {
			// TODO Auto-generated method stub
			Vector<Convenzione> res = null;
			String query = "SELECT c.*"
					+ " FROM convenzione c"
					+ " WHERE c.anno="+anno+""
					+ " and c.idEnte in (Select distinct(e1.idEnte)"
										+ "	from ente e1, sede s1 where e1.idente=s1.idEnte"
										+ "	and s1.provincia='"+provincia+"' "
										+ "and e1.visibile ='1' and s1.visibile='1')";
			Vector<Object> v =db.executeSelect(query, "Convenzione");
			if(v.size()!=0){
				res = new Vector<Convenzione>();
				for(int i=0;i<v.size();i++)
					res.add((Convenzione)v.get(i));
				} 
				                        
				return res;
		}
		public Vector<Convenzione> getallConvenzioni(int anno, String provincia)
		{
			Vector<Convenzione> allProvince = new Vector<Convenzione>();
			
			StringTokenizer st= new StringTokenizer(provincia,",");
			
			Vector<Convenzione> res = null;
			while(st.hasMoreTokens()){
				res=null;
				res=getConvenzioni(anno, st.nextToken());
				if (res!=null)
				allProvince.addAll(res);
				
			}
			
			return allProvince;
			

		}	
		public boolean isCancellable(int idConvenzione)
		{
			
			String query = "SELECT count(*)"
					+ " FROM documento "
					+ " WHERE idConvenzione="+idConvenzione+"";
			Vector<Object> supp = db.executeSelect(query, "Count");
			Integer res =(Integer)supp.get(0);
			int cont=res.intValue();
			if(cont==0)
				return true;
			else
				return false;
		}
	}



