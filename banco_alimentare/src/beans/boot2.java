package beans;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

public class boot2 
{
  public void stampaFile(String path) throws Exception 
  {
    // verifico che la funzionalit� Desktop sia supportata!
    if (!java.awt.Desktop.isDesktopSupported())
    {
      System.out.println ("Funzionalit� Desktop Non supportata!");
      return;
    }
    try
    {
      // ottengo un'istanza del Desktop corrente
      Desktop d = java.awt.Desktop.getDesktop();
			
      // mando in stampa il file - 1 sola riga di codice 
      d.print(new File(path));
    }
    catch (NullPointerException npe)
    {
      System.out.println (" Il valore specificato del parametro file � null! ");
    }
    catch (IllegalArgumentException iae)
    {
      System.out.println (" Il file specificato non esiste! ");
    }
    catch (UnsupportedOperationException uoe)
    {
      System.out.println (" La piattaforma non supporta l'azione di stampa! ");
    }
    catch (IOException ioe)
    {
      System.out.println (" Il file specificato non ha associato applicazione che supportano la stampa ");
    }
    catch (SecurityException se)
    {
      System.out.println (" Il security manager installato non consente l'accesso al file o alla stampante ");
    }	
  }
}