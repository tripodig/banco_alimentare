package beans;


public class PuntoVendita {
private int idPuntoVendita;
	
	private String Gruppo;
    private String Insegna;
    private String Provincia;
    private String Indirizzo;
    private String Cap;
    private String Comune;
    private String Citta;
    private String Telefono;
    private String Cellulare;
    private String NumCasse;
    private String Email;
    private String Regione;
    private String Area;
    private String MQvendita;
    private String Orario;
    private int Visibile;
    
	public int getVisibile() {
		return Visibile;
	}

	public void setVisibile(int visibile) {
		Visibile = visibile;
	}

	public boolean equals(Object anObject)
	{
		if ( anObject == null ) return false;
	    if (! (anObject instanceof PuntoVendita) ) return false;

	    PuntoVendita ut = (PuntoVendita) anObject;
	    if (idPuntoVendita != ut.idPuntoVendita) return false;
	    
	    return true;
	}

	public int getIdPuntoVendita() {
		return idPuntoVendita;
	}

	public void setIdPuntoVendita(int idPuntoVendita) {
		this.idPuntoVendita = idPuntoVendita;
	}

	public String getGruppo() {
		return Gruppo;
	}

	public void setGruppo(String gruppo) {
		
		gruppo=gruppo.replace("'", "`");
		Gruppo = gruppo;
		
	}

	public String getInsegna() {
		return Insegna;
	}

	public void setInsegna(String insegna) {
		
		insegna=insegna.replace("'", "`");
		Insegna = insegna;
	}

	public String getProvincia() {
		return Provincia;
	}

	public void setProvincia(String provincia) {
		
		provincia=provincia.replace("'", "`");
		Provincia = provincia;
	}

	public String getIndirizzo() {
		return Indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		
		indirizzo=indirizzo.replace("'", "`");
		Indirizzo = indirizzo;
	}

	public String getCap() {
		return Cap;
	}

	public void setCap(String cap) {
		
		cap=cap.replace("'", "`");
		Cap = cap;
	}

	public String getComune() {
		return Comune;
	}

	public void setComune(String comune) {
		
		comune=comune.replace("'", "`");
		Comune = comune;
	}

	public String getCitta() {
		
		return Citta;
	}

	public void setCitta(String citta) {
		
		citta=citta.replace("'", "`");
		
		Citta = citta;
			 
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		
		telefono=telefono.replace("'", "`");
		Telefono = telefono;
	}

	public String getNumCasse() {
		return NumCasse;
	}

	public void setNumCasse(String numCasse) {
		
		numCasse=numCasse.replace("'", "`");
		NumCasse = numCasse;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		
		email=email.replace("'", "`");
		Email = email;
	}

	public String getRegione() {
		return Regione;
	}

	public void setRegione(String regione) {
		
		regione=regione.replace("'", "`");
		Regione = regione;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		
		area=area.replace("'", "`");
		Area = area;
	}

	public String getMQvendita() {
		return MQvendita;
	}

	public void setMQvendita(String mQvendita) {
		
		mQvendita=mQvendita.replace("'", "`");
		MQvendita = mQvendita;
	}

	@Override
	public String toString() {
		return "PuntoVendita [Tipologia=" + Gruppo + ", Insegna=" + Insegna
				+ ", Provincia=" + Provincia + ", Indirizzo=" + Indirizzo
				+ ", Cap=" + Cap + ", Comune=" + Comune + ", Citta=" + Citta
				+ ", Telefono=" + Telefono + ", NumCasse=" + NumCasse
				+ ", Email=" + Email + ", Regione=" + Regione + ", Area="
				+ Area + ", MQvendita=" + MQvendita + "]";
	}

	public String getCellulare() {
		return Cellulare;
	}

	public void setCellulare(String cellulare) {
		cellulare=cellulare.replace("'", "`");
		Cellulare = cellulare;
	}

	public String getOrario() {
		return Orario;
	}

	public void setOrario(String orario) {
		orario=orario.replace("'", "`");
		Orario = orario;
	}
}
