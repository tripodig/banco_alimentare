package beans;

public class ConvenzioneFrom2017 {
	
	private int idConvenzione;
	private int idEnte;
	private int idRappLegaleEnte;
	private int idSedeLegaleEnte;
	private int idSedeOperativaEnte;
	private int idSedeMagazzinoEnte;
	private int anno;
    private int M_totIndigenti;
    private int P_totIndigenti;
    private int E_totIndigenti;
    private int D_totIndigenti;
    private int U_totIndigenti;
    
    private boolean cellaFrigo;
    private String tipoPartner;
    
    private int M_0_15;
    private int M_16_64;
    private int M_over_65;
    
    private int P_0_15;
    private int P_16_64;
    private int P_over_65;
    
    private int E_0_15;
    private int E_16_64;
    private int E_over_65;
    
    private int D_0_15;
    private int D_16_64;
    private int D_over_65;
    
    private int M_saltuari;
    private int P_saltuari;

    private int M_donne;
    private int M_migranti;
    private int M_disabili;
    private int M_senzaFissaDimora;
    
    private int P_donne;
    private int P_migranti;
    private int P_disabili;
    private int P_senzaFissaDimora;
    
    private int E_donne;
    private int E_migranti;
    private int E_disabili;
    private int E_senzaFissaDimora;
    
    private int D_donne;
    private int D_migranti;
    private int D_disabili;
    private int D_senzaFissaDimora;
    
    private int U_donne;
    private int U_migranti;
    private int U_disabili;
    private int U_senzaFissaDimora;
    
    private boolean accoglienza;
    private boolean informazione;
    private boolean accompagnamento;
    private boolean spsicologico;
    private boolean educativa;
    private boolean consulenza;
    private boolean sscolastico;
    private boolean sorientamento;
    private boolean primaassistenza;
    private boolean tutela;
    private String tipologia;

    
    private String altroTtxt;
   
    private String dataFirma;

	public int getIdConvenzione() {
		return idConvenzione;
	}

	public void setIdConvenzione(int idConvenzione) {
		this.idConvenzione = idConvenzione;
	}

	public int getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(int idEnte) {
		this.idEnte = idEnte;
	}

	public int getIdRappLegaleEnte() {
		return idRappLegaleEnte;
	}

	public void setIdRappLegaleEnte(int idRappLegaleEnte) {
		this.idRappLegaleEnte = idRappLegaleEnte;
	}

	public int getIdSedeLegaleEnte() {
		return idSedeLegaleEnte;
	}

	public void setIdSedeLegaleEnte(int idSedeLegaleEnte) {
		this.idSedeLegaleEnte = idSedeLegaleEnte;
	}

	public int getIdSedeOperativaEnte() {
		return idSedeOperativaEnte;
	}

	public void setIdSedeOperativaEnte(int idSedeOperativaEnte) {
		this.idSedeOperativaEnte = idSedeOperativaEnte;
	}

	public int getIdSedeMagazzinoEnte() {
		return idSedeMagazzinoEnte;
	}

	public void setIdSedeMagazzinoEnte(int idSedeMagazzinoEnte) {
		this.idSedeMagazzinoEnte = idSedeMagazzinoEnte;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public int getM_totIndigenti() {
		return M_totIndigenti;
	}

	public void setM_totIndigenti(int m_totIndigenti) {
		M_totIndigenti = m_totIndigenti;
	}

	public int getP_totIndigenti() {
		return P_totIndigenti;
	}

	public void setP_totIndigenti(int p_totIndigenti) {
		P_totIndigenti = p_totIndigenti;
	}

	public int getE_totIndigenti() {
		return E_totIndigenti;
	}

	public void setE_totIndigenti(int e_totIndigenti) {
		E_totIndigenti = e_totIndigenti;
	}

	public int getD_totIndigenti() {
		return D_totIndigenti;
	}

	public void setD_totIndigenti(int d_totIndigenti) {
		D_totIndigenti = d_totIndigenti;
	}

	public int getU_totIndigenti() {
		return U_totIndigenti;
	}

	public void setU_totIndigenti(int u_totIndigenti) {
		U_totIndigenti = u_totIndigenti;
	}

	public boolean isCellaFrigo() {
		return cellaFrigo;
	}

	public void setCellaFrigo(boolean cellaFrigo) {
		this.cellaFrigo = cellaFrigo;
	}

	public String getTipoPartner() {
		return tipoPartner;
	}

	public void setTipoPartner(String tipoPartner) {
		this.tipoPartner = tipoPartner;
	}

	public int getM_0_15() {
		return M_0_15;
	}

	public void setM_0_15(int m_0_15) {
		M_0_15 = m_0_15;
	}

	public int getM_16_64() {
		return M_16_64;
	}

	public void setM_16_64(int m_16_64) {
		M_16_64 = m_16_64;
	}

	public int getM_over_65() {
		return M_over_65;
	}

	public void setM_over_65(int m_over_65) {
		M_over_65 = m_over_65;
	}

	public int getP_0_15() {
		return P_0_15;
	}

	public void setP_0_15(int p_0_15) {
		P_0_15 = p_0_15;
	}

	public int getP_16_64() {
		return P_16_64;
	}

	public void setP_16_64(int p_16_64) {
		P_16_64 = p_16_64;
	}

	public int getP_over_65() {
		return P_over_65;
	}

	public void setP_over_65(int p_over_65) {
		P_over_65 = p_over_65;
	}

	public int getE_0_15() {
		return E_0_15;
	}

	public void setE_0_15(int e_0_15) {
		E_0_15 = e_0_15;
	}

	public int getE_16_64() {
		return E_16_64;
	}

	public void setE_16_64(int e_16_64) {
		E_16_64 = e_16_64;
	}

	public int getE_over_65() {
		return E_over_65;
	}

	public void setE_over_65(int e_over_65) {
		E_over_65 = e_over_65;
	}

	public int getD_0_15() {
		return D_0_15;
	}

	public void setD_0_15(int d_0_15) {
		D_0_15 = d_0_15;
	}

	public int getD_16_64() {
		return D_16_64;
	}

	public void setD_16_64(int d_16_64) {
		D_16_64 = d_16_64;
	}

	public int getD_over_65() {
		return D_over_65;
	}

	public void setD_over_65(int d_over_65) {
		D_over_65 = d_over_65;
	}

	public int getM_saltuari() {
		return M_saltuari;
	}

	public void setM_saltuari(int m_saltuari) {
		M_saltuari = m_saltuari;
	}

	public int getP_saltuari() {
		return P_saltuari;
	}

	public void setP_saltuari(int p_saltuari) {
		P_saltuari = p_saltuari;
	}

	public int getM_donne() {
		return M_donne;
	}

	public void setM_donne(int m_donne) {
		M_donne = m_donne;
	}

	public int getM_migranti() {
		return M_migranti;
	}

	public void setM_migranti(int m_migranti) {
		M_migranti = m_migranti;
	}

	public int getM_disabili() {
		return M_disabili;
	}

	public void setM_disabili(int m_disabili) {
		M_disabili = m_disabili;
	}

	public int getM_senzaFissaDimora() {
		return M_senzaFissaDimora;
	}

	public void setM_senzaFissaDimora(int m_senzaFissaDimora) {
		M_senzaFissaDimora = m_senzaFissaDimora;
	}

	public int getP_donne() {
		return P_donne;
	}

	public void setP_donne(int p_donne) {
		P_donne = p_donne;
	}

	public int getP_migranti() {
		return P_migranti;
	}

	public void setP_migranti(int p_migranti) {
		P_migranti = p_migranti;
	}

	public int getP_disabili() {
		return P_disabili;
	}

	public void setP_disabili(int p_disabili) {
		P_disabili = p_disabili;
	}

	public int getP_senzaFissaDimora() {
		return P_senzaFissaDimora;
	}

	public void setP_senzaFissaDimora(int p_senzaFissaDimora) {
		P_senzaFissaDimora = p_senzaFissaDimora;
	}

	public int getE_donne() {
		return E_donne;
	}

	public void setE_donne(int e_donne) {
		E_donne = e_donne;
	}

	public int getE_migranti() {
		return E_migranti;
	}

	public void setE_migranti(int e_migranti) {
		E_migranti = e_migranti;
	}

	public int getE_disabili() {
		return E_disabili;
	}

	public void setE_disabili(int e_disabili) {
		E_disabili = e_disabili;
	}

	public int getE_senzaFissaDimora() {
		return E_senzaFissaDimora;
	}

	public void setE_senzaFissaDimora(int e_senzaFissaDimora) {
		E_senzaFissaDimora = e_senzaFissaDimora;
	}

	public int getD_donne() {
		return D_donne;
	}

	public void setD_donne(int d_donne) {
		D_donne = d_donne;
	}

	public int getD_migranti() {
		return D_migranti;
	}

	public void setD_migranti(int d_migranti) {
		D_migranti = d_migranti;
	}

	public int getD_disabili() {
		return D_disabili;
	}

	public void setD_disabili(int d_disabili) {
		D_disabili = d_disabili;
	}

	public int getD_senzaFissaDimora() {
		return D_senzaFissaDimora;
	}

	public void setD_senzaFissaDimora(int d_senzaFissaDimora) {
		D_senzaFissaDimora = d_senzaFissaDimora;
	}

	public int getU_donne() {
		return U_donne;
	}

	public void setU_donne(int u_donne) {
		U_donne = u_donne;
	}

	public int getU_migranti() {
		return U_migranti;
	}

	public void setU_migranti(int u_migranti) {
		U_migranti = u_migranti;
	}

	public int getU_disabili() {
		return U_disabili;
	}

	public void setU_disabili(int u_disabili) {
		U_disabili = u_disabili;
	}

	public int getU_senzaFissaDimora() {
		return U_senzaFissaDimora;
	}

	public void setU_senzaFissaDimora(int u_senzaFissaDimora) {
		U_senzaFissaDimora = u_senzaFissaDimora;
	}

	public boolean isAccoglienza() {
		return accoglienza;
	}

	public void setAccoglienza(boolean accoglienza) {
		this.accoglienza = accoglienza;
	}

	public boolean isInformazione() {
		return informazione;
	}

	public void setInformazione(boolean informazione) {
		this.informazione = informazione;
	}

	public boolean isAccompagnamento() {
		return accompagnamento;
	}

	public void setAccompagnamento(boolean accompagnamento) {
		this.accompagnamento = accompagnamento;
	}

	public boolean isSpsicologico() {
		return spsicologico;
	}

	public void setSpsicologico(boolean spsicologico) {
		this.spsicologico = spsicologico;
	}

	public boolean isEducativa() {
		return educativa;
	}

	public void setEducativa(boolean educativa) {
		this.educativa = educativa;
	}

	public boolean isConsulenza() {
		return consulenza;
	}

	public void setConsulenza(boolean consulenza) {
		this.consulenza = consulenza;
	}

	public boolean isSscolastico() {
		return sscolastico;
	}

	public void setSscolastico(boolean sscolastico) {
		this.sscolastico = sscolastico;
	}

	public boolean isSorientamento() {
		return sorientamento;
	}

	public void setSorientamento(boolean sorientamento) {
		this.sorientamento = sorientamento;
	}

	public boolean isPrimaassistenza() {
		return primaassistenza;
	}

	public void setPrimaassistenza(boolean primaassistenza) {
		this.primaassistenza = primaassistenza;
	}

	public boolean isTutela() {
		return tutela;
	}

	public void setTutela(boolean tutela) {
		this.tutela = tutela;
	}

	public String getAltroTtxt() {
		return altroTtxt;
	}

	public void setAltroTtxt(String altroTtxt) {
		this.altroTtxt = altroTtxt;
	}

	public String getDataFirma() {
		return dataFirma;
	}

	public void setDataFirma(String dataFirma) {
		this.dataFirma = dataFirma;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConvenzioneFrom2017 [idConvenzione=");
		builder.append(idConvenzione);
		builder.append(", idEnte=");
		builder.append(idEnte);
		builder.append(", idRappLegaleEnte=");
		builder.append(idRappLegaleEnte);
		builder.append(", idSedeLegaleEnte=");
		builder.append(idSedeLegaleEnte);
		builder.append(", idSedeOperativaEnte=");
		builder.append(idSedeOperativaEnte);
		builder.append(", idSedeMagazzinoEnte=");
		builder.append(idSedeMagazzinoEnte);
		builder.append(", anno=");
		builder.append(anno);
		builder.append(", M_totIndigenti=");
		builder.append(M_totIndigenti);
		builder.append(", P_totIndigenti=");
		builder.append(P_totIndigenti);
		builder.append(", E_totIndigenti=");
		builder.append(E_totIndigenti);
		builder.append(", D_totIndigenti=");
		builder.append(D_totIndigenti);
		builder.append(", U_totIndigenti=");
		builder.append(U_totIndigenti);
		builder.append(", cellaFrigo=");
		builder.append(cellaFrigo);
		builder.append(", tipoPartner=");
		builder.append(tipoPartner);
		builder.append(", M_0_15=");
		builder.append(M_0_15);
		builder.append(", M_16_64=");
		builder.append(M_16_64);
		builder.append(", M_over_65=");
		builder.append(M_over_65);
		builder.append(", P_0_15=");
		builder.append(P_0_15);
		builder.append(", P_16_64=");
		builder.append(P_16_64);
		builder.append(", P_over_65=");
		builder.append(P_over_65);
		builder.append(", E_0_15=");
		builder.append(E_0_15);
		builder.append(", E_16_64=");
		builder.append(E_16_64);
		builder.append(", E_over_65=");
		builder.append(E_over_65);
		builder.append(", D_0_15=");
		builder.append(D_0_15);
		builder.append(", D_16_64=");
		builder.append(D_16_64);
		builder.append(", D_over_65=");
		builder.append(D_over_65);
		builder.append(", M_saltuari=");
		builder.append(M_saltuari);
		builder.append(", P_saltuari=");
		builder.append(P_saltuari);
		builder.append(", M_donne=");
		builder.append(M_donne);
		builder.append(", M_migranti=");
		builder.append(M_migranti);
		builder.append(", M_disabili=");
		builder.append(M_disabili);
		builder.append(", M_senzaFissaDimora=");
		builder.append(M_senzaFissaDimora);
		builder.append(", P_donne=");
		builder.append(P_donne);
		builder.append(", P_migranti=");
		builder.append(P_migranti);
		builder.append(", P_disabili=");
		builder.append(P_disabili);
		builder.append(", P_senzaFissaDimora=");
		builder.append(P_senzaFissaDimora);
		builder.append(", E_donne=");
		builder.append(E_donne);
		builder.append(", E_migranti=");
		builder.append(E_migranti);
		builder.append(", E_disabili=");
		builder.append(E_disabili);
		builder.append(", E_senzaFissaDimora=");
		builder.append(E_senzaFissaDimora);
		builder.append(", D_donne=");
		builder.append(D_donne);
		builder.append(", D_migranti=");
		builder.append(D_migranti);
		builder.append(", D_disabili=");
		builder.append(D_disabili);
		builder.append(", D_senzaFissaDimora=");
		builder.append(D_senzaFissaDimora);
		builder.append(", U_donne=");
		builder.append(U_donne);
		builder.append(", U_migranti=");
		builder.append(U_migranti);
		builder.append(", U_disabili=");
		builder.append(U_disabili);
		builder.append(", U_senzaFissaDimora=");
		builder.append(U_senzaFissaDimora);
		builder.append(", accoglienza=");
		builder.append(accoglienza);
		builder.append(", informazione=");
		builder.append(informazione);
		builder.append(", accompagnamento=");
		builder.append(accompagnamento);
		builder.append(", spsicologico=");
		builder.append(spsicologico);
		builder.append(", educativa=");
		builder.append(educativa);
		builder.append(", consulenza=");
		builder.append(consulenza);
		builder.append(", sscolastico=");
		builder.append(sscolastico);
		builder.append(", sorientamento=");
		builder.append(sorientamento);
		builder.append(", primaassistenza=");
		builder.append(primaassistenza);
		builder.append(", tutela=");
		builder.append(tutela);
		builder.append(", altroTtxt=");
		builder.append(altroTtxt);
		builder.append(", dataFirma=");
		builder.append(dataFirma);
		builder.append("]");
		return builder.toString();
	}

	public int getM_TotaleAssistitiContinuativi() {
		int result;
		result=M_0_15+M_16_64+M_over_65+M_saltuari;
		return result;
	}
	public int getD_TotaleAssistitiContinuativi() {
		int result;
		result=D_0_15+D_16_64+D_over_65;
		return result;
	}
	public int getE_TotaleAssistitiContinuativi() {
		int result;
		result=E_0_15+E_16_64+E_over_65;
		return result;
	}
	public int getP_TotaleAssistitiContinuativi() {
		int result;
		result=P_0_15+P_16_64+P_over_65+P_saltuari;
		return result;
	}

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}


    

}
