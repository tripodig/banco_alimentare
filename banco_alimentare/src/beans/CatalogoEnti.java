//by Peppet
package beans;



import java.util.StringTokenizer;
import java.util.Vector;

import util.DBManager;

public class CatalogoEnti {

	DBManager db;
	
	public CatalogoEnti()
	{
		db=new DBManager();
	}
	public CatalogoEnti(DBManager db)
	{
		this.db=db;
	}
	
	public boolean mostraEnte(Ente e)
	{
		boolean esito;
		String query = "UPDATE ente set visibile=1 WHERE idente = " +e.getIdEnte();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean nascondiEnte(Ente e)
	{
		boolean esito;
		String query = "UPDATE ente set visibile=0 WHERE idente = " +e.getIdEnte();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public Ente getEnte(int idEnte)
	{
		Ente res = null;
		String query = "SELECT * FROM ente WHERE idente = "+idEnte;
		Vector<Object> v =db.executeSelect(query, "Ente");
		if(v.size()!=0){
			res = new Ente();
			res = (Ente)v.get(0);
		}
		return res;
	}
	
	public int getidEntebycodStruttura(int codicestruttura)
	{
		Ente res = null;
		String query = "SELECT * FROM ente WHERE codiceStruttura = "+codicestruttura;
		Vector<Object> v =db.executeSelect(query, "Ente");
		if(v.size()!=0){
			res = new Ente();
			res = (Ente)v.get(0);
			System.out.println(res.getIdEnte());
			return res.getIdEnte();
		}
		else
		
			return -1;
	}
	
	public int newCodiceStruttura()
	{
		Ente res = null;
		String query = "SELECT DISTINCT * FROM ente order by codiceStruttura desc LIMIT 1";
		Vector<Object> v =db.executeSelect(query, "Ente");
		if(v.size()!=0){
			res = new Ente();
			res = (Ente)v.get(0);
			System.out.println(res.getCodiceStruttura());
			return res.getCodiceStruttura()+1;
		}
		else
		
			return -1;
	}
	
	public int getcodStrutturabyidEnte(int idEnte)
	{
		Ente res = null;
		String query = "SELECT * FROM ente WHERE idEnte = "+idEnte;
		Vector<Object> v =db.executeSelect(query, "Ente");
		if(v.size()!=0){
			res = new Ente();
			res = (Ente)v.get(0);
			
			return res.getCodiceStruttura();
		}
		else
		
			return -1;
	}
	
	public Vector<Ente> getEnti()
	{
		Vector<Ente> res = new Vector<Ente>();
		String query = "SELECT * FROM ente where visibile ='1' ORDER BY Nome";
		Vector<Object> supp = db.executeSelect(query, "Ente");
		for(int i=0;i<supp.size();i++)
			res.add((Ente)supp.get(i));
		
		return res;
	}
	
	public Vector<Ente> getEntiProvincia(String provincia)
	{
		Vector<Ente> res = new Vector<Ente>();
		String query = "SELECT e.* FROM ente e "
				+ "where e.idente in (Select distinct(e1.idEnte) "
				+ "from ente e1, sede s1 where e1.idente=s1.idEnte "
				+ "and s1.provincia='"+provincia+"' and e1.visibile ='1' and s1.visibile='1') "
				+ "ORDER BY Nome";
		Vector<Object> supp = db.executeSelect(query, "Ente");
		for(int i=0;i<supp.size();i++)
			res.add((Ente)supp.get(i));
		return res;
	}
	
	
	public Vector<Ente> getEntiByParteNome(String parteNome)
	{
		Vector<Ente> all = getEnti();
		Vector<Ente> res = new Vector<Ente>();
		
		for(Ente e:all){
			
			if(e.getNome().toUpperCase().contains(parteNome.toUpperCase())){
				res.add(e);
			}
		}
		
		return res;
	}
	
	
	public Vector<Ente> getEntiProvince(String provincia)
	{
		Vector<Ente> allAree = new Vector<Ente>();
		
		StringTokenizer st= new StringTokenizer(provincia,",");
		
		Vector<Ente> res = null;
		while(st.hasMoreTokens()){
			res=null;
			res=getEntiProvincia(st.nextToken());
			allAree.addAll(res);
			
		}
		
		return allAree;
		

	}
	
	public boolean salvaEnte(Ente e)
	{
		boolean esito;
				
		String query = "INSERT INTO ente (Nome, Descrizione, PartitaIva, CodiceStruttura, visibile ) VALUES ( '"+e.getNome()+"','"+e.getDescrizione()+"','"+e.getPartitaIva()+"',"+e.getCodiceStruttura()+",'1' )";
		esito = db.executeUpdate(query);
		return esito;
	}
	public boolean aggiornaEnte(Ente e)
	{
		boolean esito;
				
		String query = "Update ente set Nome= '"+e.getNome()+"',Descrizione='"+e.getDescrizione()+"', PartitaIva='"+e.getPartitaIva()+"', CodiceStruttura= "+e.getCodiceStruttura()+", CodiceSap= "+e.getCodiceSap()+"  where idEnte="+e.getIdEnte();
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean eliminaEnte(Ente e)
	{
		boolean esito=false;
		String query = "DELETE FROM ente WHERE idente = " +e.getIdEnte();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	/*
	 * eventualmente inserire altri metodi
	 */
	

}
