package beans;

import java.util.*;

import util.DBManager;

public class DAO_Stat1 {
	DBManager db;
	
	
	public DAO_Stat1() {
		db=new DBManager();
	}

	public DAO_Stat1(DBManager db) {
		this.db = db;
	}




	
	public Vector<Stat1> getStatgiornata(int idEvento)
	{
		Vector<Stat1> res = new Vector<Stat1>();
		
		String query = "select "
				+ " t.ora AS ORA,"
				+ " sum(t1.PESO) AS Totale,"
				+ " t.AREA AS AREA"
				+ " from"
				+ " ((select "
				+ " m.ora AS ora,"
				+ " sum((((((((((((m.peso_tot_carne + m.peso_tot_infanzia) + m.peso_tot_latte) + m.peso_tot_biscotti) + m.peso_tot_legumi) + m.peso_tot_olio) + m.peso_tot_omogeneizzati) + m.peso_tot_pasta) + m.peso_tot_pelati) + m.peso_tot_riso) + m.peso_tot_tonno) + m.peso_tot_varie)) AS PESO,"
				+ " p.area AS AREA"
				+ " from"
				+ " (modb m"
				+ " join puntovendita p)"
				+ " where"
				+ " (m.idPuntoVendita = p.idPuntovendita and m.idevento="+idEvento+")"
				+ " group by p.area , m.ora"
				+ " order by p.area , m.ora) t"
				+ " join (select "
				+ " m.ora AS ora,"
				+ " sum((((((((((((m.peso_tot_carne + m.peso_tot_infanzia) + m.peso_tot_latte) + m.peso_tot_biscotti) + m.peso_tot_legumi) + m.peso_tot_olio) + m.peso_tot_omogeneizzati) + m.peso_tot_pasta) + m.peso_tot_pelati) + m.peso_tot_riso) + m.peso_tot_tonno) + m.peso_tot_varie)) AS PESO,"
				+ " p.area AS AREA"
				+ " from"
				+ " (modb m"
				+ " join puntovendita p)"
				+ " where"
				+ " (m.idPuntoVendita = p.idPuntovendita and m.idevento="+idEvento+")"
				+ " group by p.area , m.ora"
				+ " order by p.area , m.ora) t1)"
				+ " where"
				+ " ((t.AREA = t1.AREA)"
				+ " and (t.ora >= t1.ora))"
				+ " group by t.ora , t.AREA"
				+ " order by t.AREA , t.ora";
		
		System.out.println(query);
		Vector<Object> supp = db.executeSelect(query, "Stat1");
		
		for(int i=0;i<supp.size();i++)
			res.add((Stat1)supp.get(i));
		
		return res;

	}
	
	
	public Vector<Stat1> getStatgiornata(int idEvento, String area)
	{
		Vector<Stat1> res = new Vector<Stat1>();
		Vector<Stat1> res0 = new Vector<Stat1>();
		res0=getStatgiornata(idEvento);
		for (int i=0;i<res0.size();i++)
			if(res0.get(i).getAREA().equals(area))
				res.add(res0.get(i));
		
		
		return res;

	}
	

}
