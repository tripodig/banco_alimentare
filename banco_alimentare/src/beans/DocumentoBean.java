package beans;


import java.util.Map;

public class DocumentoBean {
	
	private int idDocumento;
	private int idConvenzione;
    private String tipoDocumento;
    private Map<String, String> campi;
    
	public int getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(int idDocumento) {
		this.idDocumento = idDocumento;
	}
	public int getIdConvenzione() {
		return idConvenzione;
	}
	public void setIdConvenzione(int idConvenzione) {
		this.idConvenzione = idConvenzione;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Map<String, String> getCampi() {
		return campi;
	}
	public void setCampi(Map<String, String> listaCampi) {
		this.campi = (Map<String, String>) listaCampi;
	}

	    

    
    
    

}
