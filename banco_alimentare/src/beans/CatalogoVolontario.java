package beans;

import java.util.Vector;

import util.DBManager;

public class CatalogoVolontario {

	DBManager db;

	public CatalogoVolontario()
	{
		db=new DBManager();
	}
//	public CatalogoVolontario(DBManager db)
//	{
//		this.db=db;
//	}

	public boolean salvaVolontario(Volontario u)
	{
		boolean esito=false;

		String query = "INSERT INTO Volontari (idVolontario, nome, cognome, indirizzo, cap, citta, telefono, dataNascita, occupazione, email) VALUES ( " +u.getIdVolontario()+" ,'"+u.getNome()+"' ,'"+u.getCognome()+"', '"+u.getIndirizzo()+"','"+u.getCap()+"','"+u.getCitta()+"','"+u.getTelefono()+"', '"+u.getDataNascita()+"','"+u.getOccupazione()+"','"+u.getEmail()+"')";
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}

	public boolean eliminaVolontario(Volontario u)
	{
		boolean esito=false;
		String query = "DELETE FROM Volontari WHERE idVolontario = " +u.getIdVolontario();
		esito = db.executeUpdate(query);
		return esito;
	}

	public Vector<Volontario> getVolontarioFromNomeCognome(String nome, String cognome) {
		Vector<Volontario> res = null;
		String query = "SELECT * FROM Volontari WHERE nome = '"+nome+"' and cognome='"+cognome+"'";
		Vector<Object> v =db.executeSelect(query, "Volontario");
		
		if(v.size()!=0){ // in questo caso ho trovato almeno un volontario e posso istanziare il vettore dei volontari, altrimenti non lo istanzio
			res = new Vector<Volontario>();
			for(int i=0; i<v.size();i++)
			
			res.add((Volontario)v.get(i));
		}
		return res;
	}
	
	public Vector<Volontario> getVolontari() {
		Vector<Volontario> res = null;
		String query = "SELECT * FROM Volontari ";
		Vector<Object> v =db.executeSelect(query, "Volontario");
		
		if(v.size()!=0){ // in questo caso ho trovato almeno un volontario e posso istanziare il vettore dei volontari, altrimenti non lo istanzio
			res = new Vector<Volontario>();
			for(int i=0; i<v.size();i++)
			
			res.add((Volontario)v.get(i));
		}
		return res;
	}


//	public String getNomeVolontario(int idVolontario)
//	{
//		Volontario res = new Volontario();
//		String nome="";
//		String query = "SELECT * FROM Volontario WHERE idVolontario = "+idVolontario;
//		Vector<Object> v =db.executeSelect(query, "Volontario");
//		res = (Volontario)v.get(0);
//		nome=res.getNome();
//		return nome;
//	}
//	public String getCognomeVolontario(int idVolontario)
//	{
//		Volontario res = new Volontario();
//		String cognome="";
//		String query = "SELECT * FROM Volontario WHERE idVolontario = "+idVolontario;
//		Vector<Object> v =db.executeSelect(query, "Volontario");
//		res = (Volontario)v.get(0);
//		cognome=res.getCognome();
//		return cognome;
//	}


	public boolean aggiornaVolontario(Volontario u){

		boolean esito=false;
		String query = " UPDATE Volontari SET nome='"+u.getNome()+"', cognome='"+u.getCognome()+"', indirizzo='"+u.getIndirizzo()+"', cap='"+u.getCap()+"', citta='"+u.getCitta()+"', telefono='"+u.getTelefono()+"', dataNascita='"+u.getDataNascita()+"', occupazione='"+u.getOccupazione()+"', email='"+u.getEmail()+"' WHERE idVolontario="+u.getIdVolontario();
		esito =db.executeUpdate(query);
		return esito;
	}


//	public boolean getUsernameEsistente(String username)
//	{
//
//		String query = "SELECT COUNT(*) FROM Volontario WHERE username='"+username+"'";
//		Vector<Object> supp = db.executeSelect(query, "Count");
//		Integer res =(Integer)supp.get(0);
//		int cont=res.intValue();
//		if(cont==0)
//			return false;
//		else
//			return true;
//	}

}
