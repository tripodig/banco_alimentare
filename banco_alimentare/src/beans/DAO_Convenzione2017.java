package beans;

import java.util.StringTokenizer;
import java.util.Vector;

import util.DBManager;

public class DAO_Convenzione2017 {
	DBManager db;
	
	
	public DAO_Convenzione2017() {
		{
			db=new DBManager();
		}
	}

	public boolean salva(ConvenzioneFrom2017 c)
	{
		boolean esito=false;

		String query = "INSERT INTO convenzione_from_2017 (idEnte, idRappLegaleEnte, idSedeLegaleEnte, idSedeMagazzinoEnte, idSedeOperativaEnte, dataFirma, " +
				" anno, cellaFrigo, tipoPartner, " +
				" M_0_15, M_16_64, M_disabili, M_migranti_minoranze, M_over_65, M_saltuari, M_senza_fissa_dimora, M_totIndigenti, M_donne," +
				" P_0_15, P_16_64, P_disabili, P_migranti_minoranze, P_over_65, P_saltuari, P_senza_fissa_dimora, P_totIndigenti, P_donne," +
				" D_0_15, D_16_64, D_disabili, D_migranti_minoranze, D_over_65, D_senza_fissa_dimora, D_totIndigenti, D_donne," +
				" E_0_15, E_16_64, E_disabili, E_migranti_minoranze, E_over_65, E_senza_fissa_dimora, E_totIndigenti, E_donne," +
				" U_disabili, U_migranti_minoranze, U_senza_fissa_dimora, U_totIndigenti, U_donne, "+
				" tipologia, "+
				" accoglienza, accompagnamento, consulenza,educativa, informazione, " +
				" primaassistenza, sorientamento, spsicologico, sscolastico, tutela, altro, visibile)"
			+"VALUES("
				+c.getIdEnte()+","
				+ " "+c.getIdRappLegaleEnte()+","
				+ " "+c.getIdSedeLegaleEnte()+","
				+ " "+c.getIdSedeMagazzinoEnte()+","
				+ " "+c.getIdSedeOperativaEnte()+","
				+ "'"+c.getDataFirma()+"',"
				
				+ " "+c.getAnno()+","
				+ " "+c.isCellaFrigo()+","
				+ "'"+c.getTipoPartner()+"',"
				
				+ " "+c.getM_0_15()+","
				+ " "+c.getM_16_64()+","
				+ " "+c.getM_disabili()+","
				+ " "+c.getM_migranti()+","
				+ " "+c.getM_over_65()+","
				+ " "+c.getM_saltuari()+","
				+ " "+c.getM_senzaFissaDimora()+","
				+ " "+c.getM_totIndigenti()+","
				+ " "+c.getM_donne()+","
				
				+ " "+c.getP_0_15()+","
				+ " "+c.getP_16_64()+","
				+ " "+c.getP_disabili()+","
				+ " "+c.getP_migranti()+","
				+ " "+c.getP_over_65()+","
				+ " "+c.getP_saltuari()+","
				+ " "+c.getP_senzaFissaDimora()+","
				+ " "+c.getP_totIndigenti()+","
				+ " "+c.getP_donne()+","
				
				+ " "+c.getE_0_15()+","
				+ " "+c.getE_16_64()+","
				+ " "+c.getE_disabili()+","
				+ " "+c.getE_migranti()+","
				+ " "+c.getE_over_65()+","
				+ " "+c.getE_senzaFissaDimora()+","
				+ " "+c.getE_totIndigenti()+","
				+ " "+c.getE_donne()+","
				
				+ " "+c.getD_0_15()+","
				+ " "+c.getD_16_64()+","
				+ " "+c.getD_disabili()+","
				+ " "+c.getD_migranti()+","
				+ " "+c.getD_over_65()+","
				+ " "+c.getD_senzaFissaDimora()+","
				+ " "+c.getD_totIndigenti()+","
				+ " "+c.getD_donne()+","
				
				+ " "+c.getU_disabili()+","
				+ " "+c.getU_migranti()+","
				+ " "+c.getU_senzaFissaDimora()+","
				+ " "+c.getU_totIndigenti()+","
				+ " "+c.getU_donne()+","
				
				+ " '"+c.getTipologia()+"',"
				
				+ " "+c.isAccoglienza()+","
				+ " "+c.isAccompagnamento()+","
				+ " "+c.isConsulenza()+","
				+ " "+c.isEducativa()+","
				+ " "+c.isInformazione()+","
				+ " "+c.isPrimaassistenza()+","
				+ " "+c.isSorientamento()+","
				+ " "+c.isSpsicologico()+","
				+ " "+c.isSscolastico()+","
				+ " "+c.isTutela()+","
				+ " '"+c.getAltroTtxt()+"',1)";

		
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public ConvenzioneFrom2017 getConvenzione(int idConvenzione)
	{
		ConvenzioneFrom2017 res = new ConvenzioneFrom2017();
		String query = "SELECT * FROM convenzione_from_2017 WHERE idconvenzione = "+idConvenzione;
		Vector<Object> v =db.executeSelect(query, "ConvenzioneFrom2017");
		res =(ConvenzioneFrom2017)v.get(0);
		res.toString();
		return res;
	}
	
	public ConvenzioneFrom2017 getConvenzioneEnte(int idEnte){
		
		ConvenzioneFrom2017 res = null;
		String query = "SELECT * FROM convenzione_from_2017 WHERE idente = "+idEnte+"ORDER BY ANNO desc";
		Vector<Object> v =db.executeSelect(query, "ConvenzioneFrom2017");
		if(v.size()!=0){
			res = new ConvenzioneFrom2017();
			res =(ConvenzioneFrom2017)v.get(0);
			} 
			                        
			return res;
		
	}
	public int getidconvenzione(int idEnte, int anno){
		
		int res = 0;
		ConvenzioneFrom2017 c=null;
		String query = "SELECT * FROM convenzione_from_2017 WHERE idente = "+idEnte+" and anno="+anno;
		Vector<Object> supp =db.executeSelect(query, "ConvenzioneFrom2017");
		if(supp.size()!=0){
			c=(ConvenzioneFrom2017)supp.get(0);
			res=c.getIdConvenzione();
			} 
			                        
			return res;
		
	}
	public Vector<ConvenzioneFrom2017> getConvenzioniEnte(int idEnte){
		
		Vector<ConvenzioneFrom2017> res = null;
		String query = "SELECT * FROM convenzione_from_2017 WHERE idente = "+idEnte+" order by anno desc, datafirma desc";
		Vector<Object> v =db.executeSelect(query, "ConvenzioneFrom2017");
		if(v.size()!=0){
			res = new Vector<ConvenzioneFrom2017>();
			for(int i=0;i<v.size();i++)
				res.add((ConvenzioneFrom2017)v.get(i));
			} 
			                        
			return res;
		
	}
	public Vector<ConvenzioneFrom2017> getConvenzioni(int anno, String provincia) {
		// TODO Auto-generated method stub
		Vector<ConvenzioneFrom2017> res = null;
		String query = "SELECT c.*"
				+ " FROM convenzione_from_2017 c"
				+ " WHERE c.anno="+anno+""
				+ " and c.idEnte in (Select distinct(e1.idEnte)"
									+ "	from ente e1, sede s1 where e1.idente=s1.idEnte"
									+ "	and s1.provincia='"+provincia+"' "
									+ "and e1.visibile ='1' and s1.visibile='1')";
		Vector<Object> v =db.executeSelect(query, "ConvenzioneFrom2017");
		if(v.size()!=0){
			res = new Vector<ConvenzioneFrom2017>();
			for(int i=0;i<v.size();i++)
				res.add((ConvenzioneFrom2017)v.get(i));
			} 
			                        
			return res;
	}
	public Vector<ConvenzioneFrom2017> getallConvenzioni(int anno, String provincia)
	{
		Vector<ConvenzioneFrom2017> allProvince = new Vector<ConvenzioneFrom2017>();
		
		StringTokenizer st= new StringTokenizer(provincia,",");
		
		Vector<ConvenzioneFrom2017> res = null;
		while(st.hasMoreTokens()){
			res=null;
			res=getConvenzioni(anno, st.nextToken());
			if (res!=null)
			allProvince.addAll(res);
			
		}
		
		return allProvince;
		

	}
	
	public Vector<ConvenzioneFrom2017> getConvenzioniAnno(int anno)
	{
		Vector<ConvenzioneFrom2017> res = new Vector<ConvenzioneFrom2017>();
		String query = "SELECT * FROM convenzione_from_2017 WHERE anno="+anno;
		Vector<Object> supp = db.executeSelect(query, "ConvenzioneFrom2017");
		for(int i=0;i<supp.size();i++)
			res.add((ConvenzioneFrom2017)supp.get(i));
		System.out.println( "ConvenzioneFrom2017 x anno "+anno+" prelevate ="+supp.size());	
		
		return res;
	}
	
	public boolean isCancellable(int idConvenzione)
	{
		
		String query = "SELECT count(*)"
				+ " FROM documento "
				+ " WHERE idConvenzione="+idConvenzione+"";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont==0)
			return true;
		else
			return false;
	}
	
	public boolean eliminaConvenzione(ConvenzioneFrom2017 conv)
	{
		boolean esito;
		String query = "DELETE FROM convenzione_from_2017 WHERE idconvenzione = " +conv.getIdConvenzione();
		esito = db.executeUpdate(query);
		return esito;
	}
}
