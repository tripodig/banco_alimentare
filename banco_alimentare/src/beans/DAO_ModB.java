package beans;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import util.DBManager;

public class DAO_ModB {
	DBManager db;
	
	
	public DAO_ModB() {
		db=new DBManager();
	}

	public DAO_ModB(DBManager db) {
		this.db = db;
	}

	public ModB getTotaleComplessivo(int idEvento)
	{
		ModB res = new ModB();
		String query = "SELECT 1 as idScheda, 23 as ora, 1 as idPuntoVendita, 1 as idCapoEquipe, 1 as numScheda," +
					"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio," +
					"sum(peso_tot_omogeneizzati)as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati)as scatoli_tot_omogeneizzati," +
					"sum(peso_tot_infanzia)as peso_tot_infanzia,sum(scatoli_tot_infanzia)as scatoli_tot_infanzia," +
					"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno," +
					"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne," +
					"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati," +
					"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi," +
					"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta," +
					"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso," +
					"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero," +
					"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte," +
					"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
					"sum(peso_tot_varie) as peso_tot_varie, sum(scatoli_tot_varie) as scatoli_tot_varie" +
					" FROM modb m, puntovendita pdv where m.idPuntoVendita=pdv.idpuntovendita and pdv.visibile=1 and m.idEvento="+idEvento;
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		
			res=(ModB)v.get(0);

		return res;
	}
	
	
	
	public ModB getTotaleComplessivoByArea(String area)
	{
		ModB res = new ModB();
		String query = "SELECT 1 as idScheda, 23 as ora, 1 as idPuntoVendita, 1 as idCapoEquipe, 1 as numScheda," +
					"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio," +
					"sum(peso_tot_omogeneizzati)as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati)as scatoli_tot_omogeneizzati," +
					"sum(peso_tot_infanzia)as peso_tot_infanzia,sum(scatoli_tot_infanzia)as scatoli_tot_infanzia," +
					"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno," +
					"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne," +
					"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati," +
					"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi," +
					"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta," +
					"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso," +
					"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero," +
					"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte," +
					"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
					"sum(peso_tot_varie) as peso_tot_varie, sum(scatoli_tot_varie) as scatoli_tot_varie" +
					" FROM modb m, puntovendita pdv WHERE m.idPuntoVendita=pdv.idpuntovendita and pdv.area=?";// e m.area=area
		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		
			res=(ModB)v.get(0);

		return res;
	}
	
	public ModB getTotaleComplessivoByArea(String area, int idEvento)
	{
		ModB res = new ModB();
		String query = "SELECT 1 as idScheda, 23 as ora, 1 as idPuntoVendita, 1 as idCapoEquipe, 1 as numScheda," +
					"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio," +
					"sum(peso_tot_omogeneizzati)as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati)as scatoli_tot_omogeneizzati," +
					"sum(peso_tot_infanzia)as peso_tot_infanzia,sum(scatoli_tot_infanzia)as scatoli_tot_infanzia," +
					"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno," +
					"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne," +
					"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati," +
					"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi," +
					"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta," +
					"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso," +
					"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero," +
					"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte," +
					"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
					"sum(peso_tot_varie) as peso_tot_varie, sum(scatoli_tot_varie) as scatoli_tot_varie" +
					" FROM modb m, puntovendita pdv WHERE m.idEvento="+idEvento+" and m.idPuntoVendita=pdv.idpuntovendita and pdv.visibile=1 and pdv.area=?";// e m.area=area
		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		
			res=(ModB)v.get(0);

		return res;
	}
	
	public Vector<ModB> getTotaleComplessivoByAree(String aree)
	{
		Vector<ModB> allAree = new Vector<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		ModB res = null;
		while(st.hasMoreTokens()){
			res=null;
			res=getTotaleComplessivoByArea(st.nextToken());
			allAree.add(res);
			
		}
		
		return allAree;
		

	}
	
	public Vector<ModB> getTotaleComplessivoByAree(String aree, int idEvento)
	{
		Vector<ModB> allAree = new Vector<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		ModB res = null;
		while(st.hasMoreTokens()){
			res=null;
			res=getTotaleComplessivoByArea(st.nextToken(), idEvento);
			allAree.add(res);
			
		}
		
		return allAree;
		

	}
	
	
	
	public ModB getTotBollePDV(int idPuntoVendita)
	{
		ModB res = new ModB();
		String query = "SELECT 1 as idScheda, 23 as ora, 1 as idPuntoVendita, 1 as idCapoEquipe, 1 as numScheda," +
					"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio," +
					"sum(peso_tot_omogeneizzati)as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati)as scatoli_tot_omogeneizzati," +
					"sum(peso_tot_infanzia)as peso_tot_infanzia,sum(scatoli_tot_infanzia)as scatoli_tot_infanzia," +
					"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno," +
					"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne," +
					"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati," +
					"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi," +
					"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta," +
					"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso," +
					"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero," +
					"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte," +
					"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
					"sum(peso_tot_varie) as peso_tot_varie, sum(scatoli_tot_varie) as scatoli_tot_varie" +
					" FROM modb where idPuntoVendita="+idPuntoVendita ;
		
		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		
			res=(ModB)v.get(0);

		return res;
	}
	
	public ModB getTotBollePDV(int idPuntoVendita, int idEvento)
	{
		ModB res = new ModB();
		String query = "SELECT 1 as idScheda, "+idEvento+" as idEvento, 23 as ora, 1 as idPuntoVendita, 1 as idCapoEquipe, 1 as numScheda," +
					"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio," +
					"sum(peso_tot_omogeneizzati)as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati)as scatoli_tot_omogeneizzati," +
					"sum(peso_tot_infanzia)as peso_tot_infanzia,sum(scatoli_tot_infanzia)as scatoli_tot_infanzia," +
					"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno," +
					"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne," +
					"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati," +
					"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi," +
					"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta," +
					"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso," +
					"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero," +
					"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte," +
					"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
					"sum(peso_tot_varie) as peso_tot_varie, sum(scatoli_tot_varie) as scatoli_tot_varie" +
					" FROM modb where idPuntoVendita="+idPuntoVendita+" and idEvento="+idEvento ;
		
		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		
			res=(ModB)v.get(0);

		return res;
	}
	
	
	//slecet solo su aree dell'utente
	public Vector<ModB> getTotali()
	{
		Vector<ModB> res = new Vector<ModB>();
//		ModB vuoto =new ModB();
		String query = "SELECT * FROM modb m, puntovendita pdv where m.idPuntoVendita=pdv.idpuntovendita " ;
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
//		if(v.size()==0)
//			res.add((ModB)vuoto);
//		else
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	public Vector<ModB> getTotaliPDV(int idPuntoVendita)
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT * FROM modb where idPuntoVendita="+idPuntoVendita+" order by numscheda" ;
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	public Vector<ModB> getTotaliPDV(int idPuntoVendita, int idEvento)
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT * FROM modb where idPuntoVendita="+idPuntoVendita+" and idEvento="+idEvento+" order by numscheda" ;
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	public Vector<Double> getTotaliModB(ModB mod)
	{
		Double totScatoli =(double) 0;
		Double totPeso=(double) 0;
		totScatoli=(double) (mod.getScatoli_tot_olio()+mod.getScatoli_tot_omogeinizzati()+
				mod.getScatoli_tot_infanzia()+mod.getScatoli_tot_tonno()+
				mod.getScatoli_tot_carne()+mod.getScatoli_tot_pelati()+
				mod.getScatoli_tot_legumi()+mod.getScatoli_tot_pasta()+
				mod.getScatoli_tot_riso()+mod.getScatoli_tot_zucchero()+
				mod.getScatoli_tot_latte()+mod.getScatoli_tot_biscotti()+mod.getScatoli_tot_varie());
		totPeso=(double) (mod.getPeso_tot_olio()+mod.getPeso_tot_omogeinizzati()+
				mod.getPeso_tot_infanzia()+mod.getPeso_tot_tonno()+
				mod.getPeso_tot_carne()+mod.getPeso_tot_pelati()+
				mod.getPeso_tot_legumi()+mod.getPeso_tot_pasta()+
				mod.getPeso_tot_riso()+mod.getPeso_tot_zucchero()+
				mod.getPeso_tot_latte()+mod.getPeso_tot_biscotti()+mod.getPeso_tot_varie());
		
	    totPeso=(double) (Math.round(totPeso * 100) / 100);//ultima modifica per arrotondamento a 2 decimali
		Vector<Double> out=new Vector<Double>();
		out.add(0, totPeso);
		out.add(1, totScatoli);
		return out;
		
		
	}
	public Vector<ModB> getModbSommati()
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita "+// e m.area=area
		"group by m.idpuntovendita";		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	public Vector<ModB> getModbSommati(int evento)
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita "+// e m.area=area
		"and m.idevento="+evento+
		"group by m.idpuntovendita";		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	//OCCORRE CICLARE SU TUTTE LE AREE DELL?UTENTE (area= Cs,provincia cs      USA STRING TOKENIZER E FAI WHILe)
	public Vector<ModB> getModbSommatiByArea(String area)//riceve String area
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita and pdv.area=? "+// e m.area=area
		"group by m.idpuntovendita";		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	public Vector<ModB> getModbSommatiByArea(String area, int idEvento)//riceve String area
	{
		Vector<ModB> res = new Vector<ModB>();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita and m.idEvento="+idEvento+" and pdv.visibile=1 and pdv.area=? "+// e m.area=area
		"group by m.idpuntovendita";		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		for(int i=0;i<v.size();i++)
			res.add((ModB)v.get(i));

		return res;
	}
	
	public Vector<ModB> getModbSommatiByAree(String aree)//riceve String aree
	{
Vector<ModB> allAree = new Vector<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		Vector<ModB> res = new Vector<ModB>();
		while(st.hasMoreTokens()){
			res.clear();
			res=getModbSommatiByArea(st.nextToken());
			allAree.addAll(res);
			
		}
		return allAree;
				
	}
	
	public Vector<ModB> getModbSommatiByAree(String aree, int idEvento)//riceve String aree
	{
Vector<ModB> allAree = new Vector<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		Vector<ModB> res = new Vector<ModB>();
		while(st.hasMoreTokens()){
			res.clear();
			res=getModbSommatiByArea(st.nextToken(), idEvento);
			allAree.addAll(res);
			
		}
		return allAree;
				
	}
	
	public ModB getSOMMAModbSommati()//riceve String area
	{
		ModB res = new ModB();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita";		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		res=(ModB)v.get(0);

		return res;
	}
	
	public ModB getSOMMAModbSommati(int evento)//riceve String area
	{
		ModB res = new ModB();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita"+
		"and m.idevento="+evento;		
		System.out.println(query);
		Vector<Object> v = db.executeSelect(query, "ModB");
		res=(ModB)v.get(0);

		return res;
	}
	
	public ModB getSOMMAModBSommatiByArea(String area)//riceve String area
	{
		ModB res = new ModB();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita and pdv.area=? ";// e m.area=area		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		res=(ModB)v.get(0);

		return res;
	}
	
	public ModB getSOMMAModBSommatiByArea(String area, int idEvento)//riceve String area
	{
		ModB res = new ModB();
		String query = "SELECT m.idscheda, m.ora, m.idpuntovendita, m.idcapoequipe, m.numscheda, "+
		"sum(peso_tot_olio) as peso_tot_olio, sum(scatoli_tot_olio) as scatoli_tot_olio,"+
		"sum(peso_tot_omogeneizzati) as peso_tot_omogeneizzati,sum(scatoli_tot_omogeneizzati) as scatoli_tot_omogeneizzati,"+
		"sum(peso_tot_infanzia) as peso_tot_infanzia,sum(scatoli_tot_infanzia) as scatoli_tot_infanzia,"+
		"sum(peso_tot_tonno) as peso_tot_tonno,sum(scatoli_tot_tonno) as scatoli_tot_tonno,"+
		"sum(peso_tot_carne) as peso_tot_carne,sum(scatoli_tot_carne) as scatoli_tot_carne,"+
		"sum(peso_tot_pelati) as peso_tot_pelati,sum(scatoli_tot_pelati) as scatoli_tot_pelati,"+
		"sum(peso_tot_legumi) as peso_tot_legumi,sum(scatoli_tot_legumi) as scatoli_tot_legumi,"+
		"sum(peso_tot_pasta) as peso_tot_pasta,sum(scatoli_tot_pasta) as scatoli_tot_pasta,"+
		"sum(peso_tot_riso) as peso_tot_riso,sum(scatoli_tot_riso) as scatoli_tot_riso,"+
		"sum(peso_tot_zucchero) as peso_tot_zucchero,sum(scatoli_tot_zucchero) as scatoli_tot_zucchero,"+
		"sum(peso_tot_latte) as peso_tot_latte,sum(scatoli_tot_latte) as scatoli_tot_latte,"+
		"sum(peso_tot_biscotti) as peso_tot_biscotti,sum(scatoli_tot_biscotti) as scatoli_tot_biscotti," +
		"sum(peso_tot_varie) as peso_tot_varie,sum(scatoli_tot_varie) as scatoli_tot_varie "+
		"FROM modb m, puntovendita pdv "+
		"where m.idPuntoVendita=pdv.idpuntovendita and m.idEvento="+idEvento+" and pdv.visibile=1 and pdv.area=? ";// e m.area=area		
		System.out.println(query);
		Vector<Object> v = db.executeSelectModBSommatiByArea(query, area);
		res=(ModB)v.get(0);

		return res;
	}
	
	
	
	public List<ModB> getSOMMAModBSommatiByAree(String aree)//riceve String area
	{
		LinkedList<ModB> modelliB= new LinkedList<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		ModB modelloB=null;
		
		while(st.hasMoreTokens()){
			modelloB=null;
			modelloB=getSOMMAModBSommatiByArea(st.nextToken());
			
			modelliB.add(modelloB);
			
		}
		return modelliB;
		
		/*Vector<ModB> allAree = new Vector<ModB>();
		
		
		
		Vector<ModB> res = new Vector<ModB>();
		*/
		
		
		
	}
	public List<ModB> getSOMMAModBSommatiByAree(String aree, int idEvento)//riceve String area
	{
		LinkedList<ModB> modelliB= new LinkedList<ModB>();
		
		StringTokenizer st= new StringTokenizer(aree,",");
		
		ModB modelloB=null;
		
		while(st.hasMoreTokens()){
			modelloB=null;
			modelloB=getSOMMAModBSommatiByArea(st.nextToken(), idEvento);
			
			modelliB.add(modelloB);
			
		}
		return modelliB;
		
		/*Vector<ModB> allAree = new Vector<ModB>();
		
		
		
		Vector<ModB> res = new Vector<ModB>();
		*/
		
		
		
	}

	public ModB getModB(int idModB)
	{
		ModB res = new ModB();
		String query = "SELECT * FROM modB WHERE idScheda = "+idModB;
		Vector<Object> v =db.executeSelect(query, "ModB");
		res = (ModB)v.get(0);
		return res;
	}
	
	public boolean salvaModB(ModB b)
	{
		boolean esito;
//		ModB(int idScheda, int ora, String numeroScheda, int idCapoEquipe,
//				int idPuntoVendita, double peso_tot_olio, int scatoli_tot_olio,
//				double peso_tot_omogeinizzati, int scatoli_tot_omogeinizzati,
//				double peso_tot_infanzia, int scatoli_tot_infanzia,
//				double peso_tot_tonno, int scatoli_tot_tonno,
//				double peso_tot_carne, int scatoli_tot_carne,
//				double peso_tot_pelati, int scatoli_tot_pelati,
//				double peso_tot_legumi, int scatoli_tot_legumi,
//				double peso_tot_pasta, int scatoli_tot_pasta, double peso_tot_riso,
//				int scatoli_tot_riso, double peso_tot_zucchero,
//				int scatoli_tot_zucchero, double peso_tot_latte,
//				int scatoli_tot_latte, double peso_tot_varie, int scatoli_tot_varie)	
		
		String query = "INSERT INTO ModB VALUES ( '?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?')";
		
		query=query.replaceFirst("[?]",((Integer)b.getIdScheda()).toString());
		query=query.replaceFirst("[?]",((Integer)b.getIdEvento()).toString());
		query=query.replaceFirst("[?]",((Integer)b.getOra()).toString());
		query=query.replaceFirst("[?]",((Integer)b.getIdPuntoVendita()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getIdCapoEquipe()).toString());	
		
		query=query.replaceFirst("[?]",(b.getNumeroScheda()));	
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_olio()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_olio()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_omogeinizzati()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_omogeinizzati()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_infanzia()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_infanzia()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_tonno()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_tonno()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_carne()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_carne()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_pelati()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_pelati()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_legumi()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_legumi()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_pasta()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_pasta()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_riso()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_riso()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_zucchero()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_zucchero()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_latte()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_latte()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_biscotti()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_biscotti()).toString());
		
		query=query.replaceFirst("[?]",((Double)b.getPeso_tot_varie()).toString());	
		query=query.replaceFirst("[?]",((Integer)b.getScatoli_tot_varie()).toString());
		
		query=query.replaceFirst("[?]",("1"));
		

		
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean eliminaModB(ModB u)
	{
		boolean esito;
		String query = "DELETE FROM ModB WHERE idScheda = " +u.getIdScheda();
		esito = db.executeUpdate(query);
		return esito;
	}
}
