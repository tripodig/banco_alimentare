package beans;
import java.util.*;

import util.DBManager;

public class DAO_Aree {
	DBManager db;

	public  DAO_Aree() {
		db=new DBManager();
	}
	
	public  DAO_Aree(DBManager db) {
		this.db = db;
	
	}


	public Vector<Aree> getArea(String regione)
	{
		
		Vector<Aree> res = null;
		String query = "select * from ba_area where regione= "+regione;
		System.out.println(query);
		Vector<Object> supp = db.executeSelect(query, "Aree");
		if(supp.size()>0){
			for (int i=0;i<supp.size();i++)
				res.add((Aree) supp.get(i));
		}
		
		return res;

	}

}
