package beans;


public class Convenzione {
	
//convenzione vecchio tipo
	public Convenzione( int idEnte, int idRappLegaleEnte,int idSedeLegaleEnte,
			int idSedeOperativaEnte, int idSedeMagazzinoEnte, int anno,
			int m_totIndigenti, int r_totIndigenti, int p_totIndigenti,
			int m_mediaPresenze, int r_mediaPresenze, int p_mediaPresenze,
			int m_giorniApertura, int p_giorniApertura, int r_giorniApertura,
			boolean cellaFrigo, boolean m_pranzo, boolean m_cena,
			boolean m_colazione, boolean m_pastiFreddi,	boolean m_centroAccoglienzaDiurno, 
			int m_0_5, int m_6_18,
			int m_19_65, int m_over65, boolean r_casaFamiglia,
			boolean r_comunitaDisabili, boolean r_comunitaTossicodipendenti,
			boolean r_comunitaAnziani, boolean r_comunitaMinori, int r_0_5,
			int r_6_18, int r_19_65, int r_over65,
			boolean p_distribuzionePacchi, boolean p_consegnaDomicilio,
			String p_Altro, int p_0_5, int p_6_18, int p_19_65, int p_over65,
			String dataFirma) {
		super();
		
		this.idEnte = idEnte;
		this.setIdRappLegaleEnte(idRappLegaleEnte);
		this.idSedeLegaleEnte = idSedeLegaleEnte;
		this.idSedeOperativaEnte = idSedeOperativaEnte;
		this.idSedeMagazzinoEnte = idSedeMagazzinoEnte;
		this.anno = anno;
		M_totIndigenti = m_totIndigenti;
		R_totIndigenti = r_totIndigenti;
		P_totIndigenti = p_totIndigenti;
		M_mediaPresenze = m_mediaPresenze;
		R_mediaPresenze = r_mediaPresenze;
		P_mediaPresenze = p_mediaPresenze;
		M_giorniApertura = m_giorniApertura;
		P_giorniApertura = p_giorniApertura;
		R_giorniApertura = r_giorniApertura;
		this.cellaFrigo = cellaFrigo;
		M_pranzo = m_pranzo;
		M_cena = m_cena;
		M_colazione = m_colazione;
		M_pastiFreddi = m_pastiFreddi;
		M_centroAccoglienzaDiurno = m_centroAccoglienzaDiurno;
		M_0_5 = m_0_5;
		M_6_18 = m_6_18;
		M_19_65 = m_19_65;
		M_over65 = m_over65;
		R_casaFamiglia = r_casaFamiglia;
		R_comunitaDisabili = r_comunitaDisabili;
		R_comunitaTossicodipendenti = r_comunitaTossicodipendenti;
		R_comunitaAnziani = r_comunitaAnziani;
		R_comunitaMinori = r_comunitaMinori;
		R_0_5 = r_0_5;
		R_6_18 = r_6_18;
		R_19_65 = r_19_65;
		R_over65 = r_over65;
		P_distribuzionePacchi = p_distribuzionePacchi;
		P_consegnaDomicilio = p_consegnaDomicilio;
		P_Altro = p_Altro;
		P_0_5 = p_0_5;
		P_6_18 = p_6_18;
		P_19_65 = p_19_65;
		P_over65 = p_over65;
		this.dataFirma = dataFirma;
	}
	
//convenzione nuovo tipo
public Convenzione(int idEnte, int idRappLegaleEnte,
			int idSedeLegaleEnte, int idSedeOperativaEnte,
			int idSedeMagazzinoEnte, int anno, int m_totIndigenti,
			int r_totIndigenti, int p_totIndigenti, int m_mediaPresenze,
			int r_mediaPresenze, int p_mediaPresenze, int m_giorniApertura,
			int p_giorniApertura, int r_giorniApertura, boolean cellaFrigo,
			boolean m_pranzo, boolean m_cena, boolean m_colazione,
			boolean m_pastiFreddi, boolean m_centroAccoglienzaDiurno,
			int m_0_5, int m_6_18, int m_19_65, int m_over65,
			boolean r_casaFamiglia, boolean r_comunitaDisabili,
			boolean r_comunitaTossicodipendenti, boolean r_comunitaAnziani,
			boolean r_comunitaMinori, int r_0_5, int r_6_18, int r_19_65,
			int r_over65, boolean p_distribuzionePacchi,
			boolean p_consegnaDomicilio, String p_Altro, int p_0_5, int p_6_18,
			int p_19_65, int p_over65, String dataFirma, int m_6_65,
			int p_6_65, int e_6_65, int e_0_5, int e_over65, int m_saltuari,
			int p_saltuari, int e_saltuari, int u_saltuari,
			int u_mediaPresenze, int u_giorniApertura, int e_mediaPresenze,
			int e_giorniApertura) {
		super();
		this.idEnte = idEnte;
		this.idRappLegaleEnte = idRappLegaleEnte;
		this.idSedeLegaleEnte = idSedeLegaleEnte;
		this.idSedeOperativaEnte = idSedeOperativaEnte;
		this.idSedeMagazzinoEnte = idSedeMagazzinoEnte;
		this.anno = anno;
		M_totIndigenti = m_totIndigenti;
		R_totIndigenti = r_totIndigenti;
		P_totIndigenti = p_totIndigenti;
		M_mediaPresenze = m_mediaPresenze;
		R_mediaPresenze = r_mediaPresenze;
		P_mediaPresenze = p_mediaPresenze;
		M_giorniApertura = m_giorniApertura;
		P_giorniApertura = p_giorniApertura;
		R_giorniApertura = r_giorniApertura;
		this.cellaFrigo = cellaFrigo;
		M_pranzo = m_pranzo;
		M_cena = m_cena;
		M_colazione = m_colazione;
		M_pastiFreddi = m_pastiFreddi;
		M_centroAccoglienzaDiurno = m_centroAccoglienzaDiurno;
		M_0_5 = m_0_5;
		M_6_18 = m_6_18;
		M_19_65 = m_19_65;
		M_over65 = m_over65;
		R_casaFamiglia = r_casaFamiglia;
		R_comunitaDisabili = r_comunitaDisabili;
		R_comunitaTossicodipendenti = r_comunitaTossicodipendenti;
		R_comunitaAnziani = r_comunitaAnziani;
		R_comunitaMinori = r_comunitaMinori;
		R_0_5 = r_0_5;
		R_6_18 = r_6_18;
		R_19_65 = r_19_65;
		R_over65 = r_over65;
		P_distribuzionePacchi = p_distribuzionePacchi;
		P_consegnaDomicilio = p_consegnaDomicilio;
		P_Altro = p_Altro;
		P_0_5 = p_0_5;
		P_6_18 = p_6_18;
		P_19_65 = p_19_65;
		P_over65 = p_over65;
		this.dataFirma = dataFirma;
		M_6_65 = m_6_65;
		P_6_65 = p_6_65;
		E_6_65 = e_6_65;
		E_0_5 = e_0_5;
		E_over65 = e_over65;
		M_saltuari = m_saltuari;
		P_saltuari = p_saltuari;
		E_saltuari = e_saltuari;
		U_saltuari = u_saltuari;
		U_mediaPresenze = u_mediaPresenze;
		U_giorniApertura = u_giorniApertura;
		E_mediaPresenze = e_mediaPresenze;
		E_giorniApertura = e_giorniApertura;
	}


	private int idConvenzione;
	private int idEnte;
	private int idRappLegaleEnte;
	private int idSedeLegaleEnte;
	private int idSedeOperativaEnte;
	private int idSedeMagazzinoEnte;
	private int anno;
    private int M_totIndigenti;
    private int R_totIndigenti;
    private int P_totIndigenti;
    private int M_mediaPresenze;
    private int R_mediaPresenze;
    private int P_mediaPresenze;
    private int M_giorniApertura;
    private int P_giorniApertura;
    private int R_giorniApertura;
    private boolean cellaFrigo;
    private boolean M_pranzo;
    private boolean M_cena;
    private boolean M_colazione;
    private boolean M_pastiFreddi;
    private boolean M_centroAccoglienzaDiurno;
    private int M_0_5;
    private int M_6_18;
    private int M_19_65;
    private int M_over65;
    private boolean R_casaFamiglia;
    private boolean R_comunitaDisabili;
    private boolean R_comunitaTossicodipendenti;
    private boolean R_comunitaAnziani;
    private boolean R_comunitaMinori;
    private int R_0_5;
    private int R_6_18;
    private int R_19_65;
    private int R_over65;
    private boolean P_distribuzionePacchi;
    private boolean P_consegnaDomicilio;
    private String P_Altro;
    private int P_0_5;
    private int P_6_18;
    private int P_19_65;
    private int P_over65;
    private String dataFirma;
    private int M_6_65;
    private int P_6_65;
    private int E_6_65;
    private int E_0_5;
    private int E_over65;
    private int M_saltuari;
    private int P_saltuari;
    private int E_saltuari;
    private int U_saltuari;
    private int U_mediaPresenze;
    private int U_giorniApertura;
    private int E_mediaPresenze;
    private int E_giorniApertura;
    
   
   
    public Convenzione() {
		
    	this.idSedeLegaleEnte = 0;
		this.idSedeOperativaEnte = 0;
		this.idSedeMagazzinoEnte = 0;
		this.idRappLegaleEnte= 0;
		
		this.anno = 0;
		M_totIndigenti = 0;
		R_totIndigenti = 0;
		P_totIndigenti = 0;
		M_mediaPresenze = 0;
		R_mediaPresenze = 0;
		P_mediaPresenze = 0;
		M_giorniApertura = 0;
		P_giorniApertura = 0;
		R_giorniApertura = 0;
		this.cellaFrigo = false;
		M_pranzo = false;
		M_cena = false;
		M_colazione = false;
		M_pastiFreddi = false;
		M_centroAccoglienzaDiurno = false;
		M_0_5 = 0;
		M_19_65 = 0;
		M_over65 = 0;
		R_casaFamiglia = false;
		R_comunitaDisabili = false;
		R_comunitaTossicodipendenti = false;
		R_comunitaAnziani = false;
		R_comunitaMinori = false;
		R_0_5 = 0;
		R_19_65 = 0;
		R_over65 = 0;
		P_distribuzionePacchi = false;
		P_consegnaDomicilio = false;
		P_Altro = null;
		P_0_5 = 0;
		P_19_65 = 0;
		P_over65 = 0;
		this.dataFirma = "";
	    M_6_65= 0;
	    P_6_65= 0;
	    E_6_65= 0;
	    E_0_5= 0;
	    E_over65= 0;
	    M_saltuari= 0;
	    P_saltuari= 0;
	    E_saltuari= 0;
	    U_saltuari= 0;
	    U_mediaPresenze= 0;
	    U_giorniApertura= 0;
	    E_mediaPresenze= 0;
	    E_giorniApertura= 0;
	}
    
    




	/**
	 * Override del metodo equals della classe Object.
	 * @return - restituisce un boolean che e' true quando due utenti hanno la stessa
	 * chiave; in tutti gli altri casi restituisce false.
	 */
	public boolean equals(Object anObject)
	{
		if ( anObject == null ) return false;
	    if (! (anObject instanceof Convenzione) ) return false;

	    Convenzione conv = (Convenzione) anObject;
	    if (idConvenzione != conv.idConvenzione) return false;
	    
	    return true;
	}
 
	public int getIdConvenzione() {
		return idConvenzione;
	}

	public void setIdConvenzione(int idConvenzione) {
		this.idConvenzione = idConvenzione;
	}

	public int getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(int idEnte) {
		this.idEnte = idEnte;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public int getM_mediaPresenze() {
		return M_mediaPresenze;
	}

	public void setM_mediaPresenze(int m_mediaPresenze) {
		M_mediaPresenze = m_mediaPresenze;
	}

	public int getR_mediaPresenze() {
		return R_mediaPresenze;
	}

	public void setR_mediaPresenze(int r_mediaPresenze) {
		R_mediaPresenze = r_mediaPresenze;
	}

	public int getP_mediaPresenze() {
		return P_mediaPresenze;
	}

	public void setP_mediaPresenze(int p_mediaPresenze) {
		P_mediaPresenze = p_mediaPresenze;
	}

	public int getM_giorniApertura() {
		return M_giorniApertura;
	}

	public void setM_giorniApertura(int m_giorniApertura) {
		M_giorniApertura = m_giorniApertura;
	}

	public int getP_giorniApertura() {
		return P_giorniApertura;
	}

	public void setP_giorniApertura(int p_giorniApertura) {
		P_giorniApertura = p_giorniApertura;
	}

	public int getR_giorniApertura() {
		return R_giorniApertura;
	}

	public void setR_giorniApertura(int r_giorniApertura) {
		R_giorniApertura = r_giorniApertura;
	}

	public boolean isCellaFrigo() {
		return cellaFrigo;
	}
	
	public String isCellaFrigoIT() {
		String cellaFrigoIT="NO";
		if(cellaFrigo)
			cellaFrigoIT="SI";
		
		return cellaFrigoIT;
	}
	public String isM_pranzoIT() {
		String M_pranzoIT="NO";
		if(M_pranzo)
			M_pranzoIT="SI";
		
		return M_pranzoIT;
	}

	public String isM_cenaIT() {
		String M_cenaIT="NO";
		if(M_cena)
			M_cenaIT="SI";
		
		return M_cenaIT;
	}
	
	public String isM_colazioneIT() {
		String M_colazioneIT="NO";
		if(M_colazione)
			M_colazioneIT="SI";
		
		return M_colazioneIT;
	}
	
	public String isM_pastiFreddiIT() {
		String M_pastiFreddiIT="NO";
		if(M_pastiFreddi)
			M_pastiFreddiIT="SI";
		
		return M_pastiFreddiIT;
	}

	public String isM_centroAccoglienzaDiurnoIT() {
		String M_centroAccoglienzaDiurnoIT="NO";
		if(M_centroAccoglienzaDiurno)
			M_centroAccoglienzaDiurnoIT="SI";
		
		return M_centroAccoglienzaDiurnoIT;
	}

	public void setCellaFrigo(boolean cellaFrigo) {
		this.cellaFrigo = cellaFrigo;
	}

	public boolean isM_pranzo() {
		return M_pranzo;
	}

	public void setM_pranzo(boolean m_pranzo) {
		M_pranzo = m_pranzo;
	}

	public boolean isM_cena() {
		return M_cena;
	}

	public void setM_cena(boolean m_cena) {
		M_cena = m_cena;
	}

	public boolean isM_colazione() {
		return M_colazione;
	}

	public void setM_colazione(boolean m_colazione) {
		M_colazione = m_colazione;
	}

	public boolean isM_pastiFreddi() {
		return M_pastiFreddi;
	}

	public void setM_pastiFreddi(boolean m_pastiFreddi) {
		M_pastiFreddi = m_pastiFreddi;
	}

	public boolean isM_centroAccoglienzaDiurno() {
		return M_centroAccoglienzaDiurno;
	}

	public void setM_centroAccoglienzaDiurno(boolean m_centroAccoglienzaDiurno) {
		M_centroAccoglienzaDiurno = m_centroAccoglienzaDiurno;
	}

	public int getM_0_5() {
		return M_0_5;
	}

	public void setM_0_5(int m_0_5) {
		M_0_5 = m_0_5;
	}
	public int getM_6_18() {
		return M_6_18;
	}

	public int getM_19_65() {
		return M_19_65;
	}

	public void setM_19_65(int m_19_65) {
		M_19_65 = m_19_65;
	}

	public int getM_over65() {
		return M_over65;
	}

	public void setM_over65(int m_over65) {
		M_over65 = m_over65;
	}

	public boolean isR_casaFamiglia() {
		return R_casaFamiglia;
	}

	public void setR_casaFamiglia(boolean r_casaFamiglia) {
		R_casaFamiglia = r_casaFamiglia;
	}

	public boolean isR_comunitaDisabili() {
		return R_comunitaDisabili;
	}

	public void setR_comunitaDisabili(boolean r_comunitaDisabili) {
		R_comunitaDisabili = r_comunitaDisabili;
	}

	public boolean isR_comunitaTossicodipendenti() {
		return R_comunitaTossicodipendenti;
	}

	public void setR_comunitaTossicodipendenti(boolean r_comunitaTossicodipendenti) {
		R_comunitaTossicodipendenti = r_comunitaTossicodipendenti;
	}

	public boolean isR_comunitaAnziani() {
		return R_comunitaAnziani;
	}

	public void setR_comunitaAnziani(boolean r_comunitaAnziani) {
		R_comunitaAnziani = r_comunitaAnziani;
	}

	public boolean isR_comunitaMinori() {
		return R_comunitaMinori;
	}

	public void setR_comunitaMinori(boolean r_comunitaMinori) {
		R_comunitaMinori = r_comunitaMinori;
	}
	
	public String isR_comunitaMinoriIT() {
		String R_comunitaMinoriIT="NO";
		if(R_comunitaMinori)
			R_comunitaMinoriIT="SI";
		
		return R_comunitaMinoriIT;
	}

	public String isR_comunitaAnzianiIT() {
		String R_comunitaAnzianiIT="NO";
		if(R_comunitaAnziani)
			R_comunitaAnzianiIT="SI";
		
		return R_comunitaAnzianiIT;
	}
	
	public String isR_comunitaTossicodipendentiIT() {
		String R_comunitaTossicodipendentiIT="NO";
		if(R_comunitaTossicodipendenti)
			R_comunitaTossicodipendentiIT="SI";
		
		return R_comunitaTossicodipendentiIT;
	}
	
	public String isR_comunitaDisabiliIT() {
		String R_comunitaDisabiliIT="NO";
		if(R_comunitaDisabili)
			R_comunitaDisabiliIT="SI";
		
		return R_comunitaDisabiliIT;
	}
	
	public String isR_casaFamigliaIT() {
		String R_casaFamigliaIT="NO";
		if(R_casaFamiglia)
			R_casaFamigliaIT="SI";
		
		return R_casaFamigliaIT;
	}

	public int getR_0_5() {
		return R_0_5;
	}
	public int getR_6_18() {
		return R_6_18;
	}

	public void setR_0_5(int r_0_5) {
		R_0_5 = r_0_5;
	}

	public int getR_19_65() {
		return R_19_65;
	}

	public void setR_19_65(int r_6_65) {
		R_19_65 = r_6_65;
	}

	public int getR_over65() {
		return R_over65;
	}

	public void setR_over65(int r_over65) {
		R_over65 = r_over65;
	}

	public boolean isP_distribuzionePacchi() {
		return P_distribuzionePacchi;
	}

	public void setP_distribuzionePacchi(boolean p_distribuzionePacchi) {
		P_distribuzionePacchi = p_distribuzionePacchi;
	}

	public boolean isP_consegnaDomicilio() {
		return P_consegnaDomicilio;
	}

	public void setP_consegnaDomicilio(boolean p_consegnaDomicilio) {
		P_consegnaDomicilio = p_consegnaDomicilio;
	}

	public String getP_Altro() {
		return P_Altro;
	}

	public void setP_Altro(String p_Altro) {
		p_Altro=p_Altro.replace("'", "`");
		P_Altro = p_Altro;
	}
	
	public String isP_distribuzionePacchiIT() {
		String P_distribuzionePacchiIT="NO";
		if(P_distribuzionePacchi)
			P_distribuzionePacchiIT="SI";
		
		return P_distribuzionePacchiIT;
	}
	
	public String isP_consegnaDomicilioIT() {
		String P_consegnaDomicilioIT="NO";
		if(P_consegnaDomicilio)
			P_consegnaDomicilioIT="SI";
		
		return P_consegnaDomicilioIT;
	}

	public int getP_0_5() {
		return P_0_5;
	}

	public void setP_0_5(int p_0_5) {
		P_0_5 = p_0_5;
	}
	public int getP_6_18() {
		return P_6_18;
	}

	public int getP_19_65() {
		return P_19_65;
	}

	public void setP_19_65(int p_19_65) {
		P_19_65 = p_19_65;
	}

	public int getP_over65() {
		return P_over65;
	}

	public void setP_over65(int p_over65) {
		P_over65 = p_over65;
	}

	public String getDataFirma() {
		return dataFirma;
	}

	public void setDataFirma(String dataFirma) {
		this.dataFirma = dataFirma;
	}
	
	public void setP_6_18(int p_6_18) {
		P_6_18 = p_6_18;
	}
	public void setR_6_18(int r_6_18) {
		R_6_18 = r_6_18;
	}
	public void setM_6_18(int m_6_18) {
		M_6_18 = m_6_18;
	}


	public int getIdSedeLegaleEnte() {
		return idSedeLegaleEnte;
	}
	public void setIdSedeLegaleEnte(int idSedeLegaleEnte) {
		this.idSedeLegaleEnte = idSedeLegaleEnte;
	}
	public int getIdSedeOperativaEnte() {
		return idSedeOperativaEnte;
	}
	public void setIdSedeOperativaEnte(int idSedeOperativaEnte) {
		this.idSedeOperativaEnte = idSedeOperativaEnte;
	}
	public int getIdSedeMagazzinoEnte() {
		return idSedeMagazzinoEnte;
	}
	public void setIdSedeMagazzinoEnte(int idSedeMagazzinoEnte) {
		this.idSedeMagazzinoEnte = idSedeMagazzinoEnte;
	}

	public int getIdRappLegaleEnte() {
		return idRappLegaleEnte;
	}

	public void setIdRappLegaleEnte(int idRappLegaleEnte) {
		this.idRappLegaleEnte = idRappLegaleEnte;
	}

	public int getM_6_65() {
		return M_6_65;
	}
	public void setM_6_65(int m_6_65) {
		M_6_65 = m_6_65;
	}

	public int getP_6_65() {
		return P_6_65;
	}


	public void setP_6_65(int p_6_65) {
		P_6_65 = p_6_65;
	}


	public int getE_6_65() {
		return E_6_65;
	}


	public void setE_6_65(int e_6_65) {
		E_6_65 = e_6_65;
	}

	public int getE_0_5() {
		return E_0_5;
	}


	public void setE_0_5(int e_0_5) {
		E_0_5 = e_0_5;
	}


	public int getE_over_65() {
		return E_over65;
	}



	public void setE_over_65(int e_over_65) {
		E_over65 = e_over_65;
	}



	public int getM_saltuari() {
		return M_saltuari;
	}


	public void setM_saltuari(int m_saltuari) {
		M_saltuari = m_saltuari;
	}



	public int getP_saltuari() {
		return P_saltuari;
	}


	public void setP_saltuari(int d_saltuari) {
		P_saltuari = d_saltuari;
	}

	public int getE_saltuari() {
		return E_saltuari;
	}

	public void setE_saltuari(int e_saltuari) {
		E_saltuari = e_saltuari;
	}

	public int getU_saltuari() {
		return U_saltuari;
	}


	public void setU_saltuari(int u_saltuari) {
		U_saltuari = u_saltuari;
	}


	public int getU_mediaPresenze() {
		return U_mediaPresenze;
	}
	public int getE_mediaPresenze() {
		return E_mediaPresenze;
	}

	public void setU_mediaPresenze(int u_mediaPresenze) {
		U_mediaPresenze = u_mediaPresenze;
	}
	public int getU_giorniApertura() {
		return U_giorniApertura;
	}
	public void setE_mediaPresenze(int e_mediaPresenze) {
		E_mediaPresenze = e_mediaPresenze;
	}
	public int getE_giorniApertura() {
		return E_giorniApertura;
	}

	public void setU_giorniApertura(int u_giorniApertura) {
		U_giorniApertura = u_giorniApertura;
	}
	public void setE_giorniApertura(int e_giorniApertura) {
		E_giorniApertura = e_giorniApertura;
	}

	public int getM_TotaleAssistitiContinuativi() {
		return M_0_5+M_6_65+M_over65;
  
 }
	public int getP_TotaleAssistitiContinuativi() {
		return P_0_5+P_6_65+P_over65;
  
 }
	public int getE_TotaleAssistitiContinuativi() {
		return E_0_5+E_6_65+E_over65;
  
 }
	public int getE_totIndigenti() {
		return E_0_5+E_6_65+E_over65+E_saltuari+R_6_18+R_19_65;
	}
	
	public int getU_totIndigenti() {
		return U_saltuari;
	}
	
	public int getM_totIndigenti() {
		return M_0_5+M_6_65+M_over65+M_saltuari+M_6_18+M_19_65;
	}

	public void setM_totIndigenti(int m_totIndigenti) {
		M_totIndigenti = m_totIndigenti;
	}

	public int getR_totIndigenti() {
		return R_totIndigenti;
	}

	public void setR_totIndigenti(int r_totIndigenti) {
		R_totIndigenti = r_totIndigenti;
	}

	public int getP_totIndigenti() {
		return P_0_5+P_6_65+P_over65+P_saltuari+P_6_18+P_19_65;
	}

	public void setP_totIndigenti(int p_totIndigenti) {
		P_totIndigenti = p_totIndigenti;
	}




	public int getE_over65() {
		return E_over65;
	}


	public void setE_over65(int e_over65) {
		E_over65 = e_over65;
	}
	
	public String tipologiaConvenzione(){
		String tipo="";
		if ((this.M_totIndigenti+this.R_totIndigenti+this.P_totIndigenti)>0)//in questo caso la convenzione � di tipo banco perch� queste colonne le popolo solo se salvo una convenzione di tipo banco
			tipo="banco";
		else
			tipo="agea";
			return tipo;
		
	}











	
}
