package beans;


public class Volontario {
	
	public Volontario() {
		super();
		// TODO Auto-generated constructor stub
	}
	private int idVolontario;

    private String Nome;
    private String Cognome;
    private String Indirizzo;
    private String Cap;
    private String Citta;
    private String Telefono;
    private String DataNascita;
    private String Occupazione;
    private String Email;
    
	public int getIdVolontario() {
		return idVolontario;
	}
	public void setIdVolontario(int idVolontario) {
		this.idVolontario = idVolontario;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		nome=nome.replace("'", "`");
		Nome = nome;
	}
	public String getCognome() {
		return Cognome;
	}
	public void setCognome(String cognome) {
		cognome=cognome.replace("'", "`");
		Cognome = cognome;
	}
	public String getIndirizzo() {
		return Indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		indirizzo=indirizzo.replace("'", "`");
		Indirizzo = indirizzo;
	}
	public String getCap() {
		return Cap;
	}
	public void setCap(String cap) {
		Cap = cap;
	}
	public String getCitta() {
		return Citta;
	}
	public void setCitta(String citta) {
		citta=citta.replace("'", "`");
		Citta = citta;
	}
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String telefono) {
		Telefono = telefono;
	}
	public String getDataNascita() {
		return DataNascita;
	}
	public void setDataNascita(String dataNascita) {
		DataNascita = dataNascita;
	}
	public String getOccupazione() {
		return Occupazione;
	}
	public void setOccupazione(String occupazione) {
		occupazione=occupazione.replace("'", "`");
		Occupazione = occupazione;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
 

 }
