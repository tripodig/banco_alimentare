package beans;

public class ModB {

public ModB(int idScheda, int idEvento, int ora, String numeroScheda, int idCapoEquipe,
			int idPuntoVendita, 
			double peso_tot_olio, int scatoli_tot_olio,
			double peso_tot_omogeinizzati, int scatoli_tot_omogeinizzati,
			double peso_tot_infanzia, int scatoli_tot_infanzia,
			double peso_tot_tonno, int scatoli_tot_tonno,
			double peso_tot_carne, int scatoli_tot_carne,
			double peso_tot_pelati, int scatoli_tot_pelati,
			double peso_tot_legumi, int scatoli_tot_legumi,
			double peso_tot_pasta, int scatoli_tot_pasta, 
			double peso_tot_riso, int scatoli_tot_riso, 
			double peso_tot_zucchero, int scatoli_tot_zucchero, 
			double peso_tot_latte, int scatoli_tot_latte, 
			double peso_tot_biscotti, int scatoli_tot_biscotti, 
			double peso_tot_varie, int scatoli_tot_varie) {
		super();
		this.idScheda = idScheda;
		this.setIdEvento(idEvento);
		Ora = ora;
		NumeroScheda = numeroScheda;
		this.idCapoEquipe = idCapoEquipe;
		this.idPuntoVendita = idPuntoVendita;
		this.peso_tot_olio = peso_tot_olio;
		this.scatoli_tot_olio = scatoli_tot_olio;
		this.peso_tot_omogeinizzati = peso_tot_omogeinizzati;
		this.scatoli_tot_omogeinizzati = scatoli_tot_omogeinizzati;
		this.peso_tot_infanzia = peso_tot_infanzia;
		this.scatoli_tot_infanzia = scatoli_tot_infanzia;
		this.peso_tot_tonno = peso_tot_tonno;
		this.scatoli_tot_tonno = scatoli_tot_tonno;
		this.peso_tot_carne = peso_tot_carne;
		this.scatoli_tot_carne = scatoli_tot_carne;
		this.peso_tot_pelati = peso_tot_pelati;
		this.scatoli_tot_pelati = scatoli_tot_pelati;
		this.peso_tot_legumi = peso_tot_legumi;
		this.scatoli_tot_legumi = scatoli_tot_legumi;
		this.peso_tot_pasta = peso_tot_pasta;
		this.scatoli_tot_pasta = scatoli_tot_pasta;
		this.peso_tot_riso = peso_tot_riso;
		this.scatoli_tot_riso = scatoli_tot_riso;
		this.peso_tot_zucchero = peso_tot_zucchero;
		this.scatoli_tot_zucchero = scatoli_tot_zucchero;
		this.peso_tot_latte = peso_tot_latte;
		this.scatoli_tot_latte = scatoli_tot_latte;
		this.peso_tot_biscotti = peso_tot_biscotti;
		this.scatoli_tot_biscotti = scatoli_tot_biscotti;
		this.peso_tot_varie = peso_tot_varie;
		this.scatoli_tot_varie = scatoli_tot_varie;
	}
public ModB() {
		super();
		// TODO Auto-generated constructor stub
	}

	private int idScheda;
	private int idEvento;
	private int Ora;
	private String NumeroScheda;
	private int idCapoEquipe;
	private int idPuntoVendita;
	
	private double peso_tot_olio;
	private int scatoli_tot_olio;
	private double peso_tot_omogeinizzati;
	private int scatoli_tot_omogeinizzati;
	private double peso_tot_infanzia;
	private int scatoli_tot_infanzia;
	private double peso_tot_tonno;
	private int scatoli_tot_tonno;
	private double peso_tot_carne;
	private int scatoli_tot_carne;
	private double peso_tot_pelati;
	private int scatoli_tot_pelati;
	private double peso_tot_legumi;
	private int scatoli_tot_legumi;
	private double peso_tot_pasta;
	private int scatoli_tot_pasta;
	private double peso_tot_riso;
	private int scatoli_tot_riso;
	private double peso_tot_zucchero;
	private int scatoli_tot_zucchero;
	private double peso_tot_latte;
	private int scatoli_tot_latte;
	private double peso_tot_biscotti;
	private int scatoli_tot_biscotti;
	private double peso_tot_varie;
	private int scatoli_tot_varie;
	
	public int getIdScheda() {
		return idScheda;
	}
	public void setIdScheda(int idScheda) {
		this.idScheda = idScheda;
	}
	public int getOra() {
		return Ora;
	}
	public void setOra(int ora) {
		Ora = ora;
	}
	public String getNumeroScheda() {
		return NumeroScheda;
	}
	public void setNumeroScheda(String numeroScheda) {
		NumeroScheda = numeroScheda;
	}
	public int getIdCapoEquipe() {
		return idCapoEquipe;
	}
	public void setIdCapoEquipe(int idCapoEquipe) {
		this.idCapoEquipe = idCapoEquipe;
	}
	public int getIdPuntoVendita() {
		return idPuntoVendita;
	}
	public void setIdPuntoVendita(int idPuntoVendita) {
		this.idPuntoVendita = idPuntoVendita;
	}
	public double getPeso_tot_olio() {
		return peso_tot_olio;
	}
	public void setPeso_tot_olio(double peso_tot_olio) {
		this.peso_tot_olio = peso_tot_olio;
	}
	public int getScatoli_tot_olio() {
		return scatoli_tot_olio;
	}
	public void setScatoli_tot_olio(int scatoli_tot_olio) {
		this.scatoli_tot_olio = scatoli_tot_olio;
	}
	public double getPeso_tot_omogeinizzati() {
		return peso_tot_omogeinizzati;
	}
	public void setPeso_tot_omogeinizzati(double peso_tot_omogeinizzati) {
		this.peso_tot_omogeinizzati = peso_tot_omogeinizzati;
	}
	public int getScatoli_tot_omogeinizzati() {
		return scatoli_tot_omogeinizzati;
	}
	public void setScatoli_tot_omogeinizzati(int scatoli_tot_omogeinizzati) {
		this.scatoli_tot_omogeinizzati = scatoli_tot_omogeinizzati;
	}
	public double getPeso_tot_infanzia() {
		return peso_tot_infanzia;
	}
	public void setPeso_tot_infanzia(double peso_tot_infanzia) {
		this.peso_tot_infanzia = peso_tot_infanzia;
	}
	public int getScatoli_tot_infanzia() {
		return scatoli_tot_infanzia;
	}
	public void setScatoli_tot_infanzia(int scatoli_tot_infanzia) {
		this.scatoli_tot_infanzia = scatoli_tot_infanzia;
	}
	public double getPeso_tot_tonno() {
		return peso_tot_tonno;
	}
	public void setPeso_tot_tonno(double peso_tot_tonno) {
		this.peso_tot_tonno = peso_tot_tonno;
	}
	public int getScatoli_tot_tonno() {
		return scatoli_tot_tonno;
	}
	public void setScatoli_tot_tonno(int scatoli_tot_tonno) {
		this.scatoli_tot_tonno = scatoli_tot_tonno;
	}
	public double getPeso_tot_carne() {
		return peso_tot_carne;
	}
	public void setPeso_tot_carne(double peso_tot_carne) {
		this.peso_tot_carne = peso_tot_carne;
	}
	public int getScatoli_tot_carne() {
		return scatoli_tot_carne;
	}
	public void setScatoli_tot_carne(int scatoli_tot_carne) {
		this.scatoli_tot_carne = scatoli_tot_carne;
	}
	public double getPeso_tot_pelati() {
		return peso_tot_pelati;
	}
	public void setPeso_tot_pelati(double peso_tot_pelati) {
		this.peso_tot_pelati = peso_tot_pelati;
	}
	public int getScatoli_tot_pelati() {
		return scatoli_tot_pelati;
	}
	public void setScatoli_tot_pelati(int scatoli_tot_pelati) {
		this.scatoli_tot_pelati = scatoli_tot_pelati;
	}
	public double getPeso_tot_legumi() {
		return peso_tot_legumi;
	}
	public void setPeso_tot_legumi(double peso_tot_legumi) {
		this.peso_tot_legumi = peso_tot_legumi;
	}
	public int getScatoli_tot_legumi() {
		return scatoli_tot_legumi;
	}
	public void setScatoli_tot_legumi(int scatoli_tot_legumi) {
		this.scatoli_tot_legumi = scatoli_tot_legumi;
	}
	public double getPeso_tot_pasta() {
		return peso_tot_pasta;
	}
	public void setPeso_tot_pasta(double peso_tot_pasta) {
		this.peso_tot_pasta = peso_tot_pasta;
	}
	public int getScatoli_tot_pasta() {
		return scatoli_tot_pasta;
	}
	public void setScatoli_tot_pasta(int scatoli_tot_pasta) {
		this.scatoli_tot_pasta = scatoli_tot_pasta;
	}
	public double getPeso_tot_riso() {
		return peso_tot_riso;
	}
	public void setPeso_tot_riso(double peso_tot_riso) {
		this.peso_tot_riso = peso_tot_riso;
	}
	public int getScatoli_tot_riso() {
		return scatoli_tot_riso;
	}
	public void setScatoli_tot_riso(int scatoli_tot_riso) {
		this.scatoli_tot_riso = scatoli_tot_riso;
	}
	public double getPeso_tot_zucchero() {
		return peso_tot_zucchero;
	}
	public void setPeso_tot_zucchero(double peso_tot_zucchero) {
		this.peso_tot_zucchero = peso_tot_zucchero;
	}
	public int getScatoli_tot_zucchero() {
		return scatoli_tot_zucchero;
	}
	public void setScatoli_tot_zucchero(int scatoli_tot_zucchero) {
		this.scatoli_tot_zucchero = scatoli_tot_zucchero;
	}
	
	public double getPeso_tot_latte() {
		return peso_tot_latte;
	}
	public void setPeso_tot_latte(double peso_tot_latte) {
		this.peso_tot_latte = peso_tot_latte;
	}
	public int getScatoli_tot_latte() {
		return scatoli_tot_latte;
	}
	public void setScatoli_tot_latte(int scatoli_tot_latte) {
		this.scatoli_tot_latte = scatoli_tot_latte;
	}
	
	
	
	public double getPeso_tot_biscotti() {
		return peso_tot_biscotti;
	}
	public void setPeso_tot_biscotti(double peso_tot_biscotti) {
		this.peso_tot_biscotti = peso_tot_biscotti;
	}
	public int getScatoli_tot_biscotti() {
		return scatoli_tot_biscotti;
	}
	public void setScatoli_tot_biscotti(int scatoli_tot_biscotti) {
		this.scatoli_tot_biscotti = scatoli_tot_biscotti;
	}
	
	
	public double getPeso_tot_varie() {
		return peso_tot_varie;
	}
	public void setPeso_tot_varie(double peso_tot_varie) {
		this.peso_tot_varie = peso_tot_varie;
	}
	public int getScatoli_tot_varie() {
		return scatoli_tot_varie;
	}
	public void setScatoli_tot_varie(int scatoli_tot_varie) {
		this.scatoli_tot_varie = scatoli_tot_varie;
	}
	public int getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}


	
	
	
	
	
	
	
}
