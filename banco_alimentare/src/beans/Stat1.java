package beans;

public class Stat1 {


	
	public Stat1() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String Ora;
	private double PesoTot;
	private String AREA;
	
	public String getOra() {
		return Ora;
	}
	public void setOra(String ora) {
		Ora = ora;
	}
	public double getPesoTot() {
		return PesoTot;
	}
	public void setPesoTot(double pesoTot) {
		PesoTot = pesoTot;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	@Override
	public String toString() {
		return "Stat1 [Ora=" + Ora + ", PesoTot=" + PesoTot + ", AREA=" + AREA
				+ "]";
	}
	

}
