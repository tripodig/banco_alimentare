package beans;


public class Ente {
	
	private int idEnte;
    private String Nome;
    private String Descrizione;
    private String PartitaIva;
    private int codiceStruttura;
    private int codiceSap;
    boolean visibile;

    public Ente(int idEnte, String nome, String descrizione, String partitaIva, int codiceStruttura) {
		super();
		this.idEnte = idEnte;
		Nome = nome;
		Descrizione=descrizione;
		PartitaIva = partitaIva;
		this.codiceStruttura=codiceStruttura;
	}
    
    public Ente(String nome, String descrizione, String partitaIva) {
		super();
		Nome = nome;
		Descrizione=descrizione;
		PartitaIva = partitaIva;
	}
    
    public Ente() {
		
		
		Nome = "";
		PartitaIva = "";
	}
    
	public boolean equals(Object anObject)
	{
		if ( anObject == null ) return false;
	    if (! (anObject instanceof Ente) ) return false;

	    Ente e= (Ente) anObject;
	    if (idEnte !=e.idEnte) return false;
	    
	    return true;
	}


	public int getIdEnte() {
		return idEnte;
	}


	public void setIdEnte(int idEnte) {
		this.idEnte = idEnte;
	}


	public String getNome() {
		return Nome;
	}


	public void setNome(String nome) {
		nome=nome.replace("'", "`");
		Nome = nome;
	}
	
	public String getDescrizione() {
		return Descrizione;
	}


	public void setDescrizione(String descrizione) {
		if (descrizione!=null)
			descrizione=descrizione.replace("'", "`");
		Descrizione = descrizione;
	}


	public String getPartitaIva() {
		return PartitaIva;
	}


	public void setPartitaIva(String partitaIva) {
		PartitaIva = partitaIva;
	}

	@Override
	public String toString() {
		return "Ente [idEnte=" + idEnte + ", Nome=" + Nome + ", PartitaIva="
				+ PartitaIva + "]";
	}

	public int getCodiceStruttura() {
		return codiceStruttura;
	}

	public void setCodiceStruttura(int codiceStruttura) {
		this.codiceStruttura = codiceStruttura;
	}

	public boolean isVisibile() {
		return visibile;
	}

	public void setVisibile(boolean visibile) {
		this.visibile = visibile;
	}

	public int getCodiceSap() {
		return codiceSap;
	}
	public void setCodiceSap(int codiceSap) {
		this.codiceSap = codiceSap;
	}
 }
