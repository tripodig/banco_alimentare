package beans;
import java.util.Vector;

import util.DBManager;

public class DAO_Evento {
	DBManager db;

	
	public DAO_Evento()
	{
		db=new DBManager();
	}
	
	public DAO_Evento(DBManager db)
	{
		this.db=db;
	}
	
	public Evento getEvento()
	{
		Evento res = new Evento();
		String query = "SELECT * FROM evento WHERE visibile=true";
		Vector<Object> v =db.executeSelect(query, "Evento");
		res = (Evento)v.get(0);
		return res;
	}
	
	
	
	public Vector<Evento> getEventi()
	{
		Vector<Evento> res = new Vector<Evento>();
		String query = "SELECT * FROM evento order by data desc";
		Vector<Object> supp = db.executeSelect(query, "Evento");
		for(int i=0;i<supp.size();i++)
			res.add((Evento)supp.get(i));
		
		return res;
	}
	
	
	
	
	public Evento getEvento(int idEvento)
	{
		Evento res = new Evento();
		String query = "SELECT * FROM evento WHERE idEvento="+idEvento+" and visibile=true";
		Vector<Object> v =db.executeSelect(query, "Evento");
		res = (Evento)v.get(0);
		return res;
	}
	
}	