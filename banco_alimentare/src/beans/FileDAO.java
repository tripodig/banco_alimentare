package beans;


import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import util.DBManager;

public class FileDAO {

	DBManager db;
	
	public FileDAO()
	{
		db=new DBManager();
	}
	public FileDAO(DBManager db)
	{
		this.db=db;
	}
	
		
	public boolean salvaFile(FileBean f)
	{
		boolean esito=false;
		Connection conn = db.getConnectionForPreparedStatement();

		try {
				      
		String sql = "INSERT INTO file (idEnte, idUser, nomefile, descrizione) values (?, ?, ?, ?)";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, f.getIdEnte());
		statement.setInt(2, f.getIdUser());
		statement.setString(3, f.getNomeFile());
		statement.setString(4, f.getDescrizione());

		
		esito=db.executeUpdateByStatement(statement);
         
		 
		 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return esito;
	}
	
	public FileBean getFileById(int id)
	{
		FileBean res = null;
		String query = "SELECT * FROM file WHERE idFile = "+id;
		Vector<Object> v =db.executeSelect(query, "File");
		if(v.size()!=0){
			res = new FileBean();
			res = (FileBean)v.get(0);
			
		}
		return res;
	}
	
//	public int getLastIdInsered(int idEnte,int idUser, String descrizione, String Nomefile)
//	{
//		int res = null;
//		String query = "SELECT idFile FROM file WHERE idEnte = "+idEnte+" and  idUser= "+idUser+" and Nomefile= "+Nomefile;
//		Vector<Object> v =db.executeSelect(query, "count");
//		if(v.size()!=0){
//			res = new FileBean();
//			res = (FileBean)v.get(0);
//			
//		}
//		return res;
//	}
	

	
	public List<FileBean> getFileByIdEnte(int idEnte)
	{
		List<FileBean> res = new ArrayList<FileBean>();
		String query = "SELECT * FROM file WHERE idEnte = "+idEnte;
		List<Object> supp = db.executeSelect(query, "File");
		for(int i=0;i<supp.size();i++)
			res.add((FileBean)supp.get(i));
		
		return res;
	}
	
}
