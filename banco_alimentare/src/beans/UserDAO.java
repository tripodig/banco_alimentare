package beans;


import java.util.*;
import java.sql.*;

import util.DBManager;

public class UserDAO 	
{
	
	   static DBManager db = null;
	   static ResultSet rs = null;
	
	
	public UserDAO() {
		db=new DBManager();
	}

	@SuppressWarnings("static-access")
	public UserDAO(DBManager db) {
		this.db = db;
	}
	public boolean salva(UserBean u)
	{
		boolean esito=false;

		String query = "INSERT INTO users (idUsers, username, password, nome, cognome, area, ruolo, annoConvenzioni, provinciaConvenzioni, visibile) VALUES ( 0 ,'"+u.getUsername()+"','"+u.getPassword()+"','"+u.getnome()+"' ,'"+u.getcognome()+"', '"+u.getArea()+"','"+u.getRuolo()+"','"+u.getAnnoConvenzioni()+"','"+u.getProvinciaConvenzioni()+"','1')";
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean aggiorna(UserBean u)
	{
		boolean esito=false;
		
		String query = "UPDATE users SET username='"+u.getUsername()+"', password='"+u.getPassword()+"', nome='"+u.getnome()+"', cognome='"+u.getcognome()+"', area='"+u.getArea()+"', ruolo='"+u.getRuolo()+"', annoConvenzioni='"+u.getAnnoConvenzioni()+"', provinciaConvenzioni='"+u.getProvinciaConvenzioni()+"', visibile='1' WHERE idUsers = "+u.getIdUser();

		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean isRegistered(UserBean user)
	{
		
		boolean esito=false;
		String query = "SELECT * FROM users WHERE username = '"+user.getUsername()+"' and password='"+user.getPassword()+"'and visibile= 1";
	
		Vector<Object> v=db.executeSelect(query, "Users");
		UserBean res;
		try{
			res = (UserBean)v.get(0);
			user.setnome(res.getnome());
			user.setcognome(res.getcognome());
			user.setarea(res.getArea());
			user.setRuolo(res.getRuolo());
			user.setAnnoConvenzioni(res.getAnnoConvenzioni());
			user.setProvinciaConvenzioni(res.getProvinciaConvenzioni());
		}
		
		catch(Exception e){
			
			res=null;
		}
		if (res!=null){	
			
			esito=true;
		}

		System.out.println(esito);
		return esito;
	}
	
	public UserBean getUser(int idUsers)
	{
		UserBean res = new UserBean();
		String query = "SELECT * FROM users WHERE idUsers = "+idUsers;
		Vector<Object> v =db.executeSelect(query, "Users");
		res = (UserBean)v.get(0);
		return res;
	}
	
	public Vector<UserBean> getUtenti()
	{
		Vector<UserBean> res = new Vector<UserBean>();
		String query = "SELECT * FROM users where visibile ='1' ORDER BY Nome";
		Vector<Object> supp = db.executeSelect(query, "Users");
		for(int i=0;i<supp.size();i++)
			res.add((UserBean)supp.get(i));
		
		return res;
		
	}

	
	public void caricaArea(UserBean user)
	{
		String area="";
		
		String query = "SELECT area FROM users WHERE username = '"+user.getUsername()+"'";
				
		area=db.getUserArea(query);
		user.setarea(area);
		
		String nome="";
		
		String queryNome = "SELECT nome FROM users WHERE username = '"+user.getUsername()+"'";
				
		nome=db.getUserNome(queryNome);
		user.setnome(nome);
		
		String cognome="";
		
		String queryCognome = "SELECT cognome FROM users WHERE username = '"+user.getUsername()+"'";
				
		cognome=db.getUserCognome(queryCognome);
		user.setcognome(cognome);
		
	}	
	
	
}