package beans;

import java.util.Vector;

import util.DBManager;

public class CatalogoPersona {

	DBManager db;
	
	public CatalogoPersona()
	{
		db=new DBManager();
	}
	public CatalogoPersona(DBManager db)
	{
		this.db=db;
	}
	
	public Persona getPersona(int idPersona)
	{
		Persona res = new Persona();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idPersona = "+idPersona;
		Vector<Object> v =db.executeSelect(query, "Persona");
		res = (Persona)v.get(0);
		return res;
	}
	public Persona getPersonaRapplegale(int idEnte)
	{
		Persona res = new Persona();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idEnte = "+idEnte+" AND TitoloReferenza='Rappresentante legale'";
		System.out.println(query);
		Vector<Object> v =db.executeSelect(query, "Persona");
		res = (Persona)v.get(0);
		return res;
	}
	public Persona getPersonaAltroRef(int idEnte)
	{
		Persona res = null;
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idEnte = "+idEnte+" AND TitoloReferenza='Altro referente'";
		System.out.println(query);
		Vector<Object> v =db.executeSelect(query, "Persona");

		if(v.size()!=0){
			res= new Persona();
			res = (Persona)v.get(0);
			}
		return res;
		
	}
	
	public Vector<Persona> getPersone()
	{
		Vector<Persona> res = new Vector<Persona>();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona";
		Vector<Object> supp = db.executeSelect(query, "Persona");
		for(int i=0;i<supp.size();i++)
			res.add((Persona)supp.get(i));
		
		return res;
	}
	public Vector<Persona> getPersoneEnte(int idEnte)
	{
		Vector<Persona> res = new Vector<Persona>();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idEnte="+idEnte;
		Vector<Object> supp = db.executeSelect(query, "Persona");
		for(int i=0;i<supp.size();i++)
			res.add((Persona)supp.get(i));
		
		return res;
	}
	
	public Vector<Persona> getRappLegaleEnte(int idEnte)
	{
		Vector<Persona> res = new Vector<Persona>();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idEnte="+idEnte+" and TitoloReferenza='Rappresentante legale' order by idPersona desc";
		Vector<Object> supp = db.executeSelect(query, "Persona");
		for(int i=0;i<supp.size();i++)
			res.add((Persona)supp.get(i));
		
		return res;
	}
	
	public Persona getLastRappLegaleEnte(int idEnte)
	{
		Persona res = null;
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE idEnte="+idEnte+" and TitoloReferenza='Rappresentante legale' order by idPersona desc";
		Vector<Object> v = db.executeSelect(query, "Persona");
		
		if(v.size()!=0){
			res= new Persona();
			res = (Persona)v.get(0);
			}
		return res;
		
	}
	
	public Vector<Persona> getPersonaCapoEquipe()
	{
		Vector<Persona> res = new Vector<Persona>();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE TitoloReferenza like '%Capo Equipe%'";
		Vector<Object> supp =db.executeSelect(query, "Persona");
		for(int i=0;i<supp.size();i++)
			res.add((Persona)supp.get(i));
		
		return res;
	}
	public boolean salvaPersona(Persona u)
	{
		boolean esito;
		
		String query = "INSERT INTO persona (idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, email,CF, DataNascita, LuogoNascita)" +
				" VALUES ( " +u.getIdPersona()+" ,"+u.getIdEnte()+" ,'"+u.getTitoloReferenza()+"', '"+u.getNome()+"','"+u.getCognome()+"','"+u.getIndirizzo()+"','"+u.getCap()+"', '"+u.getComune()+"','"+u.getCitta()+"','"+u.getTelefono()+"','"+u.getCellulare()+"','"+u.getEmail()+"','"+u.getCF()+"','"+u.getDataNascita()+"','"+u.getLuogoNascita()+"')";
		
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean eliminaPersona(Persona u)
	{
		boolean esito;
		String query = "DELETE FROM persona WHERE idPersona = " +u.getIdPersona();
		esito = db.executeUpdate(query);
		return esito;
	}

	public Persona getPersonaFromNomeCognome(String nome, String cognome)
	{
		Persona res = new Persona();
		String query = "SELECT idPersona, idEnte, titoloReferenza, nome, cognome, indirizzo, cap, comune, citta, telefono, cellulare, Email, CF, dataNascita, luogoNascita FROM persona WHERE nome = '"+nome+"' and cognome='"+cognome+"'";
		Vector<Object> v =db.executeSelect(query, "Persona");
		res = (Persona)v.get(0);
		return res;
	}
	
	
	public DBManager getDB(){
		return db;
	}
	
	
	public String getNomePersona(int idPersona)
	{
		Persona res = new Persona();
		String nome="";
		String query = "SELECT nome FROM persona WHERE idPersona = "+idPersona;
		Vector<Object> v =db.executeSelect(query, "Persona");
		res = (Persona)v.get(0);
		nome=res.getNome();
		return nome;
	}
	public String getCognomePersona(int idPersona)
	{
		Persona res = new Persona();
		String cognome="";
		String query = "SELECT cognome FROM persona WHERE idPersona = "+idPersona;
		Vector<Object> v =db.executeSelect(query, "Persona");
		res = (Persona)v.get(0);
		cognome=res.getCognome();
		return cognome;
	}
	
	
	public boolean aggiornaPersona(int id, Persona u){
		
		String query = " UPDATE persona SET idPersona='" +u.getIdPersona()+"', idEnte='"+u.getIdEnte()+"', titoloReferenza='"+u.getTitoloReferenza()+"', nome='"+u.getNome()+"', cognome='"+u.getCognome()+"', indirizzo='"+u.getIndirizzo()+"', cap='"+u.getCap()+"', comune='"+u.getComune()+"', citta='"+u.getCitta()+"', telefono='"+u.getTelefono()+"', cellulare='"+u.getCellulare()+"', email='"+u.getEmail()+"', CF='"+u.getCF()+"', DataNascita='"+u.getDataNascita()+"',LuogoNascita='"+u.getLuogoNascita()+"' WHERE idPersona="+u.getIdPersona()+"";
				
		boolean esito =db.executeUpdate(query);
		
		
	return esito;
	}

	
	public boolean getUsernameEsistente(String username)
	{
		
		String query = "SELECT COUNT(idPersona) FROM persona WHERE username='"+username+"'";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont==0)
			return false;
		else
			return true;
	}
	
	public boolean isCancellable(int idPersona)
	{
		
		String query = "SELECT count(*)"
				+ " FROM convenzione c"
				+ " WHERE c.idRappLegaleEnte="+idPersona+"";
		Vector<Object> supp = db.executeSelect(query, "Count");
		Integer res =(Integer)supp.get(0);
		int cont=res.intValue();
		if(cont==0)
			return true;
		else
			return false;
	}
	
	
	
}
