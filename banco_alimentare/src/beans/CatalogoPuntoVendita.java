package beans;

import java.util.StringTokenizer;
import java.util.Vector;

import util.DBManager;

public class CatalogoPuntoVendita {

	DBManager db;
	
	public CatalogoPuntoVendita()
	{
		db=new DBManager();
	}
	public CatalogoPuntoVendita(DBManager db)
	{
		this.db=db;
	}
	

	
	public Vector<PuntoVendita> getPuntoVendita()
	{
		Vector<PuntoVendita> res = new Vector<PuntoVendita>();
		String query = "SELECT * FROM puntovendita order by area, comune, insegna";
		Vector<Object> supp = db.executeSelect(query, "PuntoVendita");
		for(int i=0;i<supp.size();i++)
			res.add((PuntoVendita)supp.get(i));
		
		return res;
	}
	
	
	private Vector<PuntoVendita> getAreaPuntoVendita(String area)
	{
		Vector<PuntoVendita> res = new Vector<PuntoVendita>();
		String query = "SELECT * FROM puntovendita WHERE visibile=1 and area = ? order by comune, insegna";
		
		Vector<Object> supp = db.executeSelectAreaPV(query, area);
		for(int i=0;i<supp.size();i++)
			res.add((PuntoVendita)supp.get(i));
		
		return res;
	}
	
	public Vector<PuntoVendita> getPuntoVenditaByAree(String area)
	{
		
		Vector<PuntoVendita> allAree = new Vector<PuntoVendita>();
		
		StringTokenizer st= new StringTokenizer(area,",");
		
		Vector<PuntoVendita> res = new Vector<PuntoVendita>();
		while(st.hasMoreTokens()){
			res.clear();
			res=getAreaPuntoVendita(st.nextToken());
			allAree.addAll(res);
			
		}
		
		return allAree;
	}
	
	public PuntoVendita getPuntoVendita(int idPuntoVendita)
	{
		PuntoVendita res = new PuntoVendita();
		String query = "SELECT * FROM puntovendita WHERE idPuntoVendita = "+idPuntoVendita;
		Vector<Object> v =db.executeSelect(query, "PuntoVendita");
		res = (PuntoVendita)v.get(0);
		return res;
	}
	
	public boolean salvaPuntoVendita(PuntoVendita u)
	{
		boolean esito;
		
		String query = "INSERT INTO puntovendita (idPuntoVendita, Gruppo, Insegna, Provincia, indirizzo, cap, comune, citta, telefono, NumCasse, email,Regione, Area, MQvendita, cellulare, orario)" +
				" VALUES ( " +u.getIdPuntoVendita()+" ,'"+u.getGruppo()+"' ,'"+u.getInsegna()+"', '"+u.getProvincia()+"','"+u.getIndirizzo()+"','"+u.getCap()+"', '"+u.getComune()+"','"+u.getCitta()+"','"+u.getTelefono()+"','"+u.getNumCasse()+"','"+u.getEmail()+"','"+u.getRegione()+"','"+u.getArea()+"','"+u.getMQvendita()+"','"+u.getCellulare()+"','"+u.getOrario()+"')";
		
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean eliminaPuntoVendita(PuntoVendita u)
	{
		boolean esito;
		String query = "DELETE FROM puntovendita WHERE idPuntoVendita = " +u.getIdPuntoVendita();
		esito = db.executeUpdate(query);
		return esito;
	}

	public boolean nascondiPuntoVendita(PuntoVendita u)
	{
		boolean esito;
		String query = "UPDATE puntovendita set visibile=0 WHERE idPuntoVendita = " +u.getIdPuntoVendita();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean mostraPuntoVendita(PuntoVendita u)
	{
		boolean esito;
		String query = "UPDATE puntovendita set visibile=1 WHERE idPuntoVendita = " +u.getIdPuntoVendita();
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public DBManager getDB(){
		return db;
	}
	
//	da completare
	public boolean aggiornaPuntoVendita(int id, PuntoVendita u){
		
		String query = " UPDATE puntovendita SET idPuntoVendita='"+u.getIdPuntoVendita()+"', Gruppo='"+u.getGruppo()+"', Insegna='"+u.getInsegna()+"', Provincia='"+u.getProvincia()+"', Indirizzo='"+u.getIndirizzo()+"', cap='"+u.getCap()+"', comune='"+u.getComune()+"', citta='"+u.getCitta()+"', telefono='"+u.getTelefono()+"', cellulare='"+u.getCellulare()+"',  orario='"+u.getOrario()+"', email='"+u.getEmail()+"', regione='"+u.getRegione()+"', numcasse='"+u.getNumCasse()+"',area='"+u.getArea()+"',mqvendita='"+u.getMQvendita()+"' WHERE idPuntoVendita="+u.getIdPuntoVendita();
		System.out.println(query);		
		boolean esito =db.executeUpdate(query);
		
		
	return esito;
	}

	

	
	
	
	
	
}
