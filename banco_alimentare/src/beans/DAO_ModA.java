package beans;

import java.util.Vector;

import util.DBManager;

public class DAO_ModA {
	DBManager db;
	
	
	public DAO_ModA() {
		db=new DBManager();
	}

	public DAO_ModA(DBManager db) {
		this.db = db;
	}

	public int lastID() {
		Vector<ModA> res = new Vector<ModA>();
		String query = "SELECT * FROM ModA ORDER BY idScheda DESC";
		Vector<Object> v = db.executeSelect(query, "ModA");
		
		res.add((ModA)v.get(0));
		System.out.println("id="+res.get(0).getIdScheda());
		return res.get(0).getIdScheda();
	}
	public Vector<ModA> getModAs()
	{
		Vector<ModA> res = new Vector<ModA>();
		String query = "SELECT * FROM ModA";
		Vector<Object> v = db.executeSelect(query, "ModA");

		for(int i=0;i<v.size();i++)
			res.add((ModA)v.get(i));

		return res;
	}
	
	public Vector<ModA> getModAspdv(int idPuntoVendita)
	{
		Vector<ModA> res = new Vector<ModA>();
		String query = "SELECT * FROM ModA where idPuntoVendita="+idPuntoVendita;
		Vector<Object> v = db.executeSelect(query, "ModA");

		for(int i=0;i<v.size();i++)
			res.add((ModA)v.get(i));

		return res;
	}
	
	
	
	public boolean salvaModA(ModA p)
	{
		boolean esito;
				
		
		String query = "INSERT INTO ModA VALUES ( '?','?','?','?')";
		
		query=query.replaceFirst("[?]",((Integer)p.getIdScheda()).toString());
		query=query.replaceFirst("[?]",p.getNumeroScheda());
		query=query.replaceFirst("[?]",((Integer)p.getIdCapoEquipe()).toString());
		query=query.replaceFirst("[?]",((Integer)p.getIdPuntoVendita()).toString());
		
		System.out.println(query);
		esito = db.executeUpdate(query);
		return esito;
	}
	
	public boolean eliminaModA(ModA u)
	{
		boolean esito;
		String query = "DELETE FROM ModA WHERE idScheda = " +u.getIdScheda();
		esito = db.executeUpdate(query);
		return esito;
	}
}
