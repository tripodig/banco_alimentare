package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.*;

/**
 * Servlet implementation class New_referente
 */
@WebServlet("/New_puntovendita")
public class New_puntovendita extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public New_puntovendita() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub

try{
	boolean result=false;
		String c1=request.getParameter("Gruppo");
		String c2=request.getParameter("Insegna");
		String c3=request.getParameter("Provincia");
		String c9=request.getParameter("Comune");
		String c10=request.getParameter("Indirizzo");
		String c11=request.getParameter("Cap");
		String c12=request.getParameter("Citta");
		String c13=request.getParameter("Email");
		String c52=request.getParameter("Telefono");
		String c53=request.getParameter("Cellulare");
		String aAM=request.getParameter("aperturaAM");
		String cAM=request.getParameter("chiusuraAM");
		String aPM=request.getParameter("aperturaPM");
		String cPM=request.getParameter("chiusuraPM");
		if(aAM==null||aAM.equals(""))aAM="--:--";
		if(cAM==null||cAM.equals(""))cAM="--:--";
		if(aPM==null||aPM.equals(""))aPM="--:--";
		if(cPM==null||cPM.equals(""))cPM="--:--";
		String c15=request.getParameter("NumCasse");
		String azione=request.getParameter("azione");
		String PuntoVenditas=request.getParameter("idPuntoVendita");
		String Regione=request.getParameter("Regione");
		String Area=request.getParameter("Area");
		String MQvendita=request.getParameter("MQvendita");
		
		System.out.println(azione);
		
		CatalogoPuntoVendita catp= new CatalogoPuntoVendita();
		
		
			PuntoVendita p=new PuntoVendita();	
		
			p.setCap(c11);
			p.setCitta(c12);
			p.setProvincia(c3);
			p.setComune(c9);
			p.setEmail(c13);
			p.setInsegna(c2);
			p.setIndirizzo(c10);
			p.setTelefono(c52);
			p.setCellulare(c53);

			p.setOrario(aAM+" "+cAM+" "+aPM+" "+cPM);
			p.setGruppo(c1);
			p.setRegione(Regione);
			p.setArea(Area);
			p.setNumCasse(c15);
			p.setMQvendita(MQvendita);
		
		
		if(azione.equals("modifica")){
			System.out.println("in azione)");
			String id=request.getParameter("idPuntoVendita");
			
			int idPuntoVendita=Integer.parseInt(id);
			PuntoVendita u=catp.getPuntoVendita(idPuntoVendita);
			u.setCap(c11);
			u.setCitta(c12);
			u.setProvincia(c3);
			u.setComune(c9);
			u.setEmail(c13);
			u.setInsegna(c2);
			u.setIndirizzo(c10);
			u.setTelefono(c52);
			u.setCellulare(c53);
			u.setOrario(aAM+" "+cAM+" "+aPM+" "+cPM);
			u.setGruppo(c1);
			u.setRegione(Regione);
			u.setArea(Area);
			u.setNumCasse(c15);
			u.setMQvendita(MQvendita);
			result= catp.aggiornaPuntoVendita(idPuntoVendita,u);
			System.out.println(result);
		}
		
		else{	
			
		
		result=catp.salvaPuntoVendita(p);
		System.out.println(result);
		}
		String jspPage;
		if (result){
			System.out.println("Operazione eseguita correttamente!");
			String commit="Operazione eseguita correttamente!";    		 
			request.setAttribute("commit", commit);
			jspPage="result/commit.jsp";
	    }
        else{
           System.out.println("!!! Operazione non eseguita !!!");
           String errore="Operazione non eseguita correttamente!";    		 
           request.setAttribute("errore", errore);
           jspPage="result/error.jsp";
        }
	    request.getRequestDispatcher( jspPage ).forward(request,response);
}
		
		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	        request.setAttribute("errore", errore);
	        request.getRequestDispatcher( jspPage ).forward(request,response);

	        Exception.printStackTrace();
			
		}
}

}
