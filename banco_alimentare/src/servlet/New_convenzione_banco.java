package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.FirstPdf;
import beans.*;

/**
 * Servlet implementation class New_convenzione_banco
 */
@WebServlet("/New_convenzione_banco")
public class New_convenzione_banco extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_convenzione_banco() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			boolean result=false;
			
			//anno della convenzione: lo prelevo dalle proprietÓ dell'utente connesso
			UserBean user=(UserBean) request.getSession().getAttribute("currentSessionUser");
			int anno= user.getAnnoConvenzioni();
			
			String c1=request.getParameter("idEnte"); // 1
			
			String h1=request.getParameter("idSedeLegaleEnte");
			String h2=request.getParameter("idSedeOperativaEnte");
			String h3=request.getParameter("idSedeMagazzinoEnte");

			String r1=request.getParameter("idRappLegaleEnte");
			
			int idEnte=Integer.parseInt(c1);
			
			int idSedeLegaleEnte=Integer.parseInt(h1);
			int idSedeOperativaEnte=Integer.parseInt(h2);
			int idSedeMagazzinoEnte=Integer.parseInt(h3);
			int idRappLegaleEnte=Integer.parseInt(r1);
			
			System.out.println("Sedi "+h1+" "+h2+" "+h3);
			
			
			
			String c25=request.getParameter("frigo"); 
			System.out.println(c25);
			boolean frigo=false;
			if (c25.equals("si"))
				frigo=true;

			System.out.println(frigo);
			
			String c27=request.getParameter("pranzo"); // 11
			boolean pranzo=false;
			if(c27!=null)
				pranzo=true;
			String c28=request.getParameter("cena"); // 12
			boolean cena=false;
			if(c28!=null)
				cena=true;
			
			String c29=request.getParameter("colazione"); // 13
			boolean colazione=false;
			if(c29!=null)
				colazione=true;
			String c30=request.getParameter("pastifreddi"); // 14
			boolean pastifreddi=false;
			if(c30!=null)
				pastifreddi=true;
			String c31=request.getParameter("centroaccoglienza"); // 15
		
			boolean centroaccoglienza=false;
			if(c31!=null)
				centroaccoglienza=true;
			String c32=request.getParameter("05Anni"); // 16
//			se non metto valori salva 0
//			if (c32.equals(""))
//				c32="0";
			int M_0_5=Integer.parseInt(c32);
			String c33=request.getParameter("665Anni"); // 17
			int M_19_65=Integer.parseInt(c33);
			String c34=request.getParameter("over65Anni"); // 18
			int M_over_65=Integer.parseInt(c34);
			String c35=request.getParameter("TotIng1");// 2
			int M_totIndigenti=Integer.parseInt(c35);
			String c36=request.getParameter("MedPres1");// 5
			int M_MediaPresenze=Integer.parseInt(c36);
			String c37=request.getParameter("GioniApertura1"); // 8
			int M_GiorniApertura=Integer.parseInt(c37);
			String c38=request.getParameter("casafamiglia"); // 19
			boolean casafamiglia=false;
			if(c38!=null)
				casafamiglia=true;
			String c39=request.getParameter("comunitadisabili"); // 20
			boolean comunitadisabili=false;
			if(c39!=null)
				comunitadisabili=true;
			String c40=request.getParameter("comunitatossicodip"); // 21
			boolean comunitatossicodip=false;
			if(c40!=null)
				comunitatossicodip=true;
			String c41=request.getParameter("comunitaanziani"); // 22
			boolean comunitaanziani=false;
			if(c41!=null)
				comunitaanziani=true;
			String c42=request.getParameter("comunitaminori"); // 23
			boolean comunitaminori=false;
			if(c42!=null)
				comunitaminori=true;
			String c43=request.getParameter("R05Anni"); // 24
			int R_0_5=Integer.parseInt(c43);
			String c44=request.getParameter("R665Anni"); // 25
			int R_19_65=Integer.parseInt(c44);
			String c45=request.getParameter("Rover65Anni"); // 26
			int R_over_65=Integer.parseInt(c45);

			String c46=request.getParameter("TotIng2");// 3
			int R_totIndigenti=Integer.parseInt(c46);
			String c47=request.getParameter("MedPres2");// 6
			int R_MediaPresenze=Integer.parseInt(c47);
			String c48=request.getParameter("distribuzionepacchi"); // 27
			boolean distribuzionepacchi=false;
			if(c48!=null)
				distribuzionepacchi=true;
			String c49=request.getParameter("consegnadomicilio"); // 28
			boolean consegnadomicilio=false;
			if(c49!=null)
				consegnadomicilio=true;
			String c50=request.getParameter("altro"); // 29
			
			if(c50!=null) // in questo caso ho checkato Altro e devo prelevarmi il valore che specifico nella casella di testo
			c50=request.getParameter("specifica");
			else
				c50="no";
			
			String c51=request.getParameter("P05Anni"); // 30
			int P_0_5=Integer.parseInt(c51);
			String c52=request.getParameter("P665Anni"); // 31
			int P_19_65=Integer.parseInt(c52);
			String c53=request.getParameter("Pover65Anni"); // 32
			int P_over_65=Integer.parseInt(c53);
			String c54=request.getParameter("TotIng3");// 4
			int P_totIndigenti=Integer.parseInt(c54);
			String c55=request.getParameter("MedPres3");// 7
			int P_MediaPresenze=Integer.parseInt(c55);
			String c56=request.getParameter("GioniApertura3");//9
			int P_GiorniApertura=Integer.parseInt(c56);
			String data=request.getParameter("Data");//9
			//String prova="lVHS";
			
			//3 elementi aggiunti ctegorie 6-18 anno 2014
			String c101=request.getParameter("M618Anni"); // 24
			int M_6_18=Integer.parseInt(c101);
			String c102=request.getParameter("P618Anni"); // 24
			int P_6_18=Integer.parseInt(c102);
			String c103=request.getParameter("R618Anni"); // 24
			int R_6_18=Integer.parseInt(c103);
	//		int idRappLegaleEnte=0;
			Convenzione c=new Convenzione(idEnte,idRappLegaleEnte,idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,anno,M_totIndigenti,R_totIndigenti,P_totIndigenti,M_MediaPresenze,R_MediaPresenze,P_MediaPresenze,M_GiorniApertura,P_GiorniApertura,365,frigo,pranzo,cena,colazione,pastifreddi,centroaccoglienza,M_0_5,M_6_18,M_19_65,M_over_65,casafamiglia,comunitadisabili,comunitatossicodip,comunitaanziani,comunitaminori,R_0_5,R_6_18,R_19_65,R_over_65,distribuzionepacchi,consegnadomicilio,c50,P_0_5,P_6_18,P_19_65,P_over_65,data);

			CatalogoConvenzioni DBconv= new CatalogoConvenzioni();
			System.out.println("cellafrigo="+ c25 );
			if(c25.equals("si"))
				c25="1";

			result=DBconv.salvaConvenzione(c);
			String jspPage;
			if (result){
				
				FirstPdf f=new FirstPdf();
				String path=request.getSession().getServletContext().getRealPath("/");
				System.out.println(path);
				String directory_separator="\\";
				String path0=path+directory_separator+"fileSupporto"+directory_separator+"modulo_empty_banco.PDF";
				String path1=path+directory_separator+"Moduli_convenzioni"+directory_separator+""+idEnte+"_modulo_convenzione_"+anno+".pdf";
				System.out.println(path0);
				System.out.println(path1);
				CatalogoConvenzioni catconv=new CatalogoConvenzioni();
				//int idConvenzione=catconv.getidconvenzione(idEnte, anno);
				int idConvenzione=catconv.getidconvenzione(idEnte, idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,anno);
				System.out.println("idconvenzione="+idConvenzione);
				f.StampaModuli_Convenzione_Banco(path0,path1, idConvenzione);
				
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!"+"<br><center><a href=\"Moduli_convenzioni/"+idEnte+"_modulo_convenzione_"+anno+".pdf\"><img src=\"image/icona_pdf.jpg\" ></a>"+
				"<br><br> Clicca sull'icona per aprire il modulo!"+
"<br><br>Stampa subito <a href=\"print_now.jsp?pathstampa=<%out.print(path1);%>\">Stampa senza aprire (sperimentale)</a></center>";    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
		    }
	        else{
	           System.out.println("!!! Operazione non eseguita !!!");
	           String errore="Operazione non eseguita correttamente!";    		 
	           request.setAttribute("errore", errore);
	           jspPage="result/error.jsp";
	        }
		    request.getRequestDispatcher( jspPage ).forward(request,response);
	}
			
			catch(Throwable Exception)
			{
				String jspPage = "result/error.jsp";
				String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
		        request.setAttribute("errore", errore);
		        request.getRequestDispatcher( jspPage ).forward(request,response);

		        Exception.printStackTrace();
				
			}
	}


}
