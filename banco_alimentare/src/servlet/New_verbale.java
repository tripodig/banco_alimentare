package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.DocumentoDTO;

import beans.CatalogoConvenzioni;
import beans.Convenzione;
import beans.DAO_Documento;
import beans.UserBean;

/**
 * Servlet implementation class New_verbale
 */
@WebServlet("/New_verbale")
public class New_verbale extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_verbale() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			boolean result=false;
			
			//parte relativa a verbale presenza fascicoli
			
			//anno della convenzione: lo prelevo dalle proprietÓ dell'utente connesso
			UserBean user=(UserBean) request.getSession().getAttribute("currentSessionUser");
			int anno= user.getAnnoConvenzioni();
			
			String strIdConvenzione=request.getParameter("idConvenzione");
			int idConvenzione=Integer.parseInt(strIdConvenzione);
			
			String strIdEnte=request.getParameter("idEnte");
			int idEnte=Integer.parseInt(strIdEnte);
			
			String strTipoDocumento=request.getParameter("tipoDocumento");
			String strData=request.getParameter("data");
			String strIncaricato=request.getParameter("incaricato");
			
			String strPresenzaElenco=request.getParameter("presenzaElenco");
			String strNumPersone=request.getParameter("numPersone");
			String strNumNuclei=request.getParameter("numNuclei");
			String strPresenzaFascicoli=request.getParameter("presenzaFascicoli");
			String strNumFascicoli=request.getParameter("numFascicoli");
			String strFascicoliCarenti=request.getParameter("fascicoliCarenti");
			String strNumFascicoliCarenti=request.getParameter("numFascicoliCarenti");
			String strNumFascicoliEsaminati=request.getParameter("numFascicoliEsaminati");

			
			String strPresenzaRegistro=request.getParameter("presenzaRegistro");
			String strRegistroInformatizzato=request.getParameter("registroInformatizzato");
			String strFogliInamovibili=request.getParameter("fogliInamovibili");
			String strRegistroErrato=request.getParameter("registroErrato");
			String strRegistroFirmato=request.getParameter("registroFirmato");
			String strPresenzaDichiarazione=request.getParameter("presenzaDichiarazione");
			String strRegistroSenzaFirma=request.getParameter("registroSenzaFirma");
			String strRegistroSenzaNumData=request.getParameter("registroSenzaNumData");
			String strPresenzaAttestati=request.getParameter("presenzaAttestati");

			String strAnnotazioni=request.getParameter("annotazioni");
			
			
			DocumentoDTO  documentoDTO= new DocumentoDTO();
			
			documentoDTO.setIdConvenzione(idConvenzione);
			documentoDTO.setTipoDocumento(strTipoDocumento);
			documentoDTO.setCampo1(strData);
			documentoDTO.setCampo2(strIncaricato);
			if(strTipoDocumento.equals("verbale_fascicoli")){
				documentoDTO.setCampo3(strPresenzaElenco);
				documentoDTO.setCampo4(strNumPersone);
				documentoDTO.setCampo5(strNumNuclei);
				documentoDTO.setCampo6(strPresenzaFascicoli);
				documentoDTO.setCampo7(strNumFascicoli);
				documentoDTO.setCampo8(strFascicoliCarenti);
				documentoDTO.setCampo9(strNumFascicoliCarenti);
				documentoDTO.setCampo10(strAnnotazioni);
				documentoDTO.setCampo11(strNumFascicoliEsaminati);
			}
			if(strTipoDocumento.equals("verbale_registro")){
				documentoDTO.setCampo3(strPresenzaRegistro);
				documentoDTO.setCampo4(strRegistroInformatizzato);
				documentoDTO.setCampo5(strFogliInamovibili);
				documentoDTO.setCampo6(strRegistroErrato);
				documentoDTO.setCampo7(strRegistroFirmato);
				documentoDTO.setCampo8(strPresenzaDichiarazione);
				documentoDTO.setCampo9(strRegistroSenzaFirma);


				documentoDTO.setCampo10(strRegistroSenzaNumData);
				documentoDTO.setCampo11(strPresenzaAttestati);
				documentoDTO.setCampo12(strAnnotazioni);
			}
			DAO_Documento DAO_documento =new DAO_Documento();
			
			result=DAO_documento.salva(documentoDTO);
			
			//recupero idente
//			CatalogoConvenzioni catc=new CatalogoConvenzioni();
//			Convenzione conv=catc.getConvenzione(idConvenzione);
//			
//			int idEnte=conv.getIdEnte();
			
			String jspPage;
			if (result){
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!"+"<br>"+
				 "<a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">VAI A ENTE</a><br>"+ "<a href=\"convenzioni/verbale.jsp?id="+ idEnte +"&idconv="+idConvenzione+"\">AGGIUNGI VERBALE</a>";    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
			}
			else{
				System.out.println("!!! Operazione non eseguita !!!");
				String errore="Operazione non eseguita correttamente!";    		 
				request.setAttribute("errore", errore);
				jspPage="result/error.jsp";
			}
			request.getRequestDispatcher( jspPage ).forward(request,response);
			
			
			
		}
		
		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	        request.setAttribute("errore", errore);
	        request.getRequestDispatcher( jspPage ).forward(request,response);

	        Exception.printStackTrace();
			
		}
	}

}
