package servlet;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.FileBean;
import beans.FileDAO;
import util.utility;


/**
 * A Java servlet that handles file upload from client.
 *
 * @author www.codejava.net
 */

@WebServlet("/uploadFile")
@MultipartConfig(maxFileSize = 16177215)
public class FileUploadServlet extends HttpServlet {


	// location to store file uploaded
//	private static final String UPLOAD_DIRECTORY = "upload";

	// upload settings
//	private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
//	private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
//	private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

	/**
	 * Upon receiving file upload submission, parses the request to read
	 * upload data and saves the file on disk.
	 */
	//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	//        // checks if the request actually contains upload file
	//        if (!ServletFileUpload.isMultipartContent(request)) {
	//            // if not, we stop here
	//            PrintWriter writer = response.getWriter();
	//            writer.println("Error: Form must has enctype=multipart/form-data.");
	//            writer.flush();
	//            return;
	//        }
	//        request.
	//        String text=request.getParameter("text");
	//        // configures upload settings
	//        DiskFileItemFactory factory = new DiskFileItemFactory();
	//        // sets memory threshold - beyond which files are stored in disk
	//        factory.setSizeThreshold(MEMORY_THRESHOLD);
	//        // sets temporary location to store files
	//        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
	// 
	//        ServletFileUpload upload = new ServletFileUpload(factory);
	//         
	//        // sets maximum size of upload file
	//        upload.setFileSizeMax(MAX_FILE_SIZE);
	//         
	//        // sets maximum size of request (include file + form data)
	//        upload.setSizeMax(MAX_REQUEST_SIZE);
	// 
	//        // constructs the directory path to store upload file
	//        // this path is relative to application's directory
	//        String uploadPath = getServletContext().getRealPath("")
	//                + File.separator + UPLOAD_DIRECTORY;
	//         
	//        // creates the directory if it does not exist
	//        File uploadDir = new File(uploadPath);
	//        if (!uploadDir.exists()) {
	//            uploadDir.mkdir();
	//        }
	// 
	//        try {
	//            // parses the request's content to extract file data
	//            @SuppressWarnings("unchecked")
	//            List<FileItem> formItems = upload.parseRequest(request);
	// 
	//            if (formItems != null && formItems.size() > 0) {
	//                // iterates over form's fields
	//                for (FileItem item : formItems) {
	//                    // processes only fields that are not form fields
	//                    if (!item.isFormField()) {
	//                        String fileName = new File(item.getName()).getName();
	//                        String filePath = uploadPath + File.separator + fileName;
	//                        File storeFile = new File(filePath);
	// 
	//                        // saves the file on disk
	//                        item.write(storeFile);
	//                        request.setAttribute("message",
	//                            "Upload has been done successfully!");
	//                    }
	//                    
	//                    	
	//                }
	//            }
	//        } catch (Exception ex) {
	//            request.setAttribute("message",
	//                    "There was an error: " + ex.getMessage());
	//        }
	//        // redirects client to message page
	//        getServletContext().getRequestDispatcher("/message.jsp").forward(
	//                request, response);
	//    }







	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		boolean esito=false;

		// gets values of text fields
		String idEnteStr = request.getParameter("idEnte");
		String idUserStr = request.getParameter("idUser");
		String descrizione = request.getParameter("descrizione");

		int idEnte= Integer.parseInt(idEnteStr);
		int idUser= Integer.parseInt(idUserStr);

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("file");
		
	

		String NomeFile=null;

		if (filePart != null) {
			NomeFile=utility.getFilename(filePart);
			System.out.println(NomeFile);
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

	        
			InputStream filecontent = null;
			OutputStream fout = null;
			filecontent = filePart.getInputStream();
		    
			
			
			// constructs the directory path to store upload file
	        // this path is relative to application's directory
			
	        String uploadPath = utility.pathWindows+ File.separator + utility.UPLOAD_DIRECTORY;
//	        String uploadPath = getServletContext().getRealPath("")+ File.separator + utility.UPLOAD_DIRECTORY;
	         
	        // creates the directory if it does not exist
	        File uploadDir = new File(uploadPath);
	        if (!uploadDir.exists()) {
	            uploadDir.mkdir();
	        }
//	        String type="";
//			String path ="E:/images/myfolder";
//	
		    //return content type of the file
//		    type=filePart.getHeader("content-type");
//		    System.out.println(type);
		    
			fout = new FileOutputStream(new File(uploadPath + File.separator + NomeFile));
            filecontent = filePart.getInputStream();
            int read = 0;
            final byte[] bytes = new byte[32*1024];
 
            while ((read =filecontent.read(bytes)) != -1) {
                fout.write(bytes, 0, read);
            }
            fout.flush();
            fout.close();

		}

		        
		        
//		        DiskFileItemFactory factory = new DiskFileItemFactory();
//		        // sets memory threshold - beyond which files are stored in disk
//		        factory.setSizeThreshold(MEMORY_THRESHOLD);
//		        // sets temporary location to store files
//		        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
//		        ServletFileUpload upload = new ServletFileUpload(factory);
//		         
//		        // sets maximum size of upload file
//		        upload.setFileSizeMax(MAX_FILE_SIZE);
//		         
//		        // sets maximum size of request (include file + form data)
//		        upload.setSizeMax(MAX_REQUEST_SIZE);
//		            @SuppressWarnings("unchecked")
//		            List<FileItem> formItems = upload.parseRequest(request);
//		 
//		            if (formItems != null && formItems.size() > 0) {
//		                // iterates over form's fields
//		                for (FileItem item : formItems) {
//		                    // processes only fields that are not form fields
//		                    if (!item.isFormField()) {
//		                        String fileName = new File(item.getName()).getName();
//		                        String filePath = uploadPath + File.separator + fileName;
//		                        File storeFile = new File(filePath);
//		 
//		                        // saves the file on disk
//		                        item.write(storeFile);
//		                        request.setAttribute("message",
//		                            "Upload has been done successfully!");
//		                    }
//		                    
//		                    	
//		                }
//		            }

		NomeFile=utility.getFilename(filePart);
		FileBean fileBean= new FileBean();
		FileDAO DAO  = new FileDAO();

		fileBean.setIdEnte(idEnte);
		fileBean.setIdUser(idUser);
		fileBean.setDescrizione(descrizione);
		fileBean.setNomeFile(NomeFile);
		//	fileBean.setDataIn(inputStream);
		esito=DAO.salvaFile(fileBean);
		String jspPage;
		String commit=null;
		if (esito){
			System.out.println("Operazione eseguita correttamente!");
			if(idEnte!=0)
				commit="Operazione eseguita correttamente!<br>"+ "<a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">VAI A ENTE</a>";    		
			
			else 
				commit="Operazione eseguita correttamente!<br>";
			request.setAttribute("commit", commit);
			jspPage="result/commit.jsp";
		}
		else{
			System.out.println("!!! Operazione non eseguita !!!");
			String errore="Operazione non eseguita correttamente!";    		 
			request.setAttribute("errore", errore);
			jspPage="result/error.jsp";
		}
		request.getRequestDispatcher( jspPage ).forward(request,response);
		


		
		}

		catch(Throwable Exception)
		{
			String jspPage ="result/error.jsp";
			String errore="Operazione noneseguita correttamente!  eccezione non aspettata";    		
			request.setAttribute("errore",errore);
			request.getRequestDispatcher(jspPage ).forward(request,response);

			Exception.printStackTrace();

		}
	}

}