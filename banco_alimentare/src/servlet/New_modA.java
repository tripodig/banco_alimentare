package servlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DAO_ModA;
import beans.Evento;
import beans.ModA;
import beans.ModB;
import beans.DAO_ModB;


/**
 * Servlet implementation class New_modB
 */
@WebServlet("/New_modA")
public class New_modA extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_modA() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean result=false;
		
		//creo i vettori delle somme del peso e delle somme del numero di pacchi
		Vector<Double> Somma=new Vector<Double>();
		Vector<Integer> pacchi=new Vector<Integer>();
		
		int Ora=0;
		DAO_ModB dm= new DAO_ModB();
		try{
			GregorianCalendar d=new GregorianCalendar();
			//Ora=((Integer)d.get(Calendar.HOUR_OF_DAY)); //ora attuale
			Ora=((((Integer)d.get(Calendar.HOUR_OF_DAY))+9)%24); //ora italia
			String c1=request.getParameter("idPuntoVendita"); 
			int idPuntoVendita=Integer.parseInt(c1);
			String c2=request.getParameter("idCapoEquipe"); 
			int idCapoEquipe=Integer.parseInt(c2);
			String c3=request.getParameter("NumScheda"); 
			String NumScheda=c3;
			
		//creo i vettori che conterranno i valori dei campi	
			Vector<String> Olio=new Vector<String>();
			Vector<String> Omogeneizzati=new Vector<String>();
			Vector<String> Infanzia=new Vector<String>();
			Vector<String> Tonno=new Vector<String>();
			Vector<String> Carne=new Vector<String>();
			Vector<String> Pelati=new Vector<String>();
			Vector<String> Legumi=new Vector<String>();
			Vector<String> Pasta=new Vector<String>();
			Vector<String> Riso=new Vector<String>();
			Vector<String> Zucchero=new Vector<String>();
			Vector<String> Latte=new Vector<String>();
			Vector<String> Biscotti=new Vector<String>();
			Vector<String> Varie=new Vector<String>();
			
		//	aggiungo il valore del peso al relativo vettore: sto prelevando il valore dalla singola cella e lo sto inserendo nel vettore corrispondente
			for (int a=1;a<11;a++){
				
				if (request.getParameter("Olio_"+a+"").equals("")){
					System.out.println(" campo olio"+a+" vuoto");
				}
				else
					Olio.add(request.getParameter("Olio_"+a+""));

				
				if (request.getParameter("Omogeneizzati_"+a+"").equals("")){
					System.out.println(" campo omog"+a+" vuoto");
				}
				else
					Omogeneizzati.add(request.getParameter("Omogeneizzati_"+a+""));
				
				if (request.getParameter("Infanzia_"+a+"").equals("")){
					System.out.println(" campo infanzia"+a+" vuoto");
				}
				else
					Infanzia.add(request.getParameter("Infanzia_"+a+""));
				
				if (request.getParameter("Tonno_"+a+"").equals("")){
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Tonno_"+a);
					Tonno.add(request.getParameter("Tonno_"+a+""));
				}
				
				if (request.getParameter("Carne_"+a+"").equals("")){
					System.out.println(" campo carne"+a+" vuoto");
					}
				else
					Carne.add(request.getParameter("Carne_"+a+""));
				
				if (request.getParameter("Pelati_"+a+"").equals("")){
					System.out.println(" campo pelati "+a+" vuoto");
				}
				else
					Pelati.add(request.getParameter("Pelati_"+a+""));
				
				if (request.getParameter("Legumi_"+a+"").equals("")){
					System.out.println(" campo legumi"+a+" vuoto");
				}
				else
					Legumi.add(request.getParameter("Legumi_"+a+""));
				
				if (request.getParameter("Pasta_"+a+"").equals("")){
					System.out.println(" campo pasta"+a+" vuoto");
				}
				else
					Pasta.add(request.getParameter("Pasta_"+a+""));
				
				if (request.getParameter("Riso_"+a+"").equals("")){
					System.out.println(" campo riso"+a+" vuoto");
				}
				else
					Riso.add(request.getParameter("Riso_"+a+""));

				if (request.getParameter("Zucchero_"+a+"").equals("")){
					System.out.println(" campo zucchero"+a+" vuoto");
				}
				else
					Zucchero.add(request.getParameter("Zucchero_"+a+""));
				
				if (request.getParameter("Latte_"+a+"").equals("")){
					System.out.println(" campo latte"+a+" vuoto");
				}
				else
					Latte.add(request.getParameter("Latte_"+a+""));
				
				if (request.getParameter("Biscotti_"+a+"").equals("")){
					System.out.println(" campo biscotti"+a+" vuoto");
				}
				else
					Biscotti.add(request.getParameter("Biscotti_"+a+""));
				
				if (request.getParameter("Varie_"+a+"").equals("")){
					System.out.println(" campo varie"+a+" vuoto");
				}
				else
					Varie.add(request.getParameter("Varie_"+a+""));

		}
			//non fa niente
System.out.println("Size ="+Olio.size());
for (int i=0;i<Olio.size();i++){
	System.out.println(Olio.get(i));

}

//blocco che serve per contare il peso totale e aggiungere il numero pacchi totale nei relativi vettori
Vector<Double> totale=new Vector<Double>();
Vector<Integer> totalepacchi=new Vector<Integer>();


//dichiaro le variabili ausiliarie che azzerer� di volta in volta
double total=0;
int totpacchi=0;

for (int i=0;i<Olio.size();i++){
	String value=Olio.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}//inserisco nei vettori i valori della somma peso e dei num pacchi, tengo conto dell'indice del vettore
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Omogeneizzati.size();i++){
	String value=Omogeneizzati.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Infanzia.size();i++){
	String value=Infanzia.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Tonno.size();i++){
	String value=Tonno.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Carne.size();i++){
	String value=Carne.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Pelati.size();i++){
	String value=Pelati.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Legumi.size();i++){
	String value=Legumi.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Pasta.size();i++){
	String value=Pasta.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Riso.size();i++){
	String value=Riso.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Zucchero.size();i++){
	String value=Zucchero.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Latte.size();i++){
	String value=Latte.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Biscotti.size();i++){
	String value=Biscotti.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;
for (int i=0;i<Varie.size();i++){
	String value=Varie.get(i).replace(",", ".");
	double add=Double.parseDouble(value);
	total=total+add;
	
	totpacchi=totpacchi+1;
	
}
Somma.add(total);
pacchi.add(totpacchi);

total=0;
totpacchi=0;

//blocco relativo al salvataggio su db
Evento e=new Evento();
e=(Evento) request.getSession().getAttribute("currentSessionEvent");
if(Ora<9)
	Ora=23;

ModB modelloB=new ModB(0, e.getIdEvento() ,Ora, NumScheda, idCapoEquipe, idPuntoVendita, 
		Somma.get(0), pacchi.get(0),
		Somma.get(1), pacchi.get(1),
		Somma.get(2), pacchi.get(2),
		Somma.get(3), pacchi.get(3),
		Somma.get(4), pacchi.get(4),
		Somma.get(5), pacchi.get(5),
		Somma.get(6), pacchi.get(6),
		Somma.get(7), pacchi.get(7),
		Somma.get(8), pacchi.get(8),
		Somma.get(9), pacchi.get(9),
		Somma.get(10), pacchi.get(10),
		Somma.get(11), pacchi.get(11),
		Somma.get(12), pacchi.get(12)
		);
result=dm.salvaModB(modelloB);




System.out.println("Ora="+Ora);





}catch(Throwable Exception)
{
	String jspPage = "result/error.jsp";

	String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	System.out.println(errore);
	request.setAttribute("errore", errore);
	request.getRequestDispatcher( jspPage ).forward(request,response);

	Exception.printStackTrace();

}
		
		if (result){
			System.out.println("Operazione eseguita correttamente!");
			
			String commit="Operazione eseguita correttamente!"+"<br><center><a href=\"colletta/Mod_A.jsp\">Aggiungi Nuova scheda Mod_A</a></center>";
			
//			String commit="Operazione eseguita correttamente!"+"<br><center><a href=\"Moduli_convenzioni/_modulo_convenzione_.pdf\"><img src=\"image/icona_pdf.jpg\" ></a>"+
//					"<br><br> Clicca sull'icona per aprire il modulo!"+
//	"<br><br>Stampa subito <a href=\"print_now.jsp?pathstampa=<%out.print(path1);%>\">Stampa senza aprire (sperimentale)</a></center>"; 
			request.setAttribute("commit", commit);
			String jspPage = "result/commit.jsp";
			request.getRequestDispatcher( jspPage ).forward(request,response);
	    }

				
}


}
