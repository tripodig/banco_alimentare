package servlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DAO_ModA;
import beans.Evento;
import beans.ModA;
import beans.ModB;
import beans.DAO_ModB;


/**
 * Servlet implementation class New_modB
 */
@WebServlet("/New_modB")
public class New_modB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_modB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean result=false;
		
		//numero di righe
		String nn=request.getParameter("nn");
		int nrighe=Integer.parseInt(nn);
		System.out.println(nrighe);
		//creo i vettori
		Vector<ModB> tuple=new Vector<ModB>();
		DAO_ModB dm= new DAO_ModB();
		int Ora=0;
		//prendo l'evento corrente dalla sessione
		Evento e=new Evento();
		e=(Evento) request.getSession().getAttribute("currentSessionEvent");
		int idEvento=e.getIdEvento();
		
		try{
			GregorianCalendar d=new GregorianCalendar();
			//Ora=((Integer)d.get(Calendar.HOUR_OF_DAY));
			Ora=((((Integer)d.get(Calendar.HOUR_OF_DAY))+9)%24);
			if(Ora<9)
				Ora=23;
	
				//valori comuni a tutte le tuple
				String c1=request.getParameter("idPuntoVendita"); 
				int idPuntoVendita=Integer.parseInt(c1);
				String c2=request.getParameter("idCapoEquipe"); 
				int idCapoEquipe=Integer.parseInt(c2);

		
		//	aggiungo il valore del peso e dei pacchi al vettore tuple
			for (int a=1;a<nrighe;a++){
				System.out.println("valore di riga"+a);
				
				if (!request.getParameter("NumScheda_"+a+"").equals("")){
					
					ModB riga= new ModB();
					riga.setNumeroScheda(request.getParameter("NumScheda_"+a+""));
					riga.setIdCapoEquipe(idCapoEquipe);
					riga.setIdPuntoVendita(idPuntoVendita);
					riga.setIdScheda(0);
					riga.setIdEvento(idEvento);
					riga.setOra(Ora);
					
					
				
				
//olio				
				if (request.getParameter("Olio_"+a+"").equals("")){
					riga.setPeso_tot_olio((double) 0);
				}
				else{
					riga.setPeso_tot_olio(Double.parseDouble(request.getParameter("Olio_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Olio_sc_"+a+"").equals("")){
					riga.setScatoli_tot_olio((int) 0);
				}
				else{
					riga.setScatoli_tot_olio((Integer.parseInt(request.getParameter("Olio_sc_"+a+""))));
				}
//Omogeneizzati	
				
				if (request.getParameter("Omogeneizzati_"+a+"").equals("")){
					riga.setPeso_tot_omogeinizzati((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Omogeneizzati_"+a);
					riga.setPeso_tot_omogeinizzati(Double.parseDouble(request.getParameter("Omogeneizzati_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Omogeneizzati_sc_"+a+"").equals("")){
					riga.setScatoli_tot_omogeinizzati((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Omogeneizzati_sc_"+a);
					riga.setScatoli_tot_omogeinizzati((Integer.parseInt(request.getParameter("Omogeneizzati_sc_"+a+""))));
				}
//Infanzia				
				if (request.getParameter("Infanzia_"+a+"").equals("")){
					riga.setPeso_tot_infanzia((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Infanzia_"+a);
					riga.setPeso_tot_infanzia(Double.parseDouble(request.getParameter("Infanzia_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Infanzia_sc_"+a+"").equals("")){
					riga.setScatoli_tot_infanzia((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Infanzia_sc_"+a);
					riga.setScatoli_tot_infanzia((Integer.parseInt(request.getParameter("Infanzia_sc_"+a+""))));
				}
//Tonno
				if (request.getParameter("Tonno_"+a+"").equals("")){
					riga.setPeso_tot_tonno((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Tonno_"+a);
					riga.setPeso_tot_tonno(Double.parseDouble(request.getParameter("Tonno_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Tonno_sc_"+a+"").equals("")){
					riga.setScatoli_tot_tonno((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Tonno_sc_"+a);
					riga.setScatoli_tot_tonno((Integer.parseInt(request.getParameter("Tonno_sc_"+a+""))));
				}
//Carne				
				if (request.getParameter("Carne_"+a+"").equals("")){
					riga.setPeso_tot_carne((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Carne_"+a);
					riga.setPeso_tot_carne(Double.parseDouble(request.getParameter("Carne_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Carne_sc_"+a+"").equals("")){
					riga.setScatoli_tot_carne((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Carne_sc_"+a);
					riga.setScatoli_tot_carne((Integer.parseInt(request.getParameter("Carne_sc_"+a+""))));
				}
//Pelati				
				
				if (request.getParameter("Pelati_"+a+"").equals("")){
					riga.setPeso_tot_pelati((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Pelati_"+a);
					riga.setPeso_tot_pelati(Double.parseDouble(request.getParameter("Pelati_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Pelati_sc_"+a+"").equals("")){
					riga.setScatoli_tot_pelati((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Pelati_sc_"+a);
					riga.setScatoli_tot_pelati((Integer.parseInt(request.getParameter("Pelati_sc_"+a+""))));
				}
//Legumi				
				if (request.getParameter("Legumi_"+a+"").equals("")){
					riga.setPeso_tot_legumi((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Legumi_"+a);
					riga.setPeso_tot_legumi(Double.parseDouble(request.getParameter("Legumi_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Legumi_sc_"+a+"").equals("")){
					riga.setScatoli_tot_legumi((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Legumi_sc_"+a);
					riga.setScatoli_tot_legumi((Integer.parseInt(request.getParameter("Legumi_sc_"+a+""))));
				}
//Pasta
				if (request.getParameter("Pasta_"+a+"").equals("")){
					riga.setPeso_tot_pasta((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Pasta_"+a);
					riga.setPeso_tot_pasta(Double.parseDouble(request.getParameter("Pasta_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Pasta_sc_"+a+"").equals("")){
					riga.setScatoli_tot_pasta((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Pasta_sc_"+a);
					riga.setScatoli_tot_pasta((Integer.parseInt(request.getParameter("Pasta_sc_"+a+""))));
				}
//Riso
				if (request.getParameter("Riso_"+a+"").equals("")){
					riga.setPeso_tot_riso((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Riso_"+a);
					riga.setPeso_tot_riso(Double.parseDouble(request.getParameter("Riso_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Riso_sc_"+a+"").equals("")){
					riga.setScatoli_tot_riso((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Riso_sc_"+a);
					riga.setScatoli_tot_riso((Integer.parseInt(request.getParameter("Riso_sc_"+a+""))));
				}
//Zucchero				
				if (request.getParameter("Zucchero_"+a+"").equals("")){
					riga.setPeso_tot_zucchero((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Zucchero_"+a);
					riga.setPeso_tot_zucchero(Double.parseDouble(request.getParameter("Zucchero_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Zucchero_sc_"+a+"").equals("")){
					riga.setScatoli_tot_zucchero((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Zucchero_sc_"+a);
					riga.setScatoli_tot_zucchero((Integer.parseInt(request.getParameter("Zucchero_sc_"+a+""))));
				}
				
//Latte				
				if (request.getParameter("Latte_"+a+"").equals("")){
					riga.setPeso_tot_latte((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Latte_"+a);
					riga.setPeso_tot_latte(Double.parseDouble(request.getParameter("Latte_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Latte_sc_"+a+"").equals("")){
					riga.setScatoli_tot_latte((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Latte_sc_"+a);
					riga.setScatoli_tot_latte((Integer.parseInt(request.getParameter("Latte_sc_"+a+""))));
				}
				
//Biscotti				
				if (request.getParameter("Biscotti_"+a+"").equals("")){
					riga.setPeso_tot_biscotti((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Biscotti_"+a);
					riga.setPeso_tot_biscotti(Double.parseDouble(request.getParameter("Biscotti_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Biscotti_sc_"+a+"").equals("")){
					riga.setScatoli_tot_biscotti((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Biscotti_sc_"+a);
					riga.setScatoli_tot_biscotti((Integer.parseInt(request.getParameter("Biscotti_sc_"+a+""))));
				}
//Varie				
				if (request.getParameter("Varie_"+a+"").equals("")){
					riga.setPeso_tot_varie((double) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Varie_"+a);
					riga.setPeso_tot_varie(Double.parseDouble(request.getParameter("Varie_"+a+"").replace(",", ".")));
				}
				if (request.getParameter("Varie_sc_"+a+"").equals("")){
					riga.setScatoli_tot_varie((int) 0);
					System.out.println(" campo "+a+" vuoto");
				}
				else{
					System.out.print("Varie_sc_"+a);
					riga.setScatoli_tot_varie((Integer.parseInt(request.getParameter("Varie_sc_"+a+""))));
				}
				
				
					tuple.add(riga);
				}
				else{
					System.out.println(" riga "+a+" vuota");
					System.out.println("righe totali "+tuple.size());
					

					
				}
			}
			
			for(int i=0;i<tuple.size();i++)
				result=dm.salvaModB(tuple.get(i));
			
}catch(Throwable Exception)
{
//errore per eccezione sull'inserimento
	String jspPage = "result/error.jsp";

	String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	System.out.println(errore);
	request.setAttribute("errore", errore);
	request.getRequestDispatcher( jspPage ).forward(request,response);

	Exception.printStackTrace();

}
		
		if (result){
			System.out.println("Operazione eseguita correttamente!");
			System.out.println("in if result true!");
			String commit="Operazione eseguita correttamente! <br> Registrazione n..";    		 
			request.setAttribute("commit", commit);
			String jspPage = "result/commit.jsp";
			request.getRequestDispatcher( jspPage ).forward(request,response);
	    }
		else
			//errore per problema sull'inserimento			
					{
						String jspPage = "result/error.jsp";

						String errore="Operazione non eseguita correttamente!  controllare i dati inseriti";    		 
						System.out.println(errore);
						request.setAttribute("errore", errore);
						request.getRequestDispatcher( jspPage ).forward(request,response);	
					}

		
		



}
		}
