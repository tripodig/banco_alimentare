package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.FirstPdf;
import beans.ConvenzioneFrom2017;
import beans.DAO_Convenzione2017;
import beans.UserBean;

/**
 * Servlet implementation class ConvenzioneServlet
 */
@WebServlet("/ConvenzioneServlet")
public class ConvenzioneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConvenzioneServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		//forward(request,response,"/convenzioni/2017_Rinnova_Convenzione.jsp"+"?"action="+action);


		boolean result=false;

		//anno della convenzione: lo prelevo dalle propriet� dell'utente connesso
		UserBean user=(UserBean) request.getSession().getAttribute("currentSessionUser");
		int anno= user.getAnnoConvenzioni();

		String c1=request.getParameter("idEnte"); // 1

		String h1=request.getParameter("idSedeLegaleEnte");
		String h2=request.getParameter("idSedeOperativaEnte");
		String h3=request.getParameter("idSedeMagazzinoEnte");

		String r1=request.getParameter("idRappLegaleEnte");

		int idEnte=Integer.parseInt(c1);

		int idSedeLegaleEnte=Integer.parseInt(h1);
		int idSedeOperativaEnte=Integer.parseInt(h2);
		int idSedeMagazzinoEnte=Integer.parseInt(h3);
		int idRappLegaleEnte=Integer.parseInt(r1);

		System.out.println("Sedi "+h1+" "+h2+" "+h3);



		String c25=request.getParameter("frigo"); 
		System.out.println(c25);
		boolean frigo=false;
		
		if (c25!=null && c25.equals("si"))
			frigo=true;


		String p1=request.getParameter("partner"); 
		System.out.println(p1);
		String partner=null;
		if (p1!=null)
			partner=p1;

		System.out.println(partner);

		//Parametri Mensa
		String c32=request.getParameter("M015Anni"); // 16
		int M_0_15=Integer.parseInt(c32);
		String c33=request.getParameter("M1664Anni"); // 17
		int M_16_64=Integer.parseInt(c33);
		String c34=request.getParameter("Mover65Anni"); // 18
		int M_over_65=Integer.parseInt(c34);
		String c35=request.getParameter("AssSaltM");// 2
		int M_AssSalt=Integer.parseInt(c35);
		String c36=request.getParameter("TotIngM");// 5
		int M_TotIng=Integer.parseInt(c36);
		String c37=request.getParameter("MDonne"); // 8
		int M_Donne=Integer.parseInt(c37);
		String c38=request.getParameter("MmigrantiMinoranze"); // 8
		int M_migrantiMinoranze=Integer.parseInt(c38);
		String c39=request.getParameter("Mdisabili"); // 8
		int M_disabili=Integer.parseInt(c39);
		String c40=request.getParameter("MsenzaFissaDimora"); // 8
		int M_senzaFissaDimora=Integer.parseInt(c40);

		//Parametri Pacchi
		String c41=request.getParameter("P015Anni"); // 16
		int P_0_15=Integer.parseInt(c41);
		String c42=request.getParameter("P1664Anni"); // 17
		int P_16_64=Integer.parseInt(c42);
		String c43=request.getParameter("Pover65Anni"); // 18
		int P_over_65=Integer.parseInt(c43);
		String c44=request.getParameter("AssSaltP");// 2
		int P_AssSalt=Integer.parseInt(c44);
		String c45=request.getParameter("TotIngP");// 5
		int P_TotIng=Integer.parseInt(c45);
		String c46=request.getParameter("PDonne"); // 8
		int P_Donne=Integer.parseInt(c46);
		String c47=request.getParameter("PmigrantiMinoranze"); // 8
		int P_migrantiMinoranze=Integer.parseInt(c47);
		String c48=request.getParameter("Pdisabili"); // 8
		int P_disabili=Integer.parseInt(c48);
		String c49=request.getParameter("PsenzaFissaDimora"); // 8
		int P_senzaFissaDimora=Integer.parseInt(c49);

		//emporio
		String c50=request.getParameter("E015Anni"); // 16
		int E_0_15=Integer.parseInt(c50);
		String c51=request.getParameter("E1664Anni"); // 17
		int E_16_64=Integer.parseInt(c51);
		String c52=request.getParameter("Eover65Anni"); // 18
		int E_over_65=Integer.parseInt(c52);

		String c53=request.getParameter("TotIngE");// 5
		int E_TotIng=Integer.parseInt(c53);
		String c54=request.getParameter("EDonne"); // 8
		int E_Donne=Integer.parseInt(c54);
		String c55=request.getParameter("EmigrantiMinoranze"); // 8
		int E_migrantiMinoranze=Integer.parseInt(c55);
		String c56=request.getParameter("Edisabili"); // 8
		int E_disabili=Integer.parseInt(c56);
		String c57=request.getParameter("EsenzaFissaDimora"); // 8
		int E_senzaFissaDimora=Integer.parseInt(c57);

		//distribuzione
		String c60=request.getParameter("D015Anni"); // 16
		int D_0_15=Integer.parseInt(c60);
		String c61=request.getParameter("D1664Anni"); // 17
		int D_16_64=Integer.parseInt(c61);
		String c62=request.getParameter("Dover65Anni"); // 18
		int D_over_65=Integer.parseInt(c62);

		String c63=request.getParameter("TotIngD");// 5
		int D_TotIng=Integer.parseInt(c63);
		String c64=request.getParameter("DDonne"); // 8
		int D_Donne=Integer.parseInt(c64);
		String c65=request.getParameter("DmigrantiMinoranze"); // 8
		int D_migrantiMinoranze=Integer.parseInt(c65);
		String c66=request.getParameter("Ddisabili"); // 8
		int D_disabili=Integer.parseInt(c66);
		String c67=request.getParameter("DsenzaFissaDimora"); // 8
		int D_senzaFissaDimora=Integer.parseInt(c67);

		//unit� di strada
		String c70=request.getParameter("TotIngU");// 5
		int U_TotIng=Integer.parseInt(c70);
		String c71=request.getParameter("UDonne"); // 8
		int U_Donne=Integer.parseInt(c71);
		String c72=request.getParameter("UmigrantiMinoranze"); // 8
		int U_migrantiMinoranze=Integer.parseInt(c72);
		String c73=request.getParameter("Udisabili"); // 8
		int U_disabili=Integer.parseInt(c73);
		String c74=request.getParameter("UsenzaFissaDimora"); // 8
		int U_senzaFissaDimora=Integer.parseInt(c74);

		//checkbox

		String m1=request.getParameter("accoglienzaEAscolto");
		String m2=request.getParameter("informazione");
		String m3=request.getParameter("accompagnamento");
		String m4=request.getParameter("sostegnoPsicologico");
		String m5=request.getParameter("EducativaAlimentare");
		String m6=request.getParameter("consulenzaNellaGestioneDelBilancioFamiliare");
		String m7=request.getParameter("sostegnoSscolastico");
		String m8=request.getParameter("sostegnoEOrientamentoAllaRicercaDiLavoro");
		String m9=request.getParameter("primaAssistenzaMedica");
		String m10=request.getParameter("tutelaLegale");
		String m11=request.getParameter("altro");
		String specificareAltro=null;

		boolean accoglienza=m1!=null ? true : false ;
		boolean informazione=m2!=null ? true : false ;
		boolean accompagnamento=m3!=null ? true : false ;
		boolean spsicologico=m4!=null ? true : false ;
		boolean educativa=m5!=null ? true : false ;
		boolean consulenza=m6!=null ? true : false ;
		boolean sscolastico=m7!=null ? true : false ;
		boolean sorientamento=m8!=null ? true : false ;
		boolean primaassistenza=m9!=null ? true : false ;
		boolean tutela=m10!=null ? true : false ;
		boolean altro=m11!=null ? true : false ;
		if(altro){
			specificareAltro=request.getParameter("specificaAltro");				
		}
		else 
			specificareAltro="no";


		String data=request.getParameter("Data");//9

		ConvenzioneFrom2017 c=new ConvenzioneFrom2017();

		c.setIdEnte(idEnte);
		c.setIdRappLegaleEnte(idRappLegaleEnte);
		c.setIdSedeLegaleEnte(idSedeLegaleEnte);
		c.setIdSedeMagazzinoEnte(idSedeMagazzinoEnte);
		c.setIdSedeOperativaEnte(idSedeOperativaEnte);
		c.setAnno(anno);
		c.setCellaFrigo(frigo);
		c.setTipoPartner(partner);

		c.setDataFirma(data);

		c.setM_0_15(M_0_15);
		c.setM_16_64(M_16_64);
		c.setM_over_65(M_over_65);
		c.setM_saltuari(M_AssSalt);


		c.setP_0_15(P_0_15);
		c.setP_16_64(P_16_64);
		c.setP_over_65(P_over_65);
		c.setP_saltuari(P_AssSalt);


		c.setE_0_15(E_0_15);
		c.setE_16_64(E_16_64);
		c.setE_over_65(E_over_65);

		c.setD_0_15(D_0_15);
		c.setD_16_64(D_16_64);
		c.setD_over_65(D_over_65);

		c.setM_totIndigenti(M_TotIng);
		c.setP_totIndigenti(P_TotIng);
		c.setE_totIndigenti(E_TotIng);
		c.setD_totIndigenti(D_TotIng);
		c.setU_totIndigenti(U_TotIng);

		c.setM_donne(M_Donne);
		c.setP_donne(P_Donne);
		c.setE_donne(E_Donne);
		c.setD_donne(D_Donne);
		c.setU_donne(U_Donne);

		c.setM_disabili(M_disabili);
		c.setP_disabili(P_disabili);
		c.setE_disabili(E_disabili);
		c.setD_disabili(D_disabili);
		c.setU_disabili(U_disabili);

		c.setM_migranti(M_migrantiMinoranze);
		c.setP_migranti(P_migrantiMinoranze);
		c.setE_migranti(E_migrantiMinoranze);
		c.setD_migranti(D_migrantiMinoranze);
		c.setU_migranti(U_migrantiMinoranze);

		c.setM_senzaFissaDimora(M_senzaFissaDimora);
		c.setP_senzaFissaDimora(P_senzaFissaDimora);
		c.setE_senzaFissaDimora(E_senzaFissaDimora);
		c.setD_senzaFissaDimora(D_senzaFissaDimora);
		c.setU_senzaFissaDimora(U_senzaFissaDimora);

		c.setAccoglienza(accoglienza);
		c.setInformazione(informazione);
		c.setAccompagnamento(accompagnamento);
		c.setSpsicologico(spsicologico);
		c.setEducativa(educativa);
		c.setConsulenza(consulenza);
		c.setSscolastico(sscolastico);
		c.setSorientamento(sorientamento);
		c.setPrimaassistenza(primaassistenza);
		c.setTutela(tutela);
		c.setAltroTtxt(specificareAltro);
		c.setTipologia("agea");


		DAO_Convenzione2017 Daoconv= new DAO_Convenzione2017();


		result=Daoconv.salva(c);
		if (result){
			
			FirstPdf f=new FirstPdf();
			String path=request.getSession().getServletContext().getRealPath("/");
			//System.out.println(path);
			String directory_separator="\\";
			String path0=path+directory_separator+"fileSupporto"+directory_separator+"All_2_Domanda_affiliazione_OpT.pdf";
			String path1=path+directory_separator+"Moduli_convenzioni"+directory_separator+""+idEnte+"_modulo_convenzione_"+anno+".pdf";
			//System.out.println(path0);
			//System.out.println(path1);
			//CatalogoConvenzioni catconv=new CatalogoConvenzioni();
			//int idConvenzione=catconv.getidconvenzione(idEnte, anno);
			//int idConvenzione=catconv.getidconvenzione(idEnte, idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,anno);
			//System.out.println("idconvenzione="+idConvenzione);
			f.StampaModuli_Convenzione2017(path0,path1, c);
			
			//System.out.println("Operazione eseguita correttamente!");
			String commit="Operazione eseguita correttamente!"+"<br><center><a href=\"Moduli_convenzioni/"+idEnte+"_modulo_convenzione_"+anno+".pdf\"><img src=\"image/icona_pdf.jpg\" ></a>"+
			"<br><br> Clicca sull'icona per aprire il modulo!</center><br>"+ "<a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">VAI A ENTE</a>" ;    		 
			request.setAttribute("commit", commit);
			String jspPage="result/commit.jsp";
	    }
		if (result)
			forward(request,response,"/result/commit.jsp");
		else
			forward(request,response,"/result/error.jsp");
	}
	
	
	
	
	
	
	private void forward(HttpServletRequest request, HttpServletResponse response, String page) 
			throws ServletException, IOException
			{
		ServletContext sc = getServletContext();
		RequestDispatcher rd = sc.getRequestDispatcher(page);
		rd.forward(request,response);
			}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
