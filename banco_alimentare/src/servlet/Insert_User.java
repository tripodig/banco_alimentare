package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CatalogoVolontario;
import beans.UserBean;
import beans.UserDAO;
import beans.Volontario;

/**
 * Servlet implementation class Insert_User
 */
@WebServlet("/Insert_User")
public class Insert_User extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insert_User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
try{
			
			boolean result=false;
			boolean esito=true; // booleano che mi serve per la validazione dei campi
			String nome=null;
			String cognome=null;
			String username=null;
			String password=null;
			String[] ruolo=null;
			String[] area=null;
			int annoconvenzioni=0;
			String[] provinciaconvenzioni=null;
			
			String tipo=null;

			
// Controllo campi nulli			
			
			
			nome=request.getParameter("Nome");
			cognome=request.getParameter("Cognome");
			username=request.getParameter("Username");
			password=request.getParameter("Password");
			ruolo=request.getParameterValues("Ruolo");
			area=request.getParameterValues("Area");
			String annoconvenzionis=request.getParameter("AnnoConvenzioni");
			annoconvenzioni=Integer.parseInt(annoconvenzionis);
			provinciaconvenzioni=request.getParameterValues("ProvinciaConvenzioni");
			tipo=request.getParameter("tipo");
			
			StringBuilder ruoli=new StringBuilder();
			StringBuilder aree=new StringBuilder();
			StringBuilder province=new StringBuilder();
		
			
			if(ruolo!=null)
			for(int i=0;i<ruolo.length;i++){
				
				ruoli.append(ruolo[i]);
				
				if(i<ruolo.length-1){
					ruoli.append(",");
				}
			}
			
			if(area!=null)
			for(int i=0;i<area.length;i++){
				
				aree.append(area[i]);
				
				if(i<area.length-1){
					aree.append(",");
				}
			}
			
			if(provinciaconvenzioni!=null)
			for(int i=0;i<provinciaconvenzioni.length;i++){
				
				province.append(provinciaconvenzioni[i]);
				
				if(i<provinciaconvenzioni.length-1){
					province.append(",");
				}
			}
			
			
			

			UserBean u1=new UserBean();
			
			// Validazione Campi
			u1.setnome(nome);
			u1.setcognome(cognome);
			u1.setUserName(username);
			u1.setPassword(password);
			u1.setarea(aree.toString());
			u1.setRuolo(ruoli.toString());
			u1.setAnnoConvenzioni(annoconvenzioni);
			u1.setProvinciaConvenzioni(province.toString());
			
			
			
				UserDAO DAO= new UserDAO();
				
				if(tipo.equalsIgnoreCase("ins")){
					
					result=DAO.salva(u1);
					
				}else{
				
					if(tipo.equalsIgnoreCase("mod")){
						String idU=request.getParameter("id");
						int id=Integer.parseInt(idU);
						
						u1.setIdUser(id);
						result=DAO.aggiorna(u1);
						
					}
					
				}
				
				
				
			
			String jspPage;
			if (result){
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!";    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
			}
			else{
				System.out.println("!!! Operazione non eseguita !!!");
				String errore="Operazione non eseguita correttamente!";    		 
				request.setAttribute("errore", errore);
				jspPage="result/error.jsp";
			}
			request.getRequestDispatcher( jspPage ).forward(request,response);
		}

		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
			request.setAttribute("errore", errore);
			request.getRequestDispatcher( jspPage ).forward(request,response);

			Exception.printStackTrace();

		}
	}

}
