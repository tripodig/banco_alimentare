package servlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.CalendarDateIT;
import util.FirstPdf;
import beans.*;

/**
 * Servlet implementation class New_convenzione
 */
public class New_convenzione extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public New_convenzione() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			boolean result=false;
			
			//anno della convenzione: lo prelevo dalle propriet� dell'utente connesso
			UserBean user=(UserBean) request.getSession().getAttribute("currentSessionUser");
			int anno= user.getAnnoConvenzioni();
			
			String c1=request.getParameter("idEnte"); // 1
			
			String h1=request.getParameter("idSedeLegaleEnte");
			String h2=request.getParameter("idSedeOperativaEnte");
			String h3=request.getParameter("idSedeMagazzinoEnte");

			String r1=request.getParameter("idRappLegaleEnte");
			
			int idEnte=Integer.parseInt(c1);
			
			int idSedeLegaleEnte=Integer.parseInt(h1);
			int idSedeOperativaEnte=Integer.parseInt(h2);
			int idSedeMagazzinoEnte=Integer.parseInt(h3);
			int idRappLegaleEnte=Integer.parseInt(r1);
			
			System.out.println("Sedi "+h1+" "+h2+" "+h3);
			
			
			
			String c25=request.getParameter("frigo"); 
			System.out.println(c25);
			boolean frigo=false;
			if (c25.equals("si"))
				frigo=true;

			System.out.println(frigo);
			

			String c32=request.getParameter("M05Anni"); // 16

			//Parametri Mensa
			int M_0_5=Integer.parseInt(c32);
			String c33=request.getParameter("M665Anni"); // 17
			int M_6_65=Integer.parseInt(c33);
			String c34=request.getParameter("Mover65Anni"); // 18
			int M_over_65=Integer.parseInt(c34);
			String c35=request.getParameter("AssSalt1");// 2
			int M_AssSalt1=Integer.parseInt(c35);
			String c36=request.getParameter("MMediaPres");// 5
			int M_MediaPresenze=Integer.parseInt(c36);
			String c37=request.getParameter("MGiorniApertura"); // 8
			int M_GiorniApertura=Integer.parseInt(c37);
			
			//Parametri Distribizione Pacchi
			String c51=request.getParameter("P05Anni"); // 30
			int P_0_5=Integer.parseInt(c51);
			String c52=request.getParameter("P665Anni"); // 31
			int P_6_65=Integer.parseInt(c52);
			String c53=request.getParameter("Pover65Anni"); // 32
			int P_over_65=Integer.parseInt(c53);
			String c54=request.getParameter("AssSalt3");// 4
			int P_AssSalt=Integer.parseInt(c54);
			String c55=request.getParameter("PMediaPres");// 7
			int P_MediaPresenze=Integer.parseInt(c55);
			String c56=request.getParameter("PGiorniApertura");//9
			int P_GiorniApertura=Integer.parseInt(c56);
			
			//Parametri Emporio Sociale
			String c43=request.getParameter("R05Anni"); // 24
			int E_0_5=Integer.parseInt(c43);
			String c44=request.getParameter("R665Anni"); // 25
			int E_6_65=Integer.parseInt(c44);
			String c45=request.getParameter("Rover65Anni"); // 26
			int E_over_65=Integer.parseInt(c45);
			String c46=request.getParameter("AssSalt2");// 3
			int E_AssSalt=Integer.parseInt(c46);
			String c47=request.getParameter("RMediaPres");// 6
			int E_MediaPresenze=Integer.parseInt(c47);
			String c536=request.getParameter("RGiorniApertura");//9
			int E_GiorniApertura=Integer.parseInt(c536);
			
			//Parametri Unit� di Strada
			String c426=request.getParameter("AssSalt4");// 3
			int U_AssSalt=Integer.parseInt(c426);
			String c457=request.getParameter("UMediaPres");// 6
			int U_MediaPresenze=Integer.parseInt(c457);
			String c5346=request.getParameter("UGiorniApertura");//9
			int U_GiorniApertura=Integer.parseInt(c5346);
			
			String data=request.getParameter("Data");//9
			
			Convenzione c=new Convenzione();

c.setIdEnte(idEnte);
c.setIdRappLegaleEnte(idRappLegaleEnte);
c.setIdSedeLegaleEnte(idSedeLegaleEnte);
c.setIdSedeMagazzinoEnte(idSedeMagazzinoEnte);
c.setIdSedeOperativaEnte(idSedeOperativaEnte);
c.setAnno(anno);
c.setCellaFrigo(frigo);
c.setDataFirma(data);

c.setM_0_5(M_0_5);
c.setM_6_65(M_6_65);
c.setM_over65(M_over_65);
c.setM_giorniApertura(M_GiorniApertura);
c.setM_mediaPresenze(M_MediaPresenze);
c.setM_saltuari(M_AssSalt1);


c.setP_0_5(P_0_5);
c.setP_6_65(P_6_65);
c.setP_over65(P_over_65);
c.setP_giorniApertura(P_GiorniApertura);
c.setP_mediaPresenze(P_MediaPresenze);
c.setP_saltuari(P_AssSalt);


c.setE_0_5(E_0_5);
c.setE_6_65(E_6_65);
c.setE_over65(E_over_65);
c.setE_giorniApertura(E_GiorniApertura);
c.setE_mediaPresenze(E_MediaPresenze);
c.setE_saltuari(E_AssSalt);

c.setU_giorniApertura(U_GiorniApertura);
c.setU_mediaPresenze(U_MediaPresenze);
c.setU_saltuari(U_AssSalt);






			CatalogoConvenzioni DBconv= new CatalogoConvenzioni();


			result=DBconv.salvaConvenzionenew(c);
			String jspPage;
			if (result){
				
				FirstPdf f=new FirstPdf();
				String path=request.getSession().getServletContext().getRealPath("/");
				System.out.println(path);
				String directory_separator="\\";
				String path0=path+directory_separator+"fileSupporto"+directory_separator+"modulo_empty_agea.pdf";
				String path1=path+directory_separator+"Moduli_convenzioni"+directory_separator+""+idEnte+"_modulo_convenzione_"+anno+".pdf";
				System.out.println(path0);
				System.out.println(path1);
				CatalogoConvenzioni catconv=new CatalogoConvenzioni();
				//int idConvenzione=catconv.getidconvenzione(idEnte, anno);
				int idConvenzione=catconv.getidconvenzione(idEnte, idSedeLegaleEnte,idSedeOperativaEnte, idSedeMagazzinoEnte,anno);
				System.out.println("idconvenzione="+idConvenzione);
				f.StampaModuli_Convenzione(path0,path1, idConvenzione);
				
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!"+"<br><center><a href=\"Moduli_convenzioni/"+idEnte+"_modulo_convenzione_"+anno+".pdf\"><img src=\"image/icona_pdf.jpg\" ></a>"+
				"<br><br> Clicca sull'icona per aprire il modulo!</center><br>"+ "<a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">VAI A ENTE</a><br>"+ "<a href=\"convenzioni/verbale.jsp?id="+ idEnte +"&idconv="+idConvenzione+"\">AGGIUNGI VERBALE</a>" ;    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
		    }
	        else{
	           System.out.println("!!! Operazione non eseguita !!!");
	           String errore="Operazione non eseguita correttamente!";    		 
	           request.setAttribute("errore", errore);
	           jspPage="result/error.jsp";
	        }
		    request.getRequestDispatcher( jspPage ).forward(request,response);
	}
			
			catch(Throwable Exception)
			{
				String jspPage = "result/error.jsp";
				String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
		        request.setAttribute("errore", errore);
		        request.getRequestDispatcher( jspPage ).forward(request,response);

		        Exception.printStackTrace();
				
			}
	}

}
