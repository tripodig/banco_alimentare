package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CatalogoEnti;
import beans.Ente;

/**
 * Servlet implementation class New_ente
 */
@WebServlet("/New_ente")
public class New_ente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_ente() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			String azione=request.getParameter("azione");
			int idEnte=0;
		boolean esito=false;
		if(azione.equals("modifica")){
			String idente=request.getParameter("id");
			idEnte=Integer.parseInt(idente);
			String Nome= request.getParameter("Nome");
			String CodiceFiscale= request.getParameter("CodiceFiscale");
			String Descrizione= request.getParameter("Descrizione");
			String CodiceStruttura= request.getParameter("CodiceStruttura");
			String CodiceSap= request.getParameter("CodiceSap");
			Ente e= new Ente();
			e.setIdEnte(idEnte);
			e.setNome(Nome);
			e.setDescrizione(Descrizione);
			e.setPartitaIva(CodiceFiscale);
			e.setCodiceStruttura(Integer.parseInt(CodiceStruttura));
			e.setCodiceSap(Integer.parseInt(CodiceSap));
			CatalogoEnti cate=new CatalogoEnti();
			esito= cate.aggiornaEnte(e);
			System.out.println(esito);
		}
		if(azione.equals("aggiungi")){
			String idente=request.getParameter("id");
			idEnte=Integer.parseInt(idente);
			String Nome= request.getParameter("Nome");
			String Descrizione= request.getParameter("Descrizione");
			String CodiceFiscale= request.getParameter("CodiceFiscale");
			String CodiceStruttura= request.getParameter("CodiceStruttura");
			Ente e= new Ente();
			e.setIdEnte(idEnte);
			e.setNome(Nome);
			e.setDescrizione(Descrizione);
			e.setPartitaIva(CodiceFiscale);
			e.setCodiceStruttura(Integer.parseInt(CodiceStruttura));
			CatalogoEnti cate=new CatalogoEnti();
			esito= cate.salvaEnte(e);
			idEnte=cate.getidEntebycodStruttura(e.getCodiceStruttura());
			System.out.println(esito);
		}
		String jspPage;
		if (esito){
			System.out.println("Operazione eseguita correttamente!");
			String commit="Operazione eseguita correttamente!  <br>"
	           		+ "<a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">VAI A ENTE</a>";    		 
			request.setAttribute("commit", commit);
			jspPage="result/commit.jsp";
	    }
        else{
           System.out.println("!!! Operazione non eseguita !!!");
           String errore="Operazione non eseguita correttamente!";    		 
           request.setAttribute("errore", errore);
           jspPage="result/error.jsp";
        }
	    request.getRequestDispatcher( jspPage ).forward(request,response);
}
		
		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	        request.setAttribute("errore", errore);
	        request.getRequestDispatcher( jspPage ).forward(request,response);

	        Exception.printStackTrace();
			
		}	
			
	}

}
