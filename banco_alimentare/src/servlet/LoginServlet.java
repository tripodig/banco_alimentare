package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DAO_Evento;
import beans.Evento;
import beans.UserBean;
import beans.UserDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
	           throws ServletException, java.io.IOException {

try
{	    

UserBean user = new UserBean();
user.setUserName(request.getParameter("un"));
user.setPassword(request.getParameter("pw"));
//recupera are da utente



//user.setarea("CS");

UserDAO u =new UserDAO();

DAO_Evento eventoDAO= new DAO_Evento();
Evento evento=new Evento();
evento=eventoDAO.getEvento();

	    
if (u.isRegistered(user))
{
 
HttpSession session = request.getSession(true);
session.setAttribute("loggato",true);
session.setAttribute("currentSessionUser",user); 

session.setAttribute("currentSessionEvent",evento);

response.sendRedirect("home.jsp"); //logged-in page
}
 
else 
response.sendRedirect("result/error.jsp"); //error page 
} 


catch (Throwable theException) 	    
{
System.out.println(theException); 
}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try
		{	    

		UserBean user = new UserBean();
		user.setUserName(request.getParameter("un"));
		user.setPassword(request.getParameter("pw"));
		//recupera are da utente



		//user.setarea("CS");

		UserDAO u =new UserDAO();

		DAO_Evento eventoDAO= new DAO_Evento();
		Evento evento=new Evento();
		

			    
		if (u.isRegistered(user))
		{
		
		evento=eventoDAO.getEvento(); 
		
		HttpSession session = request.getSession(true);
		session.setAttribute("loggato",true);
		session.setAttribute("currentSessionUser",user); 
		session.setAttribute("currentSessionEvent",evento);

		response.sendRedirect("home.jsp"); //logged-in page
		}
		 
		else 
		response.sendRedirect("result/error.jsp"); //error page 
		} 


		catch (Throwable theException) 	    
		{
		System.out.println(theException); 
		}
		}
	
	

}
