package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CatalogoConvenzioni;
import beans.CatalogoEnti;
import beans.CatalogoSede;
import beans.Ente;
import beans.Sede;

/**
 * Servlet implementation class Sede
 */
@WebServlet("/New_sede")
public class New_sede extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public New_sede() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		try{
			boolean result=false;
			String c1=request.getParameter("Tipologia");
			String c2=request.getParameter("Indirizzo");
			String c3=request.getParameter("Cap");
			String c9=request.getParameter("Comune");
			String c4=request.getParameter("Citta");
			String c10=request.getParameter("prov");
			String c11=request.getParameter("tel");
			String c12=request.getParameter("fax");
			String c13=request.getParameter("email");
			String azione=request.getParameter("azione");
			String identes=request.getParameter("idEnte");
			System.out.println(azione);
			int idEnte = 0;
			if(azione.equals("aggiungi"))
					{
			String codStruttura=request.getParameter("id");
			System.out.println(codStruttura);
			idEnte=Integer.parseInt(codStruttura);
					}
			CatalogoSede cats= new CatalogoSede();
			Sede s=new Sede();
			s.setCap(c3);
			s.setComune(c9);
			s.setCitta(c4);
			s.setEmail(c13);
			s.setFax(c12);
			s.setIndirizzo(c2);
			s.setProvincia(c10);
			s.setTelefono(c11);
			s.setTipologia(c1);
			if(azione.equals("aggiungi"))
			s.setIdEnte(idEnte);
	
			
			if(azione.equals("modifica")){
				System.out.println("in azione)");
				String id=request.getParameter("idSede");
				idEnte = Integer.parseInt(identes);
				int idSede=Integer.parseInt(id);
				Sede e= cats.getSede(idSede);
				e.setCap(c3);
				e.setComune(c9);
				e.setCitta(c4);
				e.setEmail(c13);
				e.setFax(c12);
				e.setIndirizzo(c2);
				e.setProvincia(c10);
				e.setTelefono(c11);
				e.setTipologia(c1);
				e.setIdEnte(idEnte);
				result= cats.aggiornaSede(e);
				System.out.println(result);
			}
			
			else{
			
			result=cats.salvaSede(s);
			System.out.println(result);
			String upd =null;
			}
			String jspPage;
			if (result){
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!"+"<br><br><font color=\"black\">Vuoi inserire un altro Referente per questo ente? </font><a href=\"anagrafica/Aggiungi_Referenti.jsp?id="+ idEnte +"\">Aggiungi Referente</a> <br>" +
						"<font color=\"black\">Vuoi inserire un'altra Sede per questo ente? </font><a href=\"anagrafica/Aggiungi_Sede.jsp?id="+ idEnte +"\">Aggiungi Sede</a> <br>"+
						"<font color=\"black\">Torna alla pagina di questo ente---> </font><a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">TORNA A ENTE</a>";    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
		    }
	        else{
	           System.out.println("!!! Operazione non eseguita !!!");
	           String errore="Operazione non eseguita correttamente!";    		 
	           request.setAttribute("errore", errore);
	           jspPage="result/error.jsp";
	        }
		    request.getRequestDispatcher( jspPage ).forward(request,response);
	}
			
			catch(Throwable Exception)
			{
				String jspPage = "result/error.jsp";
				String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
		        request.setAttribute("errore", errore);
		        request.getRequestDispatcher( jspPage ).forward(request,response);

		        Exception.printStackTrace();
				
			}
	
		}
		
	

}
