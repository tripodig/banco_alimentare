package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CatalogoPersona;
import beans.CatalogoSede;
import beans.Persona;

/**
 * Servlet implementation class New_referente
 */
@WebServlet("/New_referente")
public class New_referente extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public New_referente() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub

try{
	boolean result=false;
		String c1=request.getParameter("Titolo");
		String c2=request.getParameter("Nome");
		String c3=request.getParameter("Cognome");
		String c9=request.getParameter("Comune");
		String c10=request.getParameter("Indirizzo");
		String c11=request.getParameter("Cap");
		String c12=request.getParameter("Citta");
		String c13=request.getParameter("Email");
		String c52=request.getParameter("Telefono");
		String c15=request.getParameter("Cellulare");
		String azione=request.getParameter("azione");
		String identes=request.getParameter("idEnte");
		String cf=request.getParameter("Cf");
		String datanascita=request.getParameter("datanascita");
		String luogonascita=request.getParameter("luogonascita");
		System.out.println(azione);
		int idEnte = 0;
		if(azione.equals("aggiungi"))
		{
			String codStruttura=request.getParameter("id");
		System.out.println(codStruttura);
		idEnte=Integer.parseInt(codStruttura);
				}
		CatalogoPersona catp= new CatalogoPersona();
		Persona p=new Persona();
		p.setCap(c11);
		p.setCellulare(c15);
		p.setCitta(c12);
		p.setCognome(c3);
		p.setComune(c9);
		p.setEmail(c13);
		p.setCF(cf);
		if(azione.equals("aggiungi"))
			p.setIdEnte(idEnte);
	
		p.setNome(c2);
		p.setIndirizzo(c10);
		p.setTelefono(c52);
		p.setTitoloReferenza(c1);
		p.setDataNascita(datanascita);
		p.setLuogoNascita(luogonascita);
		
		
		if(azione.equals("modifica")){
			System.out.println("in azione)");
			String id=request.getParameter("idPersona");
			idEnte = Integer.parseInt(identes);
			int idPersona=Integer.parseInt(id);
			Persona u=catp.getPersona(idPersona);
			u.setCap(c11);
			u.setCellulare(c15);
			u.setCitta(c12);
			u.setCognome(c3);
			u.setComune(c9);
			u.setEmail(c13);
			u.setCF(cf);
			u.setNome(c2);
			u.setIndirizzo(c10);
			u.setTelefono(c52);
			u.setTitoloReferenza(c1);
			u.setDataNascita(datanascita);
			u.setLuogoNascita(luogonascita);
			result= catp.aggiornaPersona(idPersona,u);
			System.out.println(result);
		}
		
		else{	
			
		
		result=catp.salvaPersona(p);
		System.out.println(result);
		}
		String jspPage;
		if (result){
			System.out.println("Operazione eseguita correttamente!");
			String commit="Operazione eseguita correttamente!"+"<br><br><font color=\"black\">Vuoi inserire un altro Referente per questo ente? </font><a href=\"anagrafica/Aggiungi_Referenti.jsp?id="+ idEnte +"\">Aggiungi Referente</a> <br>" +
					"<font color=\"black\">Vuoi inserire un'altra Sede per questo ente? </font><a href=\"anagrafica/Aggiungi_Sede.jsp?id="+ idEnte +"\">Aggiungi Sede</a> <br>" +
					"<font color=\"black\">Torna alla pagina di questo ente---> </font><a href=\"anagrafica/Scheda_Ente.jsp?id="+ idEnte +"\">TORNA A ENTE</a>";    		 
			request.setAttribute("commit", commit);
			jspPage="result/commit.jsp";
	    }
        else{
           System.out.println("!!! Operazione non eseguita !!!");
           String errore="Operazione non eseguita correttamente!";    		 
           request.setAttribute("errore", errore);
           jspPage="result/error.jsp";
        }
	    request.getRequestDispatcher( jspPage ).forward(request,response);
}
		
		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
	        request.setAttribute("errore", errore);
	        request.getRequestDispatcher( jspPage ).forward(request,response);

	        Exception.printStackTrace();
			
		}
}

}
