package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CatalogoPersona;
import beans.CatalogoVolontario;
import beans.Persona;
import beans.Volontario;

/**
 * Servlet implementation class Insert_Volontario
 */

public class Insert_Volontario extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			
			boolean result=false;
			boolean esito=true; // booleano che mi serve per la validazione dei campi
			String nome=null;
			String cognome=null;
			String indirizzo=null;
			String cap=null;
			String citta=null;
			String telefono=null;
			String datanascita=null;
			String occupazione=null;
			String email=null;
			int id = 0;
			
// Controllo campi nulli			
			
			
			nome=request.getParameter("Nome");
			cognome=request.getParameter("Cognome");
			indirizzo=request.getParameter("Indirizzo");
			cap=request.getParameter("Cap");
			citta=request.getParameter("Citta");
			telefono=request.getParameter("Telefono");
			datanascita=request.getParameter("datanascita");
			occupazione=request.getParameter("occupazione");
			email=request.getParameter("Email");
			
			Volontario Vl=new Volontario();
			
			// Validazione Campi
			Vl.setNome(nome);
			Vl.setCognome(cognome);
			Vl.setIndirizzo(indirizzo);
			Vl.setCap(cap);
			Vl.setCitta(citta);
			Vl.setTelefono(telefono);
			Vl.setDataNascita(datanascita);
			Vl.setOccupazione(occupazione);
			Vl.setEmail(email);
			
			
			
				CatalogoVolontario DAO = new CatalogoVolontario();
				result=DAO.salvaVolontario(Vl);
			
			String jspPage;
			if (result){
				System.out.println("Operazione eseguita correttamente!");
				String commit="Operazione eseguita correttamente!";    		 
				request.setAttribute("commit", commit);
				jspPage="result/commit.jsp";
			}
			else{
				System.out.println("!!! Operazione non eseguita !!!");
				String errore="Operazione non eseguita correttamente!";    		 
				request.setAttribute("errore", errore);
				jspPage="result/error.jsp";
			}
			request.getRequestDispatcher( jspPage ).forward(request,response);
		}

		catch(Throwable Exception)
		{
			String jspPage = "result/error.jsp";
			String errore="Operazione non eseguita correttamente!  eccezione non aspettata";    		 
			request.setAttribute("errore", errore);
			request.getRequestDispatcher( jspPage ).forward(request,response);

			Exception.printStackTrace();

		}
	}




}
