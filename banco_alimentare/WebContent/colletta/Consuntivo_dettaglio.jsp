<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="beans.*"%> 
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>    
<jsp:useBean id="catpv" scope="page" class="beans.CatalogoPuntoVendita"/>
<jsp:useBean id="catstat1" scope="page" class="beans.DAO_Stat1"/>
<jsp:useBean id="catmodb" scope="page" class="beans.DAO_ModB"/>
<jsp:useBean id="catmoda" scope="page" class="beans.DAO_ModA"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
		<%@ include file="../component/header.jspf" %>
	</div>
	<div id="page" > 
	
<%
out.println("<h1>Resoconto Raccolta per Punto Vendita</h1>");
out.println("<br><br>");
GregorianCalendar d=new GregorianCalendar();
String Giorno=((Integer)d.get(Calendar.DAY_OF_MONTH)).toString();
int Mese=((Integer)d.get(Calendar.MONTH))+1;
String Anno=((Integer)d.get(Calendar.YEAR)).toString();
//String Ora=((Integer)d.get(Calendar.HOUR_OF_DAY)).toString();
int oraItalia=((Integer)d.get(Calendar.HOUR_OF_DAY)+9)%24;
String Minuto=((Integer)d.get(Calendar.MINUTE)).toString();

//Vector<Stat1> totali= catstat1.getStat1();
%>
<!-- testo descrittivo della pagina -->
Da questa pagina � possibile monitorare la quantit� di alimenti raccolti e censiti dalle sedi del Banco Alimentare. <br>
Le tabelle visualizzano i totali ricavati dai modelli A relativi a ciascun punto vendita.
Il dato � aggiornato in tempo reale. <br><br>L'ultimo aggiornamento � al: 
<%
out.println("<b>"+Giorno+"/"+Mese+"/"+Anno+"  -  "+oraItalia+":"+Minuto+"</b><br><br>");

	


CatalogoPuntoVendita catpdv=new CatalogoPuntoVendita();

Vector<PuntoVendita> tuttipdv=null;

String evento="";
int idEvento=0;

if (session.getAttribute("currentSessionUser") != null) {

	UserBean user = (UserBean) session
			.getAttribute("currentSessionUser");
	Evento e=new Evento();
	e=(Evento) session.getAttribute("currentSessionEvent");
	evento=e.getNome();
	idEvento=e.getIdEvento();

	if (user.getArea().equals("ALL")) {

		tuttipdv=catpdv.getPuntoVendita();

	} else {

		tuttipdv=catpdv.getPuntoVenditaByAree(user.getArea());
	}
}


if(tuttipdv!=null){

for (int p=0;p<tuttipdv.size();p++){
	

Vector<ModB> totalipdv=catmodb.getTotaliPDV(tuttipdv.get(p).getIdPuntoVendita(), idEvento);
if(totalipdv.size()>0){
ModB TotBollepdv=catmodb.getTotBollePDV(tuttipdv.get(p).getIdPuntoVendita(), idEvento);
//tabella relativa a ciascun supermercato


out.print("<font size=\"2\"");
	
	out.println("<br><br>Punto Vendita: <b>"+tuttipdv.get(p).getProvincia()+" "+tuttipdv.get(p).getComune()+" "+tuttipdv.get(p).getIdPuntoVendita()+" - "+tuttipdv.get(p).getInsegna()+" "+tuttipdv.get(p).getIndirizzo()+"</b>");
	out.print("<table frame=box border=1 bordercolor=black cellspacing=0 align=center width=960>");
	out.print("<tr bgcolor=efe4b0>");
	out.print("<th>n.</th>");
	out.print("<th colspan=\"2\">OLIO</th>");
	out.print("<th colspan=\"2\">OMOGENEIZ-<br>ZATI</th>");
	out.print("<th colspan=\"2\">ALIMENTARI<br>INFANZIA</th>");
	out.print("<th colspan=\"2\">TONNO</th>");
	out.print("<th colspan=\"2\">CARNE<br>in scatola</th>");
	out.print("<th colspan=\"2\">PELATI</th>");
	out.print("<th colspan=\"2\">LEGUMI</th>");
	out.print("<th colspan=\"2\">PASTA</th>");
	out.print("<th colspan=\"2\">RISO</th>");
	out.print("<th colspan=\"2\">ZUCCHERO</th>");
	out.print("<th colspan=\"2\">LATTE</th>");
	out.print("<th colspan=\"2\">BISCOTTI</th>");
	out.print("<th colspan=\"2\">VARIE</th>");
	out.print("</tr>");

	
out.print("<tr align=center>");
out.print("<td></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");

out.print("</tr>");


for (int n=0;n<totalipdv.size();n++){
	out.print("<td><a href=\"Elimina_ModB.jsp?id=" +totalipdv.get(n).getIdScheda()+"\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\" target=\"_blank\"><img src=\"../image/elimina.gif\"></a>&nbsp&nbsp"+totalipdv.get(n).getNumeroScheda()+"</td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_olio()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_olio()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_omogeinizzati()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_omogeinizzati()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_infanzia()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_infanzia()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_tonno()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_tonno()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_carne()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_carne()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_pelati()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_pelati()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_legumi()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_legumi()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_pasta()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_pasta()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_riso()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_riso()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_zucchero()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_zucchero()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_latte()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_latte()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_biscotti()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_biscotti()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_varie()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_varie()+" </td>");
out.print("</tr>");
}
	
	
	

	out.print("<tr bgcolor=a7c0dc>");
	out.print("<td>Tot</td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_olio()+"</td><td>"+TotBollepdv.getScatoli_tot_olio()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_omogeinizzati()+"</td><td> "+TotBollepdv.getScatoli_tot_omogeinizzati()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_infanzia()+"</td><td>"+TotBollepdv.getScatoli_tot_infanzia()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_tonno()+"</td><td>"+TotBollepdv.getScatoli_tot_tonno()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_carne()+"</td><td> "+TotBollepdv.getScatoli_tot_carne()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_pelati()+"</td><td>"+TotBollepdv.getScatoli_tot_pelati()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_legumi()+"</td><td>"+TotBollepdv.getScatoli_tot_legumi()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_pasta()+"</td><td>"+TotBollepdv.getScatoli_tot_pasta()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_riso()+"</td><td>"+TotBollepdv.getScatoli_tot_riso()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_zucchero()+"</td><td>"+TotBollepdv.getScatoli_tot_zucchero()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_latte()+"</td><td>"+TotBollepdv.getScatoli_tot_latte()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_biscotti()+"</td><td>"+TotBollepdv.getScatoli_tot_biscotti()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_varie()+"</td><td>"+TotBollepdv.getScatoli_tot_varie()+" </td>");
	out.print("</tr></table>");
	}
}
}
out.print("</font>"); 
%>
	






	</div>
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>


</body>
</html>