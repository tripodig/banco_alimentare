<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="beans.*"%> 
<%@page import="java.util.*"%>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>    
<jsp:useBean id="catpv" scope="page" class="beans.CatalogoPuntoVendita"/>
<jsp:useBean id="catmodb" scope="page" class="beans.DAO_ModB"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
<%

String area="";
String evento="";
int idEvento=0;
if (session.getAttribute("currentSessionUser") != null) {

	UserBean user = (UserBean) session
			.getAttribute("currentSessionUser");
	
	area=user.getArea();
	
	Evento e=new Evento();
	e=(Evento) session.getAttribute("currentSessionEvent");
	evento=e.getNome();
	idEvento=e.getIdEvento();
	
	
	

}else{
	
	//non deve essere possibile effettuare l'export
}
String id=request.getParameter("idpdv");
int idpdv=Integer.parseInt(id);
CatalogoPuntoVendita catpdv=new CatalogoPuntoVendita();
PuntoVendita this_pdv=catpdv.getPuntoVendita(idpdv);

	

Vector<ModB> totalipdv=catmodb.getTotaliPDV(this_pdv.getIdPuntoVendita(), idEvento);
ModB TotBollepdv=catmodb.getTotBollePDV(this_pdv.getIdPuntoVendita(), idEvento);
//tabella relativa a ciascun supermercato



	
	out.println("<br><br>Punto Vendita: <b>"+this_pdv.getProvincia()+" "+this_pdv.getComune()+" - "+this_pdv.getInsegna()+" "+this_pdv.getIndirizzo()+"</b>");
	out.print("<table frame=box border=1 bordercolor=black cellspacing=0 align=center>");
	out.print("<tr bgcolor=efe4b0>");
	out.print("<th>n.</th>");
	out.print("<th colspan=\"2\">OLIO</th>");
	out.print("<th colspan=\"2\">OMOGENEIZ-<br>ZATI</th>");
	out.print("<th colspan=\"2\">ALIMENTARI<br>INFANZIA</th>");
	out.print("<th colspan=\"2\">TONNO</th>");
	out.print("<th colspan=\"2\">CARNE<br>in scatola</th>");
	out.print("<th colspan=\"2\">PELATI</th>");
	out.print("<th colspan=\"2\">LEGUMI</th>");
	out.print("<th colspan=\"2\">PASTA</th>");
	out.print("<th colspan=\"2\">RISO</th>");
	out.print("<th colspan=\"2\">ZUCCHERO</th>");
	out.print("<th colspan=\"2\">LATTE</th>");
	out.print("<th colspan=\"2\">BISCOTTI</th>");
	out.print("<th colspan=\"2\">VARIE</th>");
	out.print("</tr>");

	
out.print("<tr align=center>");
out.print("<td></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");
out.print("<td><font size=\"2\">Kg</font></td>");
out.print("<td><font size=\"2\">Scat.</font></td>");

out.print("</tr>");


for (int n=0;n<totalipdv.size();n++){
	out.print("<td><a href=\"Elimina_ModB.jsp?id=" +totalipdv.get(n).getIdScheda()+"\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\"><img src=\"image/elimina.gif\"></a>&nbsp&nbsp"+totalipdv.get(n).getNumeroScheda()+"</td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_olio()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_olio()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_omogeinizzati()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_omogeinizzati()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_infanzia()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_infanzia()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_tonno()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_tonno()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_carne()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_carne()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_pelati()+"</b></td><td> "+totalipdv.get(n).getScatoli_tot_pelati()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_legumi()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_legumi()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_pasta()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_pasta()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_riso()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_riso()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_zucchero()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_zucchero()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_latte()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_latte()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_biscotti()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_biscotti()+" </td>");
	out.print("<td><b>"+totalipdv.get(n).getPeso_tot_varie()+"</b></td><td>"+totalipdv.get(n).getScatoli_tot_varie()+" </td>");
out.print("</tr>");
}
	
	
	

	out.print("<tr bgcolor=a7c0dc>");
	out.print("<td>Tot</td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_olio()+"</td><td>"+TotBollepdv.getScatoli_tot_olio()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_omogeinizzati()+"</td><td> "+TotBollepdv.getScatoli_tot_omogeinizzati()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_infanzia()+"</td><td>"+TotBollepdv.getScatoli_tot_infanzia()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_tonno()+"</td><td>"+TotBollepdv.getScatoli_tot_tonno()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_carne()+"</td><td> "+TotBollepdv.getScatoli_tot_carne()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_pelati()+"</td><td>"+TotBollepdv.getScatoli_tot_pelati()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_legumi()+"</td><td>"+TotBollepdv.getScatoli_tot_legumi()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_pasta()+"</td><td>"+TotBollepdv.getScatoli_tot_pasta()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_riso()+"</td><td>"+TotBollepdv.getScatoli_tot_riso()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_zucchero()+"</td><td>"+TotBollepdv.getScatoli_tot_zucchero()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_latte()+"</td><td>"+TotBollepdv.getScatoli_tot_latte()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_biscotti()+"</td><td>"+TotBollepdv.getScatoli_tot_biscotti()+" </td>");
	out.print("<td>"+TotBollepdv.getPeso_tot_varie()+"</td><td>"+TotBollepdv.getScatoli_tot_varie()+" </td>");
	out.print("</tr></table>");
	

%>
</body>
</html>