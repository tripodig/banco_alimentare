<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="util.*"%>
 <%@page import="beans.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="stile.css" rel="stylesheet" type="text/css">

<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
	 
		<%@ include file="../component/header.jspf" %>

	</div>
	<div id="page">
	
	
<% 
Evento evento=new Evento();
UserBean user=new UserBean();
if (session.getAttribute("currentSessionUser") != null) {

	user = (UserBean) session.getAttribute("currentSessionUser");
	
	evento=(Evento) session.getAttribute("currentSessionEvent");
}
String id=request.getParameter("id");
int idpdv=Integer.parseInt(id);
CatalogoPuntoVendita cpdv=new CatalogoPuntoVendita();
PuntoVendita pdv=cpdv.getPuntoVendita(idpdv);
String insegna=pdv.getInsegna();
String indirizzo=pdv.getIndirizzo();

FirstPdf f=new FirstPdf();

String path=request.getSession().getServletContext().getRealPath("/");
System.out.println(path);
String directory_separator="\\";
String path0=path+directory_separator+"fileSupporto"+directory_separator+"MOD_a.pdf";
String path1=path+directory_separator+"Modelli_A"+directory_separator+""+idpdv+"_MOD_a.pdf";

System.out.println(path0);
System.out.println(path1);

f.modA_2013(path0, path1,idpdv, evento.getIdEvento());
response.sendRedirect("../Modelli_A/"+idpdv+"_MOD_a.pdf");

%>
<center><a href="../Modelli_A/<%out.print(idpdv);%>_MOD_a.pdf"><img src="../image/icona_pdf.jpg" ></a>
<br><br> Clicca sull'icona per aprire il modulo!


 
	</div><!-- chiusura del div page-->
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>
</div><!-- chiusura del div container-->
</body>
</html>