<%@ page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="cat_pdv" scope="page"
	class="beans.CatalogoPuntoVendita" />
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede" />
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti" />
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona" />
<jsp:useBean id="catc" scope="page" class="beans.CatalogoConvenzioni" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



<%@ include file="../component/title.jspf" %>

<link href="../css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript" language="javascript" src="../js/jquery-1.11.3.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
 <script src="../js/column.js" type="text/javascript"></script>
 <!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script> -->
<!-- <script language="javascript" type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> -->
<script>

$(document).ready(function(){
	     $('#datiPdv').dataTable({"paging":false, "info":false,"language": {
	            "zeroRecords": "Nessun ente corrispondente alla ricerca",
	            "infoEmpty": "Nessun ente disponibile"} });
	});
</script>
</head>
<body>
	<div id="top">
		<%@ include file="../component/top.jspf"%>
	</div>


	<div id="container">
		<div id="header">
			<%@ include file="../component/header.jspf"%>
		</div>
		<div id="page">


			<%
				PuntoVendita pdv = new PuntoVendita();
						Ente e = new Ente();
						//Sede s = new Sede();
						//out.println(s.toString());
						Vector<PuntoVendita> PDVS = new Vector<PuntoVendita>();
						Evento evento=new Evento();
						UserBean user=new UserBean();
						if (session.getAttribute("currentSessionUser") != null) {

							user = (UserBean) session
									.getAttribute("currentSessionUser");
							
							evento=(Evento) session.getAttribute("currentSessionEvent");

							if (user.getArea().equals("ALL")) {

								PDVS = cat_pdv.getPuntoVendita();

							} else {
								System.out.println("aree dell'utente="+ user.getArea());
								PDVS = cat_pdv.getPuntoVenditaByAree(user.getArea());
							}
						}
			%>
<h1>Punti Vendita</h1><h3><%out.print(user.getArea());%></h3>
<!-- testo descrittivo della pagina -->
<p>Punti Vendita che parteciperanno alla <b><%out.print(evento.getDescrizione()); %> </b> che si svolger� il <b><%out.print(evento.getData()); %></b>. <br>
� possibile stampare i modelli precompilati, per ciascun Punto vendita, da consegnare ai Capo Equipe assieme al materiale.

</p>
			<center>
				<table id="datiPdv" class="display" cellspacing="0" align="center">
					<thead>
					<tr>
						<th>CODICE</th>
						<th colspan=2><%out.print(evento.getNome()); %> e MODELLI</th>
						<th>INSEGNA</th>
						<th>INDIRIZZO</th>
						<%//<th>RECAPITI</th> %>
						<th>AREA</th>
						
					</tr>
					</thead>
			

				<tbody>
					<%
						for (int i = 0; i < PDVS.size(); i++) {
							int codPDV = PDVS.get(i).getIdPuntoVendita();
							String insegna = PDVS.get(i).getInsegna();
							String gruppo = PDVS.get(i).getGruppo();
							String indirizzo = PDVS.get(i).getIndirizzo();
							String comune = PDVS.get(i).getComune();
							String provincia = PDVS.get(i).getProvincia();
							String area = PDVS.get(i).getArea();
							String tel = PDVS.get(i).getTelefono();
							String cell = PDVS.get(i).getCellulare();
							String orario = PDVS.get(i).getOrario();
							
							String mail = PDVS.get(i).getEmail();
							
							//out.println("<tr class=\"d"+i%2+"\"d>");
							out.println("<tr>");
							out.println("<td align=center>");
							out.println("<b> <a style=text-decoration:none href=\"Scheda_PDV.jsp?id="+ codPDV+ "\">" + codPDV + "</a> </b>");
							out.println("</td>");

							out.println("<td>");
							if (!(orario.equals("ND")|| orario.equals(""))) out.println("<b>"+orario+"</b><br>");
							
							out.println("<a style=text-decoration:none href=\"Nascondi_PDV.jsp?id=" + codPDV+ "\">- Escludi</a>");
							out.println("</td>");
							out.println("<td>");
							out.println("A <a style=text-decoration:none href=\"stampa_modA.jsp?id="
									+ codPDV
									+ "\" target=_blank><img src=../image/icona_pdf.jpg width=20 ></a>&nbsp;");
							out.println(" B <a style=text-decoration:none href=\"stampa_modB.jsp?id="
									+ codPDV
									+ "\" target=_blank><img src=../image/icona_pdf.jpg width=20 ></a>");
							out.println("</td>");
							out.println("<td>");
						/* 	out.println("<b><a style=text-decoration:none href=\"Modifica_PuntiVendita.jsp?id="
									+ codPDV
						 			+ "\">" + insegna + "</a></b> ");
						*/
						
						out.println("<b><a style=text-decoration:none href=\"Scheda_PDV.jsp?id="+ codPDV+ "\">" + insegna + "</a></b> ");
						out.println("<br><font size=1><a style=text-decoration:none href=\"Scheda_PDV.jsp?id="+ codPDV+ "\">(" + gruppo + ")</a></font> ");
						
						
						out.println("</td>");

							out.println("<td>");
							out.println("<b>" + indirizzo + " " + comune + " " + provincia+ "</b> <br>");
							if((tel.equals("ND")|| tel.equals(""))||(cell.equals("ND")|| cell.equals(""))||(mail.equals("ND")|| !mail.equals(""))){
							
							if (!(tel.equals("ND")|| tel.equals(""))) out.println(tel+"<br>");
							if (!(cell.equals("ND")|| cell.equals(""))) out.println(cell+"<br>");
							if (!(mail.equals("ND")|| equals(""))) out.println(mail);
							}
							out.println("</td>");

							/* out.println("<td>");
							out.println("<b>" + tel + "  " + mail + "</b> ");
							out.println("</td>"); */

							out.println("<td>");
							out.println("<b>" + area + "</b> ");
							out.println("</td>");

							

							out.println("</tr>");
						}
					%>
					<tbody>
				</table>
			</center>




		</div>
		<!-- chiusura del div page-->

		<div id="footer">
			<%@ include file="../component/footer.jspf"%>
		</div>
	</div>
	<!-- chiusura del div container-->

</body>
</html>