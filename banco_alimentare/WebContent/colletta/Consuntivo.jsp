<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="beans.*"%> 
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page import="util.*"%>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>    
<jsp:useBean id="catpv" scope="page" class="beans.CatalogoPuntoVendita"/>
<jsp:useBean id="catstat1" scope="page" class="beans.DAO_Stat1"/>
<jsp:useBean id="catmodb" scope="page" class="beans.DAO_ModB"/>
<jsp:useBean id="catmoda" scope="page" class="beans.DAO_ModA"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
		<%@ include file="../component/header.jspf" %>
	</div>
	<div id="page" > 
	<h1>Resoconto Raccolta </h1>
<%


GregorianCalendar d=new GregorianCalendar();
String Giorno=((Integer)d.get(Calendar.DAY_OF_MONTH)).toString();
int Mese=((Integer)d.get(Calendar.MONTH))+1;
String Anno=((Integer)d.get(Calendar.YEAR)).toString();

int oraItalia=((Integer)d.get(Calendar.HOUR_OF_DAY)+9)%24;
//String Ora=(((Integer)d.get(Calendar.HOUR_OF_DAY))).toString();
String Minuto=((Integer)d.get(Calendar.MINUTE)).toString();


%>
<!-- testo descrittivo della pagina -->
Quantit� di alimenti raccolti e censiti dalle sedi del Banco Alimentare. <br>
Totale di alimenti censito ed effettivamente presente all'interno dei magazzini. <br>
<br>L'ultimo aggiornamento � al: 
<%
out.println("<b>"+Giorno+"/"+Mese+"/"+Anno+"  -  "+oraItalia+":"+Minuto+"</b><br><br>");

ModB totalic=null;
Vector<ModB> totaliVectc=null;

String evento="";
int idEvento=0;

if (session.getAttribute("currentSessionUser") != null) {

	UserBean user = (UserBean) session.getAttribute("currentSessionUser");

	Evento e=new Evento();
	e=(Evento) session.getAttribute("currentSessionEvent");
	evento=e.getNome();
	idEvento=e.getIdEvento();
	

	if (user.getArea().equals("ALL")) {

		totalic=catmodb.getTotaleComplessivo(idEvento);
		
		%>


<!-- tabella dei totali complessivi -->

<table border="0" align="center">

<tr class=intestTab>
<th colspan="2">OLIO</th>
<th colspan="2"><font size="2">OMOGENEIZ-<br>ZATI</font></th>
<th colspan="2"><font size="2">ALIMENTARI<br>INFANZIA</font></th>
<th colspan="2">TONNO</th>
<th colspan="2">CARNE<br>in scatola</th>
<th colspan="2">PELATI</th>
<th colspan="2">LEGUMI</th>
<th colspan="2">PASTA</th>
<th colspan="2">RISO</th>
<th colspan="2"><font size="2">ZUCCHERO</font></th>
<th colspan="2">LATTE</th>
<th colspan="2">BISCOTTI</th>
<th colspan="2">VARIE</th>
</tr>

<tr align=center>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
</tr>

<%		out.println("<tr>");
		out.print("<td><b>"+totalic.getPeso_tot_olio()+"</b></td><td>"+totalic.getScatoli_tot_olio()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_omogeinizzati()+"</b></td><td> "+totalic.getScatoli_tot_omogeinizzati()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_infanzia()+"</b></td><td>"+totalic.getScatoli_tot_infanzia()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_tonno()+"</b></td><td>"+totalic.getScatoli_tot_tonno()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_carne()+"</b></td><td> "+totalic.getScatoli_tot_carne()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_pelati()+"</b></td><td> "+totalic.getScatoli_tot_pelati()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_legumi()+"</b></td><td>"+totalic.getScatoli_tot_legumi()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_pasta()+"</b></td><td>"+totalic.getScatoli_tot_pasta()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_riso()+"</b></td><td>"+totalic.getScatoli_tot_riso()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_zucchero()+"</b></td><td>"+totalic.getScatoli_tot_zucchero()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_latte()+"</b></td><td>"+totalic.getScatoli_tot_latte()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_biscotti()+"</b></td><td>"+totalic.getScatoli_tot_biscotti()+"</td>");
		out.print("<td><b>"+totalic.getPeso_tot_varie()+"</b></td><td>"+totalic.getScatoli_tot_varie()+"</td>");
		out.print("</tr>");
		
%>
		</table>
		
<% 

	} else {//se utente non � admin

		
		//statistiche

		Vector<Stat1> statistica= new Vector<Stat1>();
		DAO_Stat1 catStat=new DAO_Stat1();
		statistica=catStat.getStatgiornata(idEvento);
		Vector<Stat1> statisticaArea= new Vector<Stat1>();
		
		Vector<String> Areas= new Vector<String>();
		Vector<String> Oras= new Vector<String>();
		for (int i=0; i<statistica.size();i++){
			Areas.add(statistica.get(i).getAREA());
		Oras.add(statistica.get(i).getOra());
		}
		Areas=utility.distinct(Areas);
		Oras=utility.distinct(Oras);
		if(statistica.size()>0){
			out.print("<font size=\"2\">");
		
			out.print("<table frame=box cellspacing=0 border=1 bordercolor=black ><tr><td>ore</td>");
			for (int a=0;a<Oras.size();a++)
			{
				out.print("<td>"+Oras.get(a)+"</td>");	
			}
			out.print("</tr><tr>");
			for (int y=0;y<Areas.size();y++){
				out.print("<td>"+Areas.get(y)+"</td>");
				statisticaArea=catStat.getStatgiornata(idEvento,Areas.get(y));
				for (int z=0;z<Oras.size();z++)
				{
					out.println("<td>");
				for (int x=0; x<statisticaArea.size();x++){
					
						if(statisticaArea.get(x).getAREA().equals(Areas.get(y))&& statisticaArea.get(x).getOra().equals(Oras.get(z)))
							out.println( statisticaArea.get(x).getPesoTot());
						
						
												
					}
				out.println("</td>");
				}
				out.print("</tr>");
			}
			out.print("</table>");	
			out.print("</font>");
			out.print("<br>");
		}
			
		
		
				
		
		
		
		
		
				/* for (int i=0; i<statistica.size();i++){
					out.println("Area "+statistica.get(i).getAREA()+" Ore "+statistica.get(i).getOra()+" --- "+ statistica.get(i).getPesoTot()+" Kg.<br>");
					//out.println("<b> "+statistica.get(i).toString()+"</b> ");
					System.out.println(statistica.get(i).toString());
				} */
		
		
		
		
		
		
		totaliVectc=catmodb.getTotaleComplessivoByAree(user.getArea(), idEvento);
		
		String aree=user.getArea();
		StringTokenizer st= new StringTokenizer(aree, ",");
		int numAree=st.countTokens();
		
		ModB tmp=null;
		
		
		
		for(int i=0;i<numAree;i++){
			
			tmp=totaliVectc.get(i);
			
			System.out.println("id scheda: "+totaliVectc.get(i).getIdScheda());
			
			out.print(st.nextToken()+":");
			out.print("<br>");
			
			%>
			
<!-- tabella dei totali complessivi -->
<%out.print("<font size=2>"); %>

<table border="0" align="center" width=960>
<tr class=intestTab>
<th colspan="2">OLIO</th>
<th colspan="2"><font size="2">OMOGENEIZ-<br>ZATI</font></th>
<th colspan="2"><font size="2">ALIMENTARI<br>INFANZIA</font></th>
<th colspan="2">TONNO</th>
<th colspan="2">CARNE<br>in scatola</th>
<th colspan="2">PELATI</th>
<th colspan="2">LEGUMI</th>
<th colspan="2">PASTA</th>
<th colspan="2">RISO</th>
<th colspan="2"><font size="2">ZUCCHERO</font></th>
<th colspan="2">LATTE</th>
<th colspan="2">BISCOTTI</th>
<th colspan="2">VARIE</th>
</tr>

<tr align=center class="d0">
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
</tr>
			
			
			<%
			

		out.println("<tr class=\"d1\">");
		out.print("<td><b>"+tmp.getPeso_tot_olio()+"</b></td><td>"+tmp.getScatoli_tot_olio()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_omogeinizzati()+"</b></td><td> "+tmp.getScatoli_tot_omogeinizzati()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_infanzia()+"</b></td><td>"+tmp.getScatoli_tot_infanzia()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_tonno()+"</b></td><td>"+tmp.getScatoli_tot_tonno()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_carne()+"</b></td><td> "+tmp.getScatoli_tot_carne()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_pelati()+"</b></td><td> "+tmp.getScatoli_tot_pelati()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_legumi()+"</b></td><td>"+tmp.getScatoli_tot_legumi()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_pasta()+"</b></td><td>"+tmp.getScatoli_tot_pasta()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_riso()+"</b></td><td>"+tmp.getScatoli_tot_riso()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_zucchero()+"</b></td><td>"+tmp.getScatoli_tot_zucchero()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_latte()+"</b></td><td>"+tmp.getScatoli_tot_latte()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_biscotti()+"</b></td><td>"+tmp.getScatoli_tot_biscotti()+"</td>");
		out.print("<td><b>"+tmp.getPeso_tot_varie()+"</b></td><td>"+tmp.getScatoli_tot_varie()+"</td>");
		out.print("</tr>");
		
		//out.print("</br>");
		%>
		</table>
		<br>
		<br>
		<%
		
 		out.print("</font>"); 
		}//for singola area

		
	}//else aree specifiche
	
	
	

	
	
	
	
}else{//else non � loggato
	
	out.println("Non puoi visualizzare queste informazioni");
	
}
%>



<br>
Visualizza il totale dettagliato:<br>
<a href="Consuntivo_dettaglio.jsp">Dettaglio Punti vendita</a>







	</div>
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>


</body>
</html>