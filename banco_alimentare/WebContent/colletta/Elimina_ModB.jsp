<jsp:useBean id="DAOmodB" scope="page" class="beans.DAO_ModB"/>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
<%
String id=request.getParameter("id");
int idModB=Integer.parseInt(id);
ModB mod=DAOmodB.getModB(idModB);
boolean esito=DAOmodB.eliminaModB(mod);
String jspPage;
if (esito){
	System.out.println("Operazione eseguita correttamente!");
	String commit="Operazione eseguita correttamente!";    		 
	request.setAttribute("commit", commit);
	jspPage="../result/commit.jsp";
}
else{
   System.out.println("!!! Operazione non eseguita !!!");
   String errore="Operazione non eseguita correttamente!";    		 
   request.setAttribute("errore", errore);
   jspPage="../result/error.jsp";
}
request.getRequestDispatcher( jspPage ).forward(request,response);




%>
</body>
</html>