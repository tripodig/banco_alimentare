<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="beans.*"%> 
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>    
<jsp:useBean id="catpv" scope="page" class="beans.CatalogoPuntoVendita"/>
<jsp:useBean id="catmodb" scope="page" class="beans.DAO_ModB"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script>
function history(value)
{
//you can get the value from arguments itself
var id= value;
document.getElementById('view_history').style.display="block";
document.getElementById('view_history').innerHTML="<a target=\"_blank\" href=\"view_history.jsp?idpdv="+id+"\">Visualizza schede gi� caricate</a>";
}
</script>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>





</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>

<div id="container">
	<div id="header">
		<%@ include file="../component/header.jspf" %>
	</div>
	<div id="page" >
	<h1>Inserisci Modello B </h1>
<br>
	
	<center>
	<div style='background-color:#F6F6F6; border:5px solid #FF0000'>
	<p><b>Attenzione:</b>	Se sono gi� stati caricati i Modelli A per il supermercato interessato 
	<br><b>NON � NECESSARIO CARICARE ANCHE IL MODELLO B.</b><br>
	Si prega di Verificare.
	</p>
	</div>


<br> Se invece si � in possesso solo del modello A � necessario caricare solo quello <a href="../../banco_alimentare/colletta/Mod_A.jsp">Modello A</a>
<br><br></center>
	
<div id="mod_A">

<form method="post" action="../New_modB" >

Punto Vendita: <select name="idPuntoVendita" onchange="history(this.value)">
<option value="-1">Inserire  Punto Vendita</option>
<% 

if(session.getAttribute("currentSessionUser")!=null){
	
	UserBean user=(UserBean) session.getAttribute("currentSessionUser");

	Vector<PuntoVendita> PuntoVenditas=catpv.getPuntoVenditaByAree(user.getArea());


for (int i=0;i<PuntoVenditas.size();i++){
	System.out.println("pdv id= "+PuntoVenditas.get(i).getIdPuntoVendita());
	out.println("<option value="+PuntoVenditas.get(i).getIdPuntoVendita()+"><b>"+PuntoVenditas.get(i).getComune()+" "+PuntoVenditas.get(i).getIdPuntoVendita()+" - "+PuntoVenditas.get(i).getInsegna()+"</b> "+ PuntoVenditas.get(i).getIndirizzo()+"</option>");
}
}
%>
</select><br><br>

Capo Equipe: <select name="idCapoEquipe">
<% Vector<Persona> CapoEquipe=catp.getPersonaCapoEquipe();

for (int i=0;i<CapoEquipe.size();i++){
	System.out.println("capoeq id= "+CapoEquipe.get(i).getIdPersona());
	out.println("<option value="+CapoEquipe.get(i).getIdPersona()+">"+CapoEquipe.get(i).getCognome()+ CapoEquipe.get(i).getNome()+"</option>");
}
%>
</select>

<br><br>
<div id="view_history">Selezionare Punto Vendita! </div><br>
<table frame=box border="1" bordercolor="black" cellspacing="0" align="center">
<tr>
<th><font size="2">SCHEDA</font></th>
<th colspan="2"><font size="2">OLIO</font></th>
<th colspan="2"><font size="2">OMOGENEIZ-<br>ZATI</font></th>
<th colspan="2"><font size="2">ALIMENTARI<br>INFANZIA</font></th>
<th colspan="2"><font size="2">TONNO</font></th>
<th colspan="2"><font size="2">CARNE<br>in scatola</font></th>
<th colspan="2"><font size="2">PELATI</font></th>
<th colspan="2"><font size="2">LEGUMI</font></th>
<th colspan="2"><font size="2">PASTA</font></th>
<th colspan="2"><font size="2">RISO</font></th>
<th colspan="2"><font size="2">ZUCCHERO</font></th>
<th colspan="2"><font size="2">LATTE</font></th>
<th colspan="2"><font size="2">BISCOTTI</font></th>
<th colspan="2"><font size="2">VARIE</font></th>
</tr>

<tr align=center>
<td></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>
<td><font size="2">Kg</font></td>
<td><font size="2">Scat.</font></td>

</tr>

<%
int nn=11;//numero di righe (il righe+1 causa indice del for)
out.println("<input type=\"hidden\" name=\"nn\" value=\""+nn+"\">");
for (int a=1;a<nn;a++){ 
out.print("<tr align=center>");
out.print("<td><input type=text name=NumScheda_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Olio_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Olio_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Omogeneizzati_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Omogeneizzati_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Infanzia_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Infanzia_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Tonno_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Tonno_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Carne_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Carne_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Pelati_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Pelati_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Legumi_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Legumi_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Pasta_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Pasta_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Riso_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Riso_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Zucchero_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Zucchero_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Latte_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Latte_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Biscotti_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Biscotti_sc_"+a+" style=width:20px></td>");
out.print("<td><input type=text name=Varie_"+a+" style=width:25px></td>");
out.print("<td><input type=text name=Varie_sc_"+a+" style=width:20px></td>");
out.print("</tr>");

}
%>
</table>
<br><br>
<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
</form>
</div>

	</div><!-- chiusura del div page-->
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>
</div><!-- chiusura del div container-->

</body>
</html>