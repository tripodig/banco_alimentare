function check_domain() {
	return true
}
function carica_regioni() {
	if (!check_domain()) {
		document.getElementById('regione').options.length = 0;
		return
	}
	var a = document.getElementById('regione');
	var b = return_regioni();
	b = b.sort();
	document.getElementById('provincia').options.length = 0;
	document.getElementById('citta').options.length = 0;
	document.getElementById('cap').options.length = 0;
	a.options[0] = new Option("Scegli la Tua Regione", "NULL");
	for (i = 0; i < b.length; i++) {
		var c = b[i].split(';');
		a.options[i + 1] = new Option(c[0], c[0])
	}
}
function carica_province() {
	var a = document.getElementById('provincia');
	var b = document.getElementById('regione').value;
	if (b == 'NULL') {
		a.options.length = 0
	} else a.options[0] = new Option("Scegli la Tua Provincia", "NULL");
	document.getElementById('citta').options.length = 0;
	document.getElementById('cap').options.length = 0;
	var c = b[0];
	var d = b[1];
	var e = return_province(b);
	e = e.sort();
	for (i = 0; i < e.length; i++) {
		var f = e[i].split(';');
		a.options[i + 1] = new Option(f[0], f[1])
	}
}
function carica_citta () {
	var select_field = document.getElementById('citta');
	var regione = document.getElementById('regione').value;
	var sigla_provincia = document.getElementById('provincia').value;
	if (sigla_provincia == 'NULL') {
		select_field.options.length = 0
	} else select_field.options[0] = new Option("Scegli la Tua citta", "NULL");
	document.getElementById('cap').options.length = 0;
	var lista_comuni = return_comuni(sigla_provincia);
	for (i = 0; i < lista_comuni.length; i++) {
		var curr = lista_comuni[i].split(';');
		select_field.options[i + 1] = new Option(curr[1], curr[1]);
		select_field.options[i + 1].setAttribute("valorericerca", curr[0]);
	}
}
function carica_cap() {
	var a = document.getElementById('cap');
	var b = document.getElementById('regione').value;
	var c = document.getElementById('provincia').value;
	var d = document.getElementById('citta');
	
	var valorericerca1=d.options[d.selectedIndex].getAttribute('valorericerca');

	
	//confirm('d='+d);
	//confirm(valorericerca1);
	var e = return_cap(valorericerca1).split(',');
	if (d == 'NULL') {
		a.options.length = 0
	} else a.options[0] = new Option("Scegli il Tuo CAP", "NULL");
	for (i = 0; i < e.length; i++) a.options[i + 1] = new Option(e[i], e[i])
}