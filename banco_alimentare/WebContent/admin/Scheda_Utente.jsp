<%@ page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="utenteDao" scope="page" class="beans.UserDAO" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script type="text/javascript">

function listaAreaColl()
{
	
	if(document.getElementById('ruolocoll').checked){
		
		document.getElementById('elencoColl').style.display="block";
		document.getElementById('annoConv').style.display="block";
		
	
	}else{
		document.getElementById('elencoColl').style.display="none";
		
		if(document.getElementById('elencoConv').style.display=="none"){
			
			document.getElementById('annoConv').style.display="none";
		}
			
		}
	
	
}

function listaProvConv()
{
	
if(document.getElementById('ruoloconv').checked){
		
		document.getElementById('elencoConv').style.display="block";
		document.getElementById('annoConv').style.display="block";
		
	}else{
		document.getElementById('elencoConv').style.display="none";
		
		if(document.getElementById('elencoColl').style.display=="none"){
			
			document.getElementById('annoConv').style.display="none";
			
		}
	}
}

</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



<%@ include file="../component/title.jspf" %>
</head>
<body>
	<div id="top">
		<%@ include file="../component/top.jspf"%>
	</div>


	<div id="container">
		<div id="header">
			<%@ include file="../component/header.jspf"%>
		</div>
		<div id="page">

			<%
			UserBean u = new UserBean();
				//out.println(s.toString());
				
				String idUsers=request.getParameter("id");
				
				
				//SPOSTARE LE DUE RIGHE SUCC
				//String tipoRic=request.getParameter("tipoRic");
				
				//if(tipoRic.equalsIgnoreCase("codice")){
		
				
				//String idEntes="181012";
				
				try{
					
					
					int idUser=Integer.parseInt(idUsers);
					System.out.println(idUser);
					//ricavo l'ente tramite l'idEnte ricevuto
					u=utenteDao.getUser(idUser);
					
					
					
					if(u==null){
						
							   System.out.println("!!! Operazione non eseguita !!!");
							   String errore="Il codice inserito non corrisponde a nessun utente!";    		 
							   request.setAttribute("errore", errore);
						
							request.getRequestDispatcher( "error.jsp" ).forward(request,response);
							return;
						
					}else{//form con modifica utente
						
						%>
						
						<form method="post" action="Insert_User?tipo=mod">

						<input type="hidden" name="id" value=<%=u.getIdUser()%> required style="width: 400px">

						<table align="center">
							
							
							<tr  style="width: 600px" align="center">
								<td>Nome&nbsp</td>
								<td align="left"><input type="text" name="Nome" value=<%=u.getnome()%> required style="width: 400px"></td>
							</tr>
							
							<tr align="center">
								<td>Cognome&nbsp</td>
								<td><input type="text" name="Cognome" value=<%=u.getcognome()%> required style="width: 400px"></td>
							</tr>
							
							<tr align="center">
								<td >Username&nbsp</td>
								<td><input type="text" name="Username" value=<%=u.getUsername()%> required style="width: 400px"></td>
							</tr>
							
							<tr align="center">
								<td>Password&nbsp</td>
								<td><input type="password" name="Password" value=<%=u.getPassword()%> required style="width: 400px"></td>
							</tr>
							
							
							<tr align="center">
								<td>Ruolo/i&nbsp</td>
								
								<td>
								<input type="checkbox" id="ruolocoll" name="Ruolo" value="COLLETTA" <%if(u.getRuolo().contains("COLLETTA")){%> checked="checked" <%}%> onchange="listaAreaColl()">Colletta
								<input type="checkbox" id="ruoloconv" name="Ruolo" value="CONVENZIONI" <%if(u.getRuolo().contains("CONVENZIONI")){%> checked="checked" <%}%>  onchange="listaProvConv()">Convenzioni
								<input type="checkbox" name="Ruolo" value="ADMIN" <%if(u.getRuolo().contains("ADMIN")){%> checked="checked" <%}%> >Amministratore
								
								</td>
							</tr>					
							
							<tr>
								<td>&nbsp</td>
							</tr>					
							
							
							
							<tr align="center" id="elencoColl" <%if(u.getRuolo().contains("COLLETTA")){%> style="display: block" <%}else{%>style="display: none"<%}%>>
								<td>Area Colletta&nbsp</td>
								

								<td>
								<input type="checkbox" name="Area" value="REGGIO CALABRIA" <%if(u.getArea().contains("REGGIO CALABRIA")){%> checked="checked" <%}%>>REGGIO
								<input type="checkbox" name="Area" value="LOCRIDE RC" <%if(u.getArea().contains("LOCRIDE RC")){%> checked="checked" <%}%>>LOCRIDE
								<input type="checkbox" name="Area" value="PIANA RC" <%if(u.getArea().contains("PIANA RC")){%> checked="checked" <%}%>>PIANA
								<input type="checkbox" name="Area" value="IONICA RC" <%if(u.getArea().contains("IONICA RC")){%> checked="checked" <%}%>>IONICA RC
								<input type="checkbox" name="Area" value="PROVINCIA CS" <%if(u.getArea().contains("PROVINCIA CS")){%> checked="checked" <%}%>>PROVINCIA CS
								<input type="checkbox" name="Area" value="COSENZA" <%if(u.getArea().contains("COSENZA")){%> checked="checked" <%}%>>COSENZA
								<input type="checkbox" name="Area" value="VIBO VALENTIA" <%if(u.getArea().contains("VIBO VALENTIA")){%> checked="checked" <%}%>>VIBO VALENTIA
								<input type="checkbox" name="Area" value="CATANZARO" <%if(u.getArea().contains("CATANZARO")){%> checked="checked" <%}%>>CATANZARO
								<input type="checkbox" name="Area" value="CROTONE" <%if(u.getArea().contains("CROTONE")){%> checked="checked" <%}%>>CROTONE
								</td>
							
							
							</tr>
							
						
							
							<tr>
								<td>&nbsp</td>
							</tr>					
							
							
							<tr align="center" id="annoConv" <%if(u.getRuolo().contains("COLLETTA")||u.getRuolo().contains("CONVENZIONI")){%> style="display: block" <%}else{%>style="display: none"<%}%>>
								<td>Anno Convenzioni&nbsp</td>
								<td><input type="text" name="AnnoConvenzioni" value=<%=u.getAnnoConvenzioni()%> required style="width: 400px"></td>
							</tr>
							
							<tr align="center">
								<td>&nbsp</td>
							</tr>					
							
							
							<tr align="center" id="elencoConv" <%if(u.getRuolo().contains("CONVENZIONI")){%> style="display: block" <%}else{%>style="display: none"<%}%>>
								<td>Provincia Convenzioni&nbsp</td>
								
								<td>
								
								<input type="checkbox" name="ProvinciaConvenzioni" value="CS" <%if(u.getProvinciaConvenzioni().contains("CS")){%> checked="checked" <%}%>>Cosenza
								<input type="checkbox" name="ProvinciaConvenzioni" value="CZ" <%if(u.getProvinciaConvenzioni().contains("CZ")){%> checked="checked" <%}%>>Catanzaro
								<input type="checkbox" name="ProvinciaConvenzioni" value="RC" <%if(u.getProvinciaConvenzioni().contains("RC")){%> checked="checked" <%}%>>Reggio Calabria
								<input type="checkbox" name="ProvinciaConvenzioni" value="VV" <%if(u.getProvinciaConvenzioni().contains("VV")){%> checked="checked" <%}%>>Vibo Valentia
								<input type="checkbox" name="ProvinciaConvenzioni" value="KR" <%if(u.getProvinciaConvenzioni().contains("KR")){%> checked="checked" <%}%>>Crotone
								
								
								
								</td>
							</tr>
												
						</table>
						<br>
						<div align="center">
							<input type="reset" value="Cancella">&nbsp&nbsp<input
								type="submit" value="Aggiorna">
						</div>
					</form>
					<%	
					}//else

				}catch(NumberFormatException eccezione){
					
					System.out.println("!!! Operazione non eseguita !!!");
						   String errore="Id non valido!";    		 
						   request.setAttribute("errore", errore);
					
						request.getRequestDispatcher( "error.jsp" ).forward(request,response);
						return;
					
			}//catch
				%>
	
		</div>
		<!-- chiusura del div page-->

		<div id="footer">
			<%@ include file="../component/footer.jspf"%>
		</div>
	</div>
	<!-- chiusura del div container-->

</body>
</html>