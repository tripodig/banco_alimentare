<%@ page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="cate" scope="page" class="beans.CatalogoEnti" />
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede" />
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti" />
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona" />
<jsp:useBean id="catc" scope="page" class="beans.CatalogoConvenzioni" />
<jsp:useBean id="catc2017" scope="page" class="beans.DAO_Convenzione2017" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



<%@ include file="../component/title.jspf" %>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
$(document).ready(function(){
	   $(".editmode").hide();
	});

</script>
</head>
<body>

	<div id="top">
		<%@ include file="../component/top.jspf"%>
	</div>


	<div id="container">
		<div id="header">
			<%@ include file="../component/header.jspf"%>
		</div>
		<div id="page">
		

  	<br><br><a id="bottone"><img src=../image/Edit.png width=25 ></a>


  <script>
    $("#bottone").click(function () {
      $(".editmode").toggle("slow");
      $(".hideineditmode").toggle("slow");
      
    });    
    </script> 
			<%
				Ente e = new Ente();
				Vector<Ente> enti = new Vector<Ente>();
				Sede s = new Sede();
				int idEnte=0;
				//out.println(s.toString());
				
				Vector<Ente> VE = new Vector<Ente>();
				
				String idEntes=request.getParameter("id");//arriva da altre pagine attraverso dei link, se arriva � corretto
				if(idEntes!=null){
				
					idEnte=Integer.parseInt(idEntes);
					e=cate.getEnte(idEnte);
					Vector<Sede> VS = new Vector<Sede>();
					int codStruttura = e.getCodiceStruttura();
					String nome = e.getNome();
					String PartitaIva = e.getPartitaIva();
					String Descrizione = e.getDescrizione();
					VS = cats.getSediEnte(e.getIdEnte());
					Vector<Persona> VP = new Vector<Persona>();
					VP = catp.getPersoneEnte(e.getIdEnte());

		
				%>
				<table align="center" border="0" width="100%">
				<tr >
				<td VALIGN="top" colspan="3" >
				
			
					<%
						
					
							//out.println("<a id=\"bottone\"><img src=../image/Edit.png width=25 ></a>");
							out.println("<center><font size=\"6\"><b> "+ nome + "</b></font> <br>");
							out.println(PartitaIva+"<br>");
							out.println(Descrizione+"</center><br>");
							out.println("<b>DATI IDENTIFICATIVI</b><a class=\"editmode\" href=\"../anagrafica/Modifica_ente.jsp?id=" + idEnte+ "\"><img src=../image/Edit.png width=25 ></a>");
							out.println("<ul>");
							out.println("<li>Codice Struttura: "+codStruttura+"</li>");
							out.println("<li>Codice SAP: "+e.getCodiceSap()+"</li>");
							out.println("</ul>");
							
							
							//out.print("<br>");
					%>
					
				</td>
				
				</tr>
				<tr>
				<td colspan=3>
<!-- REFERENTI -->
				<b>REFERENTI</b>
				<%out.println("&nbsp;<a class=\"editmode\" href=\"Aggiungi_Referenti.jsp?id="+ idEnte +"\"><img src=../image/add.png width=18 ></a> <br>");
				%>	
				<ul>
				
				<%
					for (int z = 0; z < VP.size(); z++) {
						int idPersona = VP.get(z).getIdPersona();
						out.println("<li>");
						out.println("<div class=\"editmode\"><a href=\"../anagrafica/Modifica_Referenti.jsp?id=" + idPersona+ "\"><img src=../image/Edit.png width=25 ></a>");
						out.print("&nbsp;&nbsp;&nbsp;&nbsp;");
						out.println("<a  href=\"../anagrafica/Elimina_Persona.jsp?id=" + idPersona+ "\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\"><img src=../image/del.png width=18 ></a></div>");
						out.println(VP.get(z).toString3line()+"</li>");
						/* out.println("<br>");  */
						
						/* out.println("<br><br>");  */

					}
					
					%>
				
				
				</ul>
				</td>
				</tr>
				<tr>
				<td colspan=3>
				<b>SEDI</b>
<!-- SEDI -->
				<%
					out.println("&nbsp;<a class=\"editmode\" href=\"../anagrafica/Aggiungi_Sede.jsp?id="+ idEnte + "\"><img src=../image/add.png width=18 ></a><br>");
				%>
				<ul>	
				<%
				for (int j = 0; j < VS.size(); j++) {
								int idSede = VS.get(j).getIdSede();
								out.println("<li>");
								out.println("<div class=\"editmode\"><a  href=\"../anagrafica/Modifica_Sede.jsp?id=" + idSede+ "\"><img src=../image/Edit.png width=25 ></a>");
								out.print("&nbsp;&nbsp;&nbsp;&nbsp;");
								out.println("<a  href=\"../anagrafica/Elimina_Sede.jsp?id=" + idSede+ "\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\" ><img src=../image/del.png width=18 ></a></div>");
								out.println(VS.get(j).toString2line()+"</li>");
								
					}
					%>
				
				</ul>
				
				</td>
				</tr>
				<td colspan=3>
				<b>CONVENZIONI</b>
<!-- CONVENZIONI	 -->			
					
				<% 
					//anno della convenzione che vorrei fare: lo prelevo dalle propriet� dell'utente connesso
					UserBean user=(UserBean) request.getSession().getAttribute("currentSessionUser");
					int anno= user.getAnnoConvenzioni();
					
					Vector<ConvenzioneFrom2017>ConvenzioniEnte = catc2017.getConvenzioniEnte(idEnte);
					
					//non convenzionabile vuol dire che manca rapplegale e/o sede legale
					if(catc.isConvenzionabile(idEnte)){
						out.println("&nbsp;<a href=\"../convenzioni/VisualizzaConvenzione.jsp?ricercabyid="+ idEnte +"\"><img src=../image/lente.png width=20 ></a><br>");
						if(catc2017.getidconvenzione(idEnte, anno)==0){
							out.println("<div >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
							out.println("&nbsp;Domanda di Affiliazione (All. 2) AGEA  <a href=\"../convenzioni/2017_Rinnova_Convenzione.jsp?id="+ idEnte +"\"><img src=../image/add.png width=18 ></a>");
							//out.println("&nbsp;&nbsp;BANCO<a href=\"../convenzioni/Rinnova_Convenzione_Banco.jsp?id="+ idEnte +"\"><img src=../image/add.png width=18 ></a></div>");
						}
							
					}
				
 					if(ConvenzioniEnte!=null){
						out.print("<ul>");
						
						for (int z=0; z<ConvenzioniEnte.size();z++){
							int idConvenzione=ConvenzioniEnte.get(z).getIdConvenzione();
							DAO_Documento DAO_documento= new DAO_Documento();
							List<DocumentoBean> documenti= DAO_documento.getDocumentibyIdConvenzione(idConvenzione);

							
							out.print("<li> Domanda di Affiliazione " + ConvenzioniEnte.get(z).getAnno()+" "+ConvenzioniEnte.get(z).getTipologia()+" ");
							out.print("<a class=\"editmode\" href=\"../convenzioni/Elimina_Convenzione.jsp?id=" + idConvenzione+ "\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\" ><img src=../image/del.png width=18 ></a>");
							if ((ConvenzioniEnte.get(z).getAnno() == anno)){
								out.print("&nbsp;&nbsp;&nbsp;&nbsp;");
								out.print("<a href=\"../convenzioni/stampa_moduli.jsp?print=accordo&idConvenzione=" + idConvenzione+ "\"><img src=../image/icona_pdf.jpg width=25 ></a></li>");	
								out.print("<ul>");
								out.print("<div class=\"editmode\">");
								
								out.print("<li>123Verbale <a class=\"editmode\" href=\"../convenzioni/verbale.jsp?id="+ idEnte +"&idconv="+idConvenzione+"\"><img src=../image/add.png width=18 ></a></div></li>");
							if(ConvenzioniEnte.get(z).getTipologia().equals("banco") && !DAO_documento.hasVerbaleRegistro(idConvenzione)){
								out.print("<div class=\"hideineditmode\"><li>Associa Verbale (All. 4)  <a  href=\"../convenzioni/verbale.jsp?id="+ idEnte +"&idconv="+idConvenzione+"\"><img src=../image/add.png width=18 ></a></li></div>");
							}
							if(ConvenzioniEnte.get(z).getTipologia().equals("agea") && (!DAO_documento.hasVerbaleRegistro(idConvenzione) || !DAO_documento.hasVerbaleFascicoli(idConvenzione))){
								out.print("<div class=\"hideineditmode\"><li>Associa Verbale (All. 4/5)  <a  href=\"../convenzioni/verbale.jsp?id="+ idEnte +"&idconv="+idConvenzione+"\"><img src=../image/add.png width=18 ></a></div></li></div>");
							}
							
							}
							else
								out.print("</li>");
							
							if(documenti.size()>0){
								
								for(int t=0;t<documenti.size();t++){
									out.print(" <li> "+documenti.get(t).getTipoDocumento());
									out.print("<a class=\"editmode\" href=\"../convenzioni/Elimina_Documento.jsp?id=" + documenti.get(t).getIdDocumento()+ "\" onClick=\"return confirm('Sei sicuro di voler eliminare?');\" ><img src=../image/del.png width=18 ></a>");
									out.print("<a href=\"../convenzioni/stampa_moduli.jsp?print="+documenti.get(t).getTipoDocumento()+"&idConvenzione=" + idConvenzione+ "\"><img src=../image/icona_pdf.jpg width=25 ></a></li>");	
									
									out.print("</li>");
								}
								
							}
							out.print("</ul>");
							
							
							out.print("<br>");
						}
						
						
						out.print("</ul><br><br>");
					}
					else
						out.println("<br>Nessuna convenzione in corso");
					 
							%>
				
				</ul>
				
				</td>
				</tr>
				
				<tr>
				
				<%-- <td colspan="3"><br>
				<b>DOCUMENTI</b>
				<% 
				out.println("<a  href=\"../fileUpload/upload_file.jsp?idEnte=" + e.getIdEnte()+ "\"><img src=../image/add.png width=18 ></a>");
				out.println("<a  href=\"../fileUpload/visualizza_file.jsp?idEnte=" + e.getIdEnte()+ "\"><img src=../image/lente.png width=20 ></a>");

				%>
				</td> --%>
				
				</tr>
				
				
				<tr>
				<td colspan="3"><br>
				<%
				if(ConvenzioniEnte!=null){
					for (int z=0; z<ConvenzioniEnte.size();z++){
						out.print("<br>");
						out.println("Convenzione n. "+ ConvenzioniEnte.get(z).getIdConvenzione()+" Data firma "+ConvenzioniEnte.get(z).getDataFirma());
						out.print("<br>");
						out.println("<b> Anno " + ConvenzioniEnte.get(z).getAnno()+"</b> Tipologia convenzione:  "+ConvenzioniEnte.get(z).getTipologia());
						
						
						
						}
						out.print("<br>");
					 }
					
				
				out.println("");
				%>
				</td>
				</tr>

				
				</table>
				
			<%}
				
				%>	
					
					
					
				
						
	
						
		</div>
		<!-- chiusura del div page-->

		<div id="footer">
			<%@ include file="../component/footer.jspf"%>
		</div>
	</div>
	<!-- chiusura del div container-->

</body>
</html>