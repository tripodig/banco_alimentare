<%@ page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@page import="java.lang.*"%>
<jsp:useBean id="cate" scope="page" class="beans.CatalogoEnti" />
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede" />
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti" />
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona" />
<jsp:useBean id="catc" scope="page" class="beans.DAO_Convenzione2017" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<%@ include file="../component/title.jspf" %>




 <link href="../css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript" language="javascript" src="../js/jquery-1.11.3.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
 <script src="../js/column.js" type="text/javascript"></script>
 <!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script> -->
<!-- <script language="javascript" type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> -->

<script>

$(document).ready(function(){
	     $('#datiEnti').dataTable({"paging":false, "info":false,"language": {
	            "zeroRecords": "Nessun ente corrispondente alla ricerca",
	            "infoEmpty": "Nessun ente disponibile"} });
	});
</script>



</head>
<body>
	<div id="top">
		<%@ include file="../component/top.jspf"%>
	</div>


	<div id="container">
		<div id="header">
			<%@ include file="../component/header.jspf"%>
		</div>
		<div id="page">

			<%
				String provincia="";
				int anno=0;
				if(session.getAttribute("loggato") != null){
				
				UserBean user=new UserBean();
				user=(UserBean) session.getAttribute("currentSessionUser");
				provincia=user.getProvinciaConvenzioni();
				anno=user.getAnnoConvenzioni();
			}
				
				Ente e = new Ente();
				Sede s = new Sede();
				//out.println(s.toString());
				Vector<Ente> VE = new Vector<Ente>();
				Vector<ConvenzioneFrom2017> VC = new Vector<ConvenzioneFrom2017>();
				VE = cate.getEnti();
				VC =catc.getConvenzioniAnno(anno);
%>
			
				
<h1> Tutti gli Enti </h1>
<br><br>
				
				
			<table id="datiEnti" class="display" cellspacing="0" align="center">
				<thead>
				<tr>
					<th>Codice Struttura</th>
					<th>Codice SAP</th>
					<th>Denominazione struttura e recapiti</th>
					<th>Convenzione  <%out.print(anno); %></th>
				</tr>
				</thead>
			

				<tbody>
				<%
				Vector<Sede> VS =new Vector<Sede>();
				VS=cats.getSedi();
				Vector<Persona> VP =new Vector<Persona>();
				VP=catp.getPersone();
				
				for (int i = 0; i < VE.size(); i++) {
					int idEnte = VE.get(i).getIdEnte();
					int codSap=VE.get(i).getCodiceSap();
					String nome = VE.get(i).getNome();
					int codStruttura=VE.get(i).getCodiceStruttura();
					
					out.println("<tr><td><center><a style=text-decoration:none href=\"Scheda_Ente.jsp?id=" + idEnte +"\">"+codStruttura+"</a></center></td>");
					out.println("<td><center><a style=text-decoration:none href=\"Scheda_Ente.jsp?id=" + idEnte +"\">"+codSap+"</a></center></td>");
					out.println("<td>&nbsp&nbsp<b><a style=text-decoration:none href=\"Scheda_Ente.jsp?id=" + idEnte+ "\">"+nome.toUpperCase()+"</a></b>");
					out.println("<br><font size=\"2\"");
					if(VS!=null)
						for(int z=0;z<VS.size();z++)
							if(VS.get(z).getIdEnte()==idEnte)
								out.print("<br>&nbsp&nbsp&nbsp&nbsp&nbsp<b>"+VS.get(z).getTipologia()+"</b> "+VS.get(z).getIndirizzo()+" "+VS.get(z).getComune()+" "+VS.get(z).getProvincia() );
					if(VP!=null)
						for(int p=0;p<VP.size();p++){
							if(VP.get(p).getIdEnte()==idEnte){
								out.print("<br>&nbsp&nbsp&nbsp&nbsp&nbsp<b>"+VP.get(p).getTitoloReferenza()+"</b> "+VP.get(p).getNome()+" "+VP.get(p).getCognome());
								//mostro telefono solo se c'�
								if(!(VP.get(p).getTelefono().equals("") || VP.get(p).getTelefono().equals("---")))
											out.print(" "+VP.get(p).getTelefono());
								//mostro cellulare solo se c'�
								if(!(VP.get(p).getCellulare().equals("") || VP.get(p).getCellulare().equals("---"))){
									out.print(" "+VP.get(p).getCellulare());
									//mostro mail solo se c'�
								if(!(VP.get(p).getEmail().equals("") || VP.get(p).getEmail().equals("---"))){
									out.print(" "+VP.get(p).getEmail() );
							}
								
						}
					}}	
					out.print("</font>");
					out.println("</td>");
					
					out.println("<td>");
					for(int c=0;c<VC.size();c++){
						if(VC.get(c).getIdEnte()==idEnte){
							
							if(VC.get(c).getTipologia().equals("agea"))
								out.print("<img src=../image/green-a-md.png width=30 >");
							
							if(VC.get(c).getTipologia().equals("banco"))
								out.print("<img src=../image/green-b-hi.png width=30 > ");
							
						}
					}
					out.println("</td>");
					
					
					
					out.println("</tr>");
				}
				%>
				</tbody>

			</table>




		</div>
		<!-- chiusura del div page-->

		<div id="footer">
			<%@ include file="../component/footer.jspf"%>
		</div>
	</div>
	<!-- chiusura del div container-->

</body>
</html>