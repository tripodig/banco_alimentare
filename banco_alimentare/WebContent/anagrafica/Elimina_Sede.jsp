<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede" />
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
	<%
String id=request.getParameter("id");
int idSede=Integer.parseInt(id);
Sede e= cats.getSede(idSede);
boolean  esito= false;
if(cats.isCancellable(idSede))
	esito=cats.eliminaSede(e);
else
	cats.disableSede(e);

String jspPage;
if (esito){
	System.out.println("Operazione eseguita correttamente!");
	String commit="Operazione eseguita correttamente!";    		 
	request.setAttribute("commit", commit);
	jspPage="commit.jsp";
}
else{
   System.out.println("!!! Operazione non eseguita !!!");
   String errore="Operazione non eseguita correttamente!<br> Questa sede � coinvolta in una convenzione";    		 
   request.setAttribute("errore", errore);
   jspPage="error.jsp";
}
request.getRequestDispatcher( jspPage ).forward(request,response);




%>
</body>
</html>