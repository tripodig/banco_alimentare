<%@ page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%>
<%@ page import="util.*"%>
<%@page import="java.lang.*"%> 
<jsp:useBean id="cate" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede"/>  
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">




<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
		<%@ include file="../component/header.jspf" %>
	</div>
	<div id="page" > 
Da questa pagina � possibile esportare i dati su file
<table width=100%>
<tr>

<td>
<form>
Seleziona dati da esportare:
<select name="scelta">


<-- campi in base all'area dell'utente -->
<option>Totali_Colletta</option>
<option>Elenco_Volontari</option>
<option>Elenco_Enti</option>
<option>Elenco_Punti_Vendita</option>
<option>Elenco_Convenzioni_XLS</option>
<option>Elenco_Convenzioni_AGEA_XLS</option>
<option>Elenco_Convenzioni_AGEA_SIAN_XLS</option>
<option>Elenco_Convenzioni_BANCO_XLS</option>



</select>
<input type="submit" value="Scegli">
</form>
</td>

<td><center><img src="../image/export.jpg"  height="150"></center></td>

</tr>
</table>
 	

 	
<br>
<%
String nomefile="";
String path=request.getSession().getServletContext().getRealPath("/");// restituisco cos� il path assoluto,rispetto al mio progetto

export exp=new export();
path=path+"datawarehouse";
exp.setdirectorySalvataggio(path);

String scelta=request.getParameter("scelta");
GregorianCalendar d=new GregorianCalendar();
int anno=d.get(Calendar.YEAR);
int annoc=anno;
String evento="";
int idEvento=0;
if(scelta != null){
	
	String area="";
	
	if (session.getAttribute("currentSessionUser") != null) {

		UserBean user = (UserBean) session
				.getAttribute("currentSessionUser");
		
		area=user.getArea();
		annoc=user.getAnnoConvenzioni();
		Evento e=new Evento();
		e=(Evento) session.getAttribute("currentSessionEvent");
		evento=e.getNome();
		idEvento=e.getIdEvento();
		
		
		

	}else{
		
		//non deve essere possibile effettuare l'export
	}


System.out.println("anno dentro la select"+anno);
if(scelta.equals("Totali_Colletta")){
//exp.sommaModB();
//exp.sommaModBByArea(area);
exp.sommaModBByArea(area, idEvento, evento);

nomefile="Totali_Colletta_"+evento+".csv";
} 

if(scelta.equals("Elenco_Volontari")){
exp.volontari();

nomefile="Elenco_Volontari.csv";
}

if(scelta.equals("Elenco_Enti")){
exp.enti();

nomefile="Elenco_Enti.xls";
}


if(scelta.equals("Elenco_Punti_Vendita")){
exp.puntiVendita();

nomefile="Elenco_Punti_Vendita.csv";
}

if(scelta.equals("Elenco_Convenzioni_XLS")){
	
exp.convenzioni(annoc);

nomefile="Elenco_Convenzioni_"+annoc+".xls";
} 

if(scelta.equals("Elenco_Convenzioni_AGEA_XLS")){
	
exp.convenzioniAGEA(annoc);

nomefile="Elenco_Convenzioni_AGEA_"+annoc+".xls";
}

if(scelta.equals("Elenco_Convenzioni_AGEA_SIAN_XLS")){
	
exp.convenzioniAGEAxSIAN(annoc);

nomefile="Elenco_Convenzioni_AGEA_SIAN_"+annoc+".xls";
}

if(scelta.equals("Elenco_Convenzioni_BANCO_XLS")){
	
exp.convenzioniBANCO(annoc);

nomefile="Elenco_Convenzioni_BANCO_"+annoc+".xls";
}

String pathdown=path+"/"+nomefile;
System.out.println(path);
%>
<a href="../datawarehouse/<%=nomefile%>">Scarica il file <%=nomefile%></a>
<%
}
%>



	
		
	</div><!-- chiusura del div page-->
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>
</div><!-- chiusura del div container-->

</body>
</html>