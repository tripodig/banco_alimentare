<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
		<%@ include file="../component/header.jspf" %>
	</div>
	<div id="page">
       
<center><h1>Spiacente - Si � verificato un errore.</h1>
<br>
<img src="../../banco_alimentare/image/Errore.png" height="250" width="250">
<br>
<%
	 
	 String errore=(String)request.getAttribute("errore");
	 
	 if(errore != null) {
		 
		 out.println("<font color=\"blue\">"+errore+"</font><br><br>");
		 
	 }
	
	 %>
	 </center>

<a href="javascript:history.back(1);"title="Torna indietro">Torna indietro</a>
 
	</div><!-- chiusura del div page-->
	<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>
</div><!-- chiusura del div container--> 
</body>
</html>