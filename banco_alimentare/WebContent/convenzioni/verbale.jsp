<%@page import="beans.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede"/>
<jsp:useBean id="catconv" scope="page" class="beans.DAO_Convenzione2017"/>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ include file="../component/title.jspf" %>
<script>
function view_verbale_fascicoli()
{
//visualizza il modello A
document.getElementById('verbale_registro').style.display="none";
document.getElementById('verbale_fascicoli').style.display="block";
document.getElementById('verbale_registro_title').style.display="none";
document.getElementById('verbale_fascicoli_title').style.display="block";


}

function view_verbale_registro()
{
//visualizza la riga dei totali
document.getElementById('verbale_fascicoli').style.display="none";
document.getElementById('verbale_registro').style.display="block";
document.getElementById('verbale_fascicoli_title').style.display="none";
document.getElementById('verbale_registro_title').style.display="block";
}
</script>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>
<div id="container">
<div id="header">
	 
		<%@ include file="../component/header.jspf" %>

	</div>
<div id="page">
<%		GregorianCalendar d=new GregorianCalendar();
				int anno2=d.get(Calendar.YEAR);
				int mese=d.get(Calendar.MONTH)+1;
				int giorno=d.get(Calendar.DAY_OF_MONTH);
				String data=giorno+"/"+mese+"/"+anno2; 
				int anno=0;
				UserBean user=null;
 if(session.getAttribute("currentSessionUser")!=null){
	
	user=(UserBean) session.getAttribute("currentSessionUser");
	anno=user.getAnnoConvenzioni();
}	 	
				
String idEntes=request.getParameter("id");
int idEnte=Integer.parseInt(idEntes);
String idConvs=request.getParameter("idconv");
int idConvenzione=Integer.parseInt(idConvs);

String tipoConvenzione=catconv.getConvenzione(idConvenzione).getTipologia();
if(tipoConvenzione.equals("banco"))
{
	}
/* int idEnte=180904;
int idConvenzione=1023; */
Ente e=catente.getEnte(idEnte);
Persona p=catp.getPersonaRapplegale(idEnte);		
int CodiceStruttura=e.getCodiceStruttura();

String nome=e.getNome();				
String PartitaIva=e.getPartitaIva();				
				
				%> 
<br>	<br>
<% 
if(!tipoConvenzione.equals("banco"))
{
%>
	verbale_fascicoli <input type="radio" name="verbale" value="verbale_fascicoli"  onchange="view_verbale_fascicoli()"/>
<%	
}
%>

verbale_registro  <input type="radio" name="verbale" value="verbale_registro" checked="checked" onchange="view_verbale_registro()" />
<br><br><br>	
<div id="verbale_fascicoli_title" style="display:none">	
<center><b>VERBALE DI VERIFICA DEI FASCICOLI DELLE PERSONE E/O DEI NUCLEI FAMILIARI ASSISTITE/I DALLA ORGANIZZAZIONE PARTNER TERRITORIALE (OpT)</b><br><br></center>
</div>

<div id="verbale_registro_title" style="display:block">	
<center><b>VERBALE DI VERIFICA DELLA CONTABILITA’ DI MAGAZZINO DELLA ORGANIZZAZIONE PARTNER TERRITORIALE (OpT)</b><br><br></center>
</div>

<b>DATI ENTE:</b><br>

 
<p>
<%

out.println("Codice Struttura: "+CodiceStruttura+"<br>");
out.println("Denominazione: <b>"+nome+" </b><br>");
out.println("Codice Fiscale: "+PartitaIva);
out.println("<br>");



/* out.println("<br><b>Rappresentante legale</b><br>");
out.print(p.toStringline()); */

Vector<Sede> VS=new Vector<Sede>();


VS=cats.getSediEnte(idEnte);
Persona rapplegale=catp.getLastRappLegaleEnte(idEnte);
Sede sedeLegale=cats.getSedeLegale(idEnte);

out.println("<br><br>");



 out.print(rapplegale.toStringTable());
 out.println("<input type=\"hidden\" name=\"idRappLegaleEnte\" value="+rapplegale.getIdPersona()+">");
 out.println("<br><br>");
%> 
 <%out.print(sedeLegale.toStringTable()); %>


</p>
 <div id="verbale_fascicoli" style="display:none">
 <form name="totali" method="post" action="../New_verbale">
 <input type="hidden" name="idEnte" value="<% out.print(idEnte);%>">
 <input type="hidden" name="idConvenzione" value="<% out.print(idConvenzione);%>"> 
<input type="hidden" name="tipoDocumento" value="verbale_fascicoli">
         In Data <input type="text" name="data" style="width:90px" value="<%out.print(data);%>">il Sig. <input type="text" name="incaricato" style="width:120px" > 
<br>
incaricato dell’Ente caritativo Associazione di Volontariato Banco Alimentare della Calabria Onlus ha accertato quanto segue:

<br>
<br>
<table class="display" cellspacing="0" align="center">
<tr>
	<td>
A
	</td>
	<td>
PRESENZA DELL’ELENCO CARTACEO O INFORMATICO DELLE PERSONE E/O <br>DEI NUCLEI FAMILIARI ASSISTITI IN MANIERA CONTINUATIVA
	</td>
	<td>
<input type="radio" name="presenzaElenco" value="si">Si&nbsp<input type="radio" name="presenzaElenco" value="no">No
	</td>
</tr>
<tr>
	<td>
A.1)
	</td>
	<td>
Quante sono le persone singole assistite
	</td>
	<td>
<input type="number" name="numPersone" style="width:50px">
	</td>
</tr>
<tr>
	<td>
A.2)
	</td>
	<td>
Quante sono i nuclei familiari assistiti
	</td>
	<td>
<input type="number" name="numNuclei" style="width:50px">
	</td>
</tr>
<tr>

	<td>
B
	</td>
	<td>
PRESENZA DEI FASCICOLI
	</td>
	<td>
<input type="radio" name="presenzaFascicoli" value="si">Si&nbsp<input type="radio" name="presenzaFascicoli" value="no">No
	</td>
</tr>
<tr>
	<td>
B.1)
	</td>
	<td>
Quanti sono i fascicoli presenti
	</td>
	<td>
<input type="number" name="numFascicoli" style="width:50px">
	</td>
</tr>
<tr>
	<td>
B.2)
	</td>
	<td>
I fascicoli sono carenti di documentazione
	</td>
	<td>
<input type="radio" name="fascicoliCarenti" value="si">Si&nbsp<input type="radio" name="fascicoliCarenti" value="no">No

	</td>
</tr>
<tr>
	<td>

	</td>
	<td>
(in caso di risposta affermativa)<br>
i fascicoli esaminati sono (almeno il 10% del totale):
	</td>
	<td>
<input type="number" name="numFascicoliEsaminati" style="width:50px">
	</td>
	</tr>
<tr>
<td>

	</td>
		<td>

i fascicoli carenti di documentazione sono:
	</td>
	<td>
<input type="number" name="numFascicoliCarenti" style="width:50px">
	</td>
</tr>

</table>
	
<br><br>
Eventuali annotazioni:
<input type="text" name="annotazioni" style="width:650px; height: 100px;">
         <br><br>
				
				
				
				
				<br>
 				<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
         </form>
</div>

<!--  altro form-->
<div id="verbale_registro" style="display:block">
 <form name="totali" method="post" action="../New_verbale">
 <input type="hidden" name="idEnte" value="<% out.print(idEnte);%>">
 <input type="hidden" name="idConvenzione" value="<% out.print(idConvenzione);%>"> 
<input type="hidden" name="tipoDocumento" value="verbale_registro">
In Data <input type="text" name="data" style="width:90px" value="<%out.print(data);%>">il Sig. <input type="text" name="incaricato" style="width:120px" > 
<br>
incaricato dell’Ente caritativo Associazione di Volontariato Banco Alimentare della Calabria Onlus ha accertato quanto segue:

<br>
<br>

<table class="display" cellspacing="0" align="center">
<tr>
	<td>
A
	</td>
	<td>
PRESENZA DEL/I REGISTRO/I DI CARICO
	</td>
	<td>
<input type="radio" name="presenzaRegistro" value="si">Si&nbsp<input type="radio" name="presenzaRegistro" value="no">No
	</td>
</tr>
<tr>
	<td>
A.1)
	</td>
	<td>
Il/i registro/i  è/sono informatizzati
	</td>
	<td>
<input type="radio" name="registroInformatizzato" value="si">Si&nbsp<input type="radio" name="registroInformatizzato" value="no">No
	</td>
</tr>
<tr>
	<td>
A.2)
	</td>
	<td>
Il/i registro/i  è/sono costituiti da fogli inamovibili
	</td>
	<td>
<input type="radio" name="fogliInamovibili" value="si">Si&nbsp<input type="radio" name="fogliInamovibili" value="no">No
	</td>
</tr>
<tr>
	<td>
A.3)
	</td>
	<td>
Il registro/i  è/sono carenti di informazioni o recanti informazioni errate
	</td>
	<td>
<input type="radio" name="registroErrato" value="si">Si&nbsp<input type="radio" name="registroErrato" value="no">No
	</td>
</tr>
<tr>
	<td>
A.4)
	</td>
	<td>
Il registro/i  è/sono numerato, timbrato e firmato in ogni sua pagina dal legale
rappresentante

	</td>
	<td>
<input type="radio" name="registroFirmato" value="si">Si&nbsp<input type="radio" name="registroFirmato" value="no">No
	</td>
</tr>
<tr>

	<td>
B
	</td>
	<td>
PRESENZA DELLA DICHIARAZIONE DI CONSEGNA
	</td>
	<td>
<input type="radio" name="presenzaDichiarazione" value="si">Si&nbsp<input type="radio" name="presenzaDichiarazione" value="no">No
	</td>
</tr>
<tr>
	<td>
B.1)
	</td>
	<td>
le Dichiarazioni di consegna sono prive del numero e/o della quantità e/o del genere<br> di prodotto assegnato e/o della firma del legale rappresentante
	</td>
	<td>
<input type="radio" name="registroSenzaFirma" value="si">Si&nbsp<input type="radio" name="registroSenzaFirma"" value="no">No
	</td>
</tr>
<tr>
	<td>
B.2)
	</td>
	<td>
le Dichiarazioni di consegna sono prive del numero e/o della data
	</td>
	<td>
<input type="radio" name="registroSenzaNumData" value="si">Si&nbsp<input type="radio" name="registroSenzaNumData"" value="no">No

	</td>
</tr>
<tr>
	<td>
C
	</td>
	<td>
PRESENZA DEGLI ATTESTATI  DI CONSEGNA RICEVUTI DALL’ENTE
	</td>
	<td>
<input type="radio" name="presenzaAttestati" value="si">Si&nbsp<input type="radio" name="presenzaAttestati"" value="no">No
	</td>
</tr>

</table>
	
<br><br>
Eventuali annotazioni:
<input type="text" name="annotazioni" style="width:650px; height: 100px;">
         <br><br>
				
				
				
				
				<br>
 				<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
         </form>
</div>



</div>
<div id="footer">
  		<%@ include file="../component/footer.jspf" %>
	</div>
</div>

</body>
</html>