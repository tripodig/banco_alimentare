<%@page import="beans.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede"/>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ include file="../component/title.jspf" %>

<script type="text/javascript" language="javascript"><!--
		function Show(ctrl){
			if(ctrl.checked){
				document.getElementById('altrodiv').innerHTML = '<input type="text" name="specificaAltro" style="width:600px"/>';
      		}
     		else{
  				document.getElementById('altrodiv').innerHTML = '';
      		} 
		}

		function Nascondi(){
			document.getElementById('altrodiv').innerHTML = ''; 
	   	}
		
		
		function sommaTotAssContM (){
	
	
	var ind_1 = parseInt (document.totali.M015Anni.value);
	var ind_2 = parseInt (document.totali.M1664Anni.value);
	var ind_3 = parseInt (document.totali.Mover65Anni.value);
	var ind_4 = parseFloat (document.totali.AssSaltM.value);
	

	
	if (isNaN (ind_1)){
		ind_1 = 0;
		  document.totali.M015Anni.value = "";
	}
	if (isNaN (ind_2)){
		ind_2 = 0;
		  document.totali.M1664Anni.value = "";
	}
	if (isNaN (ind_3)){
		ind_3 = 0;
		  document.totali.Mover65Anni.value = "";
	}
	if (isNaN (ind_4)){
		ind_4 = 0;
		  document.totali.AssSaltM.value = "";
	}
	
	var tot= ind_1+ind_2+ind_3;
	
    document.totali.TotAssContM.value = tot;
    
	var tot2=tot+ind_4;
	
    document.totali.TotIngM.value = tot2;

   
}
		
		function sommaTotAssContE (){
			
			
			var ind_1 = parseInt (document.totali.E015Anni.value);
			var ind_2 = parseInt (document.totali.E1664Anni.value);
			var ind_3 = parseInt (document.totali.Eover65Anni.value);
			

			
			if (isNaN (ind_1)){
				ind_1 = 0;
				  document.totali.E015Anni.value = "";
			}
			if (isNaN (ind_2)){
				ind_2 = 0;
				  document.totali.E1664Anni.value = "";
			}
			if (isNaN (ind_3)){
				ind_3 = 0;
				  document.totali.Eover65Anni.value = "";
			}
						
			var tot= ind_1+ind_2+ind_3;
			
		    document.totali.TotAssContE.value = tot;
		    document.totali.TotIngE.value = tot;

		   
		}
		function sommaTotAssContD (){
			
			
			var ind_1 = parseInt (document.totali.D015Anni.value);
			var ind_2 = parseInt (document.totali.D1664Anni.value);
			var ind_3 = parseInt (document.totali.Dover65Anni.value);
			

			
			if (isNaN (ind_1)){
				ind_1 = 0;
				  document.totali.D015Anni.value = "";
			}
			if (isNaN (ind_2)){
				ind_2 = 0;
				  document.totali.D1664Anni.value = "";
			}
			if (isNaN (ind_3)){
				ind_3 = 0;
				  document.totali.Dover65Anni.value = "";
			}
						
			var tot= ind_1+ind_2+ind_3;
			
		    document.totali.TotAssContD.value = tot;
		    document.totali.TotIngD.value = tot;

		   
		}


	function sommaTotAssContP (){
	
	var ind_1 = parseFloat (document.totali.P015Anni.value);
	var ind_2 = parseFloat (document.totali.P1664Anni.value);
	var ind_3 = parseFloat (document.totali.Pover65Anni.value);
	var ind_4 = parseFloat (document.totali.AssSaltP.value);
	
	
	if (isNaN (ind_1)){
		ind_1 = 0.00;
		  document.totali.P015Anni.value = "";
	}
	if (isNaN (ind_2)){
		ind_2 = 0.00;
		  document.totali.P1664Anni.value = "";
	}
	if (isNaN (ind_3)){
		ind_3 = 0.00;
		  document.totali.Pover65Anni.value = "";
	}
	
	if (isNaN (ind_4)){
		ind_4 = 0;
		  document.totali.AssSaltP.value = "";
	}
	
	var tot= ((ind_1+ind_2+ind_3)*100/100);
	
    document.totali.TotAssContP.value = tot;
    
    var tot2= tot+ind_4;
    
    document.totali.TotIngP.value = tot2;
   
}


function sommaTotS (){
	
	var ind_2 = parseFloat (document.totali.AssSaltU.value);
	
	
	if (isNaN (ind_2)){
		ind_2 = 0.00;
		  document.totali.AssSaltU.value = "";
	}
	
	var tot= ind_2;
	
    document.totali.TotIngU.value = tot;
   
}

	</script>	
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>
<div id="container">
<div id="header">
	 
		<%@ include file="../component/header.jspf" %>

	</div>
<div id="page">
<%		GregorianCalendar d=new GregorianCalendar();
				int anno2=d.get(Calendar.YEAR);
				int mese=d.get(Calendar.MONTH)+1;
				int giorno=d.get(Calendar.DAY_OF_MONTH);
				String data=giorno+"/"+mese+"/"+anno2; 
				int anno=0;
if(session.getAttribute("currentSessionUser")!=null){
	
	UserBean user=(UserBean) session.getAttribute("currentSessionUser");
	anno=user.getAnnoConvenzioni();
}				
				
String idEntes=request.getParameter("id");
int idEnte=Integer.parseInt(idEntes);
Ente e=catente.getEnte(idEnte);
Persona p=catp.getPersonaRapplegale(idEnte);		
int CodiceStruttura=e.getCodiceStruttura();
String nome=e.getNome();				
String PartitaIva=e.getPartitaIva();				
				
				%>
				
<center><b>DOMANDA DI AFFILIAZIONE</b><br><br></center>



 <form name="totali" method="post" action="../ConvenzioneServlet">
 <input type="hidden" name="idEnte" value="<% out.print(idEnte);%>">

<b>DATI ENTE:</b>

 
<p>
<%

out.println("Codice Struttura: "+CodiceStruttura+"<br>");
out.println("Denominazione: <b>"+nome+" </b><br>");
out.println("Codice Fiscale: "+PartitaIva);
out.println("<br>");



/* out.println("<br><b>Rappresentante legale</b><br>");
out.print(p.toStringline()); */

Vector<Sede> VS=new Vector<Sede>();


VS=cats.getSediEnte(idEnte);
Persona rapplegale=catp.getLastRappLegaleEnte(idEnte);
Sede sedeLegale=cats.getSedeLegale(idEnte);

out.println("<br><br>");



 out.print(rapplegale.toStringTable());
 out.println("<input type=\"hidden\" name=\"idRappLegaleEnte\" value="+rapplegale.getIdPersona()+">");
 out.println("<br><br>");
 
 
//select per sede Legale
out.println("<b>Sede Legale </b><br><select name=\"idSedeLegaleEnte\" style=\"width:950px\" >");

for(int j=0;j<VS.size();j++){
out.println("<option value="+VS.get(j).getIdSede()+"><b>"+VS.get(j).toStringTable()+"</option>");
}
out.println("</select><br><br>");
//select per sede Operativa
out.println("<b>Sede Operativa </b><br><select name=\"idSedeOperativaEnte\"  style=\"width:950px\">");
out.println("<option value=\"0\">La stessa della sede legale</option>");

for(int j=0;j<VS.size();j++){
out.println("<option value="+VS.get(j).getIdSede()+"><b>"+VS.get(j).toStringTable()+"</option>");
}
out.println("</select><br><br>");

//select per sede  Magazzino
out.println("<b>Sede Magazzino</b> <br><select name=\"idSedeMagazzinoEnte\" style=\"width:950px\" >");
out.println("<option value=\"0\">La stessa della sede legale</option>");

for (int i=0;i<VS.size();i++){
	out.println("<option value="+VS.get(i).getIdSede()+"><b>"+VS.get(i).toStringTable()+"</option>");
}
out.println("</select><br><br>");




%>



</p>
 
         <table cellpadding="15" >
         <tr>		
					
					<tr><td>Presenza di <b>struttura frigorifera</b> (*)
					
					
<input type="radio" name="frigo" value="si">Si&nbsp<input type="radio" name="frigo" value="no">No

					</td></tr>
					
					<tr><td>
					
					Chiede di poter fruire del servizio di distribuzione dei prodotti alimentari dall’Organismo Pagatore AGEA a favore degli indigenti, ai sensi delle Istruzioni operative n. 38 del 04-09-2017 e, consapevole che chiunque rilasci dichiarazione mendaci è punito ai sensi del codice penale e delle leggi speciali in materia, ai sensi e per gli effetti degli artt. 75 e 76 del D.P.R. n. 445/2000, allo scopo<br></br>
					<div align="center"><b>DICHIARA</b></div><br>
					1. che l'Organizzazione partner, è un:<br>
					<input type="radio" name="partner" value="pubblico">Organismo pubblico
					<br>
					<input type="radio" name="partner" value="no_lucro">Organizzazione senza scopo di lucro
					
					</td></tr>					
				<tr>
				<td>
				
				2. che svolge una attività di sostentamento alimentare a persone bisognose tramite una o più delle seguenti "Attività":
				<br>
				<table border="1" style="border-spacing: 0px; padding: 0px">
				<tr>
				<td></td>
				
                <td align="center">
				
				<i>Assistiti <br>Età =<15 anni<br>(a)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti <br>Età 16-64 anni<br>(b)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti <br>=>65 anni<br>(c)</i>
				</td>
                
                <td align="center">
				
				<i>Totale assistiti continuativi<br>(d=a+b+c)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti saltuari<br>(e) max 40% di f</i>
				</td>
                
                
                
                 <td align="center">
				
				<i>Totale assistiti<br>f=(d+e)</i>
				</td>
				
				 <td align="center">
				
				<i>Di cui donne</i>
				</td>
				 <td align="center">
				
				<i>Di cui migranti, minoranze</i>
				</td>
				<td align="center">
				
				<i>Di cui disabili</i>
				</td>
				<td align="center">
				
				<i>Di cui senza fissa dimora</i>
				</td>
				
				
				</tr>
				
				<tr>
				 <td align="center">
				<b><i>MENSA</i></b>
				
                
				</td>
                <td align="center"><input type="number" id="M015Anni" name="M015Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px"></td>
                <td align="center"><input type="number" id="M1664Anni" name="M1664Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px"></td>
                <td align="center"><input type="number" id="Mover65Anni" name="Mover65Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px"></td>
                
                <td align="center"><input type="number" id="TotAssContM" name="TotAssContM" style="width:50px" readonly="readonly"></td>
                
                <td align="center"><input type="number" id="AssSaltM" name="AssSaltM" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()"  style="width:50px"></td>
				
                <td align="center"><input type="number" name="TotIngM" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="MDonne" required style="width:50px"></td>
				<td align="center" ><input type="number" name="MmigrantiMinoranze" required style="width:50px"></td>
				<td align="center" ><input type="number" name="Mdisabili" required style="width:50px"></td>
				<td align="center" ><input type="number" name="MsenzaFissaDimora" required style="width:50px"></td>
				</tr>
				
				<tr>
				<td align="center">
				<b><i>DISTRIBUZIONE PACCHI</i></b>
                
				</td>
                
                
                <td align="center"><input type="number" id="P015Anni" name="P015Anni" required onKeyUp="sommaTotAssContP()" onclick="sommaTotAssContP()" style="width:50px"></td>
                <td align="center"><input type="number" id="P1664Anni" name="P1664Anni" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px"></td>
                <td align="center"><input type="number" id="Pover65Anni" name="Pover65Anni" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px"></td>
                <td align="center"><input type="number" id="TotAssContP" name="TotAssContP" style="width:50px" readonly="readonly"></td>
                
                <td align="center"><input type="number" id="AssSaltP" name="AssSaltP" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px"></td>
                
				<td align="center"><input type="number" name="TotIngP" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="PDonne" required style="width:50px"></td>
				<td align="center" ><input type="number" name="PmigrantiMinoranze" required style="width:50px"></td>
				<td align="center" ><input type="number" name="Pdisabili" required style="width:50px"></td>
				<td align="center" ><input type="number" name="PsenzaFissaDimora" required style="width:50px"></td>
				</tr>
				<tr>
				 <td align="center">
				<b><i>EMPORIO SOCIALE</i></b> 
				</td>


                
                <td align="center"><input type="number" id="E015Anni" name="E015Anni" required onKeyUp="sommaTotAssContE()" onclick="sommaTotAssContE()" style="width:50px"></td>
                <td align="center"><input type="number" id="E1664Anni" name="E1664Anni" required onKeyUp="sommaTotAssContE()" onclick="sommaTotAssContE()" style="width:50px"></td>
                <td align="center"><input type="number" id="Eover65Anni" name="Eover65Anni" required onKeyUp="sommaTotAssContE()" onclick="sommaTotAssContE()"  style="width:50px"></td>
                <td align="center"><input type="number" id="TotAssContE" name="TotAssContE" style="width:50px" readonly="readonly"></td>
                <td align="center"><a style="width:50px">------</td>
				<td align="center"><input type="number" name="TotIngE" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="EDonne" required style="width:50px"></td>
				<td align="center" ><input type="number" name="EmigrantiMinoranze" required style="width:50px"></td>
				<td align="center" ><input type="number" name="Edisabili" required style="width:50px"></td>
				<td align="center" ><input type="number" name="EsenzaFissaDimora" required style="width:50px"></td>
				</tr>
				<tr>
				 <td align="center">
				<b><i>DISTRIBUZIONE DOMICILIARE</i></b> 
				</td>


                
                <td align="center"><input type="number" id="D015Anni" name="D015Anni" required onKeyUp="sommaTotAssContD()" onclick="sommaTotAssContD()" style="width:50px"></td>
                <td align="center"><input type="number" id="D1664Anni" name="D1664Anni" required onKeyUp="sommaTotAssContD()" onclick="sommaTotAssContD()" style="width:50px"></td>
                <td align="center"><input type="number" id="Dover65Anni" name="Dover65Anni" required onKeyUp="sommaTotAssContD()" onclick="sommaTotAssContD()"  style="width:50px"></td>
                <td align="center"><input type="number" id="TotAssContD" name="TotAssContD" style="width:50px" readonly="readonly"></td>
                <td align="center">------</td>
				<td align="center"><input type="number" name="TotIngD" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="DDonne" required style="width:50px"></td>
				<td align="center" ><input type="number" name="DmigrantiMinoranze" required style="width:50px"></td>
				<td align="center" ><input type="number" name="Ddisabili" required style="width:50px"></td>
				<td align="center" ><input type="number" name="DsenzaFissaDimora" required style="width:50px"></td>
				</tr>
                
                <tr>
				<td align="center">
				<b><i>UNITA' DI STRADA</i></b>
				</td>
                
                
                <td align="center">------</td>
                <td align="center">------</td>
                <td align="center">------</td>
                <td align="center">------</td>
                
                <td align="center">------</td>
                
				<td align="center"><input type="number" name="TotIngU" style="width:50px"></td>
				<td align="center"><input type="number" name="UDonne" required style="width:50px"></td>
				<td align="center" ><input type="number" name="UmigrantiMinoranze" required style="width:50px"></td>
				<td align="center" ><input type="number" name="Udisabili" required style="width:50px"></td>
				<td align="center" ><input type="number" name="UsenzaFissaDimora" required style="width:50px"></td>
				</tr>
				</table>
				
				</td>
				
				</tr>	
                
                
				
                
				<tr><td>
				<br>
				
				3. che non percepisce finanziamenti pubblici<br><br>
				4. che nessuno degli assisititi è obbligato alla corresponsione o che, comunque,versa un corrispettivo o contributo;<br><br>
				5. che li assistiti inseriti nella presente domanda sono persone in condizioni di estremo disagio materiale;<br><br>
				6. che attua le seguenti "misure di accompagnamento" (barrare la/e casella/e di interesse):<br>
				
				<br>
				<table>
				<tr>
				<td>I.</td>
				<td><input type="checkbox" name="accoglienzaEAscolto" value="accoglienzaEAscolto"/> </td>
				<td><b>Accoglienza e ascolto:</b> attività di primo contatto per accogliere e valutare la domanda di aiuto/bisogno</td>
				</tr>
				<tr>
				<td>II.</td>
				<td><input type="checkbox" name="informazione" value="informazione"/> </td>
				<td><b>Informazione, consulenza e orientamento:</b> attività volte a orientare e facilitare l'accesso alla rete territoriale dei servizi, informazione sulle procedure e assistenza per pratiche amministrative.di primo contatto per accogliere e valutare la domanda di aiuto/bisogno</td>
				</tr>
				<tr>
				<td>III.</td>
				<td><input type="checkbox" name="accompagnamento" value="accompagnamento"/> </td>
				<td><b>Accompagnamento ai servizi:</b> sostegno all'accesso al sistema dei servizi locali e lavoro di rete con i servizi locali.</td>
				</tr>
				<tr>
				<td>IV.</td>
				<td><input type="checkbox" name="sostegnoPsicologico" value="sostegnoPsicologico"/> </td>
				<td><b>Sostegno psicologico</b> </td>
				</tr>
				<tr>
				<td>V.</td>
				<td><input type="checkbox" name="EducativaAlimentare" value="EducativaAlimentare"/> </td>
				<td><b>Educativa alimentare:</b> supporto allo sviluppo di comportamenti alimentari corretti e consapevoli.</td>
				</tr>
				<tr>
				<td>VI.</td>
				<td><input type="checkbox" name="consulenzaNellaGestioneDelBilancioFamiliare" value="consulenzaNellaGestioneDelBilancioFamiliare"/> </td>
				<td><b>Consulenza nella gestione del bilancio familiare:</b> supporto alla pianificazione e gestione delle spese.</td>
				</tr>
				<tr>
				<td>VII.</td>
				<td><input type="checkbox" name="sostegnoSscolastico" value="sostegnoSscolastico"/> </td>
				<td><b>Sostegno scolastico:</b> sostegno ai bambini e ragazzi nelle attività di studio.</td>
				</tr>
				<tr>
				<td>VIII.</td>
				<td><input type="checkbox" name="sostegnoEOrientamentoAllaRicercaDiLavoro" value="sostegnoEOrientamentoAllaRicercaDiLavoro"/> </td>
				<td><b>Sostegno e orientamento alla ricerca di lavoro:</b> assistenza nella compilazione di C.V. e delle domande di lavoro, preparazione ai colloqui, individuazine delle offerte di impiego, indirizzamento ai Centri per l'impiego.</td>
				</tr>
				<tr>
				<td>XI.</td>
				<td><input type="checkbox" name="primaAssistenzaMedica" value="primaAssistenzaMedica"/> </td>
				<td><b>Prima assistenza medica:</b> assistenza medica qualificata, distribuzione di farmac da parte di personale specializzato, servizi ambulatoriali</td>
				</tr>
				<tr>
				<td>X.</td>
				<td><input type="checkbox" name="tutelaLegale" value="tutelaLegale"/> </td>
				<td><b>Tutela legale:</b> consulenza legale per la tutela dei diritti di cittadinanza sociale</td>
				</tr>
				<tr>
				<td>XI.</td>
				<td><input type="checkbox" name="altro" value="altro" onclick="Show(this);"/> </td>
				<td><b>Altro:</b> Specificare <div id="altrodiv" align="left">...</div></td>
				</tr>
				</table>
				<br><br>
				<div align="center"><b>e si impegna</b></div><br>
			
				1. 	a ricevere i prodotti alimentari forniti dell’Organismo Pagatore AGEA esclusivamente dalla OpC: 
				Associazione Banco Alimentare della Calabria ONLUS  <br><br>
				2. 	ad utilizzare i prodotti alimentari che riceverà esclusivamente per l’assistenza in favore di  persone bisognose secondo le modalità fissate nelle Istruzioni operative n. 38  del 04-09-2017;
				<br><br>
				3. 	a provvedere al ritiro dei prodotti presso il magazzino della OpC entro i termini fissati, pena la perdita dell’assegnazione, ed alla loro distribuzione entro la data di scadenza indicata sulla confezione;
				<br><br>
				4. 	a non depositare e/o distribuire il prodotto assegnato in magazzini diversi da quelli suindicati; 
				<br><br>
				5. 	a tenere una contabilità di magazzino conforme alle Istruzioni operative n.38  del 04-09-2017 
				<br><br>
				6. 	Dichiaro di aver ricevuto Copia Registro Carico e Scarico e Copia Estratto Istruzioni Operative 38.
				<br><br>
				
				Si comunica che le Istruzioni Operative 38 con tutti gli allegati corrispondenti sono disponibili su Sito di AGEA a questo indirizzo: 
				<br><br>
				http://www.agea.gov.it/portal/page/portal/AGEAPageGroup/HomeAGEA/VisualizzaItem?iditem=54559523&idpage=6594156&indietro=Home
				<br><br>
				</table>
				
				
				
				Data <input type="text" name="Data" style="width:90px" value="<%out.print(data);%>">
				<br>
 				<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
         </form>

<%
	if ((request.getAttribute("js") == "0")
			|| (request.getAttribute("js") == null)) {
	} else
		out.println((String) request.getAttribute("js"));
%>
</div>
<div id="footer">
  		<%@ include file="../component/footer.jspf" %>
	</div>
</div>

</body>
</html>