<%@page import="beans.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="catp" scope="page" class="beans.CatalogoPersona"/>
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede"/>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ include file="../component/title.jspf" %>

<script type="text/javascript" language="javascript"><!--
		function Show(ctrl){
			if(ctrl.checked){
				document.getElementById('nuovo2').innerHTML = '<input type="text" name="specifica" style="width:200px"/>';
      		}
     		else{
  				document.getElementById('nuovo2').innerHTML = '';
      		} 
		}

		function Nascondi(){
			document.getElementById('nuovo2').innerHTML = ''; 
	   	}
		
		
		function sommaTotAssContM (){
	
	
	var ind_1 = parseInt (document.totali.M05Anni.value);
	var ind_2 = parseInt (document.totali.M665Anni.value);
	var ind_3 = parseInt (document.totali.Mover65Anni.value);
	var ind_4 = parseFloat (document.totali.AssSalt1.value);
	

	
	if (isNaN (ind_1)){
		ind_1 = 0;
		  document.totali.M05Anni.value = "";
	}
	if (isNaN (ind_2)){
		ind_2 = 0;
		  document.totali.M665Anni.value = "";
	}
	if (isNaN (ind_3)){
		ind_3 = 0;
		  document.totali.Mover65Anni.value = "";
	}
	if (isNaN (ind_4)){
		ind_4 = 0;
		  document.totali.AssSalt1.value = "";
	}
	
	var tot= ind_1+ind_2+ind_3;
	
    document.totali.TotAssCont1.value = tot;
    
	var tot2=tot+ind_4;
	
    document.totali.TotIng1.value = tot2;

   
}


	function sommaTotAssContR (){
	
	var ind_1 = parseFloat (document.totali.R05Anni.value);
	var ind_2 = parseFloat (document.totali.R665Anni.value);
	var ind_3 = parseFloat (document.totali.Rover65Anni.value);

	var ind_4 = parseFloat (document.totali.AssSalt2.value);
	   	
	
	if (isNaN (ind_1)){
		ind_1 = 0.00;
		  document.totali.R05Anni.value = "";
	}
	if (isNaN (ind_2)){
		ind_2 = 0.00;
		  document.totali.R665Anni.value = "";
	}
	if (isNaN (ind_3)){
		ind_3 = 0.00;
		  document.totali.Rover65Anni.value = "";
	}
	
	if (isNaN (ind_4)){
		ind_4 = 0;
		  document.totali.AssSalt2.value = "";
	}
	
	var tot= ((ind_1+ind_2+ind_3)*100/100);
	
    document.totali.TotAssCont2.value = tot;
    
	var tot2= tot+ind_4;
	
    document.totali.TotIng2.value = tot2;
   
}

	function sommaTotAssContP (){
	
	var ind_1 = parseFloat (document.totali.P05Anni.value);
	var ind_2 = parseFloat (document.totali.P665Anni.value);
	var ind_3 = parseFloat (document.totali.Pover65Anni.value);
	var ind_4 = parseFloat (document.totali.AssSalt3.value);
	
	
	if (isNaN (ind_1)){
		ind_1 = 0.00;
		  document.totali.P05Anni.value = "";
	}
	if (isNaN (ind_2)){
		ind_2 = 0.00;
		  document.totali.P665Anni.value = "";
	}
	if (isNaN (ind_3)){
		ind_3 = 0.00;
		  document.totali.Pover65Anni.value = "";
	}
	
	if (isNaN (ind_4)){
		ind_4 = 0;
		  document.totali.AssSalt3.value = "";
	}
	
	var tot= ((ind_1+ind_2+ind_3)*100/100);
	
    document.totali.TotAssCont3.value = tot;
    
    var tot2= tot+ind_4;
    
    document.totali.TotIng3.value = tot2;
   
}


function sommaTotS (){
	
	var ind_2 = parseFloat (document.totali.AssSalt4.value);
	
	
	if (isNaN (ind_2)){
		ind_2 = 0.00;
		  document.totali.AssSalt4.value = "";
	}
	
	var tot= ind_2;
	
    document.totali.TotIng4.value = tot;
   
}

	</script>	
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>
<div id="container">
<div id="header">
	 
		<%@ include file="../component/header.jspf" %>

	</div>
<div id="page">
<%		GregorianCalendar d=new GregorianCalendar();
				int anno2=d.get(Calendar.YEAR);
				int mese=d.get(Calendar.MONTH)+1;
				int giorno=d.get(Calendar.DAY_OF_MONTH);
				String data=giorno+"/"+mese+"/"+anno2; 
				int anno=0;
if(session.getAttribute("currentSessionUser")!=null){
	
	UserBean user=(UserBean) session.getAttribute("currentSessionUser");
	anno=user.getAnnoConvenzioni();
}
String idconvs=request.getParameter("idconv");
int idconv=Integer.parseInt(idconvs);
//int idconv=994;
CatalogoConvenzioni catc=new CatalogoConvenzioni();
Convenzione conv=catc.getConvenzione(idconv);

int idEnte=conv.getIdEnte();
Ente e=catente.getEnte(idEnte);
Persona p=catp.getPersonaRapplegale(idEnte);		
int CodiceStruttura=e.getCodiceStruttura();
String nome=e.getNome();				
String PartitaIva=e.getPartitaIva();				
				
				%>
				
<center><b>DOMANDA DI ISCRIZIONE/CONVENZIONE  STRUTTURA CARITATIVA – ANNO <% out.print(conv.getAnno());%></b><br><br></center>



 <form name="totali" method="post" action="../Update_convenzione">
 <input type="hidden" name="idEnte" value="<% out.print(idEnte);%>">
 <input type="hidden" name="idConv" value="<% out.print(idconv);%>">
<b>DATI ENTE:</b><br>

 
<p>
<%

out.println("Codice Struttura: "+CodiceStruttura+"<br>");
out.println("Denominazione: <b>"+nome+" </b><br>");
out.println("Codice Fiscale: "+PartitaIva);
out.println("<br>");



/* out.println("<br><b>Rappresentante legale</b><br>");
out.print(p.toStringline()); */

Vector<Sede> VS=new Vector<Sede>();


VS=cats.getSediEnte(idEnte);
Persona rapplegale=catp.getLastRappLegaleEnte(idEnte);

Sede sedeLegale=cats.getSede(conv.getIdSedeLegaleEnte());
Sede sedeOperativa=new Sede();
Sede sedeMagazzino=new Sede();
if(conv.getIdSedeOperativaEnte()!=0)
sedeOperativa=cats.getSede(conv.getIdSedeOperativaEnte());
if(conv.getIdSedeMagazzinoEnte()!=0)
sedeMagazzino=cats.getSede(conv.getIdSedeMagazzinoEnte());

out.println("<br><br>");



 out.print(rapplegale.toStringTable());
 out.println("<input type=\"hidden\" name=\"idRappLegaleEnte\" value="+rapplegale.getIdPersona()+">");
 out.println("<br><br>");


 
//select per sede Legale
out.println("<b>Sede Legale </b><br><select name=\"idSedeLegaleEnte\" style=\"width:950px\" >");
out.println("<option selected value=\""+sedeLegale.getIdSede()+"\">"+sedeLegale.toStringTable()+"</option>");
for(int j=0;j<VS.size();j++){
out.println("<option value="+VS.get(j).getIdSede()+"><b>"+VS.get(j).toStringTable()+"</option>");
}
out.println("</select><br><br>");
//select per sede Operativa
out.println("<b>Sede Operativa </b><br><select name=\"idSedeOperativaEnte\"  style=\"width:950px\">");
out.println("<option value=\"0\">La stessa della sede legale</option>");

if(conv.getIdSedeOperativaEnte()!=0)
	out.println("<option selected value=\""+sedeOperativa.getIdSede()+"\">"+sedeOperativa.toStringTable()+"</option>");
for(int j=0;j<VS.size();j++){
out.println("<option value="+VS.get(j).getIdSede()+"><b>"+VS.get(j).toStringTable()+"</option>");
}
out.println("</select><br><br>");

//select per sede  Magazzino
out.println("<b>Sede Magazzino</b> <br><select name=\"idSedeMagazzinoEnte\" style=\"width:950px\" >");
out.println("<option value=\"0\">La stessa della sede legale</option>");
if(conv.getIdSedeMagazzinoEnte()!=0)
	out.println("<option selected value=\""+sedeMagazzino.getIdSede()+"\">"+sedeMagazzino.toStringTable()+"</option>");
for (int i=0;i<VS.size();i++){
	out.println("<option value="+VS.get(i).getIdSede()+"><b>"+VS.get(i).toStringTable()+"</option>");
}
out.println("</select><br><br>");




%>



</p>
 
         <table cellpadding="15" >
         <tr>		
					
					<tr><td>Presenza di <b>struttura frigorifera</b> (*)
					
					
<input type="radio" name="frigo" value="si" <%if(conv.isCellaFrigo()) out.print("checked"); %>>Si&nbsp<input type="radio" name="frigo" value="no" <%if(!conv.isCellaFrigo()) out.print("checked"); %>>No

					</td></tr>
					
					<tr><td>
					
					Chiede di poter fruire, per l’anno <b><% out.print(conv.getAnno());%></b>, del servizio di distribuzione dei prodotti alimentari dall’Associazione Banco Alimentare della Calabria ONUS a favore degli indigenti e, consapevole che chiunque rilasci dichiarazione mendaci è punito ai sensi del codice penale e delle leggi speciali in materia, ai sensi e per gli effetti degli artt. 75 e 76 del D.P.R. n. 445/2000, allo scopo<br></br>
					<div align="center"><b>DICHIARA</b></div><br>
					che la struttura ha finalità caritative senza scopo di lucro, e, ove non applicabile la deroga di cui all’art. 1 del D.P.R. 3/6/98 n. 252, non sussistono cause di decadenza, divieto o sospensione di cui all’art. 10 della legge 575/65 e che svolge una attività di sostentamento alimentare tramite:
					
					</td></tr>					
				<tr>
				<td>
				
				
				<table border="1" style="border-spacing: 0px; padding: 0px">
				<tr>
				<td></td>
				
                <td align="center">
				
				<i>Assistiti continuativi<br>0-5 anni<br>(a)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti continuativi<br>6-65 anni<br>(b)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti continuativi<br>>65 anni<br>(c)</i>
				</td>
                
                <td align="center">
				
				<i>Totale assistiti continuativi<br>(d=a+b+c)</i>
				</td>
                
                <td align="center">
				
				<i>Assistiti saltuari<br>(e)</i>
				</td>
                
                
                
                 <td align="center">
				
				<i>Totale indigenti assistiti<br>(d+e)</i>
				</td>
				
				 <td align="center">
				
				<i>Media presenze nei giorni di apertura</i>
				</td>
				 <td align="center">
				
				<i>Giorni di apertura Struttura all’anno</i>
				</td>
				
				
				</tr>
				
				<tr>
				 <td align="center">
				 
				<b><i>MENSA</i></b>
				
                
				</td>
                <td align="center"><input type="number" id="M05Anni" name="M05Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px" value="<%out.print(conv.getM_0_5()); %>"></td>
                <td align="center"><input type="number" id="M665Anni" name="M665Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px" value="<%out.print(conv.getM_6_65()); %>"></td>
                <td align="center"><input type="number" id="Mover65Anni" name="Mover65Anni" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()" style="width:50px" value="<%out.print(conv.getM_over65()); %>"></td>
                
                <td align="center"><input type="number" id="TotAssCont1" name="TotAssCont1" style="width:50px" readonly="readonly"></td>
                
                <td align="center"><input type="number" id="AssSalt1" name="AssSalt1" required onKeyUp="sommaTotAssContM()" onclick="sommaTotAssContM()"  style="width:50px" value="<%out.print(conv.getM_saltuari()); %>"></td>
				
                <td align="center"><input type="number" name="TotIng1" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="MMediaPres" required style="width:50px" value="<%out.print(conv.getM_mediaPresenze()); %>"></td>
				<td align="center" ><input type="number" name="MGiorniApertura" required style="width:50px" value="<%out.print(conv.getM_giorniApertura()); %>"></td>
				</tr>
				
				<tr>
				<td align="center">
				
				
				
				<b><i>DISTRIBUZIONE PACCHI</i></b>
                
				</td>
                
                
                <td align="center"><input type="number" id="P05Anni" name="P05Anni" required onKeyUp="sommaTotAssContP()" onclick="sommaTotAssContP()" style="width:50px" value="<%out.print(conv.getP_0_5()); %>"></td>
                <td align="center"><input type="number" id="P665Anni" name="P665Anni" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px" value="<%out.print(conv.getP_6_65()); %>"></td>
                <td align="center"><input type="number" id="Pover65Anni" name="Pover65Anni" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px" value="<%out.print(conv.getP_over65()); %>"></td>
                <td align="center"><input type="number" id="TotAssCont3" name="TotAssCont3" style="width:50px" readonly="readonly"></td>
                
                <td align="center"><input type="number" id="AssSalt3" name="AssSalt3" required onKeyUp="sommaTotAssContP()"  onclick="sommaTotAssContP()" style="width:50px" value="<%out.print(conv.getP_saltuari()); %>"></td>
                
				<td align="center"><input type="number" name="TotIng3" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="PMediaPres" required style="width:50px" value="<%out.print(conv.getP_mediaPresenze()); %>"></td>
				<td align="center"><input type="number" name="PGiorniApertura" required style="width:50px" value="<%out.print(conv.getP_giorniApertura()); %>"></td>
				</tr>
				<tr>
				 <td align="center">
				 
				 
				<b><i>EMPORIO SOCIALE</i></b> 
				</td>

                
                <td align="center"><input type="number" id="R05Anni" name="R05Anni" required onKeyUp="sommaTotAssContR()" onclick="sommaTotAssContR()" style="width:50px" value="<%out.print(conv.getE_0_5()); %>"></td>
                <td align="center"><input type="number" id="R665Anni" name="R665Anni" required onKeyUp="sommaTotAssContR()" onclick="sommaTotAssContR()" style="width:50px" value="<%out.print(conv.getE_6_65()); %>" ></td>
                <td align="center"><input type="number" id="Rover65Anni" name="Rover65Anni" required onKeyUp="sommaTotAssContR()" onclick="sommaTotAssContR()"  style="width:50px" value="<%out.print(conv.getE_over65()); %>"></td>
                <td align="center"><input type="number" id="TotAssCont2" name="TotAssCont2" style="width:50px" readonly="readonly"></td>
                <td align="center"><input type="number" id="AssSalt2" name="AssSalt2" required onKeyUp="sommaTotAssContR()" onclick="sommaTotAssContR()" style="width:50px" value="<%out.print(conv.getE_saltuari()); %>"></td>
				
				<td align="center"><input type="number" name="TotIng2" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="RMediaPres" required style="width:50px" value="<%out.print(conv.getE_mediaPresenze()); %>"></td>
				<td align="center"><input type="number" name="RGiorniApertura" required style="width:50px"  value="<%out.print(conv.getE_giorniApertura()); %>"></td>
				
				</tr>
				
                
                <tr>
				<td align="center">
				<b><i>UNITA' DI STRADA</i></b>
				</td>
                
                
                <td align="center"><a style="width:50px">------</td>
                <td align="center"><a style="width:50px">------</td>
                <td align="center"><a style="width:50px">------</td>
                <td align="center"><a style="width:50px">------</td>
                
                <td align="center"><input type="number" id="AssSalt4" name="AssSalt4" required onKeyUp="sommaTotS()" onclick="sommaTotS()" style="width:50px" value="<%out.print(conv.getU_saltuari()); %>"></td>
                
				<td align="center"><input type="number" name="TotIng4" style="width:50px" readonly="readonly"></td>
				<td align="center"><input type="number" name="UMediaPres" required style="width:50px" value="<%out.print(conv.getU_mediaPresenze()); %>"></td>
				<td align="center"><input type="number" name="UGiorniApertura" required style="width:50px" value="<%out.print(conv.getU_giorniApertura()); %>"></td>
				</tr>
				</table>
				
				</td>
				
				</tr>	
                
                
				</table>
				
				</td>
				
				</tr>
                
				<tr><td>
				<div align="center">
				<b>E SI IMPEGNA</b>
				</div> <br>
				<ol>
				<li>a ricevere i prodotti <b>esclusivamente</b> dall' ASSOCIAZIONE BANCO ALIMENTARE DELLA CALABRIA ONLUS</li>
				<li>ad utilizzare i prodotti che riceverà esclusivamente per beneficenza ed assistenza in favore di persone effettivamente bisognose.</li>
				<li>a provvedere al ritiro dei prodotti presso il magazzino entro i termini fissati, <b>pena la perdita dell’assegnazione,</b> e alla loro distribuzione entro la data di scadenza indicata sulla confezione.</li>
				<li>a non depositare e/o distribuire il prodotto assegnato in magazzini diversi da quello su indicato.</li>
				<li>a rispettare rispettare quanto previsto dall’ Associazione Banco Alimentare della Calabria ONLUS  con l’accordo di  Collaborazione stipulato e in particolare, a tenere una contabilità di magazzino.</li>
				<li>a rispettare quanto stabilito nell’accordo ed in  particolare dichiara che gli eventuali <b>finanziamenti pubblici o privati</b> che riceve in aiuto alla propria attività <b>non costituiscono sostegno finanziario adeguato</b> per il numero di assistiti sopra indicato. Si impegna, inoltre, <b>a non percepire rette, anche di modico valore, dai propri assistiti.</b></li>
				</ol><br>
				(*) <b>per struttura frigorifera</b> si intende una struttura che abbia, sia sotto il profilo qualitativo che della capacità, le caratteristiche necessarie per assicurare la buona conservazione dei prodotti deperibili richiesti.
				</td></tr>	
				<br>
				</table>
				
				
				
				Data <input type="text" name="Data" style="width:90px" value="<%out.print(conv.getDataFirma());%>">
				<br>
 				<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
         </form>

<%
	if ((request.getAttribute("js") == "0")
			|| (request.getAttribute("js") == null)) {
	} else
		out.println((String) request.getAttribute("js"));
%>
</div>
<div id="footer">
  		<%@ include file="../component/footer.jspf" %>
	</div>
</div>

</body>
</html>