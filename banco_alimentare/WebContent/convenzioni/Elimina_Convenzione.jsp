<jsp:useBean id="catc" scope="page" class="beans.DAO_Convenzione2017"/>
<%@page import="java.util.*"%>
<%@ page import="beans.*"%> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
<%
String id=request.getParameter("id");
int idConvenzione=Integer.parseInt(id);
ConvenzioneFrom2017 c=catc.getConvenzione(idConvenzione);
boolean  esito= false;
if(catc.isCancellable(c.getIdConvenzione()))
	esito=esito=catc.eliminaConvenzione(c);
String jspPage;
if (esito){
	System.out.println("Operazione eseguita correttamente!");
	String commit="Operazione eseguita correttamente!";    		 
	request.setAttribute("commit", commit);
	jspPage="../result/commit.jsp";
}
else{
   System.out.println("!!! Operazione non eseguita !!!");
   String errore="Operazione non eseguita correttamente! <br> Eliminare prima i verbali associati alla convenzione";    		 
   request.setAttribute("errore", errore);
   jspPage="../result/error.jsp";
}
request.getRequestDispatcher( jspPage ).forward(request,response);




%>
</body>
</html>