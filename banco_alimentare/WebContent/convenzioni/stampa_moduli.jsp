<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="util.*"%>
 <%@page import="beans.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="../component/title.jspf" %>
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>


<div id="container">
	<div id="header">
	 
		<%@ include file="../component/header.jspf" %>

	</div>
	<div id="page">
	
	
<%
			//String CodiceStruttura=request.getParameter("idEnte");
				//int idEnte=Integer.parseInt(CodiceStruttura);

				String idConvenziones=request.getParameter("idConvenzione");
				String print=request.getParameter("print");
				int idConvenzione=Integer.parseInt(idConvenziones);
				DAO_Convenzione2017 catc=new DAO_Convenzione2017();
				ConvenzioneFrom2017 c=catc.getConvenzione(idConvenzione);
				int idEnte=c.getIdEnte();
				int anno=c.getAnno();
				String tipologiaConvenzione= c.getTipologia();

				FirstPdf f=new FirstPdf();

				String path=request.getSession().getServletContext().getRealPath("/");
				String path0=null;
				String path1=null;
				System.out.println(path);
				String directory_separator="\\";
				
				if(print.equals("verbale_registro")){
					 path0=path+"fileSupporto"+directory_separator+"All_4_contabilita.pdf";
					 path1=path+"Moduli_convenzioni"+directory_separator+""+idEnte+"_All_4_contabilita_"+c.getAnno()+".pdf";
					 System.out.println(path0);
						System.out.println(path1);
					f.StampaModuli_verbale_registro_2017(path0, path1, c);
					response.sendRedirect("../Moduli_convenzioni/" + idEnte+ "_All_4_contabilita_"+c.getAnno()+".pdf");
				}
				if(print.equals("verbale_fascicoli")){
					 path0=path+"fileSupporto"+directory_separator+"All_5_fascicoli.pdf";
					 path1=path+"Moduli_convenzioni"+directory_separator+""+idEnte+"_All_5_fascicoli_"+c.getAnno()+".pdf";
					 System.out.println(path0);
						System.out.println(path1);
					f.StampaModuli_verbale_fascicoli_2017(path0, path1, c);
					response.sendRedirect("../Moduli_convenzioni/" + idEnte+ "_All_5_fascicoli_"+c.getAnno()+".pdf");
				}
				
				if (print.equals("accordo") && tipologiaConvenzione.equals("agea")) {
				 path0 = path + "fileSupporto" + directory_separator+ "All_2_Domanda_affiliazione_OpT.pdf";
				 path1 = path + "Moduli_convenzioni"+ directory_separator + "" + idEnte+ "_modulo_convenzione_" + c.getAnno() + ".pdf";

				System.out.println(path0);
				System.out.println(path1);

				f.StampaModuli_Convenzione2017(path0, path1, c);
				response.sendRedirect("../Moduli_convenzioni/" + idEnte+ "_modulo_convenzione_"+c.getAnno()+".pdf");
			}

			if (print.equals("accordo") && tipologiaConvenzione.equals("banco")) {
				 path0 = path + "fileSupporto" + directory_separator+ "modulo_empty_banco.pdf";
				 path1 = path + "Moduli_convenzioni"+ directory_separator + "" + idEnte+ "_modulo_convenzione_" + c.getAnno() + ".pdf";

				System.out.println(path0);
				System.out.println(path1);

				f.StampaModuli_Convenzione_Banco(path0, path1, idConvenzione);
				response.sendRedirect("../Moduli_convenzioni/" + idEnte+ "_modulo_convenzione_"+c.getAnno()+".pdf");
			}
			//response.sendRedirect("../Moduli_convenzioni/" + idEnte+ "_modulo_convenzione_" + anno + ".pdf");
			//response.sendRedirect("../"+path1);
		%>
<center><a href="../Moduli_convenzioni/<%out.print(idEnte);%>_modulo_convenzione_<%out.print(anno);%>.pdf"><img src="../image/icona_pdf.jpg" ></a>
<br><br> Clicca sull'icona per aprire il modulo!

<%-- <br><br>Stampa subito <a href="print_now.jsp?pathstampa=<%out.print(path1);%>">Stampa senza aprire (sperimentale)</a></center>
 --%>
 
	</div><!-- chiusura del div page-->
	
		<div id="footer">
 			<%@ include file="../component/footer.jspf" %>
		</div>
</div><!-- chiusura del div container-->
</body>
</html>