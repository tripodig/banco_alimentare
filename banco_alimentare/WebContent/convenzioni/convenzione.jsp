<%@page import="beans.*"%>
<%@page import="java.util.*"%>
<jsp:useBean id="catente" scope="page" class="beans.CatalogoEnti"/>
<jsp:useBean id="cats" scope="page" class="beans.CatalogoSede"/>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ include file="../component/title.jspf" %>

<script type="text/javascript" language="javascript"><!--
		function Show(ctrl){
			if(ctrl.checked){
				document.getElementById('nuovo2').innerHTML = '<input type="text" name="specifica" style="width:200px"/>';
      		}
     		else{
  				document.getElementById('nuovo2').innerHTML = '';
      		} 
		}

		function Nascondi(){
			document.getElementById('nuovo2').innerHTML = ''; 
	   	}
	</script>	
</head>
<body>
<div id="top">
<%@ include file="../component/top.jspf" %>
</div>
<div id="container">
	<div id="header">
    <%@ include file="../component/header.jspf" %>       
    </div>
     <div id="page">
<div id="contenuto" float="center"> 

<%Vector<Ente>VE =catente.getEnti();

%>


			
 <center><form method="post" action="New_convenzione">
 
 
         <table cellpadding="15" >
         <tr>		Struttura <select name="codiceStruttura" style="width:900px">
						<% for(int i=0;i<VE.size();i++){
							int idEnte=VE.get(i).getIdEnte();
							Vector<Sede>VS=cats.getSediEnte(idEnte);
						out.println("<option value=\""+VE.get(i).getIdEnte()+"\">"+VE.get(i).getIdEnte()+ VE.get(i).getNome() + VE.get(i).getPartitaIva());
						for(int j=0;j<VS.size();j++){
							out.print(VS.get(j).toStringTable());}
						out.print("</option>");
						}%>
						
</select>
</tr>
					
					<tr><td>Presenza di <b>struttura frigorifera</b> (*)
					
					
<input type="radio" name="frigo" value="si">Si&nbsp<input type="radio" name="frigo" value="no">No

					</td></tr>
					
					<tr><td>
					
					Chiede di poter fruire, per l’anno <b>2013</b>, del servizio di distribuzione dei prodotti alimentari dell’Organismo Pagatore AGEA a favore degli indigenti e, consapevole che chiunque rilasci dichiarazione mendaci è punito ai sensi del codice penale e delle leggi speciali in materia, ai sensi e per gli effetti degli artt. 75 e 76 del D.P.R. n. 445/2000, allo scopo<br></br>
					<div align="center"><b>DICHIARA</b></div><br>
					che la struttura ha finalità caritative senza scopo di lucro, e, ove non applicabile la deroga di cui all’art. 1 del D.P.R. 3/6/98 n. 252, non sussistono cause di decadenza, divieto o sospensione di cui all’art. 10 della legge 575/65 e che svolge una attività di sostentamento alimentare tramite:
					
					</td></tr>					
				<tr>
				<td>
				
				
				<table border="1" bordercolor="black">
				<tr>
				<td></td>
				<td>
				
				<i>Totale indigenti assistiti</i>
				</td>
				
				<td>
				
				<i>Media presenze nei giorni di apertura</i>
				</td>
				<td>
				
				<i>Giorni di apertura Struttura all’anno</i>
				</td>
				
				
				</tr>
				
				<tr>
				<td>
				<b><i>MENSA</i></b> a indigenti “che non hanno alcuna possibilità di sostentamento e/o per i quali comunque tale possibilità è al di sotto del limite di sopravvivenza” assistiti tramite le seguenti strutture:<br>
				
				<input type="checkbox" name="pranzo" value="Pranzo">&nbsp<b>pranzo</b>&nbsp&nbsp&nbsp<input type="checkbox" name="cena" value="Cena">&nbsp<b>cena</b>&nbsp&nbsp&nbsp<input type="checkbox" name="colazione" value="Colazione">&nbsp<b>colazione</b>&nbsp&nbsp&nbsp<input type="checkbox" name="pastifreddi" value="PastiFreddi">&nbsp<b>pasti freddi</b>&nbsp&nbsp&nbsp<input type="checkbox" name="centroaccoglienza" value="CentroAccoglienza">&nbsp<b>Centro accoglienza diurno</b><br>
				Categorie assistiti: 0-5 anni n°&nbsp<input type="text" name="05Anni" style="width:30px">&nbsp&nbsp6-65 anni n°&nbsp<input type="text" name="665Anni" style="width:30px">&nbsp&nbspover 65 n°&nbsp<input type="text" name="over65Anni" style="width:30px">
				
				</td>
				<td align="center"><input type="text" name="TotIng1" style="width:30px"></td>
				<td align="center"><input type="text" name="MedPres1" style="width:30px"></td>
				<td align="center" ><input type="text" name="GioniApertura1" style="width:30px"></td>
				</tr>
				<tr>
				<td>
				<b><i>RESIDENZA</i></b> a indigenti assistiti per tutta la giornata e per l’intero anno<br>
				
				<input type="checkbox" name="casafamiglia" value="CasaFamiglia">&nbsp<b>Casa Famiglia</b>&nbsp&nbsp&nbsp<input type="checkbox" name="comunitadisabili" value="ComunitaDisabili">&nbsp<b>Comunità disabili</b>&nbsp&nbsp&nbsp<input type="checkbox" name="comunitatossicodip" value="ComunitaTossicodip">&nbsp<b>Comunità tossicodip.</b>&nbsp&nbsp&nbsp<input type="checkbox" name="comunitaanziani" value="ComunitaAnziani">&nbsp<b>Comunità anziani</b>&nbsp&nbsp&nbsp<input type="checkbox" name="comunitaminori" value="ComunitaMinori">&nbsp<b>Comunità minori</b><br>
				Categorie assistiti:  0-5 anni n°&nbsp<input type="text" name="R05Anni" style="width:30px">&nbsp&nbsp6-65 anni n°&nbsp<input type="text" name="R665Anni" style="width:30px">&nbsp&nbspover 65 n°&nbsp<input type="text" name="Rover65Anni" style="width:30px">
				
				</td>
				<td align="center"><input type="text" name="TotIng2" style="width:30px"></td>
				<td align="center"><input type="text" name="MedPres2" style="width:30px"></td>
				<td align="center">365</td>
				</tr>
				<tr>
				<td width="600">
				<b><i>PACCHI</i></b> a indigenti “che non hanno alcuna possibilità di sostentamento e/o per i quali comunque tale possibilità è al di sotto del limite di sopravvivenza” attraverso l’assistenza a <b>famiglie, anziani, ecc.</b><br>
				
				<input type="checkbox" name="distribuzionepacchi" value="DistribuzionePacchi">&nbsp<b>Distribuzione pacchi</b>&nbsp&nbsp&nbsp<input type="checkbox" name="consegnadomicilio" value="ConsegnaDomicilio">&nbsp<b>Consegna pacchi a domicilio</b>&nbsp&nbsp&nbsp<input type="checkbox" name="altro" value="Altro" onclick="Show(this);">&nbsp<b>Altro</b>&nbsp<div id="nuovo2" align="center"></div><br>
				Categorie assistiti:  0-5 anni n°&nbsp<input type="text" name="P05Anni" style="width:30px">&nbsp&nbsp6-65 anni n°&nbsp<input type="text" name="P665Anni" style="width:30px">&nbsp&nbspover 65 n°&nbsp<input type="text" name="Pover65Anni" style="width:30px">
				
				</td>
				<td align="center"><input type="text" name="TotIng3" style="width:30px"></td>
				<td align="center"><input type="text" name="MedPres3" style="width:30px"></td>
				<td align="center"><input type="text" name="GioniApertura3" style="width:30px"></td>
				</tr>
				</table>
				
				</td>
				
				</tr>	
				<tr><td>
				<div align="center">
				<b>E SI IMPEGNA</b>
				</div> <br>
				<ol>
				<li>a ricevere i prodotti AGEA <b>esclusivamente</b> dall' ASSOCIAZIONE BANCO ALIMENTARE DELLA CALABRIA ONLUS</li>
				<li>ad utilizzare i prodotti che riceverà esclusivamente per beneficenza ed assistenza in favore di persone effettivamente bisognose.</li>
				<li>a provvedere al ritiro dei prodotti presso il magazzino entro i termini fissati, <b>pena la perdita dell’assegnazione,</b> e alla loro distribuzione entro la data di scadenza indicata sulla confezione.</li>
				<li>a non depositare e/o distribuire il prodotto assegnato in magazzini diversi da quello su indicato.</li>
				<li>a rispettare quanto previsto dalla Circolare dell’Organismo Pagatore <b>AGEA n. DPMU.2012.2818 del 14.09.2012,</b> in particolare, a tenere una contabilità di magazzino conforme alle disposizioni di cui al capitolo 7 della circolare stessa.</li>
				<li>a rispettare il capitolo 6 della circolare e pertanto dichiara che gli eventuali <b>finanziamenti pubblici o privati</b> che riceve in aiuto alla propria attività <b>non costituiscono sostegno finanziario adeguato</b> per il numero di assistiti sopra indicato. Si impegna, inoltre, <b>a non percepire rette, anche di modico valore, dai propri assistiti.</b></li>
				</ol><br>
				(*) <b>per struttura frigorifera</b> si intende una struttura che abbia, sia sotto il profilo qualitativo che della capacità, le caratteristiche necessarie per assicurare la buona conservazione dei prodotti deperibili richiesti.
				</td></tr>	
				<br>
				</table>
				
				
				
				
				
				
				
				Data <input type="text" name="Data" style="width:90px">
 				<center><input type="reset" value="cancella">&nbsp&nbsp<input type="submit" value="inserisci"> </center>
         </form>
</div>

  </div>
<%
	if ((request.getAttribute("js") == "0")
			|| (request.getAttribute("js") == null)) {
	} else
		out.println((String) request.getAttribute("js"));
%>
<div id="footer">
  		<%@ include file="../component/footer.jspf" %>
	</div>

</div>
</body>
</html>